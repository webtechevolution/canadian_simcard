<?php

class Stores extends Model {

	function get_all_stores($type = 0){

		$query = "SELECT * 
			FROM stores ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_store_stock($id){
		$query = "SELECT * 
			FROM stock WHERE store_id = $id AND quantity_sold > 0";

		return $this->db->query($query);
	}

	function get_store_by_id($id){

		$query = "SELECT * 
					FROM stores
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_store($where){

		return $this->db->delete('stores', $where);
	}

	function insert_store($value){

		return $this->db->insert('stores', $value);
	}

	function update_store($value, $where){

		return $this->db->update('stores', $value, $where);
	}
}

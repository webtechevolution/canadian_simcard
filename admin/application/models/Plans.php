<?php

class Plans extends Model {

	function get_all_plans($type = 0){

		$query = "	SELECT * 
					FROM  plans ORDER BY sort_order DESC";

		return $this->db->query($query);
	}

	function get_all_carriers($type = 0){

		$query = "	SELECT * 
					FROM  carriers ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_plans_by_id($id){

		$query = "	SELECT * 
					FROM  plans
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_plans($where){

		return $this->db->delete('plans', $where);
	}

	function insert_plans($value){

		return $this->db->insert('plans', $value);
	}

	function update_plans($value, $where){

		return $this->db->update('plans', $value, $where);
	}
}

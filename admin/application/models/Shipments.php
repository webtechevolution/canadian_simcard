<?php

class Shipments extends Model {

	function get_all_shipments($type = 0){

		$query = "SELECT * 
			FROM shipments ORDER BY shipment_id DESC";

		return $this->db->query($query);
	}

	function get_all_carriers($type = 0){

		$query = "	SELECT * FROM  carriers ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_all_stores($type = 0){

		$query = "SELECT * FROM stores ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_shipment_by_id($id){

		$query = "SELECT * FROM shipments WHERE shipment_id = $id";

		return $this->db->query($query);
	}

	function get_stock_by_id($id){

		$query = "SELECT * FROM stock WHERE shipment_id = $id";

		return $this->db->query($query);
	}

	function get_shipment_detail_by_id($id){

		$query = "SELECT * FROM shipment_detail WHERE shipment_id = $id";

		return $this->db->query($query);
	}

	function get_shipment_detail(){

		$query = "	SELECT * 
					FROM  shipment_detail WHERE sold = 0";

		return $this->db->query($query);
	}

	function get_all_shipment_detail(){

		$query = "	SELECT * 
					FROM  shipment_detail";

		return $this->db->query($query);
	}

	function delete_store($where){

		return $this->db->delete('stores', $where);
	}

	function insert_shipment($value){

		$this->db->insert('shipments', $value);
		return $this->db->lastInsertId();
	}

	function insert_stock($value){

		return $this->db->insert('stock', $value);
	}

	function update_shipment($value, $where){
		
		return $this->db->update('shipments', $value, $where);
	}

	function update_stock($value, $where){
		
		return $this->db->update('stock', $value, $where);
	}
	
	function add_sim_detail($value){
		
		return $this->db->insert('shipment_detail', $value);
	}

	function update_sim_detail($value, $where){
		
		return $this->db->update('shipment_detail', $value, $where);
	}

	function get_sim_details($sim_number){

		$query = "SELECT * FROM shipment_detail WHERE sim_number = '$sim_number'";
		return $this->db->query($query);
	}

	function get_store_by_id($storeId){

		$query = "SELECT * FROM stores WHERE id = $storeId";
		return $this->db->query($query);
	}

	function get_all_sims_added(){

		$query = "SELECT sim_number FROM shipment_detail";
		return $this->db->query($query);
	}
}

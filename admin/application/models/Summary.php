<?php

class Summary extends Model {

	function get_sales($dateFrom, $dateTo){

		$query = "SELECT * 
			FROM orders WHERE created >= '$dateFrom' AND created <= '$dateTo'";

		return $this->db->query($query);
	}

	function get_all_stores($type = 0){

		$query = "SELECT * 
			FROM stores ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_all_stock($type = 0){

		$query = "SELECT * 
			FROM stock ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_all_carriers($type = 0){

		$query = "	SELECT * FROM  carriers ORDER BY id DESC";

		return $this->db->query($query);
	}

}

<?php

class Statement extends Model {

	function get_all_stores($type = 0){

		$query = "SELECT * 
			FROM stores ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_all_stock($type = 0){

		$query = "SELECT * 
			FROM stock ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_all_carriers($type = 0){

		$query = "	SELECT * FROM  carriers ORDER BY id DESC";

		return $this->db->query($query);
	}

	function get_orders($dateFrom, $dateTo){

		$query = "SELECT * 
			FROM orders WHERE updated >= '$dateFrom' AND updated <= '$dateTo'";

		return $this->db->query($query);
	}

	function get_order_item($order_id){

		$query = "SELECT tbl1.*,tbl2.full_name FROM ordered_items tbl1 join orders tbl2 on tbl1.order_id=tbl2.id WHERE order_id = $order_id";

		return $this->db->query($query);
	}

	function get_shipment_by_id($id){

		$query = "SELECT * FROM shipments WHERE shipment_id = $id";

		return $this->db->query($query);
	}

	function get_shipment_detail($id){

		$query = "SELECT * FROM shipment_detail WHERE shipment_id = $id";

		return $this->db->query($query);
	}
	function get_shipment_detail_bysim($id){

		$query = "SELECT * FROM shipment_detail WHERE sim_number = '$id'";

		$result= $this->db->query($query);
		//echo $this->db->last_query();
		//echo $result->num_rows();
		//print_r($result);
		return count($result);
		//echo "0";
	}

}

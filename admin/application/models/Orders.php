<?php

class Orders extends Model {


	function get_all_order(){

		$query = "	SELECT *
					FROM  orders ORDER BY order_number DESC";

		return $this->db->query($query);
	}

	function get_single_order($id){

		$query = "	SELECT *
					FROM  orders
					WHERE id = $id";

		return $this->db->query($query);
	}

	function get_single_order_by_number($order_number){

		$query = "	SELECT *
					FROM  orders
					WHERE order_number = $order_number";

		return $this->db->query($query);
	}
	
	function get_order_planid($title){
		//echo "<br/>ID=".$id;
		$query = "SELECT id from plans where title='$title'";

		$selectedData = $this->db->query($query);
//echo $selectedData[0]['username'];
		return $selectedData[0]['id'];
	}
	function get_setting(){

		$query = "	SELECT *
					FROM  settings";

		return $this->db->query($query);
	}
	function get_shipment_detail_bysim($id){

		$query = "SELECT * FROM shipment_detail WHERE sim_number = '$id'";

		$result= $this->db->query($query);
		//echo $this->db->last_query();
		//echo $result->num_rows();
		//print_r($result);
		return count($result);
		//echo "0";
	}

	function get_order_item($order_id){

		$query = "	SELECT *
					FROM  ordered_items
					WHERE order_id = $order_id";

		return $this->db->query($query);
	}

	function get_order_item_by_id($order_id, $item_id){

		$query = "	SELECT *
					FROM  ordered_items
					WHERE order_id = $order_id AND id = $item_id";

		return $this->db->query($query);
	}
	
	function update_order($value, $where){

		return $this->db->update('orders', $value, $where);
	}

	function update_order_item($value, $where){

		return $this->db->update('ordered_items', $value, $where);
	}

	function update_setting($value, $where){

		return $this->db->update('settings', $value, $where);
	}

	function shipping_price(){

		$query = "	SELECT *
					FROM  shipping";

		return $this->db->query($query);
	}
	function get_client_email_By_id($id){
		//echo "<br/>ID=".$id;
		$query = "SELECT username from client where user_id=$id";

		$selectedData = $this->db->query($query);
//echo $selectedData[0]['username'];
		return $selectedData[0]['username'];
	}
	function update_stock($store_id, $carrier_id, $shipment_id){

		$query = "	SELECT quantity_sold 
					FROM  stock
					WHERE store_id = '$store_id' AND carrier_id = '$carrier_id' AND shipment_id = '$shipment_id'";

		$selectedData = $this->db->query($query);

		$value = array(
			'quantity_sold' => (int)$selectedData[0]['quantity_sold'] + 1,
		);

		$where = array(
			'store_id' => $store_id,
			'carrier_id' => $carrier_id,
			'shipment_id' => $shipment_id,
		);

		return $this->db->update('stock', $value, $where);
	}

	function update_shipment_detail($value, $where){
		return $this->db->update('shipment_detail', $value, $where);
	}

	function get_tax(){

		$query = " SELECT * 
					FROM  tax";

		return $this->db->query($query);
	}

	function insert_order($value){

		return $this->db->insert('orders', $value);
	}

	function get_last_id(){

		return $this->db->lastInsertId();
	}

}

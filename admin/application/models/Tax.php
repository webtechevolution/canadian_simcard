<?php

class Tax extends Model {

	function get_tax(){

		$query = "	SELECT * 
					FROM  tax";

		return $this->db->query($query);
	}

	function get_tax_by_id($id){

		$query = "	SELECT * 
					FROM  tax
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_tax($where){

		return $this->db->delete('tax', $where);
	}

	function insert_tax($value){

		return $this->db->insert('tax', $value);
	}

	function update_tax($value, $where){

		return $this->db->update('tax', $value, $where);
	}
}
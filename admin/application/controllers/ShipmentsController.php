<?php

class ShipmentsController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$shipments = $this->_model->get_all_shipments();
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();

		$this->_view->set('shipments', $shipments);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}

	function status($arg=false){}

	function addshipment($arg=false){
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();

		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}

	function getStoreId($storeName) {
		$store_model = new Stores();
		$stores = $store_model->get_all_stores();
		$storeName = trim(strtolower($storeName));
		$storeId = 0;
		foreach($stores as $value) {
			$arrStoreName = trim(strtolower($value['name']));
			$test = strcmp($arrStoreName, $storeName);
			if ($test == 0) {
				$storeId = $value['id'];
			}
		}
		return $storeId;
	}

	function isSmaller($str1, $str2) {
		// Calculate lengths of both string
		$n1 = strlen($str1);
		$n2 = strlen($str2);
	
		if ($n1 < $n2)
				return true;
		if ($n2 < $n1)
				return false;
	
		for ($i = 0; $i < $n1; $i++)
		{
				if ($str1[$i] < $str2[$i])
						return true;
				else if ($str1[$i] > $str2[$i])
						return false;
		}
		return false;
	}

	// Function for finding difference
	// of larger numbers
	function findDiff($str1, $str2) {
		// Before proceeding further, make
		// sure str1 is not smaller
		if ($this->isSmaller($str1, $str2))
		{
				$t = $str1;
				$str1 = $str2;
				$str2 = $t;
		}

		// Take an empty string for storing result
		$str = "";

		// Calculate lengths of both string
		$n1 = strlen($str1);
		$n2 = strlen($str2);
		$diff = $n1 - $n2;

		// Initially take carry zero
		$carry = 0;

		// Traverse from end of both strings
		for ($i = $n2 - 1; $i >= 0; $i--)
		{
				// Do school mathematics, compute
				// difference of current digits and carry
				$sub = ((ord($str1[$i + $diff]) - ord('0')) -
								(ord($str2[$i]) - ord('0')) - $carry);
				if ($sub < 0)
				{
						$sub = $sub + 10;
						$carry = 1;
				}
				else
						$carry = 0;

				$str.=chr($sub + ord("0"));
		}

		// subtract remaining digits of str1[]
		for ($i = $n1 - $n2 - 1; $i >= 0; $i--)
		{
				if ($str1[$i] == '0' && $carry > 0)
				{
						$str.="9";
						continue;
				}
				$sub = (ord($str1[$i]) - ord('0') - $carry);
				if ($i > 0 || $sub > 0) // remove preceding 0's
						$str.=chr($sub + ord("0"));
				$carry = 0;

		}

		// reverse resultant string
		return strrev($str);
	}

	function addshipment_action($arg=false){
		$this->render =0;
		if ($_REQUEST['submit'] === 'Save') {
			// $sim_card_start_serial = isset($_POST['sim_card_start_serial'])?$_POST['sim_card_start_serial']: '0';
			// $sim_card_end_serial = isset($_POST['sim_card_end_serial'])?$_POST['sim_card_end_serial']: '0';
			// $start_serial = substr($sim_card_start_serial, 0, strlen($sim_card_start_serial)-1);
			// $end_serial = substr($sim_card_end_serial, 0, strlen($sim_card_end_serial)-1);
			$store_id = $this->getStoreId($_POST['store_name']);
			$value = array(
				'store_id' => $store_id,
				'carrier_id' => isset($_POST['carrier'])?$_POST['carrier']:'',
				// 'sim_card_start_serial' => $start_serial,
				// 'sim_card_end_serial' => $end_serial,
				// 'quantity' => ($this->findDiff($start_serial, $end_serial) + 1),
				'quantity' => 0,
				'date_shipped' => isset($_POST['date_shipped']) ? $_POST['date_shipped'] : '',
				'date_received' => isset($_POST['date_received']) ? $_POST['date_received'] : ''
			);
			$shipment_id = $this->_model->insert_shipment($value);
			
			$stock_value = array(
				'shipment_id' => $shipment_id,
				'store_id' => $store_id,
				'carrier_id' => isset($_POST['carrier'])?$_POST['carrier']:'',
				// 'quantity_purchased' => ($this->findDiff($start_serial, $end_serial) + 1),
				'quantity_purchased' => 0,
				'quantity_sold' => 0
			);
			$this->_model->insert_stock($stock_value);
			header("Location: " . BASEURL . "shipments?s=" . $shipment_id);
		} else {
			header("Location: " . BASEURL . "shipments");
		}
	}

	function editshipment($arg=false){
		$shipment = $this->_model->get_shipment_by_id($arg[0]);
		$shipment_detail = $this->_model->get_shipment_detail_by_id($arg[0]);
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();
		$addedSims = $this->_model->get_all_sims_added();

		$this->_view->set('_', $shipment[0]);
		$this->_view->set('shipment_detail', $shipment_detail);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
		$this->_view->set('addedSims', $addedSims);
	}

	function addSimManually($arg=false) {
		$this->render = 0;
		
		$shipment_id = (int) isset($_POST['shipment_id']) ? $_POST['shipment_id'] : 0;
		$sim_card_number = isset($_POST['sim_card_number']) ? $_POST['sim_card_number'] : "";
		$detailValue = array(
			'shipment_id' => $shipment_id,
			'sim_number' => $sim_card_number,
			'sold' => 0,
			'number_activated_on' => ""
		);
		$shipment = $this->_model->get_shipment_by_id($shipment_id);
		$quantity = (int) $shipment[0]['quantity'];
		$shipmentValue = array(
			'quantity' => ($quantity + 1)
		);
		$shipmentWhere = array(
			'shipment_id' => $shipment_id,
		);
		$stock = $this->_model->get_stock_by_id($shipment_id);
		$stockquantity = (int) $stock[0]['quantity_purchased'];
		$stockValue = array(
			'quantity_purchased' => ($stockquantity + 1)
		);
		$stockWhere = array(
			'shipment_id' => $shipment_id,
		);
		$this->_model->add_sim_detail($detailValue);
		$this->_model->update_shipment($shipmentValue, $shipmentWhere);
		$this->_model->update_stock($stockValue, $stockWhere);
		header("Location: " . BASEURL . "shipments/editshipment/". $shipment_id);
	}

	function editSimManually($arg=false) {
		$this->render = 0;
		
		$detail_shipment_id = (int) isset($_POST['detail_shipment_id']) ? $_POST['detail_shipment_id'] : 0;
		$detail_id = (int) isset($_POST['detail_id']) ? $_POST['detail_id'] : 0;
		$sim_card_number = isset($_POST['edit_sim_card_number']) ? $_POST['edit_sim_card_number'] : "";
		$detailValue = array(
			'sim_number' => $sim_card_number
		);
		$detailWhere = array(
			'id' => $detail_id,
		);
		$this->_model->update_sim_detail($detailValue, $detailWhere);
		header("Location: " . BASEURL . "shipments/editshipment/". $detail_shipment_id);
	}

	function addFromFile($args=false) {
		$this->render = 0;
		if (isset($_FILES['file'])) {
			if ($xlsx = SimpleXLSX::parse( $_FILES['file']['tmp_name'] ) ) {
				$shipment_id = (int) isset($_POST['file_shipment_id']) ? $_POST['file_shipment_id'] : 0;
				$dim = $xlsx->dimension();
				$cols = $dim[0];
				// print_r($cols);
				// die;
				$counter = 0;
				foreach ( $xlsx->rows() as $k => $r ) {
					if ($k == 0) continue; // skip first row
					for ( $i = 0; $i < 1; $i ++ ) {
						if (isset($r[$i])) {
							$counter++;
							$detailValue = array(
								'shipment_id' => $shipment_id,
								'sim_number' => strval($r[$i]),
								'sold' => 0,
								'number_activated_on' => ""
							);
							$this->_model->add_sim_detail($detailValue);
							$detailValue = array();
							// echo '<div>' . ($r[ $i ]) . '</div>';
						}
					}
				}
				if ($counter > 0) {
					$shipment = $this->_model->get_shipment_by_id($shipment_id);
					$quantity = (int) $shipment[0]['quantity'];
					$shipmentValue = array(
						'quantity' => ($quantity + $counter)
					);
					$shipmentWhere = array(
						'shipment_id' => $shipment_id,
					);
					$stock = $this->_model->get_stock_by_id($shipment_id);
					$stockquantity = (int) $stock[0]['quantity_purchased'];
					$stockValue = array(
						'quantity_purchased' => ($stockquantity + $counter)
					);
					$stockWhere = array(
						'shipment_id' => $shipment_id,
					);
					$this->_model->update_shipment($shipmentValue, $shipmentWhere);
					$this->_model->update_stock($stockValue, $stockWhere);
					header("Location: " . BASEURL . "shipments/editshipment/". $shipment_id);
				}
			}
		}
	}

	function checkSimNumber($arg=false){
		$orderItem = array();
		$simNumber = strval($_POST['data']['simNumber']);
		$orders = $this->_model->get_sim_details($simNumber);
		if (count($orders) > 0) {
			$shipment = $this->_model->get_shipment_by_id($orders[0]['shipment_id']);
			$store = $this->_model->get_store_by_id($shipment[0]['store_id']);
			$orderItem[0]['sim_number'] = $orders[0]['sim_number'];
			$orderItem[0]['sold'] = $orders[0]['sold'];
			$orderItem[0]['store'] = $store[0]['name'];
			$orderItem[0]['number_activated_on'] = $orders[0]['number_activated_on'];
		} else {
			$orderItem[0]['sim_number'] = $simNumber;
			$orderItem[0]['sold'] = '2';
			$orderItem[0]['store'] = '';
			$orderItem[0]['number_activated_on'] = '';
		}
		echo json_encode($orderItem);
	}
}

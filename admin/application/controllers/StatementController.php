<?php

class StatementController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$this->render = 0;
	}

	function sales($arg=false){
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();

		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}

	function stock($arg=false){
		$stock = $this->_model->get_all_stock();
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();

		$this->_view->set('stock', $stock);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}

	function siminventory($arg=false){
		$shipment = $this->_model->get_shipment_by_id((int)$arg[0]);
		$shipmentDetail = $this->_model->get_shipment_detail((int)$arg[0]);
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();
		
		$this->_view->set('shipment', $shipment);
		$this->_view->set('shipmentDetail', $shipmentDetail);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}

	function getFilterData($arg=false){
		$orderItem = array();
		$orders = $this->_model->get_orders($_POST['data']['dateFrom'], $_POST['data']['dateTo']);
		$shipment_model = new Shipments();

		foreach($orders as $_) {
			$orderData = $this->_model->get_order_item($_['id']);
			foreach($orderData as $data) {
				$orderItem[] = $data;
			}
		}
		foreach($orderItem as $key=>$_) {


			$simNumber = $_['sim_number'] . $_['check_number'];

			if($this->_model->get_shipment_detail_bysim($simNumber)>0){
			$orderItem[$key]['Seller']="Canadian Sim Cards";
			}
			else{
				$orderItem[$key]['Seller']="Other";	
			}
			$simDetail = $shipment_model->get_sim_details($simNumber);
			$orderItem[$key]['number_activated_on'] = $simDetail[0]['number_activated_on'];
			
			
		}
		echo json_encode($orderItem);
	}
}

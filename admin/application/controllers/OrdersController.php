<?php

class OrdersController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in())
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$orders = $this->_model->get_all_order();

		// checking if order has with_sim entry or not
		foreach($orders as $key=>$_) {
			$items = $this->_model->get_order_item($_['id']);
			$orders[$key]['with_sim'] = '0';
			foreach($items as $key_items=>$_items) {
				if ($_items['with_sim'] == '1') {
					$orders[$key]['with_sim'] = '1';
				}
			}
		}
		// echo "<pre>"; 
		// print_r($orders);
		$this->_view->set('orders', $orders);
	}

	function settings($arg=false){
		$setting = $this->_model->get_setting();

		// print_r($orders);
		$this->_view->set('_', $setting[0]);
		
	}
	function editsettings_action($arg=false){
		$this->render =0;
		$value = array(
			'sim_cost' => isset($_POST['sim_cost'])?$_POST['sim_cost']:'',
			'new_sim_content' => isset($_POST['new_sim_content'])?$_POST['new_sim_content']:''
		);

		$where = array(
			'id' => $_POST['id'],
		);
		$this->_model->update_setting($value, $where);

		header("Location: " . BASEURL . "orders/settings");
	}
	


	function detail($arg=false){
		$order = $this->_model->get_single_order_by_number($arg[0]);
		// echo "<pre>";
		
		$order = $order[0];
		if ($order['other_orders']) {
			$other_orders = explode("-", $order['other_orders']);
			// print_r($other_orders);
			$order['other_orders'] = $other_orders;
		}
		// print_r($order);
		$hasSim = 0;
		$hasRejectedSim = 0;

		$totalWithSim = 0;
		$totalWithoutSim = 0;
		$sim_price = 0;
		$sim_stock = array();

		$items = $this->_model->get_order_item($order['id']);

		// echo "<br/><br/>Order Items-<br/>";
		// print_r($items);

		$carrier_model = new Carriers();
		$carriers = $carrier_model->get_all_carrier();
		
		$shipment_model = new Shipments();
		$shipment = $shipment_model->get_all_shipments();
		$shipment_detail = $shipment_model->get_shipment_detail();
		$all_shipment_detail = $shipment_model->get_all_shipment_detail();

		$shipping = $this->_model->shipping_price();
		$settings = $this->_model->get_setting();
		$simPhoneNumber = array();
		foreach($items as $key=>$_) {
			if($_['with_sim'] == '1') {
				$hasSim = 1;
				$sim_price = $sim_price + $shipping[1]['value'];
				$totalWithSim++;
			}
			if ($_['with_sim'] == '1' && $_['send_new_sim'] == '1') {
				$sim_price = $sim_price - $shipping[1]['value'];
				$sim_price = $sim_price + $settings[0]['sim_cost'];
			}
			if($_['with_sim'] == '0') {
				$totalWithoutSim++;
			}
			if ($_['send_new_sim'] == '2') {
				$hasRejectedSim++;
			}
			if ($_['sim_number']) {
				$simNumber = $_['sim_number'] . $_['check_number'];
				$simDetail = $shipment_model->get_sim_details($simNumber);
				if($simDetail){
					$items[$key]['number_activated_on'] = $simDetail[0]['number_activated_on'];
					$items[$key]['shipment_id'] = $simDetail[0]['shipment_id'];
					$items[$key]['activate_sim_number'] = $simDetail[0]['sim_number'];
				}	
				else{
					$items[$key]['number_activated_on'] = '';
					$items[$key]['shipment_id'] = 0;
					$items[$key]['activate_sim_number'] = $_['sim_number'];
				}
				$items[$key]['in_network'] = $this->_model->get_shipment_detail_bysim($simNumber);

			} else {
				$items[$key]['number_activated_on'] = '';
				$items[$key]['shipment_id'] = '';
				$items[$key]['activate_sim_number'] = '';
				$items[$key]['in_network'] = 0;
			}
		}
		foreach ($shipment as $skey=>$_) {
			foreach ($all_shipment_detail as $akey=>$asd) {
				if ($asd['shipment_id'] == $_['shipment_id']) {
					array_push($sim_stock, $asd);
				}
			}
		}
		
		foreach($items as $key=>$_) { 
			$items[$key]['csc_stock'] = '0';
			if ($_['sim_number'] != null && $_['sim_number'] != '') {
				foreach($sim_stock as $skey=>$ss) { 
					$truncated_sim_number = substr($ss['sim_number'], 0, strlen($ss['sim_number'])-1);
					if (bccomp($truncated_sim_number, $_['sim_number']) == 0) {
						// print_r($truncated_sim_number . ' ' . $_['sim_number'] . '<br>');
						$items[$key]['csc_stock'] = '1';
					}
				}
			}
		}
		// print_r($items);
		$this->_view->set('order', $order);
		$this->_view->set('items', $items);
		$this->_view->set('hasSim', $hasSim);
		$this->_view->set('simPrice', $sim_price);
		$this->_view->set('totalWithSim', $totalWithSim);
		$this->_view->set('totalWithoutSim', $totalWithoutSim);
		$this->_view->set('hasRejectedSim', $hasRejectedSim);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('shipment', $shipment);
		$this->_view->set('shipment_detail', $shipment_detail);
	}

	function markOrderRefund($arg=false) {
		$this->render = 0;
		if (!isset($_POST['order_id']) || $_POST['order_id'] == '') header("Location: ".BASEURL . "orders/detail");
		
		//update order status as refunded
		$value = array(
			'shipped' => 4
		);
		$where = array(
			'id' => $_POST['order_id']
		);
		
		$this->_model->update_order($value, $where);
		
		header("Location: ".BASEURL . "orders");
	}

	function markOrderReject($arg=false) {
		$this->render = 0;
		if (!isset($_POST['order_id']) || $_POST['order_id'] == '') header("Location: ".BASEURL . "orders/detail");
		
		//update order status as refunded
		$value = array(
			'shipped' => 2
		);
		$where = array(
			'id' => $_POST['order_id']
		);
		
		$this->_model->update_order($value, $where);
		
		header("Location: ".BASEURL . "orders");
	}

	function markOrderItemRefund() {
		$orderid = $_POST['orderid'];
		$itemid = $_POST['itemid'];
		$totalWithSim = 0;
		$tax_percentage = 0;
		
		$order = $this->_model->get_single_order($orderid);
		$order = $order[0];
		// echo "<pre>";
		// print_r($order);
		// die;
		$order_item = $this->_model->get_order_item_by_id($orderid, $itemid);
		$order_item = $order_item[0];
		
		$all_order_item = $this->_model->get_order_item($orderid);

		$shipping_cost = $this->_model->shipping_price();
		
		$tax_list = $this->_model->get_tax();
		$selected_province = strtolower(trim($order['province'], ""));
		foreach($tax_list as $key=>$_) {
			$list_province = strtolower(trim($_['province'], ""));
			if ($selected_province == $list_province) {
				$tax_percentage = $_['tax'];
			}
		}
		
		foreach($all_order_item as $key=>$_) {
			if($_['with_sim'] == '1') {
				$totalWithSim++;
			}
		}

		$discount = $order['discount'] / $order['total_items'];
		$order_discount = $order['discount'] - $discount;
		$sub_total = ($order['sub_total'] - $order_item['price']);
		$sim_cost = $totalWithSim * $shipping_cost[1]['value'];
		$tax = ($order_item['price'] - $discount) * $tax_percentage;
		$total = $order['total_price'] - $order_item['price'] - $tax + $discount;
		$value = array(
			'total_price' => sprintf("%.2f", $total),
			'discount' => $order_discount,
			'sub_total' => sprintf("%.2f", $sub_total),
			'tax' => sprintf("%.2f", ($order['tax'] - $tax)),
			'total_items' => ($order['total_items'] - 1),
			'amount' => sprintf("%.2f", $total)
		);
		$where = array(
			'id' => $orderid
		);

		$this->_model->update_order($value, $where);
		
		array_shift($order);
		$orderTime = time();
		$duplicate_order = $order;
		$duplicate_order['order_number'] = $orderTime;
		$duplicate_order['shipped'] = 2;
		$duplicate_order['total_price'] = sprintf("%.2f", ($duplicate_order['total_price'] - $value['total_price']));
		$duplicate_order['discount'] = sprintf("%.2f", $discount);
		$duplicate_order['shipping'] = 0;
		$duplicate_order['sub_total'] = sprintf("%.2f", ($duplicate_order['sub_total'] - $value['sub_total']));
		$duplicate_order['tax'] = sprintf("%.2f", ($duplicate_order['tax'] - $value['tax']));
		$duplicate_order['total_items'] = 1;
		$duplicate_order['amount'] = sprintf("%.2f", ($duplicate_order['amount'] - $value['total_price']));
		$duplicate_order['other_orders'] = $order['order_number'];

		$this->_model->insert_order($duplicate_order);
		$last_order_id = $this->_model->get_last_id();

		$order_item_value = array(
			'order_id' => $last_order_id
		);

		$order_item_where = array(
			'id' => $itemid
		);

		$this->_model->update_order_item($order_item_value, $order_item_where);
		
		$value = array(
			'other_orders' => $order['other_orders'] ? ($order['other_orders'] . '-' . $orderTime) : $orderTime
		);
		$where = array(
			'id' => $orderid
		);

		$this->_model->update_order($value, $where);
		// die;
		header("Location: ".BASEURL . "orders");
	}

	function active($arg=false){
		$this->render =0;

		if (!isset($_POST['acct_number']) || $_POST['acct_number'] == '') header("Location: ".BASEURL . "orders/detail");
		//if (!isset($_POST['phn_number']) || $_POST['phn_number'] == '') header("Location: ".BASEURL . "orders/detail");

		$value = array(
			'shipped' => 3,
			'acct_number' => $_POST['acct_number'],
			'phn_number' => '' //$_POST['phn_number']
		);
		$where = array(
			'id' => $_POST['order_id']
		);

		$this->_model->update_order($value, $where);

		$order = $this->_model->get_single_order($_POST['order_id']);
		$orderItem = $this->_model->get_order_item($_POST['order_id']);
		$shipmentDetails = '
			<tr>
				<td>
					<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
						<table style="width: 100%;">
							<tr>
								<td style="width: 60%;">
									<p style="font-size: 12px;text-align: center;color: #666;">SIM Number </p>
								</td>
								<td style="width: 40%;">
									<p style="font-size: 12px;text-align: center;color: #666;">Phone Number </p>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>';
		
		foreach ($orderItem as $key=>$values) {
			$sim_number = $_POST['sim_number_' . $key];
			$shipment_id = $_POST['shipment_id_' . $key];
			$phone_number = $_POST['phone_number_' . $key];
			$shipmentValue = array(
				'number_activated_on' => $phone_number
			);
			$shipmentWhere = array(
				'shipment_id' 	=> $shipment_id,
				'sim_number'	=> $sim_number
			);
			$this->_model->update_shipment_detail($shipmentValue, $shipmentWhere);	
			$shipmentDetails .= '
				<tr>
					<td>
						<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
							<table style="width: 100%;">
								<tr>
									<td style="width: 60%;">
										<p style="font-weight: bold;font-size: 16px;text-align: center;color: #292929;">'.$sim_number.'</p>
									</td>
									<td style="width: 40%;">
										<p style="font-weight: bold;font-size: 16px;text-align: center;color: #292929;">'.$phone_number.'</p>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			';
		}

		$order = $order[0];
		$mailText = 'The following is your Chatr account number and phone number for your order number ';
		$mailText2 = '';
		foreach($orderItem as $_) {
			if ($_['with_sim'] == 0) {
				$mailText = 'Following is your Chatr account number and SIM number provided in your order number ';
				$mailText2 = ' is active';
			}
			
		}

		$msg = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		<head>
			<meta charset="UTF-8">
			<meta http-equiv="x-ua-compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Your order has been shipped!</title>
			<style type="text/css">
				 p{
				 font-size: 14px;
				 font-family:Arial,sans-serif;
				 margin:0;
				 color: #292929;
				 padding:0;
				 }
			</style>
		</head>
		<body style="background: #e1e1e1;">
		<center>
			 <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
					<tr>
						 <td style="padding-top: 40px;">
								<div style="width: 520px;background: #fff;margin: 0 auto;">
									 <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
											<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
									 </div>
									 <div style="padding-left: 70px;padding-right: 70px;">
											<table>
												 <tr>
														<td>
															 <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Your Sim has <br>been activated!</p>
														</td>
												 </tr>
												 <tr><td height="20"></td></tr>
												 <tr>
														<td>
															 <p style="line-height: 1.5;color:#666;text-align: center;"><span style="font-weight: bold;color: #292929;">'. $mailText . $order['order_number']. $mailText2 .'</span></p>
														</td>
												 </tr>
												<tr><td height="20"></td></tr>
												<tr>
													<td>
														<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
															<table style="width: 100%;">
																<tr>
																	<td>
																		<p style="font-size: 12px;text-align: center;color: #666;">Chatr Account Number </p><br>
																		<p style="font-weight: bold;font-size: 16px;text-align: center;color: #292929;">'.$order['acct_number'].'</p>
																	</td>
																</tr>
															</table>
														</div>
													</td>
												</tr>
												<tr><td height="35"></td></tr>
												'. $shipmentDetails .'
												<tr><td height="35"></td></tr>
											</table>
									 </div>
								</div>
						 </td>
					</tr>
					<tr>
						 <td>
								<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
									 <div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
											<p style="font-size: 14px;color: #666;">
												If you have any questions about <br>
												your order, please contact us at <br>
												<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
												info@candiansimcards.com <br>
												<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
											</p>
									 </div>
								</div>
						 </td>
					</tr>
			 </table>
		</center>
		</body>
		</html>';

		$config = load_config('email');
		$to = $order['email'];
		$subject = '[CanadianSimCards] Sim activated!';

		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent("text/html", $msg);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
		}

		// Helper::sendMail($order['email'], '[CanadianSimCards] Order Shipped', $msg);
		// Helper::sendMail('tamilromeo@gmail.com', '[CanadianSimCards] Order Shipped', $msg);

		header("Location: ".BASEURL . "orders");


	}
	public function send_new_sim(){
		// echo "<pre>";
		$settings = $this->_model->get_setting();
		$setting=$settings[0];
		// print_r($setting);
		$orderid=$_GET['orderid'];
		$oid=$_GET['oid'];
		$clientname=$_GET['clientname'];
		$billingemail=$_GET['email'];
		$simcard_cost=$setting['sim_cost'];
		$content=$setting['new_sim_content'];

		$shipping_cost = $this->_model->shipping_price();

		// print_r($single_order);
		$single_order=$this->_model->get_single_order($oid);
		$clientemail=$this->_model->get_client_email_By_id($single_order[0]["user_id"]);
		$baseurl = str_replace("admin/","",BASEURL);
		
		$planid=20;
		// $planid=$this->_model->get_order_planid($single_order[0]["title"]);
		// $content=str_replace("{{planid}}",$planid,$content);
		$content=str_replace("{{orderid}}",$oid,$content);
		$content=str_replace("{{itemid}}",$orderid,$content);
		$content=str_replace("{{clientname}}",$clientname,$content);
		$content=str_replace("{{sim_cost}}",$simcard_cost,$content);
		$content=str_replace("{{shipping_cost}}",$shipping_cost[0]['value'],$content);
		$content=str_replace("{{baseurl}}",$baseurl,$content);
		
		
		$config = load_config('email');
		$to = $billingemail;
		
		// print_r($content);
		// die;
		$subject = '[CanadianSimCards] New Sim';
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		
		if ($to==$clientemail) {
			$email->addTo($to);
		} else {
			$email->addTo($to)->addTo($clientemail);
		}
		
		$email->addContent("text/html", $content);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);

			$itemvalue = array(
					'suggest_mail_sent' => 1
				);
				$itemwhere = array(
					'id' => $orderid
				);
				$this->_model->update_order_item($itemvalue, $itemwhere);

		} catch (Exception $e) {
		}
		header("Location: ".BASEURL . "orders/detail/".$single_order[0]['order_number']);
	}
	
	function sim_card_status(){

		$status=$_GET['status'];
		$orderid=$_GET['orderid'];
		$itemvalue = array(
					'with_sim' => $status
				);
				$itemwhere = array(
					'id' => $orderid
				);
				$this->_model->update_order_item($itemvalue, $itemwhere);

					header("Location: ".BASEURL . "orders");
	}

	function bindCarrier($carrier, $carriers) {
		$returnValue = 'NA';
		foreach($carriers as $value) {
			if ($value['id'] === $carrier) {
				$returnValue = $value['carrier'];
			}
		}
		return $returnValue;
	}

	function shipped($arg=false){
		$this->render =0;

		if (!isset($_POST['tracking_number']) || $_POST['tracking_number'] == '') header("Location: ".BASEURL . "orders/detail");
		// if (!isset($_POST['carrier']) || $_POST['carrier'] == '') header("Location: ".BASEURL . "orders/detail");
		// if (!isset($_POST['sim_card']) || $_POST['sim_card'] == '') header("Location: ".BASEURL . "orders/detail");

		$value = array(
			'shipped' => 1,
			'tracking_number' => $_POST['tracking_number']
			// 'carrier' => $_POST['carrier'],
			// 'bambora_id' => $_POST['sim_card']
		);
		$where = array(
			'id' => $_POST['order_id']
		);
		$no_of_shipping_items = 0;
		$no_of_custom_sim = 0;
		$orderitems = $this->_model->get_order_item($_POST['order_id']);
		foreach ($orderitems as $key=>$values) {
			if ((int)$orderitems[$key]['with_sim'] == 1) {
				$sim_number = $_POST['sim_number_' . $key];
				$truncated_sim_number = substr($sim_number, 0, strlen($sim_number)-1);
				$orderitems[$key]['sim_number'] = $truncated_sim_number;
				$orderitems[$key]['store'] = $_POST['store_' . $key];
				
				if ((int)$orderitems[$key]['send_new_sim'] == 1) {
					$no_of_custom_sim++;
				} else {
					$no_of_shipping_items++;
				}
				$itemvalue = array(
					'store' => $orderitems[$key]['store'],
					'sim_number' => $orderitems[$key]['sim_number'],
					'check_number' => substr($sim_number, strlen($sim_number)-1, strlen($sim_number))
				);
				$itemwhere = array(
					'id' => $orderitems[$key]['id']
				);
				$this->_model->update_order_item($itemvalue, $itemwhere);

				$this->_model->update_stock($orderitems[$key]['store'], $orderitems[$key]['carrier'], $_POST['shipment_id_' . $key]);
			}
		}
		
		$this->_model->update_order($value, $where);

		$order = $this->_model->get_single_order($_POST['order_id']);
		$order = $order[0];
		$items = $this->_model->get_order_item($order['id']);

		$carrier_model = new Carriers();
		$carriers = $carrier_model->get_all_carrier();

		$item_html = '';
		foreach ($items as $key => $_) {
			$carrierName = $this->bindCarrier($_['carrier'], $carriers);
			$item_html .= '
				<tr>
					<td style="width: 140px;text-align: left;">
						 <p style="font-size: 16px;font-weight: bold; color: #292929;">'.$_['title'].'</p>
					</td>
					<td style="width: 80px;text-align: center;">
						 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">'.$carrierName.'</p>
					</td>
					<td style="width: 80px;text-align: center;">
						 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">'.$_['quantity'].'</p>
					</td>
					<td>
						 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$_['price'].'</p>
					</td>
				</tr>
				<tr><td colspan="4" height="20"></td></tr>';
		}

		$shipping = $this->_model->shipping_price();
		$setting = $this->_model->get_setting(); //custom sim price
		$shippingcost = ($no_of_shipping_items || $no_of_custom_sim) > 0 ? $shipping[0]['value'] : 0;
		$total_shipping_cost = ($no_of_shipping_items + $no_of_custom_sim) * $shippingcost;
		$sim_price = $no_of_shipping_items * $shipping[1]['value'];
		$custom_sim_price = $no_of_custom_sim * $setting[0]['sim_cost']; //custom sim price calculation
		$total_sim_price = $sim_price + $custom_sim_price;
		$activate_link = "http://canadiansimcards.com/activate/" . $order['id'];
		$msg = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Your order has been shipped!</title>
	<style type="text/css">
		 p{
		 font-size: 14px;
		 font-family:Arial,sans-serif;
		 margin:0;
		 color: #292929;
		 padding:0;
		 }
	</style>
</head>
<body style="background: #e1e1e1;">
<center>
	 <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
			<tr>
				 <td style="padding-top: 40px;">
						<div style="width: 520px;background: #fff;margin: 0 auto;">
							 <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
									<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
							 </div>
							 <div style="padding-left: 70px;padding-right: 70px;">
									<table>
										 <tr>
												<td>
													 <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Your order has <br>been shipped!</p>
												</td>
										 </tr>
										 <tr><td height="20"></td></tr>
										 <tr>
												<td>
													 <p style="line-height: 1.5;color:#666;text-align: center;">We are happy to let you know that listed item(s) of your order number <span style="font-weight: bold;color: #292929;">'.$order['order_number'].'</span> have been shipped.</p>
												</td>
										 </tr>

										<tr><td height="35"></td></tr>
										<tr>
											<td>
												<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
													<table style="width: 100%;">
														<tr>
															<td>
																<p style="font-size: 12px;text-align: center;color: #666;">Tracking Number </p><br>
																<p style="font-weight: bold;font-size: 16px;text-align: center;color: #292929;text-decoration: underline;">'.$order['tracking_number'].'</p>
															</td>
														</tr>
													</table>
												</div>
											</td>
										</tr>


										 <tr><td height="45"></td></tr>
										 <tr>
												<td>
													 <div>
															<table style="width: 100%;">
																 <tr>
																		<td style="width: 140px;text-align: left;">
																			 <p style="font-size: 14px;color: #666;">Shipped Items</p>
																		</td>
																		<td style="width: 80px;text-align: center;">
																			 <p style="font-size: 14px;color: #666;text-align: center;">Carrier</p>
																		</td>
																		<td style="width: 80px;text-align: center;">
																			 <p style="font-size: 14px;color: #666;text-align: center;">Qty</p>
																		</td>
																		<td><p style="font-size: 14px;color: #666;">Price</p></td>
																 </tr>
																 <tr><td colspan="3" height="12"></td></tr>
																 '.$item_html.'
																 <tr>
																		<td colspan="4">
																			 <div style="padding-top: 25px;padding-bottom: 25px;">
																					<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			 </div>
																		</td>
																 </tr>
																 <tr>
																		<td colspan="3" style="text-align: left;">
																			 <p style="font-size: 16px;font-weight: bold; color: #292929;">Subtotal</p>
																		</td>
																		<td>
																			 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$order['sub_total'].'</p>
																		</td>
																 </tr>
																 <tr>
																	 <td colspan="3" style="text-align: left;">
																		 <p style="font-size: 16px;font-weight: bold; color: #292929;">One-Time Sim Cost</p>
																	 </td>
																	 <td>
																		 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $total_sim_price) . '</p>
																	 </td>
																 </tr>
																 <tr>
																	 <td colspan="3" style="text-align: left;">
																		 <p style="font-size: 16px;font-weight: bold; color: #292929;">Discount</p>
																	 </td>
																	 <td>
																		 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $order['discount']) . '</p>
																	 </td>
																 </tr>
																 <tr>
																	 <td colspan="3" style="text-align: left;">
																		 <p style="font-size: 16px;font-weight: bold; color: #292929;">Shipping</p>
																	 </td>
																	 <td>
																		 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $total_shipping_cost) . '</p>
																	 </td>
																 </tr>
																 <tr><td colspan="4" height="10"></td></tr>
																 <tr>
																		<td colspan="3" style="text-align: left;">
																			 <p style="font-size: 16px;font-weight: bold; color: #292929;">Estimated tax</p>
																		</td>
																		<td>
																			 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'. sprintf("%.2f", $order['tax']) .'</p>
																		</td>
																 </tr>
																 <tr>
																		<td colspan="4">
																			 <div style="padding-top: 25px;padding-bottom: 25px;">
																					<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			 </div>
																		</td>
																 </tr>
																 <tr>
																		<td colspan="3" style="text-align: left;">
																			 <p style="font-size: 16px;font-weight: bold; color: #292929;">Total</p>
																		</td>
																		<td>
																			 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$order['total_price'].'</p>
																		</td>
																 </tr>
																 <tr><td colspan="3" height="45"></td></tr>
															</table>
													 </div>
												</td>
										 </tr>
									</table>
							 </div>
						</div>
				 </td>
			</tr>
			<tr>
				 <td>
					<div style="text-align: center;padding-left: 40px;padding-right: 40px;">
						<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
							<a href="'.$activate_link.'"><p style="font-size: 16px;font-weight: bold; color: #15c;">Click Here To Activate SIM</p></a>
						</div>
					</div>
				 </td>
			</tr>
			<tr>
				 <td>
						<div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
							 <div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
									<p style="font-size: 14px;color: #666;">Shipping address</p><br>
									<p style="font-size: 16px;font-weight: bold;">'.$order['full_name'].'</p><br>
									<p style="font-size: 16px;">
										 '.$order['address'].' <br>
										 '.$order['city'].', '.$order['province'].', '.$order['zip'].'<br>
										 '.$order['country'].'
									</p>
							 </div>
						</div>
						<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
							 <div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
									<p style="font-size: 14px;color: #666;">
										If you have any questions about <br>
										your order, please contact us at <br>
										<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
										info@candiansimcards.com <br>
										<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
									</p>
							 </div>
						</div>
				 </td>
			</tr>
	 </table>
</center>
</body>
</html>';


		$config = load_config('email');
		$to = $order['email'];
		$subject = '[CanadianSimCards] Order Shipped';

		$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent("text/html", $msg);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);
		} catch (Exception $e) {
		}



		// Helper::sendMail($order['email'], '[CanadianSimCards] Order Shipped', $msg);
		// Helper::sendMail('tamilromeo@gmail.com', '[CanadianSimCards] Order Shipped', $msg);

		header("Location: ".BASEURL . "orders");
	}




}

<?php

class SettingsController extends Controller {

	private $_authentication;

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

	}

	function index($arg=false){
		$price = $this->_model->get_shipping_price();

		$setting = $this->_model->get_setting();

		$this->_view->set('price', $price[0]['value']);
		$this->_view->set('sim_price', $price[1]['value']);
		$this->_view->set('sim_cost', $setting[0]['sim_cost']);
		$this->_view->set('new_sim_content', $setting[0]['new_sim_content']);
	}

	function apply($arg=false){
		$this->render =0;

		$value = array(
			'value' => isset($_POST['value'])?$_POST['value']:'0'
		);
		$where = array('id' => 1);
		$this->_model->set_shipping_price($value, $where);

		$value = array(
			'value' => isset($_POST['sim_price'])?$_POST['sim_price']:'0'
		);
		$where = array('id' => 2);
		$this->_model->set_shipping_price($value, $where);

		$value = array(
			'sim_cost' => isset($_POST['sim_cost'])?$_POST['sim_cost']:'0',
			'new_sim_content' => isset($_POST['new_sim_content'])?$_POST['new_sim_content']:''
		);
		$where = array('id' => 1);
		$this->_model->set_setting_price($value, $where);

		header("Location: " . BASEURL . "settings");
	}


}













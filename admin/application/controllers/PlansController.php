<?php

class PlansController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$plans = $this->_model->get_all_plans();
		$carriers = $this->_model->get_all_carriers();

		$this->_view->set('plans', $plans);
		$this->_view->set('carriers', $carriers);
	}

	function addplan($arg=false){
		$carriers = $this->_model->get_all_carriers();
		$this->_view->set('carriers', $carriers);
	}
	function addplan_action($arg=false){
		$this->render =0;
		// print_r(isset($_POST['with_sim'])?$_POST['with_sim']:'OFF'); die;
		$value = array(
			'carrier' => isset($_POST['carrier'])?$_POST['carrier']:'',
			'title' => isset($_POST['title'])?$_POST['title']:'',
			'price' => isset($_POST['price'])?$_POST['price']:'',
			'with_sim' => isset($_POST['with_sim'])?$_POST['with_sim']:'',

			'plan_detail1' => isset($_POST['plan_detail1'])?$_POST['plan_detail1']:'',
			'plan_detail2' => isset($_POST['plan_detail2'])?$_POST['plan_detail2']:'',
			'plan_detail3' => isset($_POST['plan_detail3'])?$_POST['plan_detail3']:'',
			'plan_detail4' => isset($_POST['plan_detail4'])?$_POST['plan_detail4']:'',
			'plan_detail5' => isset($_POST['plan_detail5'])?$_POST['plan_detail5']:'',
			'plan_detail6' => isset($_POST['plan_detail6'])?$_POST['plan_detail6']:'',
			'plan_detail7' => isset($_POST['plan_detail7'])?$_POST['plan_detail7']:'',
			'plan_detail8' => isset($_POST['plan_detail8'])?$_POST['plan_detail8']:'',
			'plan_detail9' => isset($_POST['plan_detail9'])?$_POST['plan_detail9']:'',
			'plan_detail10' => isset($_POST['plan_detail10'])?$_POST['plan_detail10']:'',

			'shipping' => isset($_POST['shipping'])?$_POST['shipping']:1,
			'save' => isset($_POST['save'])?$_POST['save']:0,

			'sort_order' => isset($_POST['sort_order'])?$_POST['sort_order']:0,
			'type' => isset($_POST['type'])?$_POST['type']:''
		);

		$this->_model->insert_plans($value);

		header("Location: " . BASEURL . "plans");
	}



	function editplan($arg=false){

		$plan = $this->_model->get_plans_by_id($arg[0]);
		$carriers = $this->_model->get_all_carriers();

		$this->_view->set('_', $plan[0]);
		$this->_view->set('carriers', $carriers);
	}

	function editplan_action($arg=false){
		$this->render =0;
		$value = array(
			'carrier' => isset($_POST['carrier'])?$_POST['carrier']:'',
			'title' => isset($_POST['title'])?$_POST['title']:'',
			'price' => isset($_POST['price'])?$_POST['price']:'',
			'with_sim' => isset($_POST['with_sim'])?$_POST['with_sim']:'',

			'plan_detail1' => isset($_POST['plan_detail1'])?$_POST['plan_detail1']:'',
			'plan_detail2' => isset($_POST['plan_detail2'])?$_POST['plan_detail2']:'',
			'plan_detail3' => isset($_POST['plan_detail3'])?$_POST['plan_detail3']:'',
			'plan_detail4' => isset($_POST['plan_detail4'])?$_POST['plan_detail4']:'',
			'plan_detail5' => isset($_POST['plan_detail5'])?$_POST['plan_detail5']:'',
			'plan_detail6' => isset($_POST['plan_detail6'])?$_POST['plan_detail6']:'',
			'plan_detail7' => isset($_POST['plan_detail7'])?$_POST['plan_detail7']:'',
			'plan_detail8' => isset($_POST['plan_detail8'])?$_POST['plan_detail8']:'',
			'plan_detail9' => isset($_POST['plan_detail9'])?$_POST['plan_detail9']:'',
			'plan_detail10' => isset($_POST['plan_detail10'])?$_POST['plan_detail10']:'',

			'shipping' => isset($_POST['shipping'])?$_POST['shipping']:1,
			'save' => isset($_POST['save'])?$_POST['save']:0,

			'sort_order' => isset($_POST['sort_order'])?$_POST['sort_order']:0,
			'type' => isset($_POST['type'])?$_POST['type']:''
		);

		$where = array(
			'id' => $_POST['id'],
		);
		$this->_model->update_plans($value, $where);

		header("Location: " . BASEURL . "plans");
	}


	function deleteplan($arg=false){
		$this->render =0;

		$where = array(
			'id' => $_POST['id']
		);
		$this->_model->delete_plans($where);

		header("Location: " . BASEURL . "plans");
	}
}

<?php

class StoresController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$stores = $this->_model->get_all_stores();
		foreach($stores as $_=>$value) {
			$stock = $this->_model->get_store_stock($value['id']);
			$stores[$_]['canDelete'] = count($stock) > 0 ? 0 : 1;
		}
		$this->_view->set('stores', $stores);
	}

	function addstore($arg=false){}

	function addstore_action($arg=false){
		$this->render =0;

		if ($_REQUEST['submit'] === 'Save') {
			$value = array(
				'name' => isset($_POST['store_name'])?$_POST['store_name']:'',
				'address' => isset($_POST['address'])?$_POST['address']:'',
				'city' => isset($_POST['city'])?$_POST['city']:'',
				'province' => isset($_POST['province'])?$_POST['province']:'',
				'postal_code' => isset($_POST['postal_code'])?$_POST['postal_code']:'',
				'contact_name' => isset($_POST['contact_name'])?$_POST['contact_name']:'',
				'email' => isset($_POST['email'])?$_POST['email']:'',
				'contact_number' => isset($_POST['contact_number'])?$_POST['contact_number']:'',
			);
			$this->_model->insert_store($value);
			header("Location: " . BASEURL . "stores");
		} else {
			header("Location: " . BASEURL . "stores");
		}
	}

	function editstore($arg=false){

		$store = $this->_model->get_store_by_id($arg[0]);

		$this->_view->set('_', $store[0]);
	}

	function editstore_action($arg=false){
		$this->render =0;

		if ($_REQUEST['submit'] === 'Save') {
			$value = array(
				'name' => isset($_POST['store_name'])?$_POST['store_name']:'',
				'address' => isset($_POST['address'])?$_POST['address']:'',
				'city' => isset($_POST['city'])?$_POST['city']:'',
				'province' => isset($_POST['province'])?$_POST['province']:'',
				'postal_code' => isset($_POST['postal_code'])?$_POST['postal_code']:'',
				'contact_name' => isset($_POST['contact_name'])?$_POST['contact_name']:'',
				'email' => isset($_POST['email'])?$_POST['email']:'',
				'contact_number' => isset($_POST['contact_number'])?$_POST['contact_number']:'',
			);
			$where = array(
				'id' => $_POST['id'],
			);
			$this->_model->update_store($value, $where);
			header("Location: " . BASEURL . "stores");
		} else {
			header("Location: " . BASEURL . "stores");
		}
	}

	function deletestore($arg=false){
		$this->render =0;

		$where = array(
			'id' => $_POST['id']
		);
		$this->_model->delete_store($where);

		header("Location: " . BASEURL . "stores");
	}
}

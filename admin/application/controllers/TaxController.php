<?php

class TaxController extends Controller {

	private $_authentication;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");
	}

	function index($arg=false){
		$tax = $this->_model->get_tax();
		$this->_view->set('tax', $tax);
	}

	function addtax($arg=false){
	}

	function addtax_action($arg=false){

		$this->render =0;

		$value = array(
			'province' => $_POST['province'],
			'tax' => $_POST['tax']
		);
		$this->_model->insert_tax($value);

		header("Location: " . BASEURL . "tax");
	}

	function deletetax($arg=false){
		$this->render =0;

		$where = array(
			'id' => $_POST['id']
		);
		$this->_model->delete_tax($where);

		header("Location: " . BASEURL . "tax");
	}

	function edittax($arg=false){

		$tax_id = $arg[0];

		$tax = $this->_model->get_tax_by_id($tax_id);
		$tax = $tax[0];

		$this->_view->set('tax',$tax);
	}

	function edittax_action($arg=false){
		$this->render = 0;
		
		$value = array(
			'province' => $_POST['province'],
			'tax' => $_POST['tax']
		);

		$where = array(
			'id' => $_POST['id'],
		);
		$this->_model->update_tax($value, $where);

		header("Location: " . BASEURL . "tax");
	}
}













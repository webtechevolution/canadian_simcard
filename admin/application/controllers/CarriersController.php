<?php

class CarriersController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$carriers = $this->_model->get_all_carrier();
		$this->_view->set('carriers', $carriers);
	}

	function addcarrier($arg=false){}

	function addcarrier_action($arg=false){
		$this->render = 0;
		$uploadOk = 1;
		if ($_REQUEST['submit'] === 'Save') {
			if(isset($_FILES['fileToUpload'])) {
				$target_dir = "/public/images/uploads/";
				$fileName = time();
				$target_file = basename($_FILES["fileToUpload"]["name"]);
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
				$target_file = $target_dir . $fileName . '.' . $imageFileType;
				
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$uploadOk = 1;
					} else {
						$uploadOk = 0;
					}
				}

				// Check if file already exists
				if (file_exists($target_file)) {
					$uploadOk = 0;
				}

				// Check file size
				if ($_FILES["fileToUpload"]["size"] > 2097152) {
					$uploadOk = 0;
				}

				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded.";
				} else {
					if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], SITE_ROOT.$target_file)) {
						$uploadOk = 1;
					} else {
						$uploadOk = 0;
					}
				}
			}
			$url = trim(strtolower($_POST['carrier']));
			$url = str_replace(' ', '-', $url);
			$url = str_replace(',', '', $url);
			$url = str_replace('\'', '', $url);
			$url = str_replace('"', '', $url);
			if ($uploadOk === 1) {
				$value = array(
					'carrier' => $_POST['carrier'],
					'logo' => ltrim($target_file, $target_file[0])
				);
			} else {
				$value = array(
					'carrier' => isset($_POST['carrier']) ? $_POST['carrier'] : '',
					'logo' => ''
				);
			}
			$this->_model->insert_carrier($value);
			header("Location: " . BASEURL . "carriers");
		} else {
			header("Location: " . BASEURL . "carriers");
		}
	}

	function editcarrier($arg=false){
		$carrier_id = $arg[0];
		$carrier = $this->_model->get_carrier_by_id($carrier_id);
		$carrier = $carrier[0];
		$this->_view->set('carrier',$carrier);
	}

	function editcarrier_action($arg=false){
		$this->render = 0;
		$uploadOk = 1;
		if ($_REQUEST['submit'] === 'Save') {
			if(isset($_FILES['fileToUpload'])) {
				$target_dir = "/public/images/uploads/";
				$fileName = time();
				$target_file = basename($_FILES["fileToUpload"]["name"]);
				$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
				$target_file = $target_dir . $fileName . '.' . $imageFileType;
				
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
					$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
					if($check !== false) {
						$uploadOk = 1;
					} else {
						$uploadOk = 0;
					}
				}

				// Check if file already exists
				if (file_exists($target_file)) {
					$uploadOk = 0;
				}

				// Check file size
				if ($_FILES["fileToUpload"]["size"] > 2097152) {
					$uploadOk = 0;
				}

				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" ) {
					$uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					echo "Sorry, your file was not uploaded.";
				} else {
					if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], SITE_ROOT.$target_file)) {
						$uploadOk = 1;
					} else {
						$uploadOk = 0;
					}
				}
			}
			$url = trim(strtolower($_POST['carrier']));
			$url = str_replace(' ', '-', $url);
			$url = str_replace(',', '', $url);
			$url = str_replace('\'', '', $url);
			$url = str_replace('"', '', $url);
			if ($uploadOk === 1) {
				$value = array(
					'carrier' => $_POST['carrier'],
					'logo' => ltrim($target_file, $target_file[0])
				);
			} else {
				$value = array(
					'carrier' => $_POST['carrier']
				);
			}
			$where = array(
				'id' => $_POST['id'],
			);
			$this->_model->update_carrier($value, $where);
			header("Location: " . BASEURL . "carriers");
		} else {
			header("Location: " . BASEURL . "carriers");
		}
	}


	function deletecarrier($arg=false){
		$this->render =0;
		$where = array(
			'id' => $_POST['id']
		);
		$this->_model->delete_carrier($where);
		header("Location: " . BASEURL . "carriers");
	}
}
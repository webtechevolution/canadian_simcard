<?php

class SummaryController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function sales($arg=false){
		$stock = $this->_model->get_all_stock();
		$carriers = $this->_model->get_all_carriers();
		$stores = $this->_model->get_all_stores();

		$this->_view->set('stock', $stock);
		$this->_view->set('carriers', $carriers);
		$this->_view->set('stores', $stores);
	}
}

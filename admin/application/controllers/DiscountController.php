<?php

class DiscountController extends Controller {

	private $_authentication;
	private $session = false;
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
		if (!$this->_authentication->logged_in()) 	
			header("Location: " . BASEURL . "login");

		$this->session = new Session();
	}

	function index($arg=false){
		$codes = $this->_model->get_all_code();

		// print_r($client);

		$this->_view->set('codes', $codes);
	}

	function addcode($arg=false){}
	function addcode_action($arg=false){
		$this->render =0;

		$value = array(
			'name' => isset($_POST['name'])?$_POST['name']:'',
			'code' => isset($_POST['code'])?$_POST['code']:'',
			'value' => isset($_POST['value'])?$_POST['value']:'',
			'type' => isset($_POST['type'])?$_POST['type']:''
		);

		$this->_model->insert_code($value);

		header("Location: " . BASEURL . "discount");
	}


	function deletecode($arg=false){
		$this->render =0;

		$where = array(
			'id' => $_POST['id']
		);
		$this->_model->delete_code($where);

		header("Location: " . BASEURL . "discount");
	}




}













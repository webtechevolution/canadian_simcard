<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>

	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Carrier - Add</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" action="<?=BASEURL?>carriers/addcarrier_action" enctype="multipart/form-data">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Carriers</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="carrier" placeholder="Enter a Carrier Name" maxlength="20">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Logo</h3>
								</div>
								<div class="panel-body">
									<input type="file" class="form-control" id="file" name="fileToUpload" id="fileToUpload" placeholder="File Name" value="<?=$_['logo']?>" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel">
								<div class="panel-body">
									<input type="submit" name="submit" value="Save" class="btn btn-success">
									<input type="submit" name="submit" value="Cancel" class="btn btn-info">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
		jQuery(document).ready(function(){});
		</script>
	</body>
</html>
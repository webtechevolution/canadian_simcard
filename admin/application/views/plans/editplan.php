<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.panel-title-extra {
				font-size: 16px;
				padding: 6px 12px;
				margin-bottom: 0px;
				font-weight: 400;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
						<h1>Plans - Edit</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" id="category" action="<?=BASEURL?>plans/editplan_action">
					<input type="hidden" name="id" value="<?=$_['id']?>">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Carriers</h3>
								</div>
								<div class="panel-body">
									<select name="carrier" class="form-control">
										<?php foreach($carriers as $key): ?>
											<option value="<?= $key['id'] ?>" <?= ($_['carrier'] == $key['id']) ? 'selected' : ''?> ><?= $key['carrier']?></option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Price</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="price" placeholder="Price" value="<?=$_['price']?>">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Title</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="title" placeholder="Title" value="<?=$_['title']?>">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">With Sim</h3>
								</div>
								<div class="panel-body">
									<input name="with_sim" value="0" type="hidden">
									<input type="checkbox" name="with_sim" value="1" <?php echo ($_['with_sim'] == '1' ? 'checked' : '');?>>
									<label class="panel-title-extra">With Sim</label>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Save</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="save" placeholder="Save" value="<?=$_['save']?>">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Sort Order</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="sort_order" placeholder="Sort Order (Integer)" value="<?=$_['sort_order']?>">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Status</h3>
								</div>
								<div class="panel-body">
									<select name="type" class="form-control">
										<option value="1" <?=($_['type']==1)?'selected':''?>>Active</option>
										<option value="2" <?=($_['type']==2)?'selected':''?>>Sold Out</option>
										<option value="3" <?=($_['type']==3)?'selected':''?>>Coming Soon</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Shipping Cost?</h3>
								</div>
								<div class="panel-body">
									<select name="shipping" class="form-control">
										<option value="1" <?=($_['shipping']==1)?'selected':''?>>Yes</option>
										<option value="0" <?=($_['shipping']==0)?'selected':''?>>No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Plan Details</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="plan_detail1" placeholder="Plan Detail" value="<?=$_['plan_detail1']?>"><br>
									<input type="text" class="form-control" name="plan_detail2" placeholder="Plan Detail" value="<?=$_['plan_detail2']?>"><br>
									<input type="text" class="form-control" name="plan_detail3" placeholder="Plan Detail" value="<?=$_['plan_detail3']?>"><br>
									<input type="text" class="form-control" name="plan_detail4" placeholder="Plan Detail" value="<?=$_['plan_detail4']?>"><br>
									<input type="text" class="form-control" name="plan_detail5" placeholder="Plan Detail" value="<?=$_['plan_detail5']?>"><br>
									<input type="text" class="form-control" name="plan_detail6" placeholder="Plan Detail" value="<?=$_['plan_detail6']?>"><br>
									<input type="text" class="form-control" name="plan_detail7" placeholder="Plan Detail" value="<?=$_['plan_detail7']?>"><br>
									<input type="text" class="form-control" name="plan_detail8" placeholder="Plan Detail" value="<?=$_['plan_detail8']?>"><br>
									<input type="text" class="form-control" name="plan_detail9" placeholder="Plan Detail" value="<?=$_['plan_detail9']?>"><br>
									<input type="text" class="form-control" name="plan_detail10" placeholder="Plan Detail" value="<?=$_['plan_detail10']?>"><br>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" value="Save" class="btn btn-success btn-block">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>

		<script type="text/javascript">
			jQuery(document).ready(function(){});
		</script>
	</body>
</html>
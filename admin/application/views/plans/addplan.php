<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>
		<style>
			.panel-title-extra {
				font-size: 16px;
				padding: 6px 12px;
				margin-bottom: 0px;
				font-weight: 400;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Plans - Add</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" id="category" action="<?=BASEURL?>plans/addplan_action">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Carriers</h3>
								</div>
								<div class="panel-body">
									<select name="carrier" class="form-control">
										<?php foreach($carriers as $key): ?>
											<option value="<?= $key['id'] ?>" ><?= $key['carrier']?></option>
										<?php endforeach;?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Price</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="price" placeholder="Price">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Title</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="title" placeholder="Title">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">With SIM</h3>
								</div>
								<div class="panel-body">
									<input name="with_sim" value="0" type="hidden">
									<input type="checkbox" name="with_sim" value="1">
									<label class="panel-title-extra">With SIM</label>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Save</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="save" placeholder="Save">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Sort Order</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="sort_order" placeholder="Sort Order (Integer)">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Status</h3>
								</div>
								<div class="panel-body">
									<select name="type" class="form-control">
										<option value="1">Active</option>
										<option value="2">Sold Out</option>
										<option value="3">Coming Soon</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Shipping Cost?</h3>
								</div>
								<div class="panel-body">
									<select name="shipping" class="form-control">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Plan Details</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="plan_detail1" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail2" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail3" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail4" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail5" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail6" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail7" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail8" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail9" placeholder="Plan Detail"><br>
									<input type="text" class="form-control" name="plan_detail10" placeholder="Plan Detail"><br>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" value="Save" class="btn btn-success btn-block">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
		jQuery(document).ready(function(){});
		</script>
	</body>
</html>
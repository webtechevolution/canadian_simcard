<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.inventory {
				margin-bottom: 15px;
			}
			.inventory .col-md-6 {
				padding: 0px;
			}
			.inventory .status_select{
				width: 28%;
    		float: right;
			}
			#example-1_filter {
				display: none;
			}
			.fa {
				padding: 5px;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}

			function bindStore($store, $stores) {
				$returnValue = 'NA';
				foreach($stores as $value) {
					if ($value['id'] === $store) {
						$returnValue = $value['name'];
					}
				}
				return $returnValue;
			}

			function bindStatus($statusId) {
				$status = (int)$statusId;
				return $status == 0 ? 'In Stock' : 'Sold';
			}
		?>
		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>SIM Inventory</h1>
						<a href="<?=BASEURL?>statement/stock"><i class="fa fa-arrow-left"></i>Back Stock Report</a>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div class="row inventory">
					<div class="col-md-12">
						<div class="col-md-6">
							<div>Store - <?= bindStore($shipment[0]['store_id'], $stores) ?></div>
						</div>
						<div class="col-md-6">
							<select name="status" id="status_select" class="status_select form-control">
								<option value="">All</option>
								<option value="Sold">Sold</option>
								<option value="In Stock">In Stock</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-body">
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								$("#example-1").dataTable({
									aLengthMenu: [
										[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
									]
								});
								$('#status_select').change( function() {
									filterGlobal(this.value);
								} );
							});
							function filterGlobal (filterstr) {
								$('#example-1').DataTable().search(
									filterstr
								).draw();
							}
						</script>
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">
							<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">
								<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Shipment Number</th>
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">SIM Number</th>
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Status</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($shipmentDetail as $value): ?>
									<tr class="odd gradeX">
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?= $value['shipment_id']?></td>
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?= $value['sim_number'] ?></td>
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?= bindCarrier($shipment[0]['carrier_id'], $carriers) ?></td>
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?= bindStatus($value['sold']) ?></td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php load_view('common/footer')?>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			
		</script>
	</body>
</html>
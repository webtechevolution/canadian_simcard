<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.notes-header {
				text-align: right;
				padding-right: 0px;
			}
			.fa {
				color: #fff;
				background: #000;
				padding: 1%;
				border-radius: 50%;
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}

			function bindStore($store, $stores) {
				$returnValue = 'NA';
				foreach($stores as $value) {
					if ($value['id'] === $store) {
						$returnValue = $value['name'];
					}
				}
				return $returnValue;
			}
		?>
		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Stock</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div class="notes-header">
					<i onClick="printTable()" class="fa fa-print"></i>	
					<i onClick="saveAsExcel()" class="fa fa-download"></i>	
					<!-- <i onClick="saveAsPdf()" class="fa fa-envelope-o"></i>	 -->
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								$("#example-1").dataTable({
									aLengthMenu: [
										[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
									]
								});
							});
						</script>
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">
							<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">
								<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>
									<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Quantity</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($stock as $value): ?>
									<tr class="odd gradeX">
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><a href="<?=BASEURL?>statement/siminventory/<?=$value['shipment_id']?>"><?= bindStore($value['store_id'], $stores)?></a></td>
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?= bindCarrier($value['carrier_id'], $carriers)?></td>
										<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'><?=($value['quantity_purchased'] - $value['quantity_sold'])?></td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php load_view('common/footer')?>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			var carriers = <?php echo json_encode($carriers); ?>;
			var stores = <?php echo json_encode($stores); ?>;
			
			function bindCarrier(carrierId) {
				returnValue = 'NA';
				carriers.forEach(function(value){
					if (value['id'] === carrierId) {
						returnValue = value['carrier'];
					}
				});
				return returnValue;
			}

			function bindStore(store) {
				returnValue = 'NA';
				stores.forEach(function(value){
					if (value['id'] === store) {
						returnValue = value['name'];
					}
				});
				return returnValue;
			}
			
			function printTable() {
				var stockResponse = [];
				stockResponse = <?php echo json_encode($stock); ?>;
				var table = '<table id="example-1" class="table table-striped table-bordered" cellspacing="0" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">' +
					'<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">' +
							'<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Quantity</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>';
					stockResponse.forEach(function(response){
					var tr_str = "<tr>" +
							"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindStore(response.store_id) + "</td>" +
							"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindCarrier(response.carrier_id) + "</td>" +
							"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response['quantity_purchased'] - response['quantity_sold']) + "</td>" +
							"</tr>";
					table += tr_str;
				});
				
				table += '</tbody>' +
				'</table>';
				var heading = "<div style='font-size: 20px; color: #000; text-align: center; margin-bottom: 20px; font-weight: bold;'>Stock Statement</div>"
				newWin = window.open("");
				newWin.document.write(heading + table);
				newWin.print();
				newWin.close();
			}

			function saveAsExcel() {
				var stockResponse = [];
				stockResponse = <?php echo json_encode($stock); ?>;
				var table = '<table id="example-1" class="table table-striped table-bordered" cellspacing="0" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">' +
					'<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">' +
							'<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>' +
								'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Quantity</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody>';
					stockResponse.forEach(function(response){
						var tr_str = "<tr>" +
								"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindStore(response.store_id) + "</td>" +
								"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindCarrier(response.carrier_id) + "</td>" +
								"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response['quantity_purchased'] - response['quantity_sold']) + "</td>" +
								"</tr>";
						table += tr_str;
					});
				
				table += '</tbody>' +
				'</table>';
				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE "); 

				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
						txtArea1.document.open("txt/html","replace");
						txtArea1.document.write(table);
						txtArea1.document.close();
						txtArea1.focus(); 
						sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
				} else {
					sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table));  
				}
				return (sa);
			}

			// function saveAsPdf() {
			// 	html2canvas(document.getElementById('example-1'), {
			// 		onrendered: function (canvas) {
			// 			var data = canvas.toDataURL();
			// 			var docDefinition = {
			// 				content: [{
			// 					image: data,
			// 					width: 500
			// 				}]
			// 			};
			// 			pdfMake.createPdf(docDefinition).download("Table.pdf");
			// 		}
			// 	});
			// }
		</script>
	</body>
</html>
<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.notes-header {
				text-align: right;
				padding-right: 0px;
			}
			.fa {
				color: #fff;
				background: #000;
				padding: 1%;
				border-radius: 50%;
				cursor: pointer;
			}
			#datePickerFrom, #datePickerTo {
				padding: 4.5px;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Sale</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div>
					<div>
						Period: <input type="text" value="From" id="datePickerFrom">
						<input type="text" value="To" id="datePickerTo">
						<button onClick="getData()" class="btn btn-success">Display</button>
					</div>
					<div id="actionContainer" class="notes-header">
						<i onClick="printTable()" class="fa fa-print"></i>	
						<i onClick="saveAsExcel()" class="fa fa-download"></i>	
						<!-- <i onClick="saveAsPdf()" class="fa fa-envelope-o"></i>	 -->
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								showButtons(false);
								$(function() {
									$("#datePickerFrom").datepicker();
									$("#datePickerTo").datepicker();
								});
							});
						</script>
						<div id="table_wrapper"></div>
					</div>
				</div>
			</div>
			<?php load_view('common/footer')?>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			var carriers = <?php echo json_encode($carriers); ?>;
			var stores = <?php echo json_encode($stores); ?>;
			var salesResponse = [];
			function showButtons(show) {
				var actionContainer = document.getElementById("actionContainer");
				if (show) {
					actionContainer.style.display = "block";
				} else {
					actionContainer.style.display = "none";
				}
			}

			function formatDate(date) {
				var d = new Date(date),
						month = '' + (d.getMonth() + 1),
						day = '' + d.getDate(),
						year = d.getFullYear();

				if (month.length < 2) 
						month = '0' + month;
				if (day.length < 2) 
						day = '0' + day;

				return [year, month, day].join('-');
			}

			function bindCarrier(carrierId) {
				returnValue = 'NA';
				carriers.forEach(function(value){
					if (value['id'] === carrierId) {
						returnValue = value['carrier'];
					}
				});
				return returnValue;
			}

			function bindStore(store) {
				returnValue = 'NA';
				stores.forEach(function(value){
					if (value['id'] === store) {
						returnValue = value['name'];
					}
				});
				return returnValue;
			}

			function getData() {
				var dateFrom = $('#datePickerFrom').val();
				var dateTo = $('#datePickerTo').val();
				var obj = {
					'dateFrom': formatDate(dateFrom),
					'dateTo': formatDate(dateTo)
				};
				salesResponse = [];
				$.post('<?=BASEURL?>statement/getFilterData', {data: obj}, function(data, status, jqXHR) {
					var newdata = data.split('[{'),
					updateddata = [], response = [];
					if (newdata && newdata.length >= 2) {
						updateddata = '[{' + newdata[1];
						response = JSON.parse(updateddata);
						salesResponse = response;
					}
					var table = '<table id="example-1" class="table table-striped table-bordered" cellspacing="0" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">' +
						'<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">' +
								'<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Client</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Seller</th>' + 
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Plan</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">SIM Number</th>' + 
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Activated Phone Number</th>' + 
									
								'</tr>' +
							'</thead>' +
							'<tbody>';
					if (response && response.length > 0) {
						for(var i=0; i < response.length; i++) {
							var tr_str = "<tr>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + response[i].full_name + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + response[i].Seller + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindStore(response[i].store) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindCarrier(response[i].carrier) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + response[i].title + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response[i].sim_number ? response[i].sim_number + '' + response[i].check_number : 'NA') + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response[i].number_activated_on ? response[i].number_activated_on : 'NA') + "</td>" +

									"</tr>";
							table += tr_str;
						}
						showButtons(true);
					} else {
						table += "<td colspan='5'>No record found</td>";
						showButtons(false);
					}
					table += '</tbody>' +
					'</table>';
					$("#table_wrapper").empty();
					$("#table_wrapper").append(table);
					$("#example-1").dataTable({
						aLengthMenu: [
							[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
						]
					});
        });
			}

			function printTable() {
				var table = '<table id="example-1" class="table table-striped table-bordered" cellspacing="0" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">' +
						'<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">' +
								'<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Plan</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">SIM Number</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Activated Phone Number</th>' + 
								'</tr>' +
							'</thead>' +
							'<tbody>';
				salesResponse.forEach(function(response){
					var tr_str = "<tr>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindStore(response.store) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindCarrier(response.carrier) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + response.title + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response.sim_number ? response.sim_number : 'NA') + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response.number_activated_on ? response.number_activated_on : 'NA') + "</td>" +
									"</tr>";
							table += tr_str;
				});
				
				table += '</tbody>' +
				'</table>';
				var heading = "<div style='font-size: 20px; color: #000; text-align: center; margin-bottom: 20px; font-weight: bold;'>Sale Statement</div>"
				newWin = window.open("");
				newWin.document.write(heading + table);
				newWin.print();
				newWin.close();
			}

			function saveAsExcel() {
				// var divToSave = document.getElementById("example-1");
				var table = '<table id="example-1" class="table table-striped table-bordered" cellspacing="0" style="border: 1px solid #ddd;width: 100%; max-width: 100%; margin-bottom: 20px;">' +
						'<thead style="display: table-header-group; vertical-align: middle; border-color: inherit;">' +
								'<tr style="display: table-row; vertical-align: inherit; border-color: inherit;">' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Store</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Carrier</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Plan</th>' +
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">SIM Number</th>' + 
									'<th style="border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;">Activated Phone Number</th>' + 
								'</tr>' +
							'</thead>' +
							'<tbody>';
				salesResponse.forEach(function(response){
					var tr_str = "<tr>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindStore(response.store) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + bindCarrier(response.carrier) + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + response.title + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response.sim_number ? response.sim_number : 'NA') + "</td>" +
									"<td style='border-bottom-width: 2px; border-top: 0; border: 1px solid #ddd;'>" + (response.number_activated_on ? response.number_activated_on : 'NA') + "</td>" +
									"</tr>";
							table += tr_str;
				});
				table += '</tbody>' +
				'</table>';
				var ua = window.navigator.userAgent;
				var msie = ua.indexOf("MSIE "); 

				if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
						txtArea1.document.open("txt/html","replace");
						txtArea1.document.write(table);
						txtArea1.document.close();
						txtArea1.focus(); 
						sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
				} else {
					sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table));  
				}
				return (sa);
			}

			// function saveAsPdf() {
			// 	html2canvas(document.getElementById('example-1'), {
			// 		onrendered: function (canvas) {
			// 			var data = canvas.toDataURL();
			// 			var docDefinition = {
			// 				content: [{
			// 					image: data,
			// 					width: 500
			// 				}]
			// 			};
			// 			pdfMake.createPdf(docDefinition).download("Table.pdf");
			// 		}
			// 	});
			// }
		</script> 
	</body>
</html>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.form_error {
				color: red;
				top: 5px;
				position: relative;
			}
			.fa-check {
				color: #04cc00;
			}
			.fa-times {
				color: #ff0000;
			}
			#is_sim {
				pointer-events: none;
			}
			#is_sim_1 {
				pointer-events: none;
			}
			.disable_row {
				pointer-events: none;
				opacity: 0.4;
			}
		</style>
	</head>
	<body class="wrap-nav-sidebar">
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}
		?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Order #<?=$order['order_number']?></h1>
						
						<?php if($order['other_orders']): ?>
							<?php foreach($order['other_orders'] as $key=>$_): ?>
								<?php if($order['order_number'] < $_): ?>
									<a href="<?=BASEURL?>orders/detail/<?=$_?>"><h5>Rejected order #<?=$_?></h5></a>
								<?php else: ?>
									<a href="<?=BASEURL?>orders/detail/<?=$_?>"><h5>Created from order #<?=$_?></h5></a>
								<?php endif; ?>
							<?php endforeach;?>
						<?php endif; ?>
						<!-- <a class="btn btn-success" href="<?=BASEURL?>orders/shipped/<?=$order['id']?>">Mark as Shipped</a> -->
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div class="panel panel-default">
					<div class="panel-body">
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Title</th>
									<?php if($order['shipped'] != 2): ?>
										<th>With SIM</th>
									<?php else: ?>
									<?php endif; ?>

									<th>SIM Card Number</th>
									<th>CSC Stock</th>
									
									<?php if($order['shipped'] >= 1): ?>
										<th>SIM Card Number by Client</th>
										<th>Match</th>
									<?php else: ?>
									<?php endif; ?>
									
									<th>Activated Phone Number</th>
									
									<?php if($order['shipped'] != 2): ?>
										<th>Carrier</th>
									<?php else: ?>
									<?php endif; ?>
									
									<th>Price</th>
									<th>Quantity</th>
									<th>Offer</th>
									
									<?php if($totalWithoutSim>0): ?>
									<th>Action</th>
									<?php endif; ?>
									

								</tr>
							</thead>

							<tbody>
							<?php foreach($items as $key=>$_): ?>
								<tr class="odd gradeX">
									<td><?=$_['title']?></td>
									
									<?php if($order['shipped'] != 2): ?>
										<td><input type="checkbox" id="is_sim" name="with_sim" value="1" <?php echo ($_['with_sim'] == '1' ? 'checked' : '');?>></td>
									<?php else: ?>
									<?php endif; ?>
									
									<td><?=$_['sim_number'] ? $_['sim_number'] . $_['check_number'] : 'NA' ?></td>
									<td><input type="checkbox" id="is_sim_1" name="with_sim_1" value="1" <?php echo ($_['csc_stock'] == '1' ? 'checked' : '');?>></td>
									
									<?php if($order['shipped'] >= 1): ?>
										<td><?=$_['sim_number_client'] ? $_['sim_number_client'] . $_['check_number'] : 'NA' ?></td>
										<td>
											<?php if($_['sim_number'] == $_['sim_number_client']): ?>
												<i class="fa fa-check"></i>
											<?php else: ?>
												<?php if($_['with_sim'] == '1'): ?>
													<i class="fa fa-times"></i>
												<?php else: ?>
													NA
												<?php endif; ?>
											<?php endif; ?>
										</td>
									<?php else: ?>
									<?php endif; ?>
									
									<td><?=$_['number_activated_on'] ? $_['number_activated_on'] : 'NA' ?></td>
									<?php if($order['shipped'] != 2): ?>
										<td><?=bindCarrier($_['carrier'], $carriers)?></td>
									<?php else: ?>
									<?php endif; ?>
									<td><?=$_['price']?></td>
									<td><?=$_['quantity']?></td>
									<td>
										<?php 
											if(($_['with_sim']==1) && ($_['suggest_mail_sent']==0)){
												echo "<span>NA</span>";}
											else if(($_['with_sim']==0) && ($_['suggest_mail_sent']==0)){
												echo "<span>Not Sent</span>";}
											else if(($_['with_sim']==0) && ($_['suggest_mail_sent']==1) && ($_['send_new_sim']==0)){
												echo "<span>Sent</span>";}
										 	// else if(($_['with_sim']==0) && ($_['suggest_mail_sent']==0)){
											// 	echo "<span></span>";}
										 	else if(($_['suggest_mail_sent']==1) && ($_['send_new_sim']==1)){
												echo "<span>Accept</span>";}
										 	else if(($_['suggest_mail_sent']==1) && ($_['send_new_sim']==2)){
												echo "<span>Reject</span>";}
										?>
										
									</td>
									<?php if($totalWithoutSim>0): ?>
										<td>
											<!-- <?php if($_['in_network']==0): ?> -->
												<?php if(!($_['with_sim'])): ?>
													<?php if(($_['send_new_sim']!=2)): ?>
														<a href="<?=BASEURL?>orders/send_new_sim?orderid=<?=$_['id'];?>&clientname=<?=$order['billing_full_name'];?>&email=<?=$order['email'];?>&oid=<?=$order['id']?>"  class="form-control btn btn-success">Suggest Alternate</a>
													<?php elseif(($hasRejectedSim != $totalWithoutSim) && ($_['send_new_sim']==2)) : ?>
														<a href="javascript: confirmRefund(<?=$order['id']?>, <?=$_['id']?>);" class="form-control btn btn-success">Refund Approved</a>
													<?php elseif($totalWithSim > 0 && ($_['send_new_sim']==2)) : ?>
														<a href="javascript: confirmRefund(<?=$order['id']?>, <?=$_['id']?>);" class="form-control btn btn-success">Refund Approved</a>
													<?php endif; ?>
												<?php endif; ?>
											<!-- <?php endif; ?> -->
										</td>
									<?php endif; ?>

									
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>

						<div class="row">
							<div class="col-sm-7">
								<div class="col-sm-7">
									<h3><?= $hasSim ? 'Shipping Address' : 'Billing Address'?></h3>
									<?=$order['full_name']?> <br>
									<?=$order['address']?> <br>
									<?=$order['city']?>, <?=$order['province']?> <br>
									<?=$order['zip']?> <br>
									<?=$order['country']?> <br><br>
									<?=$order['email']?>
								</div>
								<?php if($hasSim): ?>
									<div class="col-sm-5">
										<h3>Billing Address</h3>
										<?=$order['billing_full_name']?> <br>
										<?=$order['billing_address']?> <br>
										<?=$order['billing_city']?>, <?=$order['billing_province']?> <br>
										<?=$order['billing_zip']?> <br>
										<?=$order['billing_country']?> <br><br>
										<?=$order['email']?>
									</div>
									<?php else: ?>
									<?php endif; ?>
							</div>
							<div class="col-sm-5">
								<h3 style="text-align: right;">Payment</h3>
								<div align="right">

									SubTotal: <?=$order['sub_total']?> <br>
									One Time SIM Cost: <?=$totalWithSim > 0 ? sprintf("%.2f", $simPrice) : '0.00'?> <br>
									Discount(<?=$order['discount_code']?>): <?=($order['discount']!='') ? sprintf("%.2f", $order['discount']) : '0.00'?> <br>
									Shipping: <?=$hasSim ? sprintf("%.2f", $order['shipping']) : '0.00'?><br>
									Tax: <?=sprintf("%.2f", $order['tax'])?> <br>
									Total: <?=$order['total_price']?>
								</div>
							</div>
						</div>

						<br><br>
						<hr>

						
						<?php if(!$order['shipped'] && $hasSim): ?>
						<div class="row">
							<div class="col-sm-6" style="border: 1px solid #f1f1f1;">
								<h3>Additional Info</h3>
								<form id="addSimNumberForm" action="javascript:void(0)" method="post">
									<?php foreach($items as $key=>$_): ?>
										<?php if($_['with_sim'] == '1'): ?>
											<div class="col-sm-12">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h3 class="panel-title"><?=$_['title']?></h3>
													</div>
													<div class="panel-body">
														<div class="form-group">
															<?php if($_['offer']==1){
															?>
															<input class="form-control" type="text" id="sim_number_<?=$key?>" name="sim_number_<?=$key?>" value="" required maxlength="20" minlength="16" placeholder="SIM Number">
															<?php
															}
															else{
															?>
															<input class="form-control" type="text" id="sim_number_<?=$key?>" name="sim_number_<?=$key?>" value="<?=$_['sim_number']?>" required maxlength="20" minlength="16" placeholder="SIM Number">
															<?php
															}
															?>
															
															<input type="hidden" id="store_<?=$key?>" name="store_<?=$key?>">
															<input type="hidden" id="shipment_id_<?=$key?>" name="shipment_id_<?=$key?>">
															<span class="form_error" id="sim_number_error_<?=$key?>"></span>
															<!-- <input type="text" class="form-control" name="carrier" placeholder="Carrier"> -->
														</div>
													</div>
												</div>
											</div>
										<?php else: ?>
										<?php endif; ?>
									<?php endforeach;?>
									<div class="col-sm-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">Tracking Number</h3>
											</div>
											<div class="panel-body">
												<div class="form-group">
													<input type="text" class="form-control" name="tracking_number" placeholder="Tracking Number">
												</div>

											</div>
										</div>
									</div>
									<!-- <div class="col-sm-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">SIM Card Number</h3>
											</div>
											<div class="panel-body">
												<div class="form-group">
													<input type="text" class="form-control" name="sim_card" placeholder="SIM Card Number" maxlength="20" minlength="16">
												</div>
											</div>
										</div>
									</div> -->
									<div class="col-sm-12">
										<div class="panel panel-default">
											<div class="panel-body">
											<?php if(($totalWithSim > 0) && ($hasRejectedSim > 0)): ?>
												<div class="form-group disable_row">
													<input type="hidden" name="order_id" value="<?=$order['id']?>">
													<input type="submit" class="form-control btn btn-success" value="Mark as Shipped">
												</div>
											<?php else: ?>
												<div class="form-group">
													<input type="hidden" name="order_id" value="<?=$order['id']?>">
													<input type="submit" class="form-control btn btn-success" value="Mark as Shipped">
												</div>
											<?php endif; ?> 
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<?php else: ?>
						<div class="row">
							<div class="col-sm-6" style="border: 1px solid #f1f1f1;">
								<h3>Additional Info</h3>
								<!-- <div class="col-sm-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Carrier : <?=$order['carrier']?></h3>
										</div>
									</div>
								</div> -->
								<div class="col-sm-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">Tracking Number : <?=$order['tracking_number'] ? $order['tracking_number'] : 'NA'?></h3>
										</div>
									</div>
								</div>
								<!-- <div class="col-sm-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">SIM Card Number : <?=$order['bambora_id']?></h3>
										</div>
									</div>
								</div> -->
								
								<?php if(($order['shipped'] == 0 || $order['shipped'] == 1) && ($totalWithSim == 0) && ($totalWithoutSim > 0) && ($hasRejectedSim == $totalWithoutSim)): ?>
									<form id="markAsRejectedForm" action="<?=BASEURL?>orders/markOrderReject" method="post">
										<input type="submit" class="form-control btn btn-success" value="Refund Approved for all items">
										<input type="hidden" name="order_id" value="<?=$order['id']?>">
									</form>
								<?php elseif(($order['shipped'] == 2) && ($totalWithSim == 0) && ($totalWithoutSim > 0) && ($hasRejectedSim == $totalWithoutSim)): ?>
									<form id="markAsRefundedForm" action="<?=BASEURL?>orders/markOrderRefund" method="post">
										<input type="submit" class="form-control btn btn-success" value="Mark As Refunded">
										<input type="hidden" name="order_id" value="<?=$order['id']?>">
									</form>
								<?php elseif(($order['shipped'] == 4) || ($order['shipped'] == 2)): ?>
									<input type="submit" class="form-control btn btn-success disable_row" value="Refunded">
								<?php else: ?>
									<!--form action="<?=BASEURL?>orders/active" method="post"-->
									<form id="addPhoneNumberForm" action="javascript:void(0)" method="post">
										<?php if($order['shipped'] == 3): ?>
											<div class="col-sm-12">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h3 class="panel-title">Account Number : <?=$order['acct_number']?></h3>
													</div>
												</div>
											</div>
										<?php else: ?>
											<div class="col-sm-12">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h3 class="panel-title">Account Number</h3>
													</div>
													<div class="panel-body">
														<div class="form-group">
															<input type="text" class="form-control" name="acct_number" placeholder="Account Number">
														</div>
													</div>
												</div>
											</div>
											<?php foreach($items as $key=>$_): ?>
												<div class="col-sm-12">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h3 class="panel-title"><?=$_['title']?></h3>
														</div>
														<div class="panel-body">
															<div class="form-group">
																<input class="form-control" type="text" id="phone_number_<?=$key?>" name="phone_number_<?=$key?>" required placeholder="Phone Number" maxlength="11">
																<input type="hidden" id="sim_number_<?=$key?>" name="sim_number_<?=$key?>" value="<?=$_['activate_sim_number']?>">
																<input type="hidden" id="shipment_id_<?=$key?>" name="shipment_id_<?=$key?>" value="<?=$_['shipment_id']?>">
																<span class="form_error" id="phone_number_error_<?=$key?>"></span>
															</div>
														</div>
													</div>
												</div>
											<?php endforeach;?>

											<input type="submit" class="form-control btn btn-success" value="Activate">

										<?php endif; ?>
										<input type="hidden" name="order_id" value="<?=$order['id']?>">
									</form>
								<?php endif; ?>
							</div>

						</div>
						<?php endif; ?>

					</div>
				</div>

			</div>
			<?php load_view('common/footer')?>
		</div>


		<div class="modal invert fade" id="modal">
			<div class="modal-dialog">
				<div class="modal-content">

					<form role="form" method="post" action="<?=BASEURL?>orders/markOrderItemRefund">

						<input type="hidden" name="orderid" value="" id="orderid">
						<input type="hidden" name="itemid" value="" id="itemid">

						<div class="modal-header">
							<button type="button" class="close btn btn-lg" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Refund Order - Item</h4>
						</div>

						<div class="modal-body">
							<p>A new order with status rejected will be created and the rejected item will be removed from this order, do you want to continue?</p>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">No</button>
							<button type="submit" class="btn btn-danger btn-lg">Yes</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">

			function confirmRefund(orderid, itemid){
				$('#orderid').val(orderid);
				$('#itemid').val(itemid);
				$("#modal").modal('show');
			}
      
			$(document).ready(function(){
				var orderitems = <?php echo json_encode($items); ?>;
				// console.log(orderitems);

				function checkSimNumber (arr, value, index) {
          var areSimSame = false;
          for (var i=0; i<arr.length; i++) {
            if (i != index) {
              if (arr[i] == value) {
                areSimSame = true;
              }
            }
          }
          return areSimSame;
        }
		
				$('#addPhoneNumberForm').submit(function(event) {
					var counter = 0,
						phoneNumberArray = [], arePhoneSame = false;
					orderitems.forEach(function(data, index) {
						//if (data.with_sim === '1') {
						var phone_number = $("#phone_number_" + index).val();
						phoneNumberArray.push(phone_number.replace(/\s+/g, '').toString());
						//}
					});

					phoneNumberArray.forEach(function(data, index) {
						if (checkSimNumber(phoneNumberArray, data, index)) {
							arePhoneSame = true;
							counter++;
							$("#phone_number_error_"+index).text('Same Phone number cannot be used for more than one plans').show();
						} else {
							$("#phone_number_error_"+index).text('').hide();
						}
					});
					if (counter === 0) {
						//alert('addPhoneNumberForm');
						$('#addPhoneNumberForm').attr('action', '<?=BASEURL?>orders/active');
					} else {
						event.preventDefault();
					}
				});
				
        $('#addSimNumberForm').submit(function(event) {
          var counter = 0,
						hasError = false,
						simNumberArray = [], areSimSame = false;
						
					orderitems.forEach(function(data, index) {
						if (data.with_sim === '1') {
							var sim_number = $("#sim_number_" + index).val();
							simNumberArray.push(sim_number.replace(/\s+/g, '').toString());
						}
					});
					// console.log(orderitems);
					// console.log(simNumberArray);
					simNumberArray.forEach(function(data, index) {
            if (checkSimNumber(simNumberArray, data, index)) {
              areSimSame = true;
              counter++;
              $("#sim_number_error_"+index).text('Same SIM number cannot be used for more than one plans').show();
            } else {
              $("#sim_number_error_"+index).text('').hide();
            }
					});
					
					if (!areSimSame) {
						orderitems.forEach(function(data, index) {
							if (data.with_sim === '1') {
								hasError = false;
								$("#sim_number_error_" + index).text('').hide();
								var sim_number = $("#sim_number_" + index).val();
								sim_number = sim_number.replace(/\s+/g, '');
								// var truncated_sim_number = sim_number.substring(0, sim_number.length-1);
								var simByCarrier = getSimByCarrier(data['carrier']);
								// console.log(simByCarrier);
								var isSimNotAvailable = true;
								// if (!hasError) {
								/*if(!validateSIM(sim_number)) {
									counter++;
									hasError = true;
									$("#sim_number_error_"+index).text('SIM Card Number is not valid').show();
								}*/
								if(!hasError) {
									simByCarrier.forEach(function(simdata){
										simdata.shipment_detail.forEach(function(shipDetail){
											if (isSimNotAvailable) {
												if(BigInt(sim_number) == BigInt(shipDetail['sim_number'])) {
													$("#store_"+index).val(simdata['store_id']);
													$("#shipment_id_"+index).val(simdata['shipment_id']);
													isSimNotAvailable = false;
												}
											}
										});
									});
									if (isSimNotAvailable) {
										counter++;
										$("#sim_number_error_"+index).text('SIM Card Number is not valid, Please enter SIM Card Number purchased from the store').show();
									}
								}
								// }
							}
						});
					}
          if (counter === 0) {
            // alert('orderitems');
            $('#addSimNumberForm').attr('action', '<?=BASEURL?>orders/shipped');
          } else {
            event.preventDefault();
          }
				});
				
				function validateSIM(simnum){
					var check = simnum.toString().slice(-1);
					var simnumber =  simnum.toString().substring(0, simnum.length -1);
					var singleDigit = 0;
					var result = 0;
					var multiply = 0;
					var sum = 0;
					var splitsum =0;
					for (var i = 0; i < simnumber.length; i++) {
						singleDigit = parseInt(simnumber.charAt(i));

						if( i % 2 == 0){
							multiply  = singleDigit * 2;
							if( multiply > 9){
								splitsum = 0;
								multiply = multiply.toString();
								for (var i1 = 0; i1 < 2; i1++){
									splitsum += parseInt(multiply.charAt(i1));
								}
								sum += splitsum
							} else{
								sum +=  parseInt(multiply);
							}
						} else {
						sum += singleDigit;
						}
					}
					sum = sum * 9;
					var result1 = sum % 10;
					if(parseInt(result1) == parseInt(check)){
						return true;
					}
					return false;
				}

				function getSimByCarrier(carrierId) {
					var shipment = <?php echo json_encode($shipment); ?>;
					var shipment_detail = <?php echo json_encode($shipment_detail); ?>;
          var simByCarrier = [], shipmentDetail = [];
          shipment.forEach(function(data) {
						var simData = {};
            if (data['carrier_id'] == carrierId) {
              simData = {
                'shipment_id': data['shipment_id'],
                'store_id': data['store_id'],
                'carrier_id': data['carrier_id'],
                'quantity': data['quantity'],
                'date_shipped': data['date_shipped'],
                'shipment_detail': []
              };
              shipment_detail.forEach(function(sdata) {
                if (sdata['shipment_id'] == data['shipment_id']) {
                  simData.shipment_detail.push(sdata);
                }
              });
              simByCarrier.push(simData);
            }
						// if (data['carrier_id'] == carrierId) {
            //   simByCarrier.push(data);
            // }
          });
          return simByCarrier;
        }
      });
    </script>
	</body>
</html>

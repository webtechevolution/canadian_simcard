<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>

	<?php load_view('common/css')?>
	<style>
		.dataTables_length {
			display: inline-block;
		}
		.table-filter-container {
			text-align: right;
			display: inline-block;
			float: right;
		}
		.table-filter-container select {
			height: 30px;
			line-height: 30px;
			padding: 5px 10px;
			font-size: 12px;
			border-radius: 3px;
			border: 1px solid #ccc;
		}
	</style>
</head>
<body>
<?php load_view('common/header')?>
<?php load_view('common/nav')?>



<div class="main-content">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Orders</h1>
			</div>
		</div>
	</div>
	<hr>

	<div class="container">


		<!-- Basic Setup -->
		<div class="panel panel-default">

			<div class="panel-body">

			<script type="text/javascript">
				jQuery(document).ready(function($)
				{
					var table = $("#example-1").DataTable({
						aLengthMenu: [
							[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
						],
						order: [ 0, 'desc' ],
						searching: true,
						dom: 'lr<"table-filter-container">tip',
						initComplete: function(settings){
							var api = new $.fn.dataTable.Api( settings );
							$('.table-filter-container', api.table().container()).append(
								$('#table-filter').detach().show()
							);
							
							$('#table-filter select').on('change', function(){
								table.search(this.value).draw();   
							});       
						}
					});
				});
			</script>
				<p id="table-filter" style="display:none">
					Search 
					<select>
						<option value="">All</option>
						<option>Pending Activation</option>
						<option>Pending Shipping</option>
						<option>Rejected</option>
						<option>Refunded</option>
						<option>Completed</option>
					</select>
				</p>

				<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Order Id</th>
							<th>FullName</th>
							<th>Email</th>
							<th>Amount</th>
							<th>Date</th>
							<th>Status</th>
						</tr>
					</thead>

					<tbody>
					<?php foreach($orders as $_): 
						$status = 'Pending Activation';
						if($_['shipped'] == 0 && $_['with_sim'] == 1) $status = 'Pending Shipping';
						if($_['shipped'] == 1) $status = 'Pending Activation';
						if($_['shipped'] == 2) $status = 'Rejected';
						if($_['shipped'] == 3) $status = 'Completed';
						if($_['shipped'] == 4) $status = 'Refunded';
						?>
						<tr class="odd gradeX">
							<td><a href="<?=BASEURL?>orders/detail/<?=$_['order_number']?>"><?=$_['order_number']?></a></td>
							<td><?=$_['full_name']?></td>
							<td><?=$_['email']?></td>
							<td>$<?=$_['total_price']?></td>
							<td><?=$_['updated']?></td>
							<!--<td style="background-color: <?=$status?>"></td>-->
							<td><?=$status?></td>
						</tr>

					<?php endforeach;?>
					</tbody>


				</table>

			</div>
		</div>

	</div>
	<?php load_view('common/footer')?>
</div>

<div class="modal invert fade" id="modal">
	<div class="modal-dialog">
		<div class="modal-content">

			<form role="form" method="post" action="<?=BASEURL?>plans/deleteplan">

				<input type="hidden" name="id" value="" id="id">

				<div class="modal-header">
					<button type="button" class="close btn btn-lg" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Delete Item</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete <span style="color:red" id="name"></span> item?</p>
				</div>

				

				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">No</button>
					<button type="submit" class="btn btn-danger btn-lg">Yes</button>
				</div>
			</form>
		</div>
	</div>
</div>


<?php load_view('common/js')?>

<script type="text/javascript">

	function deleteitem(id, name){

		$('#id').val(id);
		$('#name').html(name);
		$("#modal").modal('show');
	}
</script> 
</body>
</html>
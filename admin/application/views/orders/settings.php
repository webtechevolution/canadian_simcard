<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.panel-title-extra {
				font-size: 16px;
				padding: 6px 12px;
				margin-bottom: 0px;
				font-weight: 400;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						
						<h1>Settings - Edit</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" id="category" action="<?=BASEURL?>orders/editsettings_action">
					<input type="hidden" name="id" value="<?=$_['id']?>">
					<div class="row">
						

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">SIM card cost for plans without SIM</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="sim_cost" placeholder="New SIM Cost" value="<?=$_['sim_cost']?>">
								</div>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Suggest alternate message</h3>
								</div>
								<div class="panel-body">
									<textarea rows="10" name="new_sim_content" placeholder="Email Content" class="form-control"><?=$_['new_sim_content']?></textarea>
									<!--<input type="text" class="form-control" name="new_sim_content" placeholder="Email Content" value="<?=$_['new_sim_content']?>">-->
								</div>
							</div>
						</div>

					



					</div>
					<hr>
					
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" value="Save" class="btn btn-success btn-block">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>

		<script type="text/javascript">
			jQuery(document).ready(function(){});
		</script>
	</body>
</html>
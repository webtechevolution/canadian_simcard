<!DOCTYPE html>
<html>
<head>
	<title>Admin Panel</title>

	<?php load_view('common/css')?>

</head>
<body>
<?php load_view('common/header')?>
<?php load_view('common/nav')?>



<div class="main-content">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
				<h1>Discount Code - Add</h1>
			</div>
		</div>
	</div>
	<hr>

	<div class="container">


		<form method="post" role="form" id="category" action="<?=BASEURL?>discount/addcode_action">
			<div class="row">

				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Name</h3>
						</div>
						<div class="panel-body">
							<input type="text" class="form-control" name="name" placeholder="Name">
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Type</h3>
						</div>
						<div class="panel-body">
							<select name="type" class="form-control">
								<option value="1">Percentage</option>
								<option value="2">Value</option>
							</select>
						</div>
					</div>
				</div>

			</div>


			<div class="row">
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Discount Code</h3>
						</div>
						<div class="panel-body">
							<input type="text" class="form-control" name="code" placeholder="Discount Code">
						</div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Discount Value</h3>
						</div>
						<div class="panel-body">
							<input type="text" class="form-control" name="value" placeholder="Discount Value">
						</div>
					</div>
				</div>

				
			</div>

			<div class="row">

				<div class="col-sm-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<input type="submit" value="Save" class="btn btn-success btn-block">
						</div>
					</div>
				</div>
			</div>
		</form>

		<div class="clear"></div>

	</div>

</div>


<?php load_view('common/js')?>



<script type="text/javascript">
jQuery(document).ready(function(){

	

});
</script>



</body>
</html>
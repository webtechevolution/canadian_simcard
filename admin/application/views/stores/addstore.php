<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>
		<style>
			.form_error {
				color: red;
				top: 5px;
				position: relative;
			}
			.hidden_class {
				padding: 0px;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>New Stores</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" id="addStoreForm" action="javascript:void(0)">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Store Name</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" name="store_name" placeholder="Store Name" required>
									<span class="form_error" id="store_name_error"></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Street Address</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="address" name="address" placeholder="Street Address" required>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">City</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="city" name="city" placeholder="City" required maxlength="50">
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Province</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="province" name="province" placeholder="Province" required maxlength="50">
								</div>
							</div>
						</div>
						<div class="col-sm-12 hidden_class">
							<div class="col-sm-6">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Postal Code</h3>
									</div>
									<div class="panel-body">
										<input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="Postal Code" required maxlength="7">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Contact Name</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name" required>
									<span class="form_error" id="contact_name_error"></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Contact Email</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="email" name="email" placeholder="Contact Email" required maxlength="30">
									<span class="form_error" id="email_error"></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Contact Number</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Number" required maxlength="11">
									<span class="form_error" id="contact_number_error"></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" name="submit" value="Save" class="btn btn-success">
									<a href='<?=BASEURL?>stores' class="btn btn-info">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			jQuery(document).ready(function(){});

			function validateEmail(email) {
				const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(String(email).toLowerCase());
			}

			$(document).ready(function(){
				$('#addStoreForm').submit(function(event) {
					var counter = 0;
					var phoneNumber = $("#contact_number").val();
					if (/^\d{10}$/.test(phoneNumber) || /^\d{11}$/.test(phoneNumber)) {
						$("#contact_number_error").text('').hide();
					} else {
						counter++;
						$("#contact_number_error").text("Contact number must be 10-11 characters long").show();
					}

					// if ($("#store_name").val() === null || $("#store_name").val() === undefined) {
					// 	$("#store_name_error" ).text("Store Name is required").show();
					// 	counter++;
					// } else {
					// 	$("#store_name_error").text('').hide();
					// }

					if (validateEmail($("#email").val())) {
						$("#email_error").text('').hide();
					} else {
						$("#email_error" ).text("Invalid Email").show();
						counter++;
					}

					if (counter === 0) {
						$('#addStoreForm').attr('action', '<?=BASEURL?>stores/addstore_action');
					} else {
						event.preventDefault();
					}
				});

				var autocomplete = new google.maps.places.Autocomplete($("#city")[0], {});
				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					var selectedAddress = place.address_components;
					// console.log(selectedAddress);
					var address = '', city ='', province ='', postal_code = '';
					for (var i = 0; i < selectedAddress.length; i++) {
						var element = selectedAddress[i];
						//selectedAddress.forEach(element => {

							// if (element.types[0] === 'route' || element.types[0] === "sublocality_level_2") {
							// 	address = element.long_name + ' ';    
							// }  
							// if (element.types[0] === "neighborhood" || element.types[0] === "sublocality_level_1") {
							// 	address += element.long_name;    
							// }  
							if (element.types[0] === "locality") {
								city = selectedAddress[i].long_name;
							}  
							if (element.types[0] === "administrative_area_level_1") {
								province = element.long_name;
							}  
							// if (element.types[0] === "country") {
							// 	country = element.short_name;
							// }  
							if (element.types[0] === "postal_code") {
								postal_code = element.long_name;
							}  

						//});
						}
					// $('#address').val(address);
					$('#city').val(city);
					$('#province').val(province);
					// $('#country').val(country);
					$('#postal_code').val(postal_code);
				});
			});
		</script>
	</body>
</html>
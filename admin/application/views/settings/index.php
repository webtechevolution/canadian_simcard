<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.inline_grid {
				display: flex;
				width: 100%;
			}
			.inline_grid_row:before {
				content: none;
			}
			.inline_grid_row {
				width: 49%;
				display: grid;
				margin-right: 0px;
				margin-left: 0px;
			}	
			.inline_grid_row:first-child {
				margin-right: 2%;
			}
			.col-sm-12 {
				padding: 0px;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Settings</h1>
					</div>
				</div>
			</div>
			<hr>

			<div class="container">
				<form method="post" role="form" id="category" action="<?=BASEURL?>settings/apply">
					<div class="inline_grid">
						<div class="row inline_grid_row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Shipping Price</h3>
									</div>
									<div class="panel-body">
										<input type="text" class="form-control" name="value" value="<?=$price?>" placeholder="Shipping Price">
									</div>
								</div>
							</div>

							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">SIM Price</h3>
									</div>
									<div class="panel-body">
										<input type="text" class="form-control" name="sim_price" value="<?=$sim_price?>" placeholder="SIM Price">
									</div>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">SIM card cost for plans without SIM</h3>
									</div>
									<div class="panel-body">
										<input type="text" class="form-control" name="sim_cost" value="<?=$sim_cost?>" placeholder="New SIM Cost">
									</div>
								</div>
							</div>
						</div>

						<div class="row inline_grid_row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title">Suggest alternate message</h3>
									</div>
									<div class="panel-body">
										<textarea rows="14" name="new_sim_content" placeholder="Email Content" class="form-control"><?=$new_sim_content?></textarea>
										<!--<input type="text" class="form-control" name="new_sim_content" placeholder="Email Content" value="<?=$_['new_sim_content']?>">-->
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" value="Save" class="btn btn-success btn-block">
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>

		<script type="text/javascript">
			jQuery(document).ready(function(){});
		</script>

	</body>
</html>
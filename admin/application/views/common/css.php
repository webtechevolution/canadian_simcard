

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<!-- pdf files loaded -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.22/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>

<!-- calender jquery files loaded -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700|Raleway:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

<link href="<?=BASEURL?>public/css/animate.css" rel="stylesheet">

<link href="<?=BASEURL?>public/css/bootstrap.min.css" rel="stylesheet">

<link href="<?=BASEURL?>public/css/bootsnav.css" rel="stylesheet">
<link href="<?=BASEURL?>public/css/style.css" rel="stylesheet">

<link href="<?=BASEURL?>public/css/multi-select.dist.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

<!-- calender styling files loaded -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->


<style type="text/css">
/*.main-content{
	margin-left: 200px;
}*/
footer {
	/*position: absolute;*/
	padding: 2rem;
	border-top: 1px solid #DEE1EC;
	/*background: #fff;*/
	height: 70px;
	bottom: 0;
	width: 100%;
	transition: margin-left 300ms ease;
}
.notes-header{
	padding: 20px;
}
nav.bootsnav.navbar-sidebar ul.nav > li > ul > li > a{
	color: #ef4128;
}
.pow_by{
	position: fixed;
	right: 5%;
	bottom: 5%;
}
.pow_by{
	font-size: 20px;
}
.pow_by img{
	display: inline-block;
	margin-left: 20px;
	vertical-align: middle;
	max-width: 150px;
}
</style>

<nav class="navbar navbar-default navbar-sidebar bootsnav"><div class="scroller" style="height: 856px;">
		<div class="container">

			<!-- Start Header Navigation -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="#brand"><img src="<?=BASEURL?>public/images/logo.png" class="logo" alt="logo" width="100%" ></a>
			</div>
			<!-- End Header Navigation -->
			<br><br><br>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-menu">
				<!-- Start Menu -->
				<ul class="nav navbar-nav" data-in="fadeInDown" data-out="fadeOutUp">
					<li><a href="<?=BASEURL?>">Dashboard</a></li>
					<!-- <li><a href="<?=BASEURL?>orders/settings">Settings</a></li> -->
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Orders</a>
						<ul>
							<li><a href="<?=BASEURL?>orders">Orders</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Plans</a>
						<ul>
							<li><a href="<?=BASEURL?>plans">Plans</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Stores</a>
						<ul>
							<li><a href="<?=BASEURL?>stores">Stores</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Shipments</a>
						<ul>
							<li><a href="<?=BASEURL?>shipments">Shipments</a></li>
							<li><a href="<?=BASEURL?>shipments/status">SIM Number Status</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Statement</a>
						<ul>
							<li><a href="<?=BASEURL?>statement/stock">Stock</a></li>
							<li><a href="<?=BASEURL?>statement/sales">Sales</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Summary</a>
						<ul>
							<li><a href="<?=BASEURL?>summary/sales">Sales</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Carriers</a>
						<ul>
							<li><a href="<?=BASEURL?>carriers">Carriers</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">FAQ</a>
						<ul>
							<li><a href="<?=BASEURL?>faq/category">Category</a></li>
							<li><a href="<?=BASEURL?>faq/subcategory">Sub Category</a></li>
							<li><a href="<?=BASEURL?>faq/article">Question/Answers</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
						<ul>
							<li><a href="<?=BASEURL?>blog/category">Category</a></li>
							<li><a href="<?=BASEURL?>blog/article">Article</a></li>

						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Clients</a>
						<ul>
							<li><a href="<?=BASEURL?>client">Clients</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Others</a>
						<ul>
							<li><a href="<?=BASEURL?>discount">Discount Code</a></li>
							<li><a href="<?=BASEURL?>settings">Settings</a></li>
							<li><a href="<?=BASEURL?>tax">Tax</a></li>
						</ul>
					</li>
					<li><a href="<?=BASEURL?>media">File Manager</a></li>

					<li><a href="<?=BASEURL?>login/logout"><i class="fa fa-sign-out"></i> Log Out</a></li>
				</ul>
				<!-- End Menu -->
			</div><!-- /.navbar-collapse -->
		</div>
	</div>
</nav>

<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Tax - Add</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" role="form" action="<?=BASEURL?>tax/addtax_action">
					<div class="row">
						<!-- General Info -->
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Province Name</h3>
								</div>
								<div class="panel-body">
									<div class="form-group">
										<input type="text" class="form-control" name="province" placeholder="Enter a Province Name">
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Tax (in %)</h3>
								</div>
								<div class="panel-body">
									<div class="form-group">
										<input type="text" class="form-control" name="tax" placeholder="Enter a Tax">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" value="Save" class="btn btn-success btn-block">
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<?php load_view('common/footer')?>
		</div>
		<?php load_view('common/js')?>
	</body>
</html>
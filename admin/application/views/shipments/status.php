<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>
		<style>
			.form_error {
				color: red;
				padding-left: 10px;
				position: relative;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}

			function bindStore($store, $stores) {
				$returnValue = 'NA';
				foreach($stores as $value) {
					if ($value['id'] === $store) {
						$returnValue = $value['name'];
					}
				}
				return $returnValue;
			}
		?>
		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>SIM Number Status</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div class="panel panel-default">
					<div class="panel-body">
						<div>
							<span>Enter SIM Number</span>
							<input style="width: 30%; margin: 0px 10px;" type="text" name="sim_number" id="sim_number" required>
							<button onClick="checkSimNumber()" class="btn btn-success">Search</button>
							<span class="form_error" id="sim_number_error"></span>
						</div>
						<div id="table_wrapper"></div>
					</div>
				</div>
			</div>
			<?php load_view('common/footer')?>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			function checkSimNumber() {
				var simNumber = $('#sim_number').val();
				if (simNumber) {
					$("#sim_number_error").text('').hide();
					var obj = {
					'simNumber': simNumber.toString()
					};
					salesResponse = [];
					$.post('<?=BASEURL?>shipments/checkSimNumber', {data: obj}, function(data, status, jqXHR) {
						var newdata = data.split('[{'),
						updateddata = [], response = [];
						$('#sim_number').val('');
						if (newdata && newdata.length >= 2) {
							updateddata = '[{' + newdata[1];
							response = JSON.parse(updateddata);
							salesResponse = response;
						}
						console.log(response);
						var table = '', storeData = '';
						if (response && response.length > 0) {
							// for(var i=0; i < 1; i++) {
								var status = parseInt(response[0].sold);
								var stock = '';
								if (status == 0 || status == 1) {
									storeData = '<div class="col-sm-12" style="padding: 10px 0px;">\
										<div class="col-sm-6" style="padding: 0px;">\
											<div class="heading">\
												<span class="panel-title" style="font-weight: 500;">Store : </span>\
												<span class="panel-label">'+ response[0].store +'</span>\
											</div>\
										</div>\
										<div class="col-sm-6" style="padding: 0px;">\
											<div class="heading">\
												<span class="panel-title" style="font-weight: 500;">Activated Phone Number : </span>\
												<span class="panel-label">'+ response[0].number_activated_on +'</span>\
											</div>\
										</div>\
									</div>';
								}
								if (status == 0) {
									stock = 'In Stock';
								} else if (status == 1) {
									stock = 'Sold';
								} else {
									stock = 'Not in Stock';
								}
								table += '<div class="col-sm-12" style="padding: 10px 0px;">\
									<div class="col-sm-6" style="padding: 0px;">\
										<div class="heading">\
											<span class="panel-title" style="font-weight: 500;">SIM Number : </span>\
											<span class="panel-label">'+ response[0].sim_number +'</span>\
										</div>\
									</div>\
									<div class="col-sm-6" style="padding: 0px;">\
										<div class="heading">\
											<span class="panel-title" style="font-weight: 500;">Status : </span>\
											<span class="panel-label">' + stock + '</span>\
										</div>\
									</div>\
								</div>\
								' + storeData + ' ';
							// }
						}
						$("#table_wrapper").empty();
						$("#table_wrapper").append(table);
					});
				} else {
					$("#sim_number_error").text("SIM Number is mandatory").show();
				}
			}
		</script>
	</body>
</html>
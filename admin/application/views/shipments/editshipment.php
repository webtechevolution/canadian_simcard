<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>
		<style>
			.form_error {
				color: red;
				top: 5px;
				position: relative;
			}
			.panel-body-disabled {
				pointer-events: none;
				opacity: 1;
			}
			.panel-body-disabled input {
				background: #eee;
			}
			.heading {
				margin: 5px 0px 5px 0px;
			}
			.heading .panel-title{
				font-weight: 500;
			}
			.heading .panel-label{
				font-size: 16px;
			}
			.extra-padding {
				text-align: right;
				padding-right: 0;
				padding-bottom: 15px;
			}
			.addSimForm .panel {
				margin: 20px;
			}
			.fa {
				padding: 5px;
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}

			function bindStore($store, $stores) {
				$returnValue = 'NA';
				foreach($stores as $value) {
					if ($value['id'] === $store) {
						$returnValue = $value['name'];
					}
				}
				return $returnValue;
			}
		?>
		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Add/Edit SIM Numbers</h1>
						<a href="<?=BASEURL?>shipments"><i class="fa fa-arrow-left"></i>Back Shipments</a>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<input type="hidden" name="shipment_id" value="<?=$_['shipment_id']?>">
				<div class="row">
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="heading">
								<span class="panel-title">Shipment Number : </span>
								<span class="panel-label"><?=$_['shipment_id']?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="heading">
								<span class="panel-title">Store : </span>
								<span class="panel-label"><?=bindStore($_['store_id'], $stores)?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="heading">
								<span class="panel-title">Carrier : </span>
								<span class="panel-label"><?=bindCarrier($_['carrier_id'], $carriers)?></span>
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="col-sm-6">
							<div class="heading">
								<span class="panel-title">Date Shipped : </span>
								<span class="panel-label"><?=$_['date_shipped']?></span>
							</div>
						</div>
						<div class="col-sm-6 extra-padding">
							<a href="javascript: addFromFile(<?=$_['shipment_id']?>);" class="btn btn-danger btn-sm btn-icon icon-left">
								Add from Excel Sheet
							</a>
							<a href="javascript: addManually(<?=$_['shipment_id']?>);" class="btn btn-danger btn-sm btn-icon icon-left">
								Add New SIM Manually
							</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-body">
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								$("#example-1").dataTable({
									aLengthMenu: [
										[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
									]
								});
							});
						</script>
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>SIM Number</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($shipment_detail as $value): ?>
									<tr class="odd gradeX">
										<td><?=$value['sim_number']?></td>
										<td>
											<a href="javascript: editManually(<?=$value['id']?>, <?=$value['shipment_id']?>, '<?=$value['sim_number']?>');" class="btn btn-danger btn-sm btn-icon icon-left">
												Edit
											</a>
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal invert fade addSimForm" id="addManually">
			<div class="modal-dialog">
				<div class="modal-content">
					<form role="form" method="post" id="addManuallyForm" action="javascript:void(0)">
						<input type="hidden" name="shipment_id" value="" id="shipment_id">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">New SIM Number</h3>
							</div>
							<div class="panel-body">
								<input type="text" class="form-control" id="sim_card_number" name="sim_card_number" placeholder="New SIM Number" required minlength="16" maxlength="20">
								<span class="form_error" id="sim_card_number_error"></span>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-success">Add</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal invert fade addSimForm" id="editManually">
			<div class="modal-dialog">
				<div class="modal-content">
					<form role="form" method="post" id="editManuallyForm" action="javascript:void(0)">
						<input type="hidden" name="detail_shipment_id" value="" id="detail_shipment_id">
						<input type="hidden" name="detail_id" value="" id="detail_id">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Edit SIM Number</h3>
							</div>
							<div class="panel-body">
								<input type="text" class="form-control" id="edit_sim_card_number" name="edit_sim_card_number" placeholder="Edit SIM Number" required minlength="16" maxlength="20">
								<span class="form_error" id="edit_sim_card_number_error"></span>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-success">Update</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal invert fade addSimForm" id="addFromFile">
			<div class="modal-dialog">
				<div class="modal-content">
					<form role="form" method="post" enctype="multipart/form-data" id="addFromFileForm" action="javascript:void(0)">
						<input type="hidden" name="file_shipment_id" value="" id="file_shipment_id">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Import SIM Number</h3>
							</div>
							<div class="panel-body">
								<input type="file" accept=".xlsx" class="form-control" id="file" name="file" placeholder="Upload Excel File" required>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-success">Import</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			$(document).ready(function(){
				var addedSims = <?php echo json_encode($addedSims); ?>;
				var simsAdded = [];
				addedSims.forEach(function(key) {
					simsAdded.push(key['sim_number']);
				});

				$('#addManuallyForm').submit(function(event) {
					var counter = 0;
					var sim_card_number = $("#sim_card_number").val();
					
					if (isNaN(sim_card_number)) {
						counter++;
						$("#sim_card_number_error").text("SIM card must contain numbers").show();
					}

					if (!validateSIM(sim_card_number)) {
						counter++;
						$("#sim_card_number_error").text("SIM number is not valid").show();
					}

					if (simsAdded.includes(sim_card_number)) {
						counter++;
						$("#sim_card_number_error").text("SIM number is already registered").show();
					}

					if (counter === 0) {
						$("#sim_card_number_error").text('').hide();	
						// alert('addManuallyForm');
						$('#addManuallyForm').attr('action', '<?=BASEURL?>shipments/addSimManually');
					} else {
						event.preventDefault();
					}
				});

				$('#addFromFileForm').submit(function(event) {
					$('#addFromFileForm').attr('action', '<?=BASEURL?>shipments/addFromFile');
				});

				$('#editManuallyForm').submit(function(event) {
					var counter = 0;
					var sim_card_number = $("#edit_sim_card_number").val();
					
					if (isNaN(sim_card_number)) {
						counter++;
						$("#edit_sim_card_number_error").text("SIM card must contain numbers").show();
					} else {
						$("#edit_sim_card_number_error").text('').hide();
					}

					if (!validateSIM(sim_card_number)) {
						counter++;
						$("#edit_sim_card_number_error").text("SIM number is not valid").show();
					} else {
						$("#edit_sim_card_number_error").text("").hide();
					}

					if (counter === 0) {
						$('#editManuallyForm').attr('action', '<?=BASEURL?>shipments/editSimManually');
					} else {
						event.preventDefault();
					}
				});
			});

			function validateSIM(simnum){
				var check = simnum.toString().slice(-1);
				var simnumber =  simnum.toString().substring(0, simnum.length -1);
				var singleDigit = 0;
				var result = 0;
				var multiply = 0;
				var sum = 0;
				var splitsum =0;
				for (var i = 0; i < simnumber.length; i++) {
					singleDigit = parseInt(simnumber.charAt(i));

					if( i % 2 == 0){
						multiply  = singleDigit * 2;
						if( multiply > 9){
							splitsum = 0;
							multiply = multiply.toString();
							for (var i1 = 0; i1 < 2; i1++){
								splitsum += parseInt(multiply.charAt(i1));
							}
							sum += splitsum
						} else{
							sum +=  parseInt(multiply);
						}
					} else {
					sum += singleDigit;
					}
				}
				sum = sum * 9;
				var result1 = sum % 10;
				if(parseInt(result1) == parseInt(check)){
					return true;
				}
				return false;
			}

			function addFromFile(shipmentId){
				$('#file_shipment_id').val(shipmentId);
				$("#addFromFile").modal('show');
			}
			function addManually(shipmentId){
				$('#shipment_id').val(shipmentId);
				$("#addManually").modal('show');
			}
			function editManually(detail_id, detail_shipment_id, simNumber){
				$('#detail_id').val(detail_id);
				$('#detail_shipment_id').val(detail_shipment_id);
				$('#edit_sim_card_number').val(simNumber);
				$("#editManually").modal('show');
			}
		</script>
	</body>
</html>
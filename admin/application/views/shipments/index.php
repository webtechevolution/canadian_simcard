<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>

		<?php load_view('common/css')?>

	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>
		<?php
			function bindCarrier($carrier, $carriers) {
				$returnValue = 'NA';
				foreach($carriers as $value) {
					if ($value['id'] === $carrier) {
						$returnValue = $value['carrier'];
					}
				}
				return $returnValue;
			}

			function bindStore($store, $stores) {
				$returnValue = 'NA';
				foreach($stores as $value) {
					if ($value['id'] === $store) {
						$returnValue = $value['name'];
					}
				}
				return $returnValue;
			}
		?>
		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>Shipments</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<!-- Basic Setup -->
				<div class="panel panel-default">
					<div class="notes-header">
						<a href="<?=BASEURL?>shipments/addshipment" class="btn btn-success btn-icon btn-icon-standalone" id="add-note">
							<i class="fa fa-plus"></i>	
							<span>New Shipment</span>
						</a>
					</div>
					<div class="panel-body">
						<script type="text/javascript">
							jQuery(document).ready(function($)
							{
								$("#example-1").dataTable({
									aLengthMenu: [
										[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]
									],
									"order": [[ 0, "desc" ]]
								});
							});
						</script>
						<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Shipment Number</th>
									<th>Store Name</th>
									<th>Carrier</th>
									<th>Quantity</th>
									<th>Date Shipped</th>
									<th>Date Received</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($shipments as $value): ?>
									<tr class="odd gradeX">
										<td><?=$value['shipment_id']?></td>
										<td><?= bindStore($value['store_id'], $stores)?></td>
										<td><?= bindCarrier($value['carrier_id'], $carriers)?></td>
										<td><?=$value['quantity']?></td>
										<td><?=$value['date_shipped']?></td>
										<td><?=$value['date_received'] ? $value['date_received'] : 'NA'?></td>
										<td>
											<a href="<?=BASEURL?>shipments/editshipment/<?=$value['shipment_id']?>" class="btn btn-info btn-sm btn-icon icon-left">
												ADD/Update SIM
											</a>
										</td>
									</tr>
								<?php endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php load_view('common/footer')?>
		</div>

		<div class="modal invert fade" id="modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<input type="hidden" name="id" value="" id="id">
					<div class="modal-header">
						<button type="button" class="close btn btn-lg" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Shipment</h4>
					</div>
					<div class="modal-body">
						<p>The shipment has been succesfully created, please click "ADD/Update SIM" to add SIM numbers to the shipment number <span id="shipment_number"></span></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Ok</button>
					</div>
				</div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(function() {
					var urlData = getUrlParam('text','Empty');
					if (urlData && urlData.length > 0) {
						console.log(urlData);
						$('#shipment_number').html(urlData[0]);
						$("#modal").modal('show');
					}
				});

				function getUrlParam(parameter, defaultvalue){
					var vars = [];
					var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
						vars.push(value);
					});
					return vars;
				}
			});
			
		</script>
	</body>
</html>
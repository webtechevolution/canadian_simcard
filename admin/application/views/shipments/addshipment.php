<!DOCTYPE html>
<html>
	<head>
		<title>Admin Panel</title>
		<?php load_view('common/css')?>
		<style>
			.form_error {
				color: red;
				top: 5px;
				position: relative;
			}
			* {
				box-sizing: border-box;
			}

			.autocomplete {
				position: relative;
				display: inline-block;
			}

			.autocomplete-items {
				position: absolute;
				border: 1px solid #d4d4d4;
				border-bottom: none;
				border-top: none;
				z-index: 99;
				top: 71%;
				left: 7%;
				width: 86%;
				right: 0;
			}

			.autocomplete-items div {
				padding: 10px;
				cursor: pointer;
				background-color: #fff; 
				border-bottom: 1px solid #d4d4d4; 
			}

			/*when hovering an item:*/
			.autocomplete-items div:hover {
				background-color: #e9e9e9; 
			}

			/*when navigating through the items using the arrow keys:*/
			.autocomplete-active {
				background-color: DodgerBlue !important; 
				color: #ffffff; 
			}
		</style>
	</head>
	<body>
		<?php load_view('common/header')?>
		<?php load_view('common/nav')?>

		<div class="main-content">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>New Shipment</h1>
					</div>
				</div>
			</div>
			<hr>
			<div class="container">
				<form method="post" autocomplete="off" role="form" id="addShipmentForm" action="javascript:void(0)">
					<div class="row">
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Store</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="store_name" name="store_name" placeholder="Store Name" required>
									<span class="form_error" id="store_name_error"></span>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Carrier</h3>
								</div>
								<div class="panel-body">
									<select name="carrier" class="form-control" required>
										<?php foreach($carriers as $key): ?>
											<option value="<?= $key['id'] ?>" ><?= $key['carrier']?></option>
										<?php endforeach;?>
									</select>
									<!-- <input type="text" class="form-control" id="carrier" name="carrier" placeholder="Carrier" required> -->
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Date Shipped</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="date_shipped" name="date_shipped" placeholder="Date Shipped" required>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Date Received</h3>
								</div>
								<div class="panel-body">
									<input type="text" class="form-control" id="date_received" name="date_received" placeholder="Date Received">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<input type="submit" name="submit" value="Save" class="btn btn-success">
									<a href='<?=BASEURL?>shipments' class="btn btn-info">Cancel</a>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="clear"></div>
			</div>
		</div>

		<?php load_view('common/js')?>
		<script type="text/javascript">
			$(document).ready(function(){
				$(function() {
					$("#date_shipped").datepicker();
					$("#date_received").datepicker();
				});

				$('#addShipmentForm').submit(function(event) {
					var counter = 0;
					var sim_card_start_serial = $("#sim_card_start_serial").val();
					var sim_card_end_serial = $("#sim_card_end_serial").val();
					var store_name = $("#store_name").val();
					var isStoreNameValid = false;

					if (store_name) {
						for (var i=0; i<stores.length; i++) {
							if (stores[i].name.toLowerCase().trim() === store_name.toLowerCase().trim()) {
								isStoreNameValid = true;
							}
						}
						if (isStoreNameValid) {
							$("#store_name_error").text('').hide();
						} else {
							counter++;
							$("#store_name_error").text("Invalid store name entered").show();
						}
					} else {
						counter++;
						$("#store_name_error").text("Invalid store name entered").show();
					}
					
					if (counter === 0) {
						$('#addShipmentForm').attr('action', '<?=BASEURL?>shipments/addshipment_action');
					} else {
						event.preventDefault();
					}
				});

				function autocomplete(inp, arr) {
					/*the autocomplete function takes two arguments,
					the text field element and an array of possible autocompleted values:*/
					var currentFocus;
					/*execute a function when someone writes in the text field:*/
					inp.addEventListener("input", function(e) {
							var a, b, i, val = this.value;
							/*close any already open lists of autocompleted values*/
							closeAllLists();
							if (!val) { return false;}
							currentFocus = -1;
							/*create a DIV element that will contain the items (values):*/
							a = document.createElement("DIV");
							a.setAttribute("id", this.id + "autocomplete-list");
							a.setAttribute("class", "autocomplete-items");
							/*append the DIV element as a child of the autocomplete container:*/
							this.parentNode.appendChild(a);
							/*for each item in the array...*/
							for (i = 0; i < arr.length; i++) {
								/*check if the item starts with the same letters as the text field value:*/
								if (arr[i].name.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
									/*create a DIV element for each matching element:*/
									b = document.createElement("DIV");
									/*make the matching letters bold:*/
									b.innerHTML = "<strong>" + arr[i].name.substr(0, val.length) + "</strong>";
									b.innerHTML += arr[i].name.substr(val.length);
									/*insert a input field that will hold the current array item's value:*/
									b.innerHTML += "<input type='hidden' value='" + arr[i].name + "'>";
									/*execute a function when someone clicks on the item value (DIV element):*/
									b.addEventListener("click", function(e) {
											/*insert the value for the autocomplete text field:*/
											inp.value = this.getElementsByTagName("input")[0].value;
											/*close the list of autocompleted values,
											(or any other open lists of autocompleted values:*/
											closeAllLists();
									});
									a.appendChild(b);
								}
							}
					});
					/*execute a function presses a key on the keyboard:*/
					inp.addEventListener("keydown", function(e) {
							var x = document.getElementById(this.id + "autocomplete-list");
							if (x) x = x.getElementsByTagName("div");
							if (e.keyCode == 40) {
								/*If the arrow DOWN key is pressed,
								increase the currentFocus variable:*/
								currentFocus++;
								/*and and make the current item more visible:*/
								addActive(x);
							} else if (e.keyCode == 38) { //up
								/*If the arrow UP key is pressed,
								decrease the currentFocus variable:*/
								currentFocus--;
								/*and and make the current item more visible:*/
								addActive(x);
							} else if (e.keyCode == 13) {
								/*If the ENTER key is pressed, prevent the form from being submitted,*/
								e.preventDefault();
								if (currentFocus > -1) {
									/*and simulate a click on the "active" item:*/
									if (x) x[currentFocus].click();
								}
							}
					});
					function addActive(x) {
						/*a function to classify an item as "active":*/
						if (!x) return false;
						/*start by removing the "active" class on all items:*/
						removeActive(x);
						if (currentFocus >= x.length) currentFocus = 0;
						if (currentFocus < 0) currentFocus = (x.length - 1);
						/*add class "autocomplete-active":*/
						x[currentFocus].classList.add("autocomplete-active");
					}
					function removeActive(x) {
						/*a function to remove the "active" class from all autocomplete items:*/
						for (var i = 0; i < x.length; i++) {
							x[i].classList.remove("autocomplete-active");
						}
					}
					function closeAllLists(elmnt) {
						/*close all autocomplete lists in the document,
						except the one passed as an argument:*/
						var x = document.getElementsByClassName("autocomplete-items");
						for (var i = 0; i < x.length; i++) {
							if (elmnt != x[i] && elmnt != inp) {
								x[i].parentNode.removeChild(x[i]);
							}
						}
					}
					/*execute a function when someone clicks in the document:*/
					document.addEventListener("click", function (e) {
							closeAllLists(e.target);
					});
				}

				var stores = <?php echo json_encode($stores); ?>;
				/*initiate the autocomplete function on the "store_name" element, and pass along the countries array as possible autocomplete values:*/
				autocomplete(document.getElementById("store_name"), stores);
			});
		</script>
	</body>
</html>
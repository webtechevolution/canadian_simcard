<?php if (__FILE__ == $_SERVER['SCRIPT_FILENAME']) exit('No direct access allowed.');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require rootPath.'/application/library/phpmailer/vendor/autoload.php';

/*
 *---------------------------------------------------------------
 * APPLICATION ENVIRONMENT
 *---------------------------------------------------------------
 *
 * Set this to false if in production mode.
 *
 */

	// if($_SERVER['SERVER_NAME'] == 'localhost') define ('DEVELOPMENT_ENVIRONMENT', true);
	// else define ('DEVELOPMENT_ENVIRONMENT', false);

	define ('DEVELOPMENT_ENVIRONMENT', false);





/*
 *---------------------------------------------------------------
 * 
 *---------------------------------------------------------------
 */
	date_default_timezone_set("America/Toronto");
	ini_set('memory_limit', '-1');
	set_time_limit(0);

	function sendPHPMailer($to,$subject,$message,$attachment=""){
		$config = load_config('email');
		$mail = new PHPMailer(true);

		try {
		    //Server settings
		    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
		    $mail->isSMTP();                                            // Send using SMTP
		    $mail->Host       = '10.10.10.41';                    // Set the SMTP server to send through
		    $mail->SMTPAuth   = false;                                   // Enable SMTP authentication
		    $mail->Username   = 'no-reply@canadiansimcards.com';                     // SMTP username
		    $mail->Password   = '';                               // SMTP password
		    //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
		    $mail->Port       = 25;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

		    //Recipients
		    $mail->setFrom($config['from'], $config['from_name']);
		    $mail->addAddress($to, 'Information');     // Add a recipient
		    //$mail->addAddress('ellen@example.com');               // Name is optional
		    $mail->addReplyTo($config['from'], 'Information');
		    //$mail->addCC('cc@example.com');
		    //$mail->addBCC('bcc@example.com');

		    // Attachments
		    if( $attachment!=""){
		    	$mail->addAttachment($attachment);         // Add attachments

		    }
		    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		    // Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = $subject;
		    $mail->Body    = $message;
		    $mail->AltBody = '';

		    $mail->send();
		    return true;
		} catch (Exception $e) {
		    return $mail->ErrorInfo;
		}
}
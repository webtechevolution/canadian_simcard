<?php

class NotverifiedController extends Controller {

	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->_authentication = new Authentication();
    $this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {
	}

  function verifyemail(){

    $username = $_POST['email'];
    $vkey = '';

    if(!filter_var($username, FILTER_VALIDATE_EMAIL)){
      echo "Email address is not valid.";
      die();
    }

    if($this->_authentication->is_valid_user($username, $vkey)){
      $this->verifyNewEmail($username, $vkey);
    }
    else{
      header("Location: " . BASEURL . 'notverified/error');
    }
  }

  function verifyNewEmail($username, $vkey){

    $msg = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
      <meta charset="UTF-8">
      <meta http-equiv="x-ua-compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Thank you for Signing up!</title>
      <style type="text/css">
        p{
        font-size: 14px;
        font-family:Arial,sans-serif;
        margin:0;
        color: #292929;
        padding:0;
        }
        .btn_shadow{
             filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
        -webkit-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
        -moz-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
        }
        .btn{
            display: inline-block;
            line-height: 1;
            width: 200px;
            margin-top: 15px;
            padding-top: 15px;
            padding-bottom: 13px;
            margin-bottom: 15px;
            font-family: Arial,sans-serif;
            font-weight: bold;
            text-align: center;
            font-size: 14px;
            color: #fff;
            text-transform: uppercase;
            background: #e94044;
            -webkit-clip-path: polygon(6% 100%, 0 81%, 0 18%, 6% 0, 94% 0, 100% 19%, 100% 81%, 94% 100%);
            clip-path: polygon(6% 100%, 0 81%, 0 19%, 6% 0, 94% 0, 100% 20%, 100% 81%, 94% 100%);
        }
        .btn a{
            display: block;
        }
        .btn.long{
          -webkit-clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
          clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
        }
        @media (max-width: 768px){
            .btn{
                padding-top: 13px;
                padding-bottom: 13px;
            }
        }
      </style>
      </head>
      <body style="background: #e1e1e1;">
      <center>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
          <tr>
            <td style="padding-top: 40px;">
              <div style="width: 520px;background: #fff;margin: 0 auto;">
                <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
                  <img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
                </div>
                <div style="padding-left: 70px;padding-right: 70px;">
                  <table>
                    <tr>
                      <td>
                        <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you!</p>
                      </td>
                    </tr>
                    <tr><td height="20"></td></tr>
                    <tr>
                      <td>
                        <p style="line-height: 1.5;color:#666;text-align: center;">In order to activate your account, please click the link below. Thank you</p>
                      </td>
                    </tr>
                    <tr><td height="35"></td></tr>
                    <tr>
                      <td height="45">
                        <div align="center" class="btn_shadow">
                          <a href="http://canadiansimcards.com/verifiedemail/index.php?vkey='.$vkey.'"><div class="btn long">Verify Email Now</div></a>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
              </div>
              <div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
              </div>
            </td>
          </tr>
        </table>
      </center>
      </body>
    </html>';

    $config = load_config('email');
    $to = $username;
    $subject = '[CanadianSimCards] Please Re-verify Email';

    // $email = new \SendGrid\Mail\Mail();
    // $email->setFrom($config['from'], $config['from_name']);
    // $email->setSubject($subject);
    // $email->addTo($to);
    // $email->addContent("text/html", $msg);
    // $sendgrid = new \SendGrid('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');

    // try {
    //   $response = $sendgrid->send($email);
    //   header("Location: " . BASEURL . 'notverified/newverify');
    // } catch (Exception $e) {
    //   echo 'location.reload();';
    // }
    sendPHPMailer($to,$subject,$msg);
    header("Location: " . BASEURL . 'notverified/newverify');
  }

  function error($arg=false) {
  }

  function newverify($arg=false) {
  }

}

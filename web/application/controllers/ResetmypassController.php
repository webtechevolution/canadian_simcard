<?php

class ResetmypassController extends Controller {

	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->session = new Session();
		//$this->_model = new Cart();
		$this->_model = new Verify();
		// $this->setVariable('cart', $this->session->get('cart'));
		// $this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		// if (!empty($_GET['vkey']) && isset($_GET['vkey'])) {
		// 	header("Location: " . BASEURL . 'emailverify/verify');
		// }

	}

	function proceed($arg=false){
		//if (!empty($_GET['vkey']) && isset($_GET['vkey'])) {
			$verify_model = new Verify();
			$clientArr = $verify_model->get_forgetpassuserId($_GET['vkey']);
			//print '<pre>'; print_r($clientArr); print '</pre>';
			$validity = false;
			$err_msg = "Your request is invalid for the process";
			if( $clientArr[0]['user_id'] > 0 ){
				$forget_time = strtotime($clientArr[0]['forget_code_time']);
				$forget_time_plus_ten_minutes = strtotime("+10 minutes", $forget_time);
				$timeNow = strtotime( date("Y-m-d H:i:s") );
				if($timeNow >=$forget_time_plus_ten_minutes) {
					$validity = false;
					$err_msg = "Code was valid for 10 minutes , please reset password again to get a new link in your email";
				} else {
					$validity = true;
				}
			} 

			if($validity ){ 
				$this->_view->set('client', $clientArr);
				//header("Location: " . BASEURL . 'resetmypass/proceed?vkey='.$_GET['vkey']);
				exit;
			} else { 
				$this->_view->set('errormsg', $err_msg);
				header("Location: " . BASEURL . 'resetmypass/failed?error='.$err_msg);
				
			}
		//}
	}

	function submitforgetpass($arg=false){ 
		$this->render = 0;
		// print_r($_POST);
		// die;
		if( ($_POST['password']!="" && $_POST['confirm_password']!="") && $_POST['password']== $_POST['confirm_password'] ){	
			$verify_model = new Verify();
			$resp = $verify_model->update_password( $_POST['forget_code'] );
			if($resp){
				header("Location: " . BASEURL . 'resetmypass/success');		
			} else {
				$err_msg = "Invalid request, can not be proceed";
				header("Location: " . BASEURL . 'resetmypass/failed?error='.$err_msg);
			}
		} else {
			$err_msg = "Invalid request, can not be proceed";
			header("Location: " . BASEURL . 'resetmypass/failed?error='.$err_msg);
		}
	}
	function success($arg=false){

	}
	function failed($arg=false){
		$this->_view->set('errormsg', $errormsg);
		
	}

}

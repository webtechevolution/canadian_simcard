<?php

class EmailverifyController extends Controller {

	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Cart();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		if (!empty($_GET['vkey']) && isset($_GET['vkey'])) {
			header("Location: " . BASEURL . 'emailverify/verify');
		}

	}

}

<?php

class AllplansController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Plans();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$faq = Helper::getFaq();
		$faq = array_slice($faq, 0, 4);

		$plans = $this->_model->get_all_plans_with_sim();
		$carriers = $this->_model->get_all_carrier();
		foreach($carriers as &$data) {
			if (strpos(BASEURL, 'web') !== false) {
			} else {
				$data['logo'] = 'web/' . $data['logo'];
			}
			$data['logo'] = BASEURL . $data['logo'];
			$data['logo'] = str_replace('web','admin', $data['logo']);
		}
		$this->_view->set('faq', $faq);
		$this->_view->set('plans', $plans);
		$this->_view->set('carriers', $carriers);
	}

}
<?php

class VerifiedemailController extends Controller {

	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Verify();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

    if(isset($_GET['vkey'])){
      $activation_code = $_GET['vkey'];

      $user = $this->_model->get_userWithKey($activation_code);

      if(isset($user[0])){
        $value = array(
          'active' => 1
        );
        $where = array(
          'activation_code' => $activation_code
        );

        $this->_model->update_userAccount($value, $where);

        //header("Location: " . BASEURL . 'verifiedemail');
      }
    }
    else {
      header("Location: " . BASEURL);
    }

	}

}

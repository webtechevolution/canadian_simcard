<?php

class TermsController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);


		$this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {
	}

}
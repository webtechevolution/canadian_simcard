<?php

class MainController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);
		
		$this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$faq = Helper::getFaq();
		$faq = array_slice($faq, 0, 4);

		if ($this->session->get('sim_custom_added') == 'yes') {
			$this->session->set('cart', []);
			$this->session->set('sim_custom_added', 'no');
			$this->session->set('sim_custom_itemid', '');
			$this->session->set('sim_custom_orderid', '');
			$this->setVariable('cart', $this->session->get('cart'));
		}
		// echo '<pre>';
		// print_r($faq);
		// echo '</pre>';

		$this->_view->set('faq', $faq);
	}

}
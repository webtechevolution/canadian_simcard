<?php
require("application/library/sendgrid-php/sendgrid-php.php");
class CartController extends Controller {

	function __construct($controller, $action) {

		parent::__construct($controller, $action);

		$this->session = new Session();
		// $this->session->set('cart', false);
		$cart = [];
		$plan_without_sim_added = 0;
		$plan_with_sim_added = 0;
	



		if($this->session->get('cart')) $cart = $this->session->get('cart');
		$this->setVariable('cart', $cart);

		$discount_code = '';
		if($this->session->get('discount_code')) $discount_code = $this->session->get('discount_code');
		$this->setVariable('discount_code', $discount_code);

		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function add_sim_card($arg=false){
		if ($arg[0]== 20) {
			$this->session->set('sim_custom_added', 'yes');
			$this->session->set('sim_custom_orderid', $arg[1]);
			$this->session->set('sim_custom_itemid', $arg[2]);
		} else {
			$this->session->set('sim_custom_added', 'no');
		}
		$this->render = 0;
		if(!isset($arg[0]) || (isset($arg[0]) && $arg[0]==''))  header("Location: " . BASEURL);

		//Update plan
		$plans_model = new Plans();

		$value = array(
			'with_sim' => 1,
			'shipping' => 1
		);

		$where = array(
			'id' => $arg[0],
		);
		$plans_model->update_plans($value, $where);
		// echo "<pre>"; 
		// print_r($arg[0]);
		// die;

		$product_id = $arg[0];
		$simcard_cost = 0;
		if ($product_id == 20) {
			$settings = $this->_model->get_setting();
			$setting = $settings[0];
			$simcard_cost = $setting['sim_cost'];
		}

		$cart = $this->session->get('cart');
		
		if($product_id == 20) $cart = [];

		if (isset($cart[$product_id])) {
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => $cart[$product_id]['quantity'] + 1
			);
		} else {
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => 1
			);
		}

		$this->session->set('cart', $cart);

		header("Location: " . BASEURL . 'cart');
	}
	
	function index($arg=false) {
		$plan_without_sim_added = 0;
		$plan_with_sim_added = 0;
		$sim_price = 0;
		$plans_model = new Plans();
		$plans = [];
		if ($this->session->get('sim_custom_added') == "yes") {
			$plans = $plans_model->get_all_plans_include_custom();
		} else {
			$plans = $plans_model->get_all_plans();
		}

		// echo "<pre>";
		// print_r($plans);
		$new_plans = [];
		foreach ($plans as $_) {
			$new_plans[$_['id']] = $_;
		}

		$cart = $this->session->get('cart');

		// print_r($cart);
		// print_r($new_plans);
		// print_r($this->session->get('sim_custom_added'));
		
		foreach ($cart as $item) {
			// $plan_without_sim_added = 0;
			// $plan_with_sim_added = 0;
			$_ = $new_plans[$item['id']];
			if($this->session->get('sim_custom_added')=="yes"){
				$plan_with_sim_added = 1;
				$_['with_sim']=1;
				$_['shipping']=1;
				// print_r($_);
			} else {
				if ($_['with_sim'] == '0') {
					$plan_without_sim_added = 1;
				}
				if ($_['with_sim'] == '1') {
					$plan_with_sim_added = 1;
				}
				if ($_['with_sim'] == '2') {
					$plan_with_sim_added = 1;
				}
			}
		}
		$this->setVariable('sim_custom_added', $this->session->get('sim_custom_added'));
		$this->setVariable('plan_without_sim_added', $plan_without_sim_added);
		$this->session->set('plan_without_sim_added', $plan_without_sim_added);
		$this->session->set('plan_with_sim_added', $plan_with_sim_added);
		// $this->session->set('sim_details_added', 0);
		// $this->session->set('sim_details', []);

		// $this->session->set('cart', array());

		// $cart = $this->session->get('cart');
		// echo '<pre>';
		// print_r( $plans );
		// echo '</pre>';
		// echo '<pre>';
		// print_r( $cart );
		// echo '</pre>';

		// echo '<pre>';
		// print_r( $cart);
		// print_r( $new_plans);
		// echo '</pre>';

		$shipping = $this->_model->shipping_price();
		$sim_price = $shipping[1]['value'];
		if ($this->session->get('sim_custom_added') == "yes") {
			$settings = $this->_model->get_setting();
			$sim_price = $settings[0]['sim_cost'];
		}
		// print_r($sim_price);
		/*if($shipping){
			$has_shipping = true;
		}*/

		$this->_view->set('plans', $new_plans);
		$this->_view->set('shipping', $shipping[0]['value']);
		$this->_view->set('sim_price', $sim_price);
		//$this->_view->set('has_shipping', $has_shipping);
	}

	function sim_card_status(){

		$status = $_GET['status'];
		$orderid = $_GET['orderid'];
		$itemid = $_GET['itemid'];
		// update order item status to reject if status is 2
		if ($status == 2) {
			$itemvalue = array(
				'with_sim' => $status == 2 ? 0 : $status,
				'offer'=> $status,
				'send_new_sim'=> $status
			);
			$itemwhere = array(
				'id' => $itemid
			);
			$this->_model->update_order_item($itemvalue, $itemwhere);
			// mark complete order rejected if it has only 1 item
			// $orderDetails = $this->_model->get_order($orderid);
			// if ($orderDetails[0]['total_items'] == 1) {
			// 	$ordervalue = array(
			// 		'shipped' => 2, //rejected
			// 	);
			// 	$orderwhere = array(
			// 		'id' => $orderid
			// 	);
			// 	$this->_model->update_order($ordervalue, $orderwhere);
			// }
		}
		
	// die;

		if ($status == 2) {
			//reject
			$message="Your response has been received successfully. Customer service team will get back to you.";
		}
		else if ($status == 1){
			//accept
			// update billing address with base address
			$this->_model->update_order_billing_details($orderid);
			$message="Thank you for accepting the offer. You will be redirected to payment page to complete the purchase process for the new SIM card.";
		}
		$this->_view->set('message', $message);
		$this->_view->set('status', $status);
		$this->_view->set('orderid', $orderid);
		$this->_view->set('itemid', $itemid);
		$this->_view->set('_', '');
	}

	function getCarrier($carrierId, $carriers) {
		$carrierName = '';
		foreach($carriers as $value) {
			if ($value['id'] == $carrierId) {
				$carrierName = $value['carrier'];
			}
		}
		return $carrierName;
	}

	function add($arg=false){
		$this->render = 0;
		if(!isset($arg[0]) || (isset($arg[0]) && $arg[0]==''))  header("Location: " . BASEURL);

		$product_id = $arg[0];
		$simcard_cost=0;
		if($product_id==20){
			$settings = $this->_model->get_setting();
			$setting=$settings[0];
			$simcard_cost=$setting['sim_cost'];
		}

		$cart = $this->session->get('cart');
		if(!$cart) $cart = [];

		if(isset($cart[$product_id])){
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => $cart[$product_id]['quantity'] + 1
			);
		}else{
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => 1
			);
		}

		$this->session->set('cart', $cart);

		header("Location: " . BASEURL . 'cart/simdetail' . $product_id);
	}

	function remove($arg=false){
		$this->render = 0;
		if(!isset($arg[0]) || (isset($arg[0]) && $arg[0]==''))  header("Location: " . BASEURL);

		$product_id = $arg[0];
		$cart = $this->session->get('cart');
		if(!$cart) $cart = [];

		if(isset($cart[$product_id])){
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => $cart[$product_id]['quantity'] - 1
			);

			if($cart[$product_id]['quantity'] == 0){
				unset($cart[$product_id]);
			}
		}

		$this->session->set('cart', $cart);

		header("Location: " . BASEURL . 'cart');
	}

	function applydiscountcode($arg=false){
		$this->render = 0;

		//print_r($_POST);

		$discount_code = $_POST['discount'];

		$discount_code = $this->_model->get_by_code($discount_code);

		// echo '<pre>';
		// print_r($discount_code);
		// echo '</pre>';
		if(!isset($discount_code[0])) $discount_code = '';
		$this->session->set('discount_code', $discount_code);

		header("Location: " . BASEURL . 'cart');
	}


	function add_address($arg=false){
		$this->render = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		$value = array(
			'type' => 'shipping',
			'user_id' => $this->session->get('user_id'),
			'full_name' => $_POST['full_name'],
			'address' => $_POST['address'],
			'city' => $_POST['city'],
			'province' => $_POST['province'],
			'zip' => $_POST['zip'],
			'country' => $_POST['country']
		);
		$this->_model->insert_address($value);
		header("Location: " . BASEURL . 'cart/shipping');
	}

	function add_billingaddress($arg=false){
		$this->render = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		$value = array(
			'type' => 'billing',
			'user_id' => $this->session->get('user_id'),
			'full_name' => $_POST['full_name'],
			'address' => $_POST['address'],
			'city' => $_POST['city'],
			'province' => $_POST['province'],
			'zip' => $_POST['zip'],
			'country' => $_POST['country'],
			'contact_number' => $_POST['contact_number'],
			'email' => $_POST['email']
		);
		$this->_model->insert_address($value);
		header("Location: " . BASEURL . 'cart/billing');
	}

	function remove_address($arg=false){
		$this->render = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );

		$where = array(
			'user_id' 	=> $this->session->get('user_id'),
			'id'		=> $arg[0]
		);
		$this->_model->delete_address($where);

		header("Location: " . BASEURL . 'cart/shipping');
	}

	function shipping($arg=false){

		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );

		if(!$this->validateSimDetails()) header("Location: " . BASEURL . 'cart/simdetail');

		$address = $this->_model->get_address_by_userid($this->session->get('user_id'), 'shipping');

		$this->_view->set('address', $address);
		$this->_view->set('sim_custom_added', $this->session->get('sim_custom_added'));
	}

	function billing($arg=false){

		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		
		if(!$this->validateSimDetails()) header("Location: " . BASEURL . 'cart/simdetail');

		$address = $this->_model->get_address_by_userid($this->session->get('user_id'), 'billing');
		$this->_view->set('billingaddress', $address);
		$this->_view->set('sim_custom_added', $this->session->get('sim_custom_added'));
	}

	function orderupdate($arg=false){
		$sim_price = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		
		$plan_with_sim_added = $this->session->get('plan_with_sim_added');
		
		if ($plan_with_sim_added) {
		
			if( !isset($_POST['selected_address']) ) header("Location: " . BASEURL );
		
			if( $_POST['selected_address'] == 'new' || $_POST['selected_address'] == '' ) header("Location: " . BASEURL . 'cart/shipping');

			$address = $this->_model->get_address_by_id($_POST['selected_address']);

			$this->session->set('address', $address[0]);
			// $this->_view->set('address', $address[0]);
			// $this->_view->set('province', $address[0]['province']);
		}
		$billingaddress = $this->session->get('billingaddress');
		$this->_view->set('address', $billingaddress);
		$this->_view->set('province', $billingaddress['province']);

		$plans_model = new Plans();
		// $plans = $plans_model->get_all_plans();
		$plans = [];
		if ($this->session->get('sim_custom_added') == "yes") {
			$plans = $plans_model->get_all_plans_include_custom();
		} else {
			$plans = $plans_model->get_all_plans();
		}

		$new_plans = [];
		foreach ($plans as $_) {
			$new_plans[$_['id']] = $_;
		}

		$shipping = $this->_model->shipping_price();
		$sim_price = $shipping[1]['value'];
		if ($this->session->get('sim_custom_added') == "yes") {
			$settings = $this->_model->get_setting();
			$sim_price = $settings[0]['sim_cost'];
		}

		$tax_list = $this->_model->get_tax();

		$this->_view->set('plans', $new_plans);
		$this->_view->set('shipping', $shipping[0]['value']);
		$this->_view->set('sim_price', $sim_price);
		$this->_view->set('discount_code', $this->session->get('discount_code'));
		$this->_view->set('plan_with_sim_added', $plan_with_sim_added);
		$this->_view->set('tax_list', $tax_list);
		$this->setVariable('sim_custom_added', $this->session->get('sim_custom_added'));
	}

	function billingorderupdate($arg=false){
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		if( !isset($_POST['selected_address']) ) header("Location: " . BASEURL );

		if( $_POST['selected_address'] == 'new' || $_POST['selected_address'] == '' ) header("Location: " . BASEURL . 'cart/billing');

		$address = $this->_model->get_address_by_id($_POST['selected_address']);

		$this->session->set('address', $address[0]);
		$this->session->set('billingaddress', $address[0]);
		$this->_view->set('billingaddress', $address[0]);
		$this->_view->set('province', $address[0]['province']);

		$plans_model = new Plans();
		$plans = $plans_model->get_all_plans();

		$new_plans = [];
		foreach ($plans as $_) {
			$new_plans[$_['id']] = $_;
		}

		$shipping = $this->_model->shipping_price();

		$this->_view->set('plans', $new_plans);
		$this->_view->set('shipping', $shipping[0]['value']);
		$this->_view->set('sim_price', $shipping[1]['value']);
		$this->_view->set('discount_code', $this->session->get('discount_code'));
		$plan_with_sim_added = $this->session->get('plan_with_sim_added');
		if ($plan_with_sim_added) {
			header("Location: " . BASEURL . 'cart/shipping');
		} else {
			header("Location: " . BASEURL . 'cart/orderupdate');
		}
	}

	function paymentmethod($arg=false){
		// $this->render = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		$this->_view->set('address', $this->session->get('address'));
		$this->_view->set('sim_custom_added', $this->session->get('sim_custom_added'));
	}

	function error(){}
	function confirmed(){
		
		$this->_view->set('plan_without_sim_added', $this->session->get('plan_without_sim_added'));
		$this->_view->set('plan_with_sim_added', $this->session->get('plan_with_sim_added'));
		$this->_view->set('sim_custom_added', $this->session->get('sim_custom_added'));
		$discount_code = '';
		
		$this->session->set('discount_code', $discount_code);
		$this->session->set('address', []);
		$this->session->set('billingaddress', []);
		$this->session->set('cart', []);
		$this->session->set('sim_details', []);
		$this->session->set('tempsimdetail', []);
		$this->session->set('sim_details_added', 0);
		$this->session->set('plan_without_sim_added', 0);
		$this->session->set('plan_with_sim_added', 0);
		$this->session->set('sim_custom_added', 'no');
		$this->session->set('sim_custom_itemid', '');
		$this->session->set('sim_custom_orderid', '');
	}

	function pay($arg=false){

		// echo "<pre>";
		// print_r('sim_custom_orderid ' . $this->session->get('sim_custom_orderid'). '<br>');
		// print_r('sim_custom_itemid ' . $this->session->get('sim_custom_itemid'). '<br>');
		// print_r($this->session->get('sim_custom_added'));

		$this->render = 0;

		if (!isset($_POST['card_number']) || $_POST['card_number'] == '') header("Location: " . BASEURL . 'cart/shipping');


		// die();
		$cart = $this->session->get('cart');
		$sim_details = $this->session->get('sim_details');
		// echo "<pre>";
		// die;
		if(!$cart) header("Location: " . BASEURL . 'cart/shipping');
		
		$address = $this->session->get('address');
		$billingaddress = $this->session->get('billingaddress');
		// print_r($address);
		// print_r($billingaddress);
		// die;
		$plans_model = new Plans();
		$plans = [];
		if ($this->session->get('sim_custom_added') == 'yes') {
			$plans = $plans_model->get_all_plans_include_custom();
		} else {
			$plans = $plans_model->get_all_plans();
		}
		$carriers = $plans_model->get_all_carrier();
		// $selected_carrier = '';
		
		foreach ($plans as $key => $_) {
			// print_r($_);
			foreach ($cart as $tmp => $__) {
				if($__['id'] == $_['id']) $cart[$tmp]['details'] = $_;

				$cart[$tmp]['details']['store'] = '';
				$cart[$tmp]['details']['shipment_id'] = '';
				$cart[$tmp]['details']['phone_number'] = '';
				$cart[$tmp]['details']['sim_number'] = '';
				$cart[$tmp]['details']['sim_number_client'] = '';
				$cart[$tmp]['details']['check_number'] = '';
			}
		}
		// print_r($plans);
		// print_r($cart);
		// die;
		$tmpNoSimCart = array();
		$finalCart = array();
		$withSim = 0;
		$withoutSim = 0;
		$addressBlock = '';
		foreach ($cart as $key => $_) {
			for ($i = 0; $i < (int)$_['quantity']; $i++) {
				if ((int)$_['details']['with_sim'] == 0) {
					$tmpNoSimCart[] = $_;
					$withoutSim = 1;
				} else {
					$withSim = 1;
					$finalCart[] = $_;
				}
			}
		}
		foreach ($sim_details as $key => $_) {
			$tmpNoSimCart[$key]['details']['with_sim'] = 0;
			$tmpNoSimCart[$key]['details']['store'] = $_['store'];
			$tmpNoSimCart[$key]['details']['shipment_id'] = $_['shipment_id'];
			$tmpNoSimCart[$key]['details']['phone_number'] = $_['phone_number'];
			$tmpNoSimCart[$key]['details']['sim_number'] = substr($_['sim_number'], 0, strlen($_['sim_number'])-1);
			$tmpNoSimCart[$key]['details']['check_number'] = substr($_['sim_number'], strlen($_['sim_number'])-1, strlen($_['sim_number']));
		}
		foreach ($tmpNoSimCart as $data) {
			$finalCart[] = $data;
		}
		if ($withoutSim) {
			$addressBlock = '<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
				<p style="font-size: 14px;color: #666;">Billing Address</p>
				<br>
				<p style="font-size: 16px;font-weight: bold;">' . $billingaddress['full_name'] . '</p>
				<br>
				<p style="font-size: 16px;">
					' . $billingaddress['address'] . ' <br>
					' . $billingaddress['city'] . ', ' . $billingaddress['province'] . ', ' . $billingaddress['zip'] . '<br>
					' . $billingaddress['country'] . '
				</p>
			</div>';
		}
		if ($withSim) {
			if (!$withoutSim) {
				$addressBlock = '<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
					<p style="font-size: 14px;color: #666;">Billing Address</p>
					<br>
					<p style="font-size: 16px;font-weight: bold;">' . $billingaddress['full_name'] . '</p>
					<br>
					<p style="font-size: 16px;">
						' . $billingaddress['address'] . ' <br>
						' . $billingaddress['city'] . ', ' . $billingaddress['province'] . ', ' . $billingaddress['zip'] . '<br>
						' . $billingaddress['country'] . '
					</p>
				</div>';
			}
			$addressBlock .= '<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
				<p style="font-size: 14px;color: #666;">Shipping Address</p>
				<br>
				<p style="font-size: 16px;font-weight: bold;">' . $address['full_name'] . '</p>
				<br>
				<p style="font-size: 16px;">
					' . $address['address'] . ' <br>
					' . $address['city'] . ', ' . $address['province'] . ', ' . $address['zip'] . '<br>
					' . $address['country'] . '
				</p>
			</div>';
		}
		
		// foreach ($cart as $key => $_) {
		// 	$selected_carrier .= $this->getCarrier($_['details']['carrier'], $carriers) . ' ';
		// }
		
		$sub_total = 0;
		$total_items = 0;
		$no_of_shipping_items = 0;
		$sim_price = 0;
		foreach ($cart as $key => $_) {
			$sub_total += $_['quantity'] * $_['details']['price'];
			$total_items += $_['quantity'];
			if ($_['details']['with_sim'] == 1){
				$no_of_shipping_items += $_['quantity'];
			}
		}
		
		$sub_total = round($sub_total, 2);

		$discount = 0;
		$discount_code = $this->session->get('discount_code');
		if(isset($discount_code[0])){
			if($discount_code[0]['type'] == 1){
				$discount = round($sub_total * $discount_code[0]['value']/100, 2);
			}
			else{
				$discount = $discount_code[0]['value'];
			}
		}

		$shipping = $this->_model->shipping_price();
		$settings = $this->_model->get_setting();
		$shippingcost = $no_of_shipping_items > 0 ? $shipping[0]['value'] : 0;
		if ($this->session->get('sim_custom_added') == 'yes') {
			$sim_price = $no_of_shipping_items * $settings[0]['sim_cost'];
		} else {
			$sim_price = $no_of_shipping_items * $shipping[1]['value'];
		}

		//calculate tax based on province dynamically
		$tax = 0;
		$tax_percentage = 0;
		$tax_list = $this->_model->get_tax();
		// if ($billingaddress['type'] == 'shipping') {
			foreach($tax_list as $key=>$_) {
				$list_province = strtolower(trim($_['province'], ""));
				$selected_province = strtolower(trim($billingaddress['province'], ""));
				if ($selected_province == $list_province) {
					$tax_percentage = $_['tax'];
				}
			}
		// }

		$tax = round(($sub_total + $sim_price + $shippingcost - $discount) * $tax_percentage, 2);
		$total = $sub_total + $sim_price + $shippingcost - $discount + $tax;
		$total = number_format($total, 2, '.', '');
		// $sub_total = $sub_total + $sim_price + $shippingcost;
		if($tax<0) $tax = 0;
		if($total<0) $total = 0;
		if($sub_total<0) $sub_total = 0;
		
		// print_r('tax ' . $tax. '<br>');
		// print_r('sub_total ' . $sub_total. '<br>');
		// print_r('shippingcost ' . $shippingcost. '<br>');
		// print_r('sim_price ' . $sim_price. '<br>');
		// print_r('discount ' . $discount. '<br>');
		// print_r('total ' . $total. '<br>');
		// print_r('discount_code ' . $discount_code[0]['code']. '<br>');
		// die;
		// echo '<pre>';
		// print_r($_POST );
		// echo '</pre>';

		try {
			// load_lib('mpgClasses');

			// $store_id='store5';
			// $api_token='yesguy';
			// $store_id='gwca016137';
			// $api_token='6N4sNjqmtQMsCwFSFfNa';

			$order_number = time();
			$txnArray =array(
				'type'=>'purchase',
				'order_id'=>$order_number,
				'cust_id'=>$this->session->get('user_id'),
				'amount'=>$total,
				'pan'=> $_POST['card_number'],
				'expdate'=> $_POST['month'].$_POST['year'],
				'crypt_type'=>'7',
				'dynamic_descriptor'=>'123'
			);
			// $mpgTxn = new mpgTransaction($txnArray);
			// $mpgRequest = new mpgRequest($mpgTxn);
			// $mpgRequest->setProcCountryCode("CA"); //"US" for sending transaction to US environment
			// // $mpgRequest->setTestMode(true); //false or comment out this line for production transactions
			// $mpgHttpPost  =new mpgHttpsPost($store_id,$api_token,$mpgRequest);
			// $mpgResponse = $mpgHttpPost->getMpgResponse();

			// echo '<pre>';
			// print_r($txnArray);
			// echo '</pre>';
			// echo '<pre>';
			// print_r($mpgRequest);
			// echo '</pre>';

			// $charge = array(
			// 	'status' => intval($mpgResponse->getResponseCode()),
			// 	'id' => $mpgResponse->getReferenceNum(),
			// 	'created' => $mpgResponse->getTransDate(),
			// 	'amount' => $mpgResponse->getTransAmount()
			// );
			
			// dummy payment, remove in production
			$charge = array(
				'status' => 5,
				'id' => '9999403408', 
				'created' => '2021-04-08',
				'amount' => $total
			);

			// echo '<pre>';
			// print_r($charge);
			// echo '</pre>';

			// die();

			if ($charge['status'] >= 3 && $charge['status'] <= 29 && $this->session->get('sim_custom_added') != 'yes') {
				$order = array(
					'user_id' => $this->session->get('user_id'),
					'full_name' => $address['full_name'],
					'email' => $this->session->get('username'),
					'address' => $address['address'],
					'city' => $address['city'],
					'province' => $address['province'],
					'zip' => $address['zip'],
					'country' => $address['country'],

					'total_price' => sprintf("%.2f", $total),
					'discount' => $discount,
					'shipping' => $shippingcost,
					'discount_code' => isset($discount_code[0])?$discount_code[0]['code']:'',
					'sub_total' => sprintf("%.2f", $sub_total),
					'tax' => sprintf("%.2f", $tax),
					'total_items' => $total_items,

					'authorizing_merchant_id' => $charge['id'],
					'order_number' => $order_number,
					'created' => $charge['created'],
					'amount' => $charge['amount'],
					'info' => json_encode($mpgResponse),

					'bambora_id' => '',
					'shipped' => 0,
					'carrier' => '',
					'tracking_number' => '',

					'billing_full_name' => $withSim == 1 ? $billingaddress['full_name'] : '',
					'billing_address' => $withSim == 1 ? $billingaddress['address'] : '',
					'billing_city' => $withSim == 1 ? $billingaddress['city'] : '',
					'billing_province' => $withSim == 1 ? $billingaddress['province'] : '',
					'billing_zip' => $withSim == 1 ? $billingaddress['zip'] : '',
					'billing_country' => $withSim == 1 ? $billingaddress['country'] : ''
				);
				$this->_model->insert_order($order);
				$order_id = $this->_model->get_last_id();

				//echo "here order id -".$order_id;
				//echo "Phone numbre-".$_['phone_number'];
				if ($order_id) {
					//echo "Now";

					foreach ($sim_details as $_) {
						$this->_model->update_stock($_['store'], $_['carrier'],$_['shipment_id']);
						$value = array(
							'sold' => 1,
							'number_activated_on' => $_['phone_number']
						);
						$where = array(
							'shipment_id' 	=> $_['shipment_id'],
							'sim_number'		=> $_['sim_number']
						);
						//echo "<br/>Two-";
						$res1=$this->_model->update_shipment_detail($value, $where);
						//echo "rest=".$res1;
					}

					foreach ($finalCart as $key => $_) {
						$value = array(
							'order_id' => $order_id,
							'title' => $_['details']['title'],
							'price' => sprintf("%.2f", $_['details']['price']),
							'quantity' => '1', //$_['quantity'],
							'with_sim' => $_['details']['with_sim'],
							'carrier' => $_['details']['carrier'],
							'store' => $_['details']['store'],
							'sim_number' => $_['details']['sim_number'],
							'sim_number_client' => $_['details']['sim_number_client'],
							'check_number' => $_['details']['check_number'],
							'plan_detail1' => $_['details']['plan_detail1'],
							'plan_detail2' => $_['details']['plan_detail2'],
							'plan_detail3' => $_['details']['plan_detail3'],
							'plan_detail4' => $_['details']['plan_detail4'],
							'plan_detail5' => $_['details']['plan_detail5'],
							'plan_detail6' => $_['details']['plan_detail6'],
							'plan_detail7' => $_['details']['plan_detail7'],
							'plan_detail8' => $_['details']['plan_detail8'],
							'plan_detail9' => $_['details']['plan_detail9'],
							'plan_detail10' => $_['details']['plan_detail10']
						);

						$this->_model->insert_ordered_items($value);
					}
					
					$item_html = '';
					foreach ($cart as $key => $_) {
						$item_carrier = $this->getCarrier($_['details']['carrier'], $carriers);
						$item_html .= '
							<tr>
								<td style="width: 140px;text-align: left;">
									 <p style="font-size: 16px;font-weight: bold; color: #292929;">' . $_['details']['title'] . '</p>
								</td>
								<td style="width: 80px;text-align: center;">
									 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">' . $item_carrier . '</p>
								</td>
								<td style="width: 80px;text-align: center;">
									 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">' . $_['quantity'] . '</p>
								</td>
								<td>
									 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: left;">$' . sprintf("%.2f", $_['details']['price']) . '</p>
								</td>
							</tr>
							<tr><td colspan="3" height="20"></td></tr>';
					}



					$msg = '
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
						<head>
						<meta charset="UTF-8">
						<meta http-equiv="x-ua-compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>Thank you for your order!</title>
						<style type="text/css">
							p{
							font-size: 14px;
							font-family:Arial,sans-serif;
							margin:0;
							color: #292929;
							padding:0;
							}
						</style>
						</head>
						<body style="background: #fff;">
						<center>
							<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
								<tr>
									<td style="padding-top: 40px;">
										<div style="width: 520px;background: #fff;margin: 0 auto;">
											<div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
												<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
											</div>
											<div style="padding-left: 70px;padding-right: 70px;">
												<table>
													<tr>
														<td>
															<p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you<br>for your order!</p>
														</td>
													</tr>
													<tr><td height="20"></td></tr>
													<tr>
														<td>
															<p style="line-height: 1.5;color:#666;text-align: center;">We are happy to let you know that your order is confirmed. We will ship your order within 5 -7 working days. </p>
														</td>
													</tr>
													<tr><td height="35"></td></tr>
													<tr>
														<td>
															<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
																<p style="font-size: 20px;font-weight: bold;text-align: center;">
																	Order No. ' . $order['order_number'] . '
																</p>
																<br>
																<p style="font-size: 16px;color: #666;text-align: center;">
																	Placed on: ' . date("M d, Y", strtotime($order['created'])) . '
																</p>
															</div>
														</td>
													</tr>
													<tr><td height="45"></td></tr>
													<tr>
														<td>
															<div>
																<table style="width: 100%;">
																	<tr>
																		<td style="width: 140px;text-align: left;">
																			<p style="font-size: 14px;color: #666;">Shipped Items</p>
																		</td>
																		<td style="width: 80px;text-align: center;">
																			<p style="font-size: 14px;color: #666;text-align: center;">Carrier</p>
																		</td>
																		<td style="width: 80px;text-align: center;">
																			<p style="font-size: 14px;color: #666;text-align: center;">Qty</p>
																		</td>
																		<td>
																			<p style="font-size: 14px;color: #666;">Price</p>
																		</td>
																	</tr>
																	<tr><td colspan="3" height="12"></td></tr>
																	' . $item_html . '
																	<tr>
																		<td colspan="4">
																			<div style="padding-top: 25px;padding-bottom: 25px;">
																				<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Subtotal</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['sub_total'] . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">One-Time Sim Cost</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $sim_price) . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Discount</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $order['discount']) . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Shipping</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $shippingcost) . '</p>
																		</td>
																	</tr>
																	<tr><td colspan="4" height="10"></td></tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Tax</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . sprintf("%.2f", $order['tax']) . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="4">
																			<div style="padding-top: 25px;padding-bottom: 25px;">
																				<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Total</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['total_price'] . '</p>
																		</td>
																	</tr>
																	<tr><td colspan="4" height="45"></td></tr>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
											' . $addressBlock . '
										</div>
										<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
											<div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
												<p style="font-size: 14px;color: #666;">
													If you have any questions about <br>
													your order, please contact us at <br>
													<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
													info@candiansimcards.com <br>
													<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
												</p>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</center>
						</body>
					</html>';

					$config = load_config('email');
					$to = $order['email'];
					$subject = '[CanadianSimCards] Order Confirmation';

					//$email = new \SendGrid\Mail\Mail();
					//$email->setFrom($config['from'], $config['from_name']);
					//$email->setSubject($subject);
					//$email->addTo($to);
					//$email->addContent("text/html", $msg);
					//$sendgrid = new \SendGrid('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');

					// try {
					// 	$response = $sendgrid->send($email);
					// } catch (Exception $e) {
					// }
					sendPHPMailer($to,$subject,$msg);
					$this->session->set('cart', []);
					header("Location: " . BASEURL . 'cart/confirmed');
				}
			} else if ($charge['status'] >= 3 && $charge['status'] <= 29 && $this->session->get('sim_custom_added') == 'yes') {
				
				$item_value = array(
					'with_sim' => 1,
					'offer' => 1,
					'send_new_sim' => 1
				);
				$item_where = array(
					'id' => $this->session->get('sim_custom_itemid')
				);
				$this->_model->update_order_item($item_value, $item_where);
				$old_order = $this->_model->get_order($this->session->get('sim_custom_orderid'));
				// print_r($old_order);
				$order_value = array(
					'full_name' => $address['full_name'],
					'address' => $address['address'],
					'city' => $address['city'],
					'province' => $address['province'],
					'zip' => $address['zip'],
					'country' => $address['country'], 
					'tax' => $tax + $old_order[0]['tax'],
					'shipping' => $shippingcost + $old_order[0]['shipping'],
					'discount' => $discount + $old_order[0]['discount'],
					// 'discount_code' => $old_order[0]['discount_code'],
					'total_price' => $total + $old_order[0]['total_price'],
					'amount' => $total + $old_order[0]['total_price'],
					'authorizing_merchant_id' => $charge['id'],
					'updated' => $charge['created'],
					'shipped' => 0
				);
				$order_where = array(
					'id' => $this->session->get('sim_custom_orderid')
				);
				$this->_model->update_order($order_value, $order_where);
				
				$this->session->set('cart', []);
				header("Location: " . BASEURL . 'cart/confirmed');
				
				// print_r($order_value);
				// die;
			} else{
				header("Location: " . BASEURL . 'cart/error');
			}
		} catch (Exception $e) {
			header("Location: " . BASEURL . 'cart/error');
		}
	}

	function testpay(){
		load_lib('mpgClasses');
		$this->render = 0;

		/**************************** Request Variables *******************************/
		$store_id='store2';
		$api_token='yesguy';

		// $store_id='gwca016137';
		// $api_token='6N4sNjqmtQMsCwFSFfNa';

		/************************* Transactional Variables ****************************/
		$cust_id='22222222222';
		$order_id='ord-'.date("dmy-G:i:s");

		$amount='1.00';
		$pan='4242424242424242';
		$expiry_date='1912';

		$dynamic_descriptor='123';

		/*********************** Transactional Associative Array **********************/

		$txnArray =array(
			'type'=>'purchase',
			'order_id'=>$order_id,
			'cust_id'=>$cust_id,
			'amount'=>$amount,
			'pan'=>$pan,
			'expdate'=>$expiry_date,
			'crypt_type'=>'7',
			'dynamic_descriptor'=>$dynamic_descriptor
		);

		/**************************** Transaction Object *****************************/

		$mpgTxn = new mpgTransaction($txnArray);

		/****************************** Request Object *******************************/

		$mpgRequest = new mpgRequest($mpgTxn);
		$mpgRequest->setProcCountryCode("CA"); //"US" for sending transaction to US environment
		$mpgRequest->setTestMode(true); //false or comment out this line for production transactions


		$mpgHttpPost  =new mpgHttpsPost($store_id,$api_token,$mpgRequest);

		/******************************* Response ************************************/

		$mpgResponse = $mpgHttpPost->getMpgResponse();

		// echo '<pre>';
		// print_r($mpgResponse);
		// echo '</pre>';

		print("\nCardType = " . $mpgResponse->getCardType());
		print("\nTransAmount = " . $mpgResponse->getTransAmount());
		print("\nTxnNumber = " . $mpgResponse->getTxnNumber());
		print("\nReceiptId = " . $mpgResponse->getReceiptId());
		print("\nTransType = " . $mpgResponse->getTransType());
		print("\nReferenceNum = " . $mpgResponse->getReferenceNum());
		print("\nResponseCode = " . $mpgResponse->getResponseCode());
		print("\nISO = " . $mpgResponse->getISO());
		print("\nMessage = " . $mpgResponse->getMessage());
		print("\nIsVisaDebit = " . $mpgResponse->getIsVisaDebit());
		print("\nAuthCode = " . $mpgResponse->getAuthCode());
		print("\nComplete = " . $mpgResponse->getComplete());
		print("\nTransDate = " . $mpgResponse->getTransDate());
		print("\nTransTime = " . $mpgResponse->getTransTime());
		print("\nTicket = " . $mpgResponse->getTicket());
		print("\nTimedOut = " . $mpgResponse->getTimedOut());
		print("\nStatusCode = " . $mpgResponse->getStatusCode());
		print("\nStatusMessage = " . $mpgResponse->getStatusMessage());
	}
	function pay_stripe($arg=false){

		$this->render = 0;

		if (!isset($_POST['token']) || $_POST['token'] == '') header("Location: " . BASEURL . 'cart/shipping');

		$cart = $this->session->get('cart');
		$address = $this->session->get('address');

		$plans_model = new Plans();
		$plans = $plans_model->get_all_plans();

		foreach ($plans as $key => $_) {
			foreach ($cart as $tmp => $__) {
				if($__['id'] == $_['id']) $cart[$tmp]['details'] = $_;
			}
		}

		$sub_total = 0;
		$total_items = 0;
		foreach ($cart as $key => $_) {
			$sub_total += $_['quantity'] * $_['details']['price'];
			$total_items += $_['quantity'];
		}
		$sub_total = round($sub_total, 2);
		$tax = round($sub_total * 0.13, 2);
		$total = $sub_total + $tax;


		// echo '<pre>';
		// print_r($_POST );
		// echo '</pre>';

		try {
			$order_number = time();
			// \Stripe\Stripe::setApiKey("sk_test_H5hC5NeKSuDMfx5ld2fFbMmf");
			\Stripe\Stripe::setApiKey("sk_test_d9VNT9Nmlx29pP6NQCQU559C");
			// \Stripe\Stripe::setApiKey("sk_live_417eXRDUQPr0Bof9qXF3nHco");

			$charge = \Stripe\Charge::create([
				'amount' => $total * 100,
				'currency' => 'cad',
				'source' => $_POST['token'],
				'receipt_email' => 'tamilromeo@gmail.com',
			]);

			// echo '<pre>';
			// print_r($charge);
			// echo '</pre>';
			if ($charge['status'] == 'succeeded') {

				$order = array(
					'user_id' => $this->session->get('user_id'),
					'full_name' => $address['full_name'],
					'email' => $this->session->get('username'),
					'address' => $address['address'],
					'city' => $address['city'],
					'province' => $address['province'],
					'zip' => $address['zip'],
					'country' => $address['country'],

					'total_price' => sprintf("%.2f", $total),
					'sub_total' => sprintf("%.2f", $sub_total),
					'tax' => sprintf("%.2f", $tax),
					'total_items' => $total_items,

					'authorizing_merchant_id' => $charge['id'],
					'order_number' => $order_number,
					'created' => $charge['created'],
					'amount' => $charge['amount'],
					'info' => json_encode($charge),

					'bambora_id' => '',
					'shipped' => 0,
					'carrier' => '',
					'tracking_number' => ''
				);
				$this->_model->insert_order($order);
				$order_id = $this->_model->get_last_id();

				if ($order_id) {

					$item_html = '';
					foreach ($cart as $key => $_) {
						$value = array(
							'order_id' => $order_id,
							'title' => $_['details']['title'],
							'price' => sprintf("%.2f", $_['details']['price']),
							'quantity' => $_['quantity'],
							'plan_detail1' => $_['details']['plan_detail1'],
							'plan_detail2' => $_['details']['plan_detail2'],
							'plan_detail3' => $_['details']['plan_detail3'],
							'plan_detail4' => $_['details']['plan_detail4'],
							'plan_detail5' => $_['details']['plan_detail5'],
							'plan_detail6' => $_['details']['plan_detail6'],
							'plan_detail7' => $_['details']['plan_detail7'],
							'plan_detail8' => $_['details']['plan_detail8'],
							'plan_detail9' => $_['details']['plan_detail9'],
							'plan_detail10' => $_['details']['plan_detail10']
						);

						$this->_model->insert_ordered_items($value);

						$item_html .= '
							<tr>
								<td style="width: 220px;text-align: left;">
									 <p style="font-size: 16px;font-weight: bold; color: #292929;">' . $_['details']['title'] . '</p>
								</td>
								<td style="width: 80px;text-align: center;">
									 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">' . $_['quantity'] . '</p>
								</td>
								<td>
									 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $_['details']['price'] . '</p>
								</td>
							</tr>
							<tr><td colspan="3" height="20"></td></tr>';
					}



					$msg = '
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
					<head>
					<meta charset="UTF-8">
					<meta http-equiv="x-ua-compatible" content="IE=edge">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<title>Thank you for your order!</title>
					<style type="text/css">
						p{
						font-size: 14px;
						font-family:Arial,sans-serif;
						margin:0;
						color: #292929;
						padding:0;
						}
					</style>
					</head>
					<body style="background: #e1e1e1;">
					<center>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
							<tr>
								<td style="padding-top: 40px;">
									<div style="width: 520px;background: #fff;margin: 0 auto;">
										<div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
											<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
										</div>
										<div style="padding-left: 70px;padding-right: 70px;">
											<table>
												<tr>
													<td>
														<p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you<br>for your order!</p>
													</td>
												</tr>
												<tr><td height="20"></td></tr>
												<tr>
													<td>
														<p style="line-height: 1.5;color:#666;text-align: center;">We are happy to let you know that your order is confirmed. We will ship your order within 5 -7 working days. </p>
													</td>
												</tr>
												<tr><td height="35"></td></tr>
												<tr>
													<td>
														<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
															<p style="font-size: 20px;font-weight: bold;text-align: center;">
																Order No. ' . $order['order_number'] . '
															</p>
															<br>
															<p style="font-size: 16px;color: #666;text-align: center;">
																Placed on: ' . date("M d, Y", strtotime($order['created'])) . '
															</p>
														</div>
													</td>
												</tr>
												<tr><td height="45"></td></tr>
												<tr>
													<td>
														<div>
															<table style="width: 100%;">
																<tr>
																	<td style="width: 220px;text-align: left;">
																		<p style="font-size: 14px;color: #666;">Shipped Items</p>
																	</td>
																	<td style="width: 80px;text-align: center;">
																		<p style="font-size: 14px;color: #666;text-align: center;">Qty</p>
																	</td>
																	<td>
																		<p style="font-size: 14px;color: #666;">Price</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="12"></td></tr>
																' . $item_html . '
																<tr>
																	<td colspan="3">
																		<div style="padding-top: 25px;padding-bottom: 25px;">
																			<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Subtotal</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['sub_total'] . '</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="10"></td></tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Estimated tax</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['tax'] . '</p>
																	</td>
																</tr>
																<tr>
																	<td colspan="3">
																		<div style="padding-top: 25px;padding-bottom: 25px;">
																			<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Total</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['total_price'] . '</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="45"></td></tr>
															</table>
														</div>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
										<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
											<p style="font-size: 14px;color: #666;">Shipping address</p>
											<br>
											<p style="font-size: 16px;font-weight: bold;">' . $order['full_name'] . '</p>
											<br>
											<p style="font-size: 16px;">
												' . $order['address'] . ' <br>
												' . $order['city'] . ', ' . $order['province'] . ', ' . $order['zip'] . '<br>
												' . $order['country'] . '
											</p>
										</div>
									</div>
									<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
										<div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
											<p style="font-size: 14px;color: #666;">
												If you have any questions about <br>
												your order, please contact us at <br>
												<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
												info@candiansimcards.com <br>
												<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
											</p>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</center>
					</body>
					</html>';

					$config = load_config('email');
					$to = $order['email'];
					$subject = '[CanadianSimCards] Order Conformation';

					// $email = new \SendGrid\Mail\Mail();
					// $email->setFrom($config['from'], $config['from_name']);
					// $email->setSubject($subject);
					// $email->addTo($to);
					// $email->addContent("text/html", $msg);
					// $sendgrid = new \SendGrid('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');

					// try {
					// 	$response = $sendgrid->send($email);
					// } catch (Exception $e) {
					// }
					sendPHPMailer($to,$subject,$msg);
					header("Location: " . BASEURL . 'cart/confirmed');
				}
			}
		} catch (Exception $e) {
			header("Location: " . BASEURL . 'cart/error');
		}
	}
	function pay_working_beanstreamd ($arg=false){

		$this->render = 0;

		if(!isset($_POST['card_name']) || $_POST['card_name']=='') header("Location: " . BASEURL . 'cart/shipping');
		if(!isset($_POST['card_number']) || $_POST['card_number']=='') header("Location: " . BASEURL . 'cart/shipping');
		if(!isset($_POST['cvv']) || $_POST['cvv']=='') header("Location: " . BASEURL . 'cart/shipping');

		$cart = $this->session->get('cart');
		$address = $this->session->get('address');

		$plans_model = new Plans();
		$plans = $plans_model->get_all_plans();

		foreach ($plans as $key => $_) {
			foreach ($cart as $tmp => $__) {
				if($__['id'] == $_['id']) $cart[$tmp]['details'] = $_;
			}
		}

		$sub_total = 0;
		$total_items = 0;
		foreach ($cart as $key => $_) {

			$sub_total += $_['quantity'] * $_['details']['price'];
			$total_items += $_['quantity'];
		}
		$sub_total = round($sub_total, 2);
		$tax = round($sub_total * 0.13, 2);
		$total = $sub_total + $tax;

		$merchant_id = '300206040';
		$api_key = '1579C4B9341b4e6Eb91c804DA77Ab818';
		$api_version = 'v1';
		$platform = 'www';
		$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version);
		$payment_data = array(
			'order_number' => time(),
			'amount' => $total,
			'payment_method' => 'card',
			'card' => array(
				'name' => $_POST['card_name'],
				'number' => $_POST['card_number'],
				'expiry_month' => $_POST['month'],
				'expiry_year' => $_POST['year'],
				'cvd' => $_POST['cvv']
			)
		);

		//Try to submit a Card Payment
		try {

			$result = $beanstream->payments()->makeCardPayment($payment_data, true);

			// echo '<pre>';
			// print_r( $result );
			// echo '</pre>';

			if($result['approved']){

				$order = array(
					'user_id' 	=> $this->session->get('user_id'),
					'full_name' => $address['full_name'],
					'email' => $this->session->get('username'),
					'address' => $address['address'],
					'city' => $address['city'],
					'province' => $address['province'],
					'zip' => $address['zip'],
					'country' => $address['country'],

					'total_price' => sprintf("%.2f", $total),
					'sub_total' => sprintf("%.2f", $sub_total),
					'tax' => sprintf("%.2f", $tax),
					'total_items' => $total_items,

					'authorizing_merchant_id' => $result['authorizing_merchant_id'],
					'order_number' => $result['order_number'],
					'bambora_id' => $result['id'],
					'created' => $result['created'],
					'amount' => $result['amount'],
					'info' => json_encode($result)
				);
				$this->_model->insert_order($order);
				$order_id = $this->_model->get_last_id();

				if($order_id){

					$item_html = '';
					foreach ($cart as $key => $_) {
						$value = array(
							'order_id' => $order_id,
							'title' => $_['details']['title'],
							'price' => sprintf("%.2f", $_['details']['price']),
							'quantity' => $_['quantity'],
							'plan_detail1' => $_['details']['plan_detail1'],
							'plan_detail2' => $_['details']['plan_detail2'],
							'plan_detail3' => $_['details']['plan_detail3'],
							'plan_detail4' => $_['details']['plan_detail4'],
							'plan_detail5' => $_['details']['plan_detail5'],
							'plan_detail6' => $_['details']['plan_detail6'],
							'plan_detail7' => $_['details']['plan_detail7'],
							'plan_detail8' => $_['details']['plan_detail8'],
							'plan_detail9' => $_['details']['plan_detail9'],
							'plan_detail10' => $_['details']['plan_detail10']
						);

						$this->_model->insert_ordered_items($value);

						$item_html .= '
							<tr>
								<td style="width: 220px;text-align: left;">
									 <p style="font-size: 16px;font-weight: bold; color: #292929;">'.$_['details']['title'].'</p>
								</td>
								<td style="width: 80px;text-align: center;">
									 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">'.$_['quantity'].'</p>
								</td>
								<td>
									 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$_['details']['price'].'</p>
								</td>
							</tr>
							<tr><td colspan="3" height="20"></td></tr>';
					}

					$msg = '
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
					<head>
					<meta charset="UTF-8">
					<meta http-equiv="x-ua-compatible" content="IE=edge">
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<title>Thank you for your order!</title>
					<style type="text/css">
						p{
						font-size: 14px;
						font-family:Arial,sans-serif;
						margin:0;
						color: #292929;
						padding:0;
						}
					</style>
					</head>
					<body style="background: #e1e1e1;">
					<center>
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
							<tr>
								<td style="padding-top: 40px;">
									<div style="width: 520px;background: #fff;margin: 0 auto;">
										<div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
											<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
										</div>
										<div style="padding-left: 70px;padding-right: 70px;">
											<table>
												<tr>
													<td>
														<p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you<br>for your order!</p>
													</td>
												</tr>
												<tr><td height="20"></td></tr>
												<tr>
													<td>
														<p style="line-height: 1.5;color:#666;text-align: center;">We are happy to let you know that your order is confirmed. We will ship your order within 5 -7 working days. </p>
													</td>
												</tr>
												<tr><td height="35"></td></tr>
												<tr>
													<td>
														<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
															<p style="font-size: 20px;font-weight: bold;text-align: center;">
																Order No. '.$order['order_number'].'
															</p>
															<br>
															<p style="font-size: 16px;color: #666;text-align: center;">
																Placed on: '.date("M d, Y", strtotime($order['created'])).'
															</p>
														</div>
													</td>
												</tr>
												<tr><td height="45"></td></tr>
												<tr>
													<td>
														<div>
															<table style="width: 100%;">
																<tr>
																	<td style="width: 220px;text-align: left;">
																		<p style="font-size: 14px;color: #666;">Shipped Items</p>
																	</td>
																	<td style="width: 80px;text-align: center;">
																		<p style="font-size: 14px;color: #666;text-align: center;">Qty</p>
																	</td>
																	<td>
																		<p style="font-size: 14px;color: #666;">Price</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="12"></td></tr>
																'.$item_html.'
																<tr>
																	<td colspan="3">
																		<div style="padding-top: 25px;padding-bottom: 25px;">
																			<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Subtotal</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$order['sub_total'].'</p>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">One-Time Sim Cost</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $sim_price . '</p>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Shipping</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $shippingcost . '</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="10"></td></tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Tax</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['tax'] . '</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="10"></td></tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Estimated tax</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$order['tax'].'</p>
																	</td>
																</tr>
																<tr>
																	<td colspan="3">
																		<div style="padding-top: 25px;padding-bottom: 25px;">
																			<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td colspan="2" style="text-align: left;">
																		<p style="font-size: 16px;font-weight: bold; color: #292929;">Total</p>
																	</td>
																	<td>
																		<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$'.$order['total_price'].'</p>
																	</td>
																</tr>
																<tr><td colspan="3" height="45"></td></tr>
															</table>
														</div>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
										<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
											<p style="font-size: 14px;color: #666;">Shipping address</p>
											<br>
											<p style="font-size: 16px;font-weight: bold;">'.$order['full_name'].'</p>
											<br>
											<p style="font-size: 16px;">
												'.$order['address'].' <br>
												'.$order['city'].', '.$order['province'].', '.$order['zip'].'<br>
												'.$order['country'].'
											</p>
										</div>
									</div>
									<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
										<div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
											<p style="font-size: 14px;color: #666;">
												If you have any questions about <br>
												your order, please contact us at <br>
												<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
												info@candiansimcards.com <br>
												<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
											</p>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</center>
					</body>
					</html>';

					// echo $order['email'];

					// Helper::sendMail('tamilromeo@gmail.com', '[CanadianSimCards] Order Conformation', $msg);
					// Helper::sendMail($order['email'], '[CanadianSimCards] Order Conformation', $msg);
					// Helper::sendMail('tamilromeo@gmail.com', '[CanadianSimCards] Order Conformation', 'dslkfjalf');
					// $helper->send_mail($order['email'], '[CanadianSimCards] Order Shipped', $msg);

					$config = load_config('email');
					$to = $order['email'];
					$subject= '[CanadianSimCards] Order Conformation';

					/*$email = new \SendGrid\Mail\Mail();
					$email->setFrom($config['from'], $config['from_name']);
					$email->setSubject($subject);
					$email->addTo($to);
					$email->addContent("text/html", $msg);
					$sendgrid = new \SendGrid('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');

					try {
						$response = $sendgrid->send($email);
						// echo '<pre>';
						// print_r( $response );
						// echo '</pre>';
						// return true;
					} catch (Exception $e) {
						// echo '<pre>';
						// print_r( $e );
						// echo '</pre>';
						// return false;
					} */

					sendPHPMailer($to,$subject,$msg);

					header("Location: " . BASEURL . 'cart/confirmed');
				}
			}

		} catch (\Beanstream\Exception $e) {

			// echo '<pre>';
			// print_r( $e );
			// echo '</pre>';
			header("Location: " . BASEURL . 'cart/error');
		}
	}
	function pay1111111($arg=false){

		$cart = $this->session->get('cart');

		$plans_model = new Plans();
		$plans = $plans_model->get_all_plans();

		foreach ($plans as $key => $_) {
			foreach ($cart as $tmp => $__) {
				if($__['id'] == $_['id']) $cart[$tmp]['details'] = $_;
			}
		}

		$sub_total = 0;
		$total_items = 0;
		foreach ($cart as $key => $_) {

			$sub_total += $_['quantity'] * $_['details']['price'];
			$total_items += $_['quantity'];
		}
		$sub_total = round($sub_total, 2);
		$tax = round($sub_total * 0.13, 2);
		$total = $sub_total + $tax;

		$address = $this->session->get('address');



		$merchant_id = '300206040';
		$api_key = '1579C4B9341b4e6Eb91c804DA77Ab818';
		$api_version = 'v1';
		$platform = 'www';

		//Create Beanstream Gateway
		$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version);

		//Example Card Payment Data
		$payment_data = array(
			'order_number' => time(),
			'amount' => 1.00,
			'payment_method' => 'card',
			'card' => array(
				'name' => 'Mr. Card Testerson',
				'number' => '4030000010001234',
				'expiry_month' => '07',
				'expiry_year' => '22',
				'cvd' => '123'
			)
		);

		//Try to submit a Card Payment
		try {

			$result = $beanstream->payments()->makeCardPayment($payment_data, true);

			echo '<pre>';
			print_r( $result );
			echo '</pre>';

			// $result = array(
			// 	'id' => '10000001',
			// 	'authorizing_merchant_id' => '300206040',
			// 	'created' => '2018-11-05T08:52:53',
			// 	'order_number' => '1541436771',
			// 	'amount' => '999'
			// );

			$order = array(
				'user_id' 	=> $this->session->get('user_id'),
				'full_name' => $address['full_name'],
				'email' => $this->session->get('username'),
				'address' => $address['address'],
				'city' => $address['city'],
				'province' => $address['province'],
				'zip' => $address['zip'],
				'country' => $address['country'],

				'total_price' => sprintf("%.2f", $total),
				'sub_total' => sprintf("%.2f", $sub_total),
				'tax' => sprintf("%.2f", $tax),
				'total_items' => $total_items,

				'authorizing_merchant_id' => $result['authorizing_merchant_id'],
				'order_number' => $result['order_number'],
				'bambora_id' => $result['id'],
				'created' => $result['created'],
				'amount' => $result['amount'],
				'info' => json_encode($result)
			);
			$this->_model->insert_order($order);
			$order_id = $this->_model->get_last_id();

			// echo '<pre>';
			// print_r( $order_id );
			// echo '</pre>';

			// echo '<pre>';
			// print_r( $value );
			// echo '</pre>';

			if($order_id){
				foreach ($cart as $key => $_) {

					$value = array(
						'order_id' => $order_id,
						'title' => $_['details']['title'],
						'price' => sprintf("%.2f", $_['details']['price']),
						'quantity' => $_['quantity'],
						'plan_detail1' => $_['details']['plan_detail1'],
						'plan_detail2' => $_['details']['plan_detail2'],
						'plan_detail3' => $_['details']['plan_detail3'],
						'plan_detail4' => $_['details']['plan_detail4'],
						'plan_detail5' => $_['details']['plan_detail5'],
						'plan_detail6' => $_['details']['plan_detail6'],
						'plan_detail7' => $_['details']['plan_detail7'],
						'plan_detail8' => $_['details']['plan_detail8'],
						'plan_detail9' => $_['details']['plan_detail9'],
						'plan_detail10' => $_['details']['plan_detail10']
					);

					$this->_model->insert_ordered_items($value);
				}
			}

		} catch (\Beanstream\Exception $e) {

			echo '<pre>';
			print_r( $e );
			echo '</pre>';
		}

		// echo '<pre>';
		// print_r( $sub_total );
		// echo '</pre>';

		// echo '<pre>';
		// print_r( $address );
		// echo '</pre>';

		/*
			Array
			(
			    [card_name] =>
			    [card_number] =>
			    [month] => 1
			    [year] => 2018
			    [cvv] =>
			)
		*/



		// echo '<pre>';
		// print_r( $cart );
		// echo '</pre>';




		// echo '<pre>';
		// print_r( $_POST );
		// echo '</pre>';

		/*
			Array
			(
			    [card_name] =>
			    [card_number] =>
			    [month] => 1
			    [year] => 2018
			    [cvv] =>
			)
		*/

		//empty cart
		//delete address form session
		// header("Location: " . BASEURL . 'cart/confirmed');
	}
	function pay11($arg=false){

		$this->render = 0;

		$merchant_id = '300206040';
		$api_key = '1579C4B9341b4e6Eb91c804DA77Ab818';
		$api_version = 'v1';
		$platform = 'www';

		//Create Beanstream Gateway
		$beanstream = new \Beanstream\Gateway($merchant_id, $api_key, $platform, $api_version);

		//Example Card Payment Data
		$payment_data = array(
			'order_number' => time(),
			'amount' => 1.00,
			'payment_method' => 'card',
			'card' => array(
				'name' => 'Mr. Card Testerson',
				'number' => '4030000010001234',
				'expiry_month' => '07',
				'expiry_year' => '22',
				'cvd' => '123'
			)
		);

		//Try to submit a Card Payment
		try {

			$result = $beanstream->payments()->makeCardPayment($payment_data, true);

			echo '<pre>';
			print_r( $result );
			echo '</pre>';
		} catch (\Beanstream\Exception $e) {

			echo '<pre>';
			print_r( $e );
			echo '</pre>';
		}
	}

	function newtestpay($arg=false){

		$this->render = 0;

		// echo '<pre>';
		// print_r($_POST);
		// echo '</pre>';

		if (!isset($_POST['card_number']) || $_POST['card_number'] == '') header("Location: " . BASEURL . 'cart/shipping');


		// die();
		$cart = $this->session->get('cart');
		if(!$cart) header("Location: " . BASEURL . 'cart/shipping');

		$address = $this->session->get('address');

		$plans_model = new Plans();
		$plans = $plans_model->get_all_plans();

		foreach ($plans as $key => $_) {
			// print_r($_);
			foreach ($cart as $tmp => $__) {
				if($__['id'] == $_['id']) $cart[$tmp]['details'] = $_;
			}
		}

		$sub_total = 0;
		$total_items = 0;
		foreach ($cart as $key => $_) {
			$sub_total += $_['quantity'] * $_['details']['price'];
			$total_items += $_['quantity'];
		}
		$sub_total = round($sub_total, 2);

		$discount = 0;
		$discount_code = $this->session->get('discount_code');
		if(isset($discount_code[0])){
			if($discount_code[0]['type'] == 1){
				$discount = round($sub_total * $discount_code[0]['value']/100, 2);
			}
			else{
				$discount = $discount_code[0]['value'];
			}
		}

		$shipping = $this->_model->shipping_price();
		$shippingcost = $shipping[0]['value'];
		$sim_price = $total_items * $shipping[1]['value'];

		switch($address['province']){
			case 'Ontario':
				$tax_percentage = 0.13;
				break;

			case 'Alberta':
			case 'British Columbia':
			case 'Manitoba':
			case 'Northwest Territories':
			case 'Nunavut':
			case 'Quebec':
			case 'Saskatchewan':
			case 'Yukon':
				$tax_percentage = 0.05;
				break;

			case 'New Brunswick':
			case 'Newfoundland and Labrador':
			case 'Newfoundland':
			case 'Nova Scotia':
			case 'Prince Edward Island':
				$tax_percentage = 0.15;
				break;

			default:
				$tax_percentage = 0;
				break;
		}

		$tax = round(($sub_total + $sim_price + $shippingcost - $discount) * $tax_percentage, 2);
		$total = $sub_total + $sim_price + $shippingcost - $discount + $tax;

		if($tax<0) $tax = 0;
		if($total<0) $total = 0;
		if($sub_total<0) $sub_total = 0;

		// echo '<pre>';
		// print_r($_POST );
		// echo '</pre>';

			// echo '<pre>';
			// print_r($charge);
			// echo '</pre>';

			// die();

				$order = array(
					'user_id' => $this->session->get('user_id'),
					'full_name' => $address['full_name'],
					'email' => $this->session->get('username'),
					'address' => $address['address'],
					'city' => $address['city'],
					'province' => $address['province'],
					'zip' => $address['zip'],
					'country' => $address['country'],

					'total_price' => sprintf("%.2f", $total),
					'discount' => $discount,
					'shipping' => $shippingcost,
					'discount_code' => isset($discount_code[0])?$discount_code[0]['code']:'',
					'sub_total' => sprintf("%.2f", $sub_total),
					'tax' => sprintf("%.2f", $tax),
					'total_items' => $total_items,

					'authorizing_merchant_id' => '',
					'order_number' => time(),
					'created' => '',
					'amount' => '',
					'info' => '',

					'bambora_id' => '',
					'shipped' => 0,
					'carrier' => '',
					'tracking_number' => ''
				);
				$this->_model->insert_order($order);
				$order_id = $this->_model->get_last_id();

				if ($order_id) {

					$item_html = '';
					foreach ($cart as $key => $_) {
						$value = array(
							'order_id' => $order_id,
							'title' => $_['details']['title'],
							'price' => sprintf("%.2f", $_['details']['price']),
							'quantity' => $_['quantity'],
							'plan_detail1' => $_['details']['plan_detail1'],
							'plan_detail2' => $_['details']['plan_detail2'],
							'plan_detail3' => $_['details']['plan_detail3'],
							'plan_detail4' => $_['details']['plan_detail4'],
							'plan_detail5' => $_['details']['plan_detail5'],
							'plan_detail6' => $_['details']['plan_detail6'],
							'plan_detail7' => $_['details']['plan_detail7'],
							'plan_detail8' => $_['details']['plan_detail8'],
							'plan_detail9' => $_['details']['plan_detail9'],
							'plan_detail10' => $_['details']['plan_detail10']
						);

						$this->_model->insert_ordered_items($value);

						$item_html .= '
							<tr>
								<td style="width: 220px;text-align: left;">
									 <p style="font-size: 16px;font-weight: bold; color: #292929;">' . $_['details']['title'] . '</p>
								</td>
								<td style="width: 80px;text-align: center;">
									 <p style="font-size: 16px;font-weight: bold;color: #292929;text-align: center;">' . $_['quantity'] . '</p>
								</td>
								<td>
									 <p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $_['details']['price'] . '</p>
								</td>
							</tr>
							<tr><td colspan="3" height="20"></td></tr>';
					}



					$msg = '
					<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
						<head>
						<meta charset="UTF-8">
						<meta http-equiv="x-ua-compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<title>Thank you for your order!</title>
						<style type="text/css">
							p{
							font-size: 14px;
							font-family:Arial,sans-serif;
							margin:0;
							color: #292929;
							padding:0;
							}
						</style>
						</head>
						<body style="background: #e1e1e1;">
						<center>
							<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
								<tr>
									<td style="padding-top: 40px;">
										<div style="width: 520px;background: #fff;margin: 0 auto;">
											<div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
												<img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
											</div>
											<div style="padding-left: 70px;padding-right: 70px;">
												<table>
													<tr>
														<td>
															<p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you<br>for your order!</p>
														</td>
													</tr>
													<tr><td height="20"></td></tr>
													<tr>
														<td>
															<p style="line-height: 1.5;color:#666;text-align: center;">We are happy to let you know that your order is confirmed. We will ship your order within 5 -7 working days. </p>
														</td>
													</tr>
													<tr><td height="35"></td></tr>
													<tr>
														<td>
															<div style="background: #f8f8f8;padding-top: 15px;padding-bottom: 15px;">
																<p style="font-size: 20px;font-weight: bold;text-align: center;">
																	Order No. ' . $order['order_number'] . '
																</p>
																<br>
																<p style="font-size: 16px;color: #666;text-align: center;">
																	Placed on: ' . date("M d, Y", strtotime($order['created'])) . '
																</p>
															</div>
														</td>
													</tr>
													<tr><td height="45"></td></tr>
													<tr>
														<td>
															<div>
																<table style="width: 100%;">
																	<tr>
																		<td style="width: 220px;text-align: left;">
																			<p style="font-size: 14px;color: #666;">Shipped Items</p>
																		</td>
																		<td style="width: 80px;text-align: center;">
																			<p style="font-size: 14px;color: #666;text-align: center;">Qty</p>
																		</td>
																		<td>
																			<p style="font-size: 14px;color: #666;">Price</p>
																		</td>
																	</tr>
																	<tr><td colspan="3" height="12"></td></tr>
																	' . $item_html . '
																	<tr>
																		<td colspan="3">
																			<div style="padding-top: 25px;padding-bottom: 25px;">
																				<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Subtotal</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['sub_total'] . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">One-Time Sim Cost</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $sim_price . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Shipping</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $shippingcost . '</p>
																		</td>
																	</tr>
																	<tr><td colspan="3" height="10"></td></tr>
																	<tr>
																		<td colspan="2" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Tax</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['tax'] . '</p>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="3">
																			<div style="padding-top: 25px;padding-bottom: 25px;">
																				<div style="height: 1px;width: 100%;background:#cfcfcf;"></div>
																			</div>
																		</td>
																	</tr>
																	<tr>
																		<td colspan="2" style="text-align: left;">
																			<p style="font-size: 16px;font-weight: bold; color: #292929;">Total</p>
																		</td>
																		<td>
																			<p style="font-size: 16px;font-weight: bold;color: #e94044;text-align: right;">$' . $order['total_price'] . '</p>
																		</td>
																	</tr>
																	<tr><td colspan="3" height="45"></td></tr>
																</table>
															</div>
														</td>
													</tr>
												</table>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
											<div style="background: #f9f9f9;padding-top: 40px;padding-bottom: 35px;">
												<p style="font-size: 14px;color: #666;">Shipping address</p>
												<br>
												<p style="font-size: 16px;font-weight: bold;">' . $order['full_name'] . '</p>
												<br>
												<p style="font-size: 16px;">
													' . $order['address'] . ' <br>
													' . $order['city'] . ', ' . $order['province'] . ', ' . $order['zip'] . '<br>
													' . $order['country'] . '
												</p>
											</div>
										</div>
										<div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
											<div style="background: #f9f9f9;padding-bottom: 35px;padding-top: 5px;">
												<p style="font-size: 14px;color: #666;">
													If you have any questions about <br>
													your order, please contact us at <br>
													<img style="top:1px; position:relative;" src="https://www.canadiansimcards.com/public/img/mail-ico.png">
													info@candiansimcards.com <br>
													<img src="https://www.canadiansimcards.com/public/img/phone-i.png" style="top: 2px; position: relative;"> 647-338-8819
												</p>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</center>
						</body>
					</html>';

					$config = load_config('email');
					$to = $order['email'];
					$subject = '[CanadianSimCards] Order Confirmation';

					/*$email = new \SendGrid\Mail\Mail();
					$email->setFrom($config['from'], $config['from_name']);
					$email->setSubject($subject);
					$email->addTo($to);
					$email->addContent("text/html", $msg);
					$sendgrid = new \SendGrid('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');

					try {
						$response = $sendgrid->send($email);
					} catch (Exception $e) {
					} */
					sendPHPMailer($to,$subject,$msg);
					header("Location: " . BASEURL . 'cart/confirmed');
				}


	}

	function captcha($arg=false){
		//reCaptcha Area start
		if(strstr(BASEURL, 'canadiansimcards.com')){
			$secretkey = "6LcOAvAUAAAAAJOyBEmm6yp4JMd1biwqAziJ8e7Y"; // Secret Key

			$msg = '';

			if(isset($_POST)){

				$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);

				if(($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

					$responseData = json_decode($response);

					if($responseData->success) {
						$msg = 'Verified';
						header("Location: " . BASEURL . 'cart/shipping');
					}

				}else{				
					$msg = 'Verify Captcha';
					//header("Location: " . BASEURL . 'cart/shipping');
					header("Location: " . BASEURL . 'cart');
				}

			}

		} else{
			$msg = 'Verified';
			// $plan_without_sim_added = $this->session->get('plan_without_sim_added');
			// if ($plan_without_sim_added == 0) {
				header("Location: " . BASEURL . 'cart/billing');
			// } else {
			// 	header("Location: " . BASEURL . 'cart/simdetail');
			// }
			exit;
		}

	}

	function simdetail($arg=false){
		$simdetail = array();
		
		// if($this->session->get('sim_details')) $simdetail = $this->session->get('sim_details');
		
		
			$plans_model = new Plans();
			$plans = $plans_model->get_plans_by_id($arg[0]);
			//print_r($plans);
			/*$simdetail = array(
				"id" => $plans['id'],
				"title" => $plans['title'],
				"price" => $plans['price'],
				"quantity" => "1",
				"carrier" => $plans['carrier'],
				"sim_number" => "",
				"store" => "",
				"shipment_id" => "",
				"phone_number" => "",
				"check_number" => ""
			);*/
			$simdetail = $plans;
					
				
			//echo "<br/><br/>";

			//echo $plans['title'];
			// echo "<pre>";
			// print_r($arg[0]);

		$shipping = $this->_model->shipping_price();
		$this->_view->set('sim_price', $shipping[1]['value']);
		
		$shipment = $this->_model->get_shipment();
		$shipment_detail = $this->_model->get_shipment_detail();

		$this->_view->set('planid', $arg[0]);
		$this->_view->set('simdetail', $simdetail);
		$this->_view->set('shipment', $shipment);
		$this->_view->set('shipment_detail', $shipment_detail);
		$this->session->set('tempsimdetail', $simdetail);
		
	}

	function saveSimDetails($args=false) {
		//Update plan
		$plans_model = new Plans();
		$added_sim_details = array();
		if($this->session->get('sim_details')) $added_sim_details = $this->session->get('sim_details');

		$value = array(
			'with_sim' => 0,
			'shipping' => 0
		);

		$where = array(
			'id' => $args[0],
		);
		$plans_model->update_plans($value, $where);

		$this->render = 0;

		/* Add to cart */
		// $this->session->set('sim_custom_added', 'yes');

		$this->render = 0;
		//if(!isset($args[0]) || (isset($args[0]) && $args[0]==''))  header("Location: " . BASEURL);


		$product_id = $args[0];
		$simcard_cost=0;
		if($product_id==20){
			$settings = $this->_model->get_setting();
			$setting=$settings[0];
			$simcard_cost=$setting['sim_cost'];
		}

		$cart = $this->session->get('cart');
		if(!$cart) $cart = [];

		if(isset($cart[$product_id])){
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => $cart[$product_id]['quantity'] + 1
			);
		}else{
			$cart[$product_id] = array(
				'id' => $product_id,
				'quantity' => 1
			);
		}

		$this->session->set('cart', $cart);
		/* Add to cart */
		

		$tempsimdetail = $this->session->get('tempsimdetail');
		
		foreach ($tempsimdetail as $key=>$value) {
			// $sim_number = $_POST['sim_number_' . $key]; 
			// $truncated_sim_number = substr($sim_number, 0, strlen($sim_number)-1);
			$tempsimdetail[$key]['sim_number'] = $_POST['sim_number_' . $key];
			$tempsimdetail[$key]['store'] = $_POST['store_' . $key];
			$tempsimdetail[$key]['shipment_id'] = $_POST['shipment_id_' . $key];
			$tempsimdetail[$key]['phone_number'] = ''; //$_POST['phone_number_' . $key];
		}
		array_push($added_sim_details, $tempsimdetail[0]);
		$this->session->set('sim_details_added', 1);
		$this->session->set('sim_details', $added_sim_details);
		// echo "here";
		
		header("Location: " . BASEURL . 'cart');
	}

	function validateSimDetails($args=false) {
		$plan_without_sim_added = $this->session->get('plan_without_sim_added') ? $this->session->get('plan_without_sim_added') : 0;
		$sim_details_added = $this->session->get('sim_details_added') ? $this->session->get('sim_details_added') : 0;
		if ($sim_details_added == 0 && $plan_without_sim_added == 1) {
			return 0;
		} else {
			return 1;
		}
	}

}
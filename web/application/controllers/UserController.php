<?php
require("application/library/sendgrid-php/sendgrid-php.php");

class UserController extends Controller {

	private $_authentication;
	private $_session;
	private $_helper;

	function __construct($controller,$action){
		parent::__construct($controller,$action);

		$this->_authentication = new Authentication();
		$this->_session = new Session();
		$this->_helper = new Helper();

		$this->render = 0;
	}

	function index(){

		header("Location: " . BASEURL);
	}

	function adduser(){

		// echo "<pre>";
		// print_r($_POST);
		// echo "</pre>";
		// die();

		if($_POST['email']==='' || $_POST['password']===''){
			echo "Email Or Password Empty";
			die();
		}

		if($_POST['confirm_password'] !== $_POST['password']){
			echo "Passwords do not match";
			die();
		}

		$name = isset($_POST['full_name'])?$_POST['full_name']:'';
		$username = $_POST['email'];
		$password = $_POST['password'];
		$vkey = '';

		if(!filter_var($username, FILTER_VALIDATE_EMAIL)){
			echo "Email address is not valid.";
			die();
		}

		$secretkey = "6LeSwe8UAAAAAI7pEH9qFSakmQ6y7hEPAIkwJEmh"; // Secret Key

		if(isset($_POST)){
			$vkey = md5(uniqid(time(), true));
			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretkey."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
			if(strstr(BASEURL, 'canadiansimcards.com')){

				if(($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

					$responseData = json_decode($response);

					if($responseData->success) {

						if( $this->_authentication->add_user($username, $password, $name, $vkey) ){
							$this->buildVerifyEmail($username, $vkey);
						}
						else echo "Failed";
					}
					else{
						echo "Response Failed";
						die();
					}

				}else{
					$msg = 'Verify Captcha';
					echo 'Verify Captcha';
					die();
				}
			} else {
				if( $this->_authentication->add_user($username, $password, $name, $vkey) ){
					$this->buildVerifyEmail($username, $vkey);
				} else {
					echo "Failed";
				}
			}

		}

	}

	function buildVerifyEmail($username, $vkey){

		$msg = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		  <head>
		  <meta charset="UTF-8">
		  <meta http-equiv="x-ua-compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Thank you for Signing up!</title>
		  <style type="text/css">
		    p{
		    font-size: 14px;
		    font-family:Arial,sans-serif;
		    margin:0;
		    color: #292929;
		    padding:0;
		    }
		    .btn_shadow{
		         filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -webkit-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -moz-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    }
		    .btn{
		        display: inline-block;
		        line-height: 1;
		        width: 200px;
		        margin-top: 15px;
		        padding-top: 15px;
		        padding-bottom: 13px;
		        margin-bottom: 15px;
		        font-family: Arial,sans-serif;
		        font-weight: bold;
		        text-align: center;
		        font-size: 14px;
		        color: #fff;
		        text-transform: uppercase;
		        background: #e94044;
		        -webkit-clip-path: polygon(6% 100%, 0 81%, 0 18%, 6% 0, 94% 0, 100% 19%, 100% 81%, 94% 100%);
		        clip-path: polygon(6% 100%, 0 81%, 0 19%, 6% 0, 94% 0, 100% 20%, 100% 81%, 94% 100%);
		    }
		    .btn a{
		        display: block;
		    }
		    .btn.long{
		      -webkit-clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		      clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		    }
		    @media (max-width: 768px){
		        .btn{
		            padding-top: 13px;
		            padding-bottom: 13px;
		        }
		    }
		  </style>
		  </head>
		  <body style="background: #e1e1e1;">
		  <center>
		    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
		      <tr>
		        <td style="padding-top: 40px;">
		          <div style="width: 520px;background: #fff;margin: 0 auto;">
		            <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
		              <img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
		            </div>
		            <div style="padding-left: 70px;padding-right: 70px;">
		              <table>
		                <tr>
		                  <td>
		                    <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Thank you<br>for Signing up!</p>
		                  </td>
		                </tr>
		                <tr><td height="20"></td></tr>
		                <tr>
		                  <td>
		                    <p style="line-height: 1.5;color:#666;text-align: center;">In order to activate your account, please click the link below. Thank you</p>
		                  </td>
		                </tr>
		                <tr><td height="35"></td></tr>
		                <tr>
		                  <td height="45">
		                    <div align="center" class="btn_shadow">
		                      <a href="'.BASEURL.'verifiedemail/index.php?vkey='.$vkey.'"><div class="btn long">Verify Email Now</div></a>
		                    </div>
		                  </td>
		                </tr>
		              </table>
		            </div>
		          </div>
		        </td>
		      </tr>
		      <tr>
		        <td>
		          <div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
		          </div>
		          <div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
		          </div>
		        </td>
		      </tr>
		    </table>
		  </center>
		  </body>
		</html>';

	  $config = load_config('email');
		$to = $username;
		$subject = '[CanadianSimCards] Please Verify Email';

		/*$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent("text/html", $msg);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);
			echo 'location.assign("emailverify");';
		} catch (Exception $e) {
			echo 'location.reload();';
		} */
		try {
			$response = sendPHPMailer($to,$subject,$msg);
			echo 'location.assign("emailverify");';
		} catch (Exception $e) {
			echo 'location.reload();';
		}
		

	}
	/*
	function login(){

		if($this->_authentication->is_user_active($_POST["email"]))
		{
			if( $this->_authentication->login($_POST["email"],$_POST["password"]) ){

				echo 'location.reload();';

			}
			else echo "Failed";
		}
		else{
			echo 'location.assign("notverified");';
		}

	} */

	function login() {
		if($_POST['email']==='' || $_POST['password']===''){
			echo "Please enter email and password";
			die();
		}
		
		$user_id = $this->_authentication->validateEmail($_POST["email"]);
		if ($user_id > 0 && $user_id !== '') {
			$user_active = $this->_authentication->validateUserAccount($_POST["email"]);
			if ($user_active == 1) {
				if( $this->_authentication->login($_POST["email"],$_POST["password"]) ){
					echo 'location.reload();';
				}
				else echo "Failed";
			} else {
				echo "Inactive";	
			}
		}
		else echo "Failed";
	}

	function logout(){

		$this->_authentication->logout();
		header("Location: " . BASEURL);
	}

	function forgotPassword(){
		if( $_POST['email']=='' ) {
			echo "Please enter email and password";
			die();
		} elseif(!filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL) ) {
			echo "Invalid email address";
			die();
		} else {
			$user_id = $this->_authentication->validateEmail( $_POST["email"] );
			if( $user_id > 0 ) {
				$forget_code = md5($user_id.time().time());
				if( $this->_authentication->updateForgetPasswordCode( $_POST["email"],$forget_code ) ) {
					$this->buildForgetPasswordEmail($_POST["email"],$forget_code);
				}				
			} else {
				echo "Wrong email address";
				die();
			}
		}
	}
	function testemail(){

		echo "test1";

		$this->buildForgetPasswordEmail1("bhallaheemanshu@gmail.com","p0ooooop0");

		echo "Done";
	
	}

	function buildForgetPasswordEmail1($email,$forget_code){
		
		echo "In function";
		$msg = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		  <head>
		  <meta charset="UTF-8">
		  <meta http-equiv="x-ua-compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Password Reset</title>
		  <style type="text/css">
		    p{
		    font-size: 14px;
		    font-family:Arial,sans-serif;
		    margin:0;
		    color: #292929;
		    padding:0;
		    }
		    .btn_shadow{
		         filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -webkit-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -moz-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    }
		    .btn{
		        display: inline-block;
		        line-height: 1;
		        width: 200px;
		        margin-top: 15px;
		        padding-top: 15px;
		        padding-bottom: 13px;
		        margin-bottom: 15px;
		        font-family: Arial,sans-serif;
		        font-weight: bold;
		        text-align: center;
		        font-size: 14px;
		        color: #fff;
		        text-transform: uppercase;
		        background: #e94044;
		        -webkit-clip-path: polygon(6% 100%, 0 81%, 0 18%, 6% 0, 94% 0, 100% 19%, 100% 81%, 94% 100%);
		        clip-path: polygon(6% 100%, 0 81%, 0 19%, 6% 0, 94% 0, 100% 20%, 100% 81%, 94% 100%);
		    }
		    .btn a{
		        display: block;
		    }
		    .btn.long{
		      -webkit-clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		      clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		    }
		    @media (max-width: 768px){
		        .btn{
		            padding-top: 13px;
		            padding-bottom: 13px;
		        }
		    }
		  </style>
		  </head>
		  <body style="background: #e1e1e1;">
		  <center>
		    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
		      <tr>
		        <td style="padding-top: 40px;">
		          <div style="width: 520px;background: #fff;margin: 0 auto;">
		            <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
		              <img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
		            </div>
		            <div style="padding-left: 70px;padding-right: 70px;">
		              <table>
		                <tr>
		                  <td>
		                    <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Forget Password: </p>
		                  </td>
		                </tr>
		                <tr><td height="20"></td></tr>
		                <tr>
		                  <td>
		                    <p style="line-height: 1.5;color:#666;text-align: center;">To reset your password please click this link below:</p>
		                  </td>
		                </tr>
		                <tr><td height="35"></td></tr>
		                <tr>
		                  <td height="45">
		                    <div align="center" class="btn_shadow">
		                      <a href="'.BASEURL.'resetmypass/proceed?vkey='.$forget_code.'"><div class="btn long">Reset Password</div></a>
		                    </div>
		                  </td>
		                </tr>
		              </table>
		            </div>
		          </div>
		        </td>
		      </tr>
		      <tr>
		        <td>
		          <div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
		          </div>
		          <div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
		          </div>
		        </td>
		      </tr>
		    </table>
		  </center>
		  </body>
		</html>';

	  $config = load_config('email');
		$to = $email;
		$subject = '[CanadianSimCards] Reset Password';

		/*$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent("text/html", $msg);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);
			echo 'location.assign("resetmypass");';
		} catch (Exception $e) {
			echo 'location.reload();';
		}
		*/
		try {
			$response = sendPHPMailer($to,$subject,$msg);
			echo 'location.assign("resetmypass");';
		} catch (Exception $e) {
			echo 'location.reload();';
		}
		
	}

	function buildForgetPasswordEmail($email,$forget_code){
		
		$msg = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		  <head>
		  <meta charset="UTF-8">
		  <meta http-equiv="x-ua-compatible" content="IE=edge">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <title>Password Reset</title>
		  <style type="text/css">
		    p{
		    font-size: 14px;
		    font-family:Arial,sans-serif;
		    margin:0;
		    color: #292929;
		    padding:0;
		    }
		    .btn_shadow{
		         filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -webkit-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    -moz-filter: drop-shadow(0px 0px 4px rgba(0,0,0,0.23));
		    }
		    .btn{
		        display: inline-block;
		        line-height: 1;
		        width: 200px;
		        margin-top: 15px;
		        padding-top: 15px;
		        padding-bottom: 13px;
		        margin-bottom: 15px;
		        font-family: Arial,sans-serif;
		        font-weight: bold;
		        text-align: center;
		        font-size: 14px;
		        color: #fff;
		        text-transform: uppercase;
		        background: #e94044;
		        -webkit-clip-path: polygon(6% 100%, 0 81%, 0 18%, 6% 0, 94% 0, 100% 19%, 100% 81%, 94% 100%);
		        clip-path: polygon(6% 100%, 0 81%, 0 19%, 6% 0, 94% 0, 100% 20%, 100% 81%, 94% 100%);
		    }
		    .btn a{
		        display: block;
		    }
		    .btn.long{
		      -webkit-clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		      clip-path: polygon(2% 100%, 0 85%, 0 15%, 2% 0, 98% 0, 100% 15%, 100% 85%, 98% 100%);
		    }
		    @media (max-width: 768px){
		        .btn{
		            padding-top: 13px;
		            padding-bottom: 13px;
		        }
		    }
		  </style>
		  </head>
		  <body style="background: #e1e1e1;">
		  <center>
		    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" id="bodyTable" style="height:100%;width:600px;" bgcolor="#05b2d6">
		      <tr>
		        <td style="padding-top: 40px;">
		          <div style="width: 520px;background: #fff;margin: 0 auto;">
		            <div style="padding-top: 20px;padding-bottom: 30px;padding-left: 20px;padding-right: 20px;">
		              <img src="http://canadiansimcards.com/public/img/logo.png" style="width: 75px;">
		            </div>
		            <div style="padding-left: 70px;padding-right: 70px;">
		              <table>
		                <tr>
		                  <td>
		                    <p style="font-size: 36px;line-height: 1.2;font-weight: bold;text-align: center;">Forget Password: </p>
		                  </td>
		                </tr>
		                <tr><td height="20"></td></tr>
		                <tr>
		                  <td>
		                    <p style="line-height: 1.5;color:#666;text-align: center;">To reset your password please click this link below:</p>
		                  </td>
		                </tr>
		                <tr><td height="35"></td></tr>
		                <tr>
		                  <td height="45">
		                    <div align="center" class="btn_shadow">
		                      <a href="'.BASEURL.'resetmypass/proceed?vkey='.$forget_code.'"><div class="btn long">Reset Password</div></a>
		                    </div>
		                  </td>
		                </tr>
		              </table>
		            </div>
		          </div>
		        </td>
		      </tr>
		      <tr>
		        <td>
		          <div style="background: #05b2d6;text-align: center;padding-left: 40px;padding-right: 40px;">
		          </div>
		          <div style="background: #fff;text-align: center;padding-left: 40px;padding-right: 40px;padding-bottom: 40px;">
		          </div>
		        </td>
		      </tr>
		    </table>
		  </center>
		  </body>
		</html>';

	  $config = load_config('email');
		$to = $email;
		$subject = '[CanadianSimCards] Reset Password';

		/*$email = new \SendGrid\Mail\Mail();
		$email->setFrom($config['from'], $config['from_name']);
		$email->setSubject($subject);
		$email->addTo($to);
		$email->addContent("text/html", $msg);
		$sendgrid = new \SendGrid('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');

		try {
			$response = $sendgrid->send($email);
			echo 'location.assign("resetmypass");';
		} catch (Exception $e) {
			echo 'location.reload();';
		}
		*/
		try {
			$response = sendPHPMailer($to,$subject,$msg);
			echo 'location.assign("resetmypass");';
		} catch (Exception $e) {
			echo 'location.reload();';
		}
		
	}
	/*
	function forgotPassword(){
		if($_POST['email']==='' || $_POST['password']===''){
			echo "Please enter email and password";
			die();
		}

		if($_POST['confirm_password'] !== $_POST['password']){
			echo "Passwords do not match";
			die();
		}
		
		$user_id = $this->_authentication->validateEmail($_POST["email"]);
		if($user_id > 0 && $user_id !== ''){
			if ($this->_authentication->update_password($user_id, $_POST["password"])) {
				echo "Password updated successfully";
			}
		}
		else echo "Please enter correct email address";
	} 
	*/
}

<?php

class BlogController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$blogs = $this->_model->get_article();

		// echo '<pre>';
		// print_r($blogs);
		// echo '<pre>';

		$this->_view->set('blogs', $blogs);
	}
	function post($arg=false) {

		if(!isset($arg[0]) || (isset($arg[0]) && $arg[0]==''))  header("Location: " . BASEURL);

		// echo '<pre>';
		// print_r($arg);
		// echo '<pre>';

		$blog = $this->_model->get_article_by_url($arg[0]);

		if(!isset($blog[0])) header("Location: " . BASEURL);

		$blogs = $this->_model->get_article();
		$blogs = array_slice($blogs, 0, 4);

		// echo '<pre>';
		// print_r($blog);
		// echo '<pre>';

		$this->_view->set('_', $blog[0]);
		$this->_view->set('blogs', $blogs);

	}

}
<?php

class ActivateplandetailsController extends Controller {
	
	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Plans();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$plan_id = $this->session->get('selected_plan');
		if($plan_id == '') header("Location: " . BASEURL);

		$faq = Helper::getFaq();
		$faq = array_slice($faq, 0, 4);

		// $plans = $this->_model->get_all_plans_without_sim();
		$plans = $this->_model->get_all_plans();
		shuffle($plans);
		$selected_plan = false;

		foreach ($plans as $key => $_) {
			if($plan_id == $_['id']){
				$selected_plan = $plans[$key];
				unset($plans[$key]);
			}
		}

		// print_r($selected_plan);

		$this->_view->set('faq', $faq);
		$this->_view->set('plans', $plans);
		$this->_view->set('_', $selected_plan);
	}

	function select($arg=false){

		$this->session->set('selected_plan', $arg[0]);
		header("Location: " . BASEURL . 'activateplandetails');
	}

}
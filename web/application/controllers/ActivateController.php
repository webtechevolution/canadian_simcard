<?php

class ActivateController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Cart();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {
		$orderId = $arg[0];
		
		$orderDetails = $this->_model->get_order($orderId);
		
		$orderItems = $this->_model->get_order_item_with_sim($orderId);
		$this->_view->set('orderItems', $orderItems);
	}

	function verify($arg=false){

		$orderId = $arg[0];
		
		$orderItems = $this->_model->get_order_item_with_sim($orderId);
		
		$simMatched = 0;
		foreach($orderItems as $key=>$_) {
			$sim_number = $_POST['sim_number_' . $key]; 
			$phone_number = ''; //$_POST['phone_number_' . $key]; 
			$truncated_sim_number = substr($sim_number, 0, strlen($sim_number)-1);
			if ($_['sim_number'] == $truncated_sim_number) {
				$simMatched = 1;
				$itemvalue = array(
					'sim_number_client' => $_['sim_number']
				);
				$itemwhere = array(
					'id' => $_['id']
				);
				$this->_model->update_order_item($itemvalue, $itemwhere);

				$value = array(
					'sold' => 1
					//'number_activated_on' => $phone_number
				);
				$where = array(
					// 'shipment_id' 	=> $_['shipment_id'],
					'sim_number'		=> $sim_number
				);
				$this->_model->update_shipment_detail($value, $where);

			} else {
				$simMatched = 0;
			}
		}
		if ($simMatched) {
			$value = array(
				'shipped' => 1
			);
			$where = array(
				'id' => $orderId
			);
			$this->_model->update_order($value, $where);
			header("Location: " . BASEURL . 'activate/complete');
		} else{
			header("Location: " . BASEURL . 'activate/error');
		}
	}

	function error($arg=false) {
	}

	function complete($arg=false) {
	}

}
<?php

class MyprofileController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );
		$profile = $this->_model->get_profile_by_userid($this->session->get('user_id'));
		// echo '<pre>';
		// print_r( $profile );
		// echo '</pre>';
		$this->_view->set('profile', $profile);
	}

	function map($arg=false) {
	}

	function submit_profile($arg=false){
		$this->render = 0;
		if(!$this->session->get('logged_in')) header("Location: " . BASEURL );

		if($_REQUEST['btn_submit'] == "Save") {
			$value = array(
				'full_name' => $_POST['full_name'],
				'username' => $_POST['email'],
				'email' => $_POST['email']
			);
			$where = array(
				'user_id' => $this->session->get('user_id')
			);
			$this->_model->update_profile($value, $where);
			$this->session->set('username', $_POST['email']);
  		$this->session->set('full_name', $_POST['full_name']);
			header("Location: " . BASEURL);
		} else if ($_REQUEST['btn_submit'] == "Cancel") {
			header("Location: " . BASEURL);
		}
	}

}
<?php

class CampplanController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->_model = new Plans();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$faq = Helper::getFaq();
		$faq = array_slice($faq, 0, 4);

		$plans = $this->_model->get_all_plans();

		// echo '<pre>';
		// print_r($plans);
		// echo '</pre>';

		$this->_view->set('faq', $faq);
		$this->_view->set('plans', $plans);
	}

}
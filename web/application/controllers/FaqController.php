<?php

class FaqController extends Controller {

	function __construct($controller, $action) {
	
		parent::__construct($controller, $action);

		$this->session = new Session();
		$this->setVariable('cart', $this->session->get('cart'));
		$this->setVariable('logged_in', $this->session->get('logged_in'));
	}

	function index($arg=false) {

		$faq = Helper::getFaq();
		$faq_full = $faq;

		// echo '<pre>';
		// print_r($arg);
		// echo '</pre>';

		$cat_found = false;
		if(isset($arg[0])){
			foreach ($faq as $key => $_) {
				if($_['url'] == $arg[0]){
					$faq = [];
					$faq[] = $_;
					break;
				}
			}
		}

		if(isset($arg[1])){
			foreach ($faq as $key => $_) {
				foreach ($_['sub_cat'] as $sub_key => $sub_cat) {
					if($sub_cat['url'] == $arg[1]){
						$faq[$key]['sub_cat'] = [];
						$faq[$key]['sub_cat'][] = $sub_cat;
						break;
					}
				}
			}
		}

		// echo '<pre>';
		// print_r($faq);
		// echo '</pre>';


		$selected = $faq[0]['sub_cat'][0];

		// echo '<pre>';
		// print_r($selected);
		// echo '</pre>';
		// echo '<pre>';
		// print_r($faq_full);
		// echo '</pre>';

		$this->_view->set('faq', $faq_full);
		$this->_view->set('selected', $selected);
	}

}
<?php

class Cart extends Model {
	function get_last_id(){

		return $this->db->lastInsertId();
	}

	function get_address(){

		$query = "	SELECT * 
					FROM  address";

		return $this->db->query($query);
	}

	function get_address_by_userid($user_id, $type){

		$query = "	SELECT * 
					FROM  address
					WHERE user_id = $user_id AND type = '$type'";

		return $this->db->query($query);
	}
	function get_setting(){

		$query = "	SELECT *
					FROM  settings";

		return $this->db->query($query);
	}
function update_order_billing_details($id){
		//update orders set billing_full_name=full_name,billing_address=address,billing_city=city,billing_province=province,billing_zip=zip,billing_country=country where id=163

		$query = "update orders set billing_full_name=full_name,billing_address=address,billing_city=city,billing_province=province,billing_zip=zip,billing_country=country where id=$id";

		return $this->db->query($query);
	}
	function get_address_by_id($id){

		$query = "	SELECT * 
					FROM  address
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_address($where){

		return $this->db->delete('address', $where);
	}

	function insert_address($value){

		return $this->db->insert('address', $value);
	}

	function update_address($value, $where){

		return $this->db->update('address', $value, $where);
	}



	function insert_order($value){

		return $this->db->insert('orders', $value);
	}
	function insert_ordered_items($value){

		return $this->db->insert('ordered_items', $value);
	}

	function update_order($value, $where){

		return $this->db->update('orders', $value, $where);
	}

	function update_order_item($value, $where){

		return $this->db->update('ordered_items', $value, $where);
	}

	function get_order($orderId){
		$query = "	SELECT * 
					FROM  orders
					WHERE id = $orderId";

		return $this->db->query($query);
	}

	function get_order_item_with_sim($order_id){

		$query = "	SELECT *
					FROM  ordered_items
					WHERE order_id = $order_id AND with_sim = 1";

		return $this->db->query($query);
	}

	function get_order_sim($sim){

		$query = "	SELECT * 
					FROM  orders
					WHERE bambora_id = '$sim'";

		return $this->db->query($query);
	}

	function get_by_code($id){

		$query = "	SELECT * 
					FROM  discount_code
					WHERE code = '$id'";

		return $this->db->query($query);
	}

	function shipping_price(){

		$query = "	SELECT * 
					FROM  shipping";

		return $this->db->query($query);
	}

	function get_shipment(){

		$query = "	SELECT * 
					FROM  shipments";

		return $this->db->query($query);
	}

	function get_shipment_detail(){

		$query = "	SELECT * 
					FROM  shipment_detail WHERE sold = 0";

		return $this->db->query($query);
	}

	function update_stock($store_id, $carrier_id, $shipment_id){

		$query = "	SELECT quantity_sold 
					FROM  stock
					WHERE store_id = '$store_id' AND carrier_id = '$carrier_id' AND shipment_id = '$shipment_id'";

		$selectedData = $this->db->query($query);

		$value = array(
			'quantity_sold' => (int)$selectedData[0]['quantity_sold'] + 1,
		);

		$where = array(
			'store_id' => $store_id,
			'carrier_id' => $carrier_id,
			'shipment_id' => $shipment_id,
		);

		return $this->db->update('stock', $value, $where);
	}

	function update_shipment_detail($value, $where){
		return $this->db->update('shipment_detail', $value, $where);
	}

	function get_tax(){

		$query = " SELECT * 
					FROM  tax";

		return $this->db->query($query);
	}
}

<?php

class Faq extends Model {

	function get_category(){

		$query = "	SELECT * 
					FROM  faq_category";

		return $this->db->query($query);
	}

	function get_category_by_id($id){

		$query = "	SELECT * 
					FROM  faq_category
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_category($where){

		return $this->db->delete('faq_category', $where);
	}

	function insert_category($value){

		return $this->db->insert('faq_category', $value);
	}

	function update_category($value, $where){

		return $this->db->update('faq_category', $value, $where);
	}




	function get_sub_category(){

		$query = "	SELECT * 
					FROM  faq_sub_category";

		return $this->db->query($query);
	}

	function get_sub_category_by_id($id){

		$query = "	SELECT * 
					FROM  faq_sub_category
					WHERE id = $id";

		return $this->db->query($query);
	}

	function delete_sub_category($where){

		return $this->db->delete('faq_sub_category', $where);
	}

	function insert_sub_category($value){

		return $this->db->insert('faq_sub_category', $value);
	}

	function update_sub_category($value, $where){

		return $this->db->update('faq_sub_category', $value, $where);
	}




	function get_article(){

		$query = "	SELECT * 
					FROM faq_article";

		return $this->db->query($query);
	}

	function get_article_by_id($id){

		$query = "	SELECT * 
					FROM faq_article
					WHERE id = $id";

		return $this->db->query($query);
	}

	function insert_article($value){

		return $this->db->insert('faq_article', $value);
	}
	function delete_article($where){

		return $this->db->delete('faq_article', $where);
	}

	function update_article($value, $where){

		return $this->db->update('faq_article', $value, $where);
	}




}












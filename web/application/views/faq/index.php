<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>

<body>
<?=load_view('common/header')?>

<div class="help_topics admin_page">
<div class="container">
	<div class="row">
		<div class="col-sm-2 d-none d-md-block">
			<div class="topic_navigation">
				<h3>Help Topics</h3>

				<?php foreach ($faq as $cat): ?>
				<h5><?=$cat['name']?></h5>
				<ul>
					<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
					<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
					<?php endforeach;?>
				</ul>
				<?php endforeach;?>

			</div>
		</div>
		<div class="col-sm-9 sm-offset-1">
			<div class="answers">
				<div class="coverage">
					<div class="questions">
						<div class="mob_filter topic_navigation">
							<div class="info">
								<div class="title">
									<p>Topics</p>
									<img src="<?=BASEURL?>public/img/arrow_down.png" class="arrow">
								</div>
								<div class="content">
									<?php foreach ($faq as $cat): ?>
									<h5><?=$cat['name']?></h5>
									<ul>
										<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
										<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
										<?php endforeach;?>
									</ul>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>

					<!-- <p class="all_plans">
						<a href="#"><img src="<?=BASEURL?>public/img/left_arrow.png">Support</a>
					</p> -->


					<h1><?=$selected['name']?></h1>
					<div class="questions">
						<?php foreach ($selected['faqs'] as $faq): ?>
						<div class="info">
							<div class="title">
							<p><?=$faq['question']?></p>
							<img src="<?=BASEURL?>public/img/arrow_down.png" class="arrow">
							</div>
							<div class="content">
								<p><?=$faq['answer']?></p>
							</div>
						</div>
						<?php endforeach;?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
</style>
<body>
<?=load_view('common/header')?>
<div class="coverage">
<div class="top_poster container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8">
				<div class="box_container">
					<div class="box">
						<h1>Check coverage at <span>your place</span></h1>
						<div class="btn_shadow">
						  <div class="btn long"><a href="<?=BASEURL?>coverage/map">CHECK COVERAGE</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="g3 container-fluid">
	<div class="container">
		<h4>4G data network</h4>
		<p>4G is a faster data network with greater capacity and significantly 
improved speeds. This allows for faster downloading, smoother streaming,
and a better overall experience.</p>
	</div>
</div>


<div class="questions container-fluid">
	<div class="container">
	<h4>Questions about coverage ?</h4>
	<div class="info">
		<div class="title">
		<p>What data speeds can I expect to get?</p>
		<img src="<?=BASEURL?>public/img/arrow_down.png" class="arrow">
		</div>
		<div class="content">
			<p>
				The data speeds you’re able to achieve will depend in part on the type of device you use to access the network, and your travel destination. It's important to use a device capable of the network type you desire, and be in a location that has cellular network coverage.
			</p>
		</div>
	</div>
	<div class="info">
		<div class="title">
		<p>Can I get coverage anywhere in the Canada?</p>
		<img src="<?=BASEURL?>public/img/arrow_down.png" class="arrow">
		</div>
		<div class="content">
			<p>Please check the coverage map to see where in Canada you will receive coverage.</p>
		</div>
	</div>
	<div class="info">
		<div class="title">
		<p>What factors will affect coverage?</p>
		<img src="<?=BASEURL?>public/img/arrow_down.png" class="arrow">
		</div>
		<div class="content">
			<p>Depending on where in Canada you are using you device, coverage may get affected.</p>
		</div>
	</div>
	<p class="not_see">Don’t see question you are looking for?
		<a href="<?=BASEURL?>faq">Browse Here</a></p>
	</div>
</div>
</div>
<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>
<div class="about_us">
<div class="coverage">
<div class="top_poster container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8">
				<div class="box_container">
					<div class="box">
						<h1>Contact us</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content container-fluid">
	<div class="container">
	
	<div class="row">
	
	<div class="col-sm-4">
	<div class="contact-info">
	<h3><i><img src="https://www.canadiansimcards.com/public/img/address-icon.png"></i> 3-643 Chrislea Road, 
			Woodbridge,<br> ON, 
			L4L 8A3 </h3>
	
		<h3><i><img src="https://www.canadiansimcards.com/public/img/phone-i.png"></i> 647-338-8819 </h3>
		
		<h3><i><img src="https://www.canadiansimcards.com/public/img/mail-ico.png"></i> info@candiansimcards.com</h3>
	</div>
	</div>
	
	<div class="col-sm-8">
	
	<div class="contact-map">
	
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2879.3276038384442!2d-79.54660148485767!3d43.80756367911632!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b2f7649d44f41%3A0x2bcfe49ebefd5f76!2sValue%20Mobile!5e0!3m2!1sen!2sin!4v1608839553156!5m2!1sen!2sin" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	
	</div>
	
	</div>
	
	</div>
	
	
		
	</div>
</div>
</div>
</div>
<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

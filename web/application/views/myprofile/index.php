<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Billing</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
</style>
<body>
<?=load_view('common/header')?>
<div class="myprofile admin_page">
<div class="container-fluid">
<div class="container">
	<!-- <p class="all_plans">
		<a href="<?=BASEURL?>cart"><img src="<?=BASEURL?>public/img/left_arrow.png">All Plans</a>
	</p> -->
	<h1>My Profile</h1>
	<!-- <p class="underheader">1 Saved address</p> -->
	<!-- <p class="underheader_2">Shipping address</p> -->

	<div class="row">
    <div class="col-md-6 offset-md-3">
			<div class="new_shipping">
				<form action="<?=BASEURL?>myprofile/submit_profile" method="post">
					<div class="input_container">
						<input type="text" name="full_name" required value="<?=$profile[0]['full_name']?>">
						<span>Full Name*</span>
					</div>
					<div class="input_container">
						<input type="text" name="email" required value="<?=$profile[0]['email']?>">
						<span>Email*</span>
					</div>
					<!-- <div class="input_container">
						<input type="text" name="password" required>
						<span>Password*</span>
					</div> -->

					<button type="submit" name="btn_submit" value="Save" class="btn btn1">Save</button>
					<button type="submit" name="btn_submit" value="Cancel" class="btn">Cancel</button>
					<br><br>
				</form>
			</div>
		</div>
	</div>

</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

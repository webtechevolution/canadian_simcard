<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>

<body>
<?=load_view('common/header')?>

<div class="blog_post">
<div class="admin_page">
	<p class="all_plans">
	<a href="<?=BASEURL?>blog"><img src="<?=BASEURL?>public/img/left_arrow.png">Blog Home</a>
	</p>
</div>
<div class="container-fluid post">
	<div class="container">
		<h1><?=$_['title']?></h1>
		<!-- <p class="date">Melinda May <span>&#9670;</span> 23 May </p> -->
		<img src="<?=BASEURL?>files/source/<?=$_['image']?>" class="featured_img img-fluid">
		<div class="content">
			<?=$_['content']?>

			<!-- <div class="share">
				<p>Share the article :</p>
				<a href="#">
					<img src="<?=BASEURL?>public/img/fb.png">
				</a>
				<a href="#">
					<img src="<?=BASEURL?>public/img/twitter.png">
				</a>
			</div> -->
		</div>
	</div>
</div>

<div class="container-fluid read_next">
	<div class="container">
		<h5>Read Next</h5>
		<div class="row">
			<?php foreach ($blogs as $_): ?>
			<div class="col-md-4">
				<div class="article">
				<div class="row">
					<div class="col-8">
						<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><h4><?=$_['title']?></h4></a>
						<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><p><?=$_['short_description']?></p></a>	
					</div>
					<div class="col-4">
					<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><img src="<?=BASEURL?>files/source/<?=$_['image']?>" class="img-fluid"></a>
					</div>
				</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>


</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

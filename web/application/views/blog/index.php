<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
</style>
<body>
<?=load_view('common/header')?>

<div class="blog">
<div class="top_poster container-fluid">
	<div class="container">
		<h1>Blog posts</h1>
	</div>
</div>
<div class="posts container-fluid">
	<div class="container">
		<?php if(isset($blogs[0])): $_ = array_shift($blogs);?>
		<div class="featured post">
			<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><img src="<?=BASEURL?>files/source/<?=$_['image']?>" class="img-fluid"></a>
			<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><h2><?=$_['title']?></h2></a>
			<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><?=$_['short_description']?></a>
			<!-- <div class="info">
			  <span class="author">Melinda May</span>&#9670;
			  <span class="date">23 May</span>&#9670;
			  <span class="date">2 min read</span>
			</div> -->
		</div>
		<?php endif;?>
		<?php foreach ($blogs as $_):?>
		<div class="post">
			<div class="row">
				<div class="col-md-9 col-8">
					<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><h3><?=$_['title']?></h3></a>
					<a href="<?=BASEURL?>blog/post/<?=$_['url']?>"><?=$_['short_description']?></a>
					<!-- <div class="info">
					  <span class="author">Melinda May</span>&#9670;
					  <span class="date">23 May</span>&#9670;
					  <span class="date">2 min read</span>
					</div> -->
				</div>
				<div class="col-md-2 offset-md-1 text-right col-4">
					<a href="<?=BASEURL?>blog/post"><img src="<?=BASEURL?>files/source/<?=$_['image']?>" class="img-fluid"></a>
				</div>
			</div>
		</div>
		<?php endforeach;?>

		<!-- <div class="btn_shadow">
			<div class="btn load_more long">Load More</div>
		</div> -->
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

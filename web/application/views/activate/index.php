<!doctype html>
<html lang="en">
	<head>
		<title>Sim Activated</title>
		<?=load_view('common/files')?>
	</head>
	<style type="text/css">
    .textbox {
      padding: 5px 10px !important;
		}
		thead {
      font-weight: 800;
    }
    button {
      width: 20%;
		}
		.form_error {
			color: red;
			top: 5px;
			position: relative;
		}
  </style>
	<body>
		<?=load_view('common/header')?>

		<div class="sim_activated activated_sim admin_page">
			<div class="top_poster">
				<div class="container">
					<h1>Activate your sim</h1>
					<h4>Please enter the sim id number present on the back of the sim</h4>

					<div class="row">
            <div class="col-md-10 offset-md-1">
							<!-- <form method="post" id="addSimDetailForm" action="javascript:void(0)" action="<?=BASEURL?>activate/verify/<?=$orderItems[0]['order_id']?>"> -->
							<form method="post" id="addSimDetailForm" action="javascript:void(0)">
								<table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Plan</th>
											<th>Price</th>
											<th>SIM Card Number</th>
											<!--<th>Phone Number</th>-->
										</tr>
									</thead>
									<tbody>
										<?php foreach($orderItems as $key=>$value): ?>
											<tr class="odd gradeX">
												<td><?=$value['title']?></td>
												<td>$<?=$value['price']?></td>
												<td>
													<input class="textbox" type="text" id="sim_number_<?=$key?>" name="sim_number_<?=$key?>" required maxlength="20" minlength="16" placeholder="SIM Number">
													<span class="form_error" id="sim_number_error_<?=$key?>"></span>
												</td>
												<!--<td>
													<input class="textbox" type="text" id="phone_number_<?=$key?>" name="phone_number_<?=$key?>" required placeholder="Phone Number" minlength="10" maxlength="10">
													<span class="form_error" id="phone_number_error_<?=$key?>"></span>
												</td>-->
											</tr>
										<?php endforeach;?>
									</tbody>
								</table>
								<div class="underform text-center">
									<div class="btn_shadow">
										<button type="submit" class="btn">Activate SIM</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?=load_view('common/js')?>
		<script type="text/javascript">
      $(document).ready(function(){
        var orderItems = <?php echo json_encode($orderItems); ?>;

        function checkSimNumber (arr, value, index) {
          var areSimSame = false;
          for (var i=0; i<arr.length; i++) {
            if (i != index) {
              if (arr[i] == value) {
                areSimSame = true;
              }
            }
          }
          return areSimSame;
				}
				
				function validateSIM(simnum){
					var check = simnum.toString().slice(-1);
					var simnumber =  simnum.toString().substring(0, simnum.length -1);
					var singleDigit = 0;
					var result = 0;
					var multiply = 0;
					var sum = 0;
					var splitsum =0;
					for (var i = 0; i < simnumber.length; i++) {
						singleDigit = parseInt(simnumber.charAt(i));

						if( i % 2 == 0){
							multiply  = singleDigit * 2;
							if( multiply > 9){
								splitsum = 0;
								multiply = multiply.toString();
								for (var i1 = 0; i1 < 2; i1++){
									splitsum += parseInt(multiply.charAt(i1));
								}
								sum += splitsum
							} else{
								sum +=  parseInt(multiply);
							}
						} else {
						sum += singleDigit;
						}
					}
					sum = sum * 9;
					var result1 = sum % 10;
					if(parseInt(result1) == parseInt(check)){
						return true;
					}
					return false;
				}

        $('#addSimDetailForm').submit(function(event) {
          var counter = 0,
            hasError = false,
            simNumberArray = [], areSimSame = false;
			//phoneNumberArray = [], arePhoneSame = false;
		  orderItems.forEach(function(data, index) {
            var sim_number = $("#sim_number_" + index).val();
            simNumberArray.push(sim_number.replace(/\s+/g, '').toString());
          
			//var phone_number = $("#phone_number_" + index).val();
            //phoneNumberArray.push(phone_number.replace(/\s+/g, '').toString());
          });
          simNumberArray.forEach(function(data, index) {
            if (checkSimNumber(simNumberArray, data, index)) {
              areSimSame = true;
              counter++;
              $("#sim_number_error_"+index).text('Same SIM number cannot be used for more than one plans').show();
            } else if(!validateSIM(data)) {
                counter++;
                $("#sim_number_error_"+index).text('SIM Card Number is not valid.').show();
						} else {
              $("#sim_number_error_"+index).text('').hide();
            }
          });
          //phoneNumberArray.forEach(function(data, index) {
          //  if (checkSimNumber(phoneNumberArray, data, index)) {
          //    arePhoneSame = true;
          //    counter++;
          //    $("#phone_number_error_"+index).text('Same Phone number cannot be used for more than one plans').show();
          //  } else {
          //    $("#phone_number_error_"+index).text('').hide();
          //  }
          //});

          if (counter === 0) {
						orderId = orderItems[0]['order_id'];
						// alert(orderId);
						$('#addSimDetailForm').attr('action', '<?=BASEURL?>activate/verify/'+orderId);
          } else {
            event.preventDefault();
          }
        });
      });
    </script>
	</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Sim Confirmed</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="sim_activated">
<div class="main_page">
	<div class="how_works container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_3.png">
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<div class="top_poster">
	<div class="container">
		<!-- <h1>Sim activated!</h1>
		<h4>Your sim is now activated. Just pop the sim in & enjoy the service</h4> -->
		<h1>Sim Confirmed!</h1>
		<h4>Your SIM will be activated in 1-2 business days</h4>
		<div class="btn_shadow">
			<a href="<?=BASEURL?>"><div class="btn">Go to Homepage</div></a>
		</div>
	</div>
</div>
</div>

<?=load_view('common/js')?>

</body>
</html>

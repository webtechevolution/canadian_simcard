<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Order Error</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="sim_activated order_confirmed">
<div class="main_page">
	<div class="how_works container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_4.png">
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<div class="top_poster">
	<div class="container">
		<h1>Error!</h1>
		<h4>There was an error with your sim card Number. Please try again.</h4>
		<div class="btn_shadow">
			<a href="<?=BASEURL?>"><div class="btn">Go to Homepage</div></a>
		</div>
	</div>
</div>
</div>

<?=load_view('common/js')?>

</body>
</html>

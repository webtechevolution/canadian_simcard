<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Plan Details</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
</style>
<body>
<?=load_view('common/header')?>
<div class="plan_details">
<div class="plan_info container-fluid">
	<div class="container">
	<div class="row">
		<div class="col-md-7">
		  <p class="all_plans">
		  	<a href="<?=BASEURL?>allplans"><img src="<?=BASEURL?>public/img/left_arrow.png">All Plans</a>
		  </p>
		  <h1><?=$_['title']?></h1>
		  <div class="price">
			  	<p class="number">$<?=$_['price']?></p>
			  	<p class="month">.00<br><span>/month</span></p>
		   </div>
		   <div class="btn_shadow">
			  	<!-- <div class="btn get_plan">Get Plan</div> -->
			  	<div class="btn"><a href="<?=BASEURL?>cart/add/<?=$_['id']?>">Get Plan</a></div>
			</div>
		</div>
		<div class="col-md-4 md-offset-1">
			<h3>Features</h3>
			<ul>
				<?php if($_['plan_detail1']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail1']?></li>
				<?php endif;?>
				<?php if($_['plan_detail2']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail2']?></li>
				<?php endif;?>
				<?php if($_['plan_detail3']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail3']?></li>
				<?php endif;?>
				<?php if($_['plan_detail4']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail4']?></li>
				<?php endif;?>
				<?php if($_['plan_detail5']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail5']?></li>
				<?php endif;?>
				<?php if($_['plan_detail6']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail6']?></li>
				<?php endif;?>
				<?php if($_['plan_detail7']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail7']?></li>
				<?php endif;?>
				<?php if($_['plan_detail8']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail8']?></li>
				<?php endif;?>
				<?php if($_['plan_detail9']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail9']?></li>
				<?php endif;?>
				<?php if($_['plan_detail10']!=''): ?>
					<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail10']?></li>
				<?php endif;?>	

				<p class="disclaimer">
					* Continue using data at a reduced speed untill your next anniversary date.
				</p> 		
			</ul>
		</div>
	</div>
	</div>
</div>
<div class="main_page">
	<div class="qna container-fluid">
		<div class="container">
			<h4>Got a question?<br> <span>We’ve the answers</span></h4>
			<div class="desktop">
				<div class="row">
					<?php foreach ($faq as $cat): ?>
					<div class="col-md-3 col-sm-6">
						<p class="title"><?=$cat['name']?></p>
						<ul>
							<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
							<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
							<?php endforeach;?>
							<li class="show_all"><a href="<?=BASEURL?>faq">Show all</a></li>
						</ul>
					</div>
					<?php endforeach;?>
				</div>
			</div>
			<div class="mobile">
				<?php foreach ($faq as $cat): ?>
				<div class="info_box">
					<div class="title">
						<p><?=$cat['name']?></p><img src="<?=BASEURL?>public/img/arrow_up.png" class="arrow">
					</div>
					<div class="content">
						<ul>
					  		<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
							<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
							<?php endforeach;?>
						</ul>
					</div>
				</div>
				<?php endforeach;?>
			</div>
			
		</div>
	</div>
</div>

<div class="all_plans similar_plans">
	<div class="top_poster container-fluid">
	<div class="container">
		<h4>Similar plans</h4>
		<div class="row">

			<?php foreach ($plans as $_): ?>
			<div class="col-md-6">
				<div class="box_container">
					<div class="box">
						<h3><?=$_['title']?></h3>
						<div class="price">
							<p class="number">$<?=$_['price']?></p>
							<p class="month">.00<br><span>/month</span></p>
						</div>
						<div class="list_container">
						<ul>
							<?php if($_['plan_detail1']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail1']?></li>
							<?php endif;?>
							<?php if($_['plan_detail2']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail2']?></li>
							<?php endif;?>
							<?php if($_['plan_detail3']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail3']?></li>
							<?php endif;?>
							<?php if($_['plan_detail4']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail4']?></li>
							<?php endif;?>
							<?php if($_['plan_detail5']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail5']?></li>
							<?php endif;?>
							<?php if($_['plan_detail6']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail6']?></li>
							<?php endif;?>
							<?php if($_['plan_detail7']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail7']?></li>
							<?php endif;?>
							<?php if($_['plan_detail8']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail8']?></li>
							<?php endif;?>
							<?php if($_['plan_detail9']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail9']?></li>
							<?php endif;?>
							<?php if($_['plan_detail10']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail10']?></li>
							<?php endif;?>	
						</ul>
						</div>
						<div class="row">
							<div class="col-6 text-right">
								<div class="btn_shadow">
									<div class="btn"><a href="<?=BASEURL?>cart/add/<?=$_['id']?>">Get Plan</a></div>
								</div>
							</div>
							<div class="col-6">
								<p class="view_details"><a href="<?=BASEURL?>plandetails/select/<?=$_['id']?>">View Details</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

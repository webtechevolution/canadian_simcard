<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
</style>
<body>
<?=load_view('common/header')?>

<div class="terms">
<div class="container-fluid top_poster">
	<div class="container">
		<h1>Privacy Notice</h1>
	</div>
</div>
<div class="content container-fluid">
	<div class="container">
		<div class="section">
			<p>This privacy notice discloses the privacy practices for Canadiansimcards.com This privacy notice applies solely to information collected by this website. It will notify you of the following:</p>

			<ul>
				<li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li>
				<li>What choices are available to you regarding the use of your data.</li>
				<li>The security procedures in place to protect the misuse of your information.</li>
				<li>How you can correct any inaccuracies in the information.</li>
			</ul>
		</div>
		<div class="section">
			<h2>Information Collection, Use, and Sharing</h2>
			<p>We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. </p>

			<p>We will not sell or rent this information to anyone.</p>

			<p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. to ship an order.</p>

			<p>Unless you ask us not to, we may contact you via email in the future to tell you about specials, new products or services, or changes to this privacy policy.</p>
		</div>

		<div class="section">
			<h2>Your Access to and Control Over Information</h2>
			<p>You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p>

			<ul>
				<li>See what data we have about you, if any.</li>
				<li>Change/correct any data we have about you.</li>
				<li>Have us delete any data we have about you.</li>
				<li>Express any concern you have about our use of your data.</li>
			</ul>
			
		</div>

		<div class="section">
			<h2>Security</h2>

			<p>We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p>

			<p>Wherever we collect sensitive information (such as credit card data), that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.</p>
			
			<p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job (for example, billing or customer service) are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>
		</div>

	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

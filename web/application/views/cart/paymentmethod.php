<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Plan Details</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
	/*.detaild_err{background: #FFBABA; color: #D8000C; padding: 8px; margin-bottom: 15px;}*/
	.detaild_err ul{background: #FFBABA; padding: 7px;}
	.detaild_err ul li { color: #D8000C !important; padding: 5px; font-size: 12px !important;   }
	.err{ border-color: #D8000C !important; }
</style>
<body>
<?=load_view('common/empty_header')?>
<div class="shipping payment admin_page">
<div class="container-fluid">
	<div class="container">
		<!-- <p class="all_plans">
			<a href="<?=BASEURL?>cart/shipping"><img src="<?=BASEURL?>public/img/left_arrow.png">Billing & shipping</a>
		</p> -->
		<h1>Payment Details</h1>

		<div class="row">
			<!-- <div class="col-sm-5">
				<div class="pay_paypal">
					<p>Pay with
					    <img src="<?=BASEURL?>public/img/paypal_logo.png">
					</p>
				</div>
			</div>
			<div class="col-sm-2">
				<p class="or">Or</p>
			</div> -->
			<div class="col-sm-5 offset-sm-3">
				<div class="pay_card">
					<!-- <p class="pay">Pay with credit card</p> -->

					<div>
						<input type="hidden" class="address" value="<?=$address['address']?>"/>
						<input type="hidden" class="city" value="<?= $address['city'] ?>"/>
						<input type="hidden" class="state" value="<?= $address['province'] ?>"/>
						<input type="hidden" class="zip" value="<?= $address['zip'] ?>"/>
						<input type="hidden" class="country" value="<?= $address['country'] ?>"/>
					</div>
					<form action="<?=BASEURL?>cart/pay" method="post" id="payment-form">
						<div class="input_container">
							<input type="text" name="card_name" class="card-holder-name" placeholder="Enter name" >
							<span>Name on Card *</span>
						</div>
						<div class="input_container">
							<input type="number" name="card_number" class="card-number" placeholder="xxxxxxxxxxxxxxxx"  pattern="\d*">
							<span>Card Number *</span>
							<img src="<?=BASEURL?>public/img/lock.png">
						</div>
						<div class="row">
							<div class="col-6">
								<div class="select_container">
									<select name="month"  class="card-expiry-month">
										<option value="01" selected="selected">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
									<img src="<?=BASEURL?>public/img/arrow_down.png">
									<span>Expiry Date *</span>
								</div>
							</div>
							<div class="col-6">
								<div class="select_container">
									<select name="year"  class="card-expiry-year">
										<?php $count = date('y');
										for ($i=0; $i < 20; $i++) {
											echo "<option>".($count+$i)."</option>";
										}
										?>
									</select>
									<img src="<?=BASEURL?>public/img/arrow_down.png">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="input_container">
									<input type="text" name="cvv" placeholder="xxx" class="card-cvc"  pattern="\d*">
									<span>Security Code *</span>
								</div>
							</div>
							<div class="col-6">
								<div class="we_accept">
									<p>We accept</p>
									<img src="<?=BASEURL?>public/img/mastercard.png">
									<img src="<?=BASEURL?>public/img/visa.png" class="visa">
									<img src="<?=BASEURL?>public/img/americanexpress.png">
								</div>
							</div>
						</div>
						<div class="input_container payment-errors" style="display:none; color: maroon;"></div>
						<div class="btn_shadow">
							<button class="btn" type="submit" id="pay_btn">Pay</button>
							<?php if ($sim_custom_added != "yes"): ?>
								<br/>
								<br/>
								<a href="<?=BASEURL?>activateplans" class="btn long">Continue Shopping</a>
							<?php endif; ?>
						</div>
					</form>
				</div>

				<hr>

				<div id='paypal'></div>

			</div>
		</div>
	</div>
</div>
</div>
<?php if ($sim_custom_added != "yes"): ?>
	<?=load_view('common/footer')?>
<?php endif; ?>
<?=load_view('common/js')?>


<script src="https://www.paypal.com/sdk/js?client-id=AV-jBZohaplYHN-p8hAVbey4oZH9W6Pw2D4jcsXUjsVi973jfgKccs2NR3-6VKn5t475xBjXest_MITc&currency=USD" data-namespace="paypal_sdk"></script>

<script>
    // Render the PayPal button into #paypal-button-container
    /*
    paypal_sdk.Buttons({
        // Set up the transaction
        createOrder: function(data, actions) {
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '0.01'
                    }
                }]
            });
        },
        // Finalize the transaction
        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                // Show a success message to the buyer
                alert('Transaction completed by ' + details.payer.name.given_name + '!');
            });
        }
    }).render('#paypal');
    */
</script>


<script src="https://js.stripe.com/v2/"></script>
<script>
function ValidateCreditCardNumber($cardNo) {

	  var ccNum = $cardNo;
	  var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
	  var mastercardRegEx = /^(?:5[1-5][0-9]{14})$/;
	  var amexpRegEx = /^(?:3[47][0-9]{13})$/;
	  var discovRegEx = /^(?:6(?:011|5[0-9][0-9])[0-9]{12})$/;
	  var isValid = false;

	  if (visaRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(mastercardRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(amexpRegEx.test(ccNum)) {
	    isValid = true;
	  } else if(discovRegEx.test(ccNum)) {
	    isValid = true;
	  }

	  if(isValid) {
	     return true;
	  } else {
	     return false;
	  }
}

	jQuery(document).ready(function($){
	    jQuery("#payment-form").submit(function($){
	        $ct = 0;
	        jQuery(this).find(".err").removeClass('err');	       
	        jQuery(this).find(".detaild_err").remove();	       
	        $name = jQuery(".card-holder-name").val();
	        $card_number = jQuery(".card-number").val();
	        $expiry_month = jQuery(".card-expiry-month").val();
	        $expiry_year = jQuery(".card-expiry-year").val();
	        $cvv = jQuery(".card-cvc").val();
	        $errors = [];
	        if( $name=="" ){
	            jQuery(".card-holder-name").addClass('err');
	            $errors.push("Card holder's name can not be empty.");
	            $ct++;
	        } 
	        if( $card_number=="" ){         
	            jQuery(".card-number").addClass('err');
	            $errors.push("Card number can not be empty.");
	            $ct++;
	        } else {
	            $regex = /[^0-9]/g;
	            if( !ValidateCreditCardNumber($card_number) ){
	                jQuery(".card-number").addClass('err');
	                $errors.push("Invalid card number.");
	                $ct++;
	            }
	        }
	        if( $expiry_month=="" || $expiry_year==""){
	        	 jQuery(".card-expiry-month").addClass('err');
	        	 jQuery(".card-expiry-year").addClass('err');
	        	 $errors.push("Invalid expiry date.");
	                $ct++;
	        }
	        if(  $cvv==""){	        	 
	        	 jQuery(".card-cvc").addClass('err');
	        	 $errors.push("CVV can not be empty.");
	               $ct++;
	        } 
	        else{
	        	$regex = /^[0-9]{3,4}$/;
	            if( !$regex.test($cvv) ){
	                jQuery(".card-cvc").addClass('err');
	                $errors.push("Invalid card CVV.");
	                $ct++;
	            }
	        }
	        if($ct > 0){
	        	$detaild_err = '<div class="detaild_err"><ul>';
	        	jQuery( $errors ).each(function( index, element ) {
				   $detaild_err+='<li> - '+element+'</li>';
				});
	        	$detaild_err+= '</ul></div>';
	        	jQuery(this).prepend($detaild_err);
	        	return false;
	        }

	    })
})

// var stripe = Stripe('pk_test_H2lNOADh9MNZrvdjlQLcJo8L');
// Stripe.setPublishableKey('pk_test_H2lNOADh9MNZrvdjlQLcJo8L');
// Stripe.setPublishableKey('pk_live_oTRxEz6v2pcmhaofAGruG70S');

// function stripeResponseHandler(status, response) {
// 	console.log('response', response);
// 	if (response.error) {
// 		$(".payment-errors").html(response.error.message);
// 		$('.payment-errors').show();
// 	} else {
// 		var form$ = $("#payment-form");
// 		var token = response['id'];
// 		form$.append("<input type='hidden' name='token' value='" + token + "' />");
// 		form$.get(0).submit();
// 	}
// }


// $(document).ready(function(){

// 	$('#payment-form').submit(function( event ) {
// 		event.preventDefault();

// 		Stripe.card.createToken({
// 			number: $('.card-number').val(),
// 			cvc: $('.card-cvc').val(),
// 			exp_month: $('.card-expiry-month').val(),
// 			exp_year: $('.card-expiry-year').val(),
// 			name: $('.card-holder-name').val(),

// 			address_line1: $('.address').val(),
// 			address_city: $('.city').val(),
// 			address_zip: $('.zip').val(),
// 			address_state: $('.state').val(),
// 			address_country: $('.country').val()
// 		}, stripeResponseHandler);
// 		return false;
// 	});
// });

</script>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Order Confirmed</title>
<?=load_view('common/files')?>
<!-- Global site tag (gtag.js) - Google Ads: 625313153 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-625313153"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'AW-625313153');
</script>

<!-- Event snippet for Submit lead form conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-625313153/dvyMCIbX6O0BEIGLlqoC'});
</script>
<style>
	.top_poster {
		margin-bottom: 20px !important;
	}
</style>
</head>
<body>
<?=load_view('common/header')?>

<div class="sim_activated order_confirmed">
<div class="main_page">
	<div class="how_works container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_4.png">
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<div class="top_poster">
	<div class="container">
		<h1>Order confirmed!</h1>
		<?php if($sim_custom_added == 'yes'): ?>
			<h4>Thank you for your purchase.</h4>
		<?php else: ?>
			<h4>Thank you for your purchase. A confirmation email will be delivered in your mailbox</h4>
		<?php endif;?>
		<?php if($plan_with_sim_added == 1): ?>
			<h4>- SIM Card will be shipped and confirmation email will be sent within 24 hours</h4>
		<?php endif;?>
		<?php if($plan_without_sim_added == 1): ?>
			<h4>- Activation process may take max 24 hours and confirmation will be sent as soon as the activation is complete</h4>
		<?php endif;?>
		<!-- <h4>We’ll send you a shipping confirmation email as soon as your order ships</h4> -->
		<div class="btn_shadow">
			<a href="<?=BASEURL?>"><div class="btn">Go to Homepage</div></a>
		</div>
	</div>
</div>
</div>

<?=load_view('common/js')?>

</body>
</html>

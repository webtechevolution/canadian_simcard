<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | Shipping</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
</style>
<body>
<?=load_view('common/empty_header')?>
<div class="shipping admin_page">
<div class="container-fluid">
<div class="container">
	<p class="all_plans">
		<!-- <a href="<?=BASEURL?>cart"><img src="<?=BASEURL?>public/img/left_arrow.png">Shopping Cart</a> -->
		<a href="<?=BASEURL?>cart">Shopping Cart</a>
		<?php if ($sim_custom_added != "yes"): ?>
      <a href="<?=BASEURL?>activateplans">Continue Shopping</a>
    <?php endif; ?>
	</p>
	<h1>Shipping Details</h1>
	<!-- <p class="underheader">1 Saved address</p> -->
	<!-- <p class="underheader_2">Shipping address</p> -->

	<div class="row">
		<?php $count = 0; ?>
		<?php $first_id = isset($address[0])?$address[0]['id']:''; ?>
		<?php foreach ($address as $key => $_): ?>
		<div class="col-md-4 offset-md-1">
			<div class="input_container">
				<input class="address" type="radio" name="shipping" id="shipping_saved<?=$_['id']?>" value="<?=$_['id']?>" <?=($count == 0)?'checked':''?> >
				<label for="shipping_saved<?=$_['id']?>" class="radio"><?=$_['full_name']?></label>
			</div>
			<ul>
				<li><?=$_['address']?></li>
				<li><?=$_['city']?>, <?=$_['province']?> <?=$_['zip']?> </li>
				<li><?=$_['country']?></li>
				<li><a href="<?=BASEURL?>cart/remove_address/<?=$_['id']?>">Remove</a></li>
			</ul>
		</div>
		<?php $count++; ?>
		<?php endforeach;?>

		<div class="col-md-6 offset-md-1">
			<div class="input_container">
				<input class="address" type="radio" name="shipping" id="shipping_new" value="new" <?=($count == 0)?'checked':''?>>
				<label for="shipping_new" class="radio">New address</label>
			</div>
			<div class="new_shipping">
				<form action="<?=BASEURL?>cart/add_address" method="post">
					<div class="input_container">
						<input type="text" name="full_name" required>
						<span>Full Name*</span>
					</div>
					<div class="input_container">
						<input type="text" name="address" required>
						<span>Street Address*</span>
					</div>
					<div class="input_container">
						<input type="text" id="city" name="city" required>
						<span>City*</span>
					</div>

					<div class="input_container">
						<input type="text" id="province" name="province" required>
						<span>State / Province / Region*</span>
					</div>

					<div class="input_container">
						<input type="text" name="zip" required>
						<span>Postal Code / Zip Code*</span>
					</div>

					<div class="select_container">
						<select name="country" id="country">
							<option value="CA" selected="selected">Canada</option>
							<option value="US">United States</option>
              <option value="AF">Afghanistan</option>
              <option value="AL">Albania</option>
              <option value="DZ">Algeria</option>
              <option value="AS">American Samoa</option>
              <option value="AD">Andorra</option>
              <option value="AO">Angola</option>
              <option value="AI">Anguilla</option>
              <option value="AQ">Antarctica</option>
              <option value="AG">Antigua and Barbuda</option>
              <option value="AR">Argentina</option>
              <option value="AM">Armenia</option>
              <option value="AW">Aruba</option>
              <option value="AU">Australia</option>
              <option value="AT">Austria</option>
              <option value="AZ">Azerbaijan</option>
              <option value="BS">Bahamas</option>
              <option value="BH">Bahrain</option>
              <option value="BD">Bangladesh</option>
              <option value="BB">Barbados</option>
              <option value="BY">Belarus</option>
              <option value="BE">Belgium</option>
            	<option value="BZ">Belize</option>
              <option value="BJ">Benin</option>
              <option value="BM">Bermuda</option>
              <option value="BT">Bhutan</option>
              <option value="BO">Bolivia</option>
              <option value="BA">Bosnia and Herzegowina</option>
              <option value="BW">Botswana</option>
              <option value="BV">Bouvet Island</option>
              <option value="BR">Brazil</option>
              <option value="IO">British Indian Ocean Territory</option>
              <option value="BN">Brunei Darussalam</option>
              <option value="BG">Bulgaria</option>
              <option value="BF">Burkina Faso</option>
              <option value="BI">Burundi</option>
              <option value="KH">Cambodia</option>
              <option value="CM">Cameroon</option>
              <option value="CV">Cape Verde</option>
              <option value="KY">Cayman Islands</option>
              <option value="CF">Central African Republic</option>
              <option value="TD">Chad</option>
              <option value="CL">Chile</option>
            	<option value="CN">China</option>
              <option value="CX">Christmas Island</option>
              <option value="CC">Cocos (Keeling) Islands</option>
              <option value="CO">Colombia</option>
              <option value="KM">Comoros</option>
              <option value="CG">Congo</option>
              <option value="CD">Congo, the Democratic Republic of the</option>
              <option value="CK">Cook Islands</option>
              <option value="CR">Costa Rica</option>
              <option value="CI">Cote d'Ivoire</option>
              <option value="HR">Croatia (Hrvatska)</option>
              <option value="CU">Cuba</option>
              <option value="CY">Cyprus</option>
              <option value="CZ">Czech Republic</option>
              <option value="DK">Denmark</option>
              <option value="DJ">Djibouti</option>
              <option value="DM">Dominica</option>
              <option value="DO">Dominican Republic</option>
              <option value="TP">East Timor</option>
              <option value="EC">Ecuador</option>
              <option value="EG">Egypt</option>
              <option value="SV">El Salvador</option>
              <option value="GQ">Equatorial Guinea</option>
              <option value="ER">Eritrea</option>
              <option value="EE">Estonia</option>
              <option value="ET">Ethiopia</option>
              <option value="FK">Falkland Islands (Malvinas)</option>
              <option value="FO">Faroe Islands</option>
              <option value="FJ">Fiji</option>
              <option value="FI">Finland</option>
              <option value="FR">France</option>
              <option value="FX">France, Metropolitan</option>
              <option value="GF">French Guiana</option>
              <option value="PF">French Polynesia</option>
              <option value="TF">French Southern Territories</option>
              <option value="GA">Gabon</option>
              <option value="GM">Gambia</option>
            	<option value="GE">Georgia</option>
              <option value="DE">Germany</option>
              <option value="GH">Ghana</option>
              <option value="GI">Gibraltar</option>
              <option value="GR">Greece</option>
              <option value="GL">Greenland</option>
              <option value="GD">Grenada</option>
              <option value="GP">Guadeloupe</option>
              <option value="GU">Guam</option>
              <option value="GT">Guatemala</option>
              <option value="GN">Guinea</option>
              <option value="GW">Guinea-Bissau</option>
              <option value="GY">Guyana</option>
          		<option value="HT">Haiti</option>
              <option value="HM">Heard and Mc Donald Islands</option>
              <option value="VA">Holy See (Vatican City State)</option>
              <option value="HN">Honduras</option>
              <option value="HK">Hong Kong</option>
              <option value="HU">Hungary</option>
              <option value="IS">Iceland</option>
              <option value="IN">India</option>
              <option value="ID">Indonesia</option>
              <option value="IR">Iran (Islamic Republic of)</option>
              <option value="IQ">Iraq</option>
              <option value="IE">Ireland</option>
              <option value="IL">Israel</option>
              <option value="IT">Italy</option>
              <option value="JM">Jamaica</option>
              <option value="JP">Japan</option>
              <option value="JO">Jordan</option>
              <option value="KZ">Kazakhstan</option>
              <option value="KE">Kenya</option>
              <option value="KI">Kiribati</option>
              <option value="KP">Korea, Democratic People's Republic of</option>
              <option value="KR">Korea, Republic of</option>
              <option value="KW">Kuwait</option>
              <option value="KG">Kyrgyzstan</option>
              <option value="LA">Lao People's Democratic Republic</option>
              <option value="LV">Latvia</option>
              <option value="LB">Lebanon</option>
              <option value="LS">Lesotho</option>
              <option value="LR">Liberia</option>
              <option value="LY">Libyan Arab Jamahiriya</option>
              <option value="LI">Liechtenstein</option>
              <option value="LT">Lithuania</option>
              <option value="LU">Luxembourg</option>
              <option value="MO">Macau</option>
              <option value="MK">Macedonia, The Former Yugoslav Republic of</option>
              <option value="MG">Madagascar</option>
              <option value="MW">Malawi</option>
              <option value="MY">Malaysia</option>
              <option value="MV">Maldives</option>
              <option value="ML">Mali</option>
              <option value="MT">Malta</option>
              <option value="MH">Marshall Islands</option>
              <option value="MQ">Martinique</option>
              <option value="MR">Mauritania</option>
              <option value="MU">Mauritius</option>
              <option value="YT">Mayotte</option>
              <option value="MX">Mexico</option>
              <option value="FM">Micronesia, Federated States of</option>
              <option value="MD">Moldova, Republic of</option>
              <option value="MC">Monaco</option>
              <option value="MN">Mongolia</option>
              <option value="MS">Montserrat</option>
              <option value="MA">Morocco</option>
              <option value="MZ">Mozambique</option>
              <option value="MM">Myanmar</option>
              <option value="NA">Namibia</option>
              <option value="NR">Nauru</option>
              <option value="NP">Nepal</option>
              <option value="NL">Netherlands</option>
              <option value="AN">Netherlands Antilles</option>
              <option value="NC">New Caledonia</option>
              <option value="NZ">New Zealand</option>
              <option value="NI">Nicaragua</option>
              <option value="NE">Niger</option>
              <option value="NG">Nigeria</option>
              <option value="NU">Niue</option>
              <option value="NF">Norfolk Island</option>
              <option value="MP">Northern Mariana Islands</option>
              <option value="NO">Norway</option>
              <option value="OM">Oman</option>
              <option value="PK">Pakistan</option>
              <option value="PW">Palau</option>
              <option value="PA">Panama</option>
              <option value="PG">Papua New Guinea</option>
              <option value="PY">Paraguay</option>
              <option value="PE">Peru</option>
              <option value="PH">Philippines</option>
              <option value="PN">Pitcairn</option>
              <option value="PL">Poland</option>
              <option value="PT">Portugal</option>
              <option value="PR">Puerto Rico</option>
              <option value="QA">Qatar</option>
              <option value="RE">Reunion</option>
              <option value="RO">Romania</option>
              <option value="RU">Russian Federation</option>
              <option value="RW">Rwanda</option>
              <option value="KN">Saint Kitts and Nevis</option>
              <option value="LC">Saint LUCIA</option>
              <option value="VC">Saint Vincent and the Grenadines</option>
              <option value="WS">Samoa</option>
              <option value="SM">San Marino</option>
              <option value="ST">Sao Tome and Principe</option>
              <option value="SA">Saudi Arabia</option>
              <option value="SN">Senegal</option>
              <option value="SC">Seychelles</option>
              <option value="SL">Sierra Leone</option>
              <option value="SG">Singapore</option>
              <option value="SK">Slovakia (Slovak Republic)</option>
              <option value="SI">Slovenia</option>
              <option value="SB">Solomon Islands</option>
              <option value="SO">Somalia</option>
              <option value="ZA">South Africa</option>
              <option value="GS">South Georgia and the South Sandwich Islands</option>
              <option value="ES">Spain</option>
              <option value="LK">Sri Lanka</option>
							<option value="SH">St. Helena</option>
							<option value="PM">St. Pierre and Miquelon</option>
							<option value="SD">Sudan</option>
							<option value="SR">Suriname</option>
							<option value="SJ">Svalbard and Jan Mayen Islands</option>
							<option value="SZ">Swaziland</option>
							<option value="SE">Sweden</option>
							<option value="CH">Switzerland</option>
							<option value="SY">Syrian Arab Republic</option>
							<option value="TW">Taiwan, Province of China</option>
							<option value="TJ">Tajikistan</option>
							<option value="TZ">Tanzania, United Republic of</option>
							<option value="TH">Thailand</option>
							<option value="TG">Togo</option>
							<option value="TK">Tokelau</option>
							<option value="TO">Tonga</option>
							<option value="TT">Trinidad and Tobago</option>
							<option value="TN">Tunisia</option>
							<option value="TR">Turkey</option>
							<option value="TM">Turkmenistan</option>
							<option value="TC">Turks and Caicos Islands</option>
							<option value="TV">Tuvalu</option>
							<option value="UG">Uganda</option>
							<option value="UA">Ukraine</option>
							<option value="AE">United Arab Emirates</option>
							<option value="GB">United Kingdom</option>
							<option value="UM">United States Minor Outlying Islands</option>
							<option value="UY">Uruguay</option>
							<option value="UZ">Uzbekistan</option>
							<option value="VU">Vanuatu</option>
							<option value="VE">Venezuela</option>
							<option value="VN">Viet Nam</option>
							<option value="VG">Virgin Islands (British)</option>
							<option value="VI">Virgin Islands (U.S.)</option>
							<option value="WF">Wallis and Futuna Islands</option>
							<option value="EH">Western Sahara</option>
							<option value="YE">Yemen</option>
							<option value="YU">Yugoslavia</option>
							<option value="ZM">Zambia</option>
							<option value="ZW">Zimbabwe</option>
						</select>
						<img src="<?=BASEURL?>public/img/arrow_down.png">
						<span>Country*</span>
					</div>

					<button type="submit" class="btn">Add New Address</button>
					<br><br>
				</form>
			</div>
		</div>
	</div>

	<div class="underform text-center">
		<div class="line"></div>
		<!-- <p class="underheader_2">Billing address</p>
		<div class="input_container text-center">
			<input type="checkbox" name="same_as_shipping" id="same_as_shipping">
			<label for="same_as_shipping" class="checkbox">Same as shipping address</label>
		</div> -->
		<div class="btn_shadow <?=($count == 0 && ($first_id == 'new' || $first_id == ''))? 'hide_button': ''?>">
			<form action="<?=BASEURL?>cart/orderupdate" method="post">
				<input type="hidden" class="selected_address" name="selected_address" value="<?=$first_id?>">
        <button type="submit" class="btn">Proceed</button>
        <?php if ($sim_custom_added != "yes"): ?>
          <br/>
          <br/>
          <a href="<?=BASEURL?>activateplans" style="color: #fff" class="btn long">Continue Shopping</a>
        <?php endif; ?>
			</form>
		</div>
	</div>
</div>
</div>
</div>

<?php if ($sim_custom_added != "yes"): ?>
	<?=load_view('common/footer')?>
<?php endif; ?>
<?=load_view('common/js')?>
<script type="text/javascript">
$(document).ready(function(){

	$('.address').on('change', function( event ) {
		console.log($(this).val())
		$('.selected_address').val( $(this).val() );
  });

  var addressCount = <?php echo json_encode($address); ?>;
  if (addressCount && addressCount.length == 0) {
    $("#shipping_new").click();
  }
  
  var autocomplete = new google.maps.places.Autocomplete($("#city")[0], {});
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var selectedAddress = place.address_components;
      // console.log(selectedAddress);
      var address = '', city ='', province ='', zip = '', country = ''; 
      for (var i = 0; i < selectedAddress.length; i++) {
       var element=selectedAddress[i];
      //selectedAddress.forEach(element => {

               // if (element.types[0] === 'route' || element.types[0] === "sublocality_level_2") {
               //   address = element.long_name + ' ';    
               // }  
               // if (element.types[0] === "neighborhood" || element.types[0] === "sublocality_level_1") {
               //   address += element.long_name;    
               // }  
               if (element.types[0] === "locality") {
                 city = selectedAddress[i].long_name;
               }  
               if (element.types[0] === "administrative_area_level_1") {
                 province = element.long_name;
               }  
               if (element.types[0] === "country") {
                 country = element.short_name;
               }  
               // if (element.types[0] === "postal_code_prefix") {
               //   zip = element.long_name;
               // }  

      //});
       }
      // $('#address').val(address);
      $('#city').val(city);
      $('#province').val(province);
      $('#country').val(country);
      // $('#zip').val(zip);
  });
});
</script>
</body>
</html>

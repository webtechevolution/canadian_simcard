<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
.row .select_container span {
	width: 100%;
}
.align_checkbox {
	position: relative;
	margin: 0 auto;
	display: block !important;
}
</style>
<body>
<?=load_view('common/empty_header')?>
<div class="your_cart admin_page">
<div class="top_block container-fluid">
	<div class="container">
    <!-- <p class="all_plans">
			<?php if($plan_with_sim_added == 1): ?>
				<a href="<?=BASEURL?>cart/shipping"><img src="<?=BASEURL?>public/img/left_arrow.png">Shipping Details</a>
			<?php endif;?>
			<?php if($plan_with_sim_added == 0): ?>
				<a href="<?=BASEURL?>cart/billing"><img src="<?=BASEURL?>public/img/left_arrow.png">Billing Details</a>
			<?php endif;?>
    </p> -->
		<h1>Your Updated Cart</h1>
		<h3><?=is_array($cart) && count($cart)>0?count($cart):''?> items in cart</h3>
	</div>
</div>
<div class="items container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-lg-8 item_list">
			<div class="box_container">
				<div class="box">
					<?php
					$sub_total = 0;
					$has_shipping = 0;
					$total_quantity = 0;
					?>
					<?php foreach ($cart as $item): ?>
						<?php
							$_ = $plans[$item['id']];
							if($_['shipping']) {
								$total_quantity += $item['quantity'];
								$has_shipping = 1;
							}
						?>

						<?php $sub_total += $item['quantity'] * $_['price']; ?>
						<div class="item">
							<div class="row">
								<div class="col-sm-4">
									<?php if($sim_custom_added=="yes"): ?>
										<p class="title" style="margin: 20px 0px 0px 0px;"><?=$_['title']?></p>
									<?php else: ?>
										<p class="title"><?=$_['title']?></p>
									<?php endif; ?>
									<ul>
										<?php if($_['plan_detail1']!=''): ?>
											<li><?=$_['plan_detail1']?></li>
										<?php endif;?>
										<?php if($_['plan_detail2']!=''): ?>
											<li><?=$_['plan_detail2']?></li>
										<?php endif;?>
										<?php if($_['plan_detail3']!=''): ?>
											<li><?=$_['plan_detail3']?></li>
										<?php endif;?>
										<?php if($_['plan_detail4']!=''): ?>
											<li><?=$_['plan_detail4']?></li>
										<?php endif;?>
										<?php if($_['plan_detail5']!=''): ?>
											<li><?=$_['plan_detail5']?></li>
										<?php endif;?>
										<?php if($_['plan_detail6']!=''): ?>
											<li><?=$_['plan_detail6']?></li>
										<?php endif;?>
										<?php if($_['plan_detail7']!=''): ?>
											<li><?=$_['plan_detail7']?></li>
										<?php endif;?>
										<?php if($_['plan_detail8']!=''): ?>
											<li><?=$_['plan_detail8']?></li>
										<?php endif;?>
										<?php if($_['plan_detail9']!=''): ?>
											<li><?=$_['plan_detail9']?></li>
										<?php endif;?>
										<?php if($_['plan_detail10']!=''): ?>
											<li><?=$_['plan_detail10']?></li>
										<?php endif;?>
									</ul>
								</div>
								<div class="col-sm-3 col-lg-2 col-6">
									<div class="select_container">
										<?=$item['quantity']?>
										<!-- <select>
											<option selected>1</option>
											<option>2</option>
											<option>3</option>
										</select>
										<img src="<?=BASEURL?>public/img/arrow_down.png"> -->
										<span>Quantity</span>
									</div>
								</div>
								<div class="col-md-3 col-lg-2 col-6 d-none d-lg-block">
									<div class="select_container">
										<p class="price">$<?=$_['price']?></p>
										<span>Price</span>
									</div>
								</div>
								<div class="col-md-3 col-lg-2 col-6">
									<div class="select_container">
										<input type="checkbox" class="align_checkbox" name="with_sim" disabled value="1" <?php echo ($_['with_sim'] == '1' ? 'checked' : '');?>>
										<span>With Sim</span>
									</div>
								</div>
								<div class="col-sm-3 col-lg-2 col-6">
									<div class="select_container">	
										<?php if($sim_custom_added=="yes"): ?>
											<img style="opacity:0.3; position: relative; top: 0; right: 0;" src="<?=BASEURL?>public/img/delete_icon.png">
										<?php else: ?>
											<a href="<?=BASEURL?>cart/remove/<?=$_['id']?>">
												<!-- <p class="remove">Remove</p> -->
												<img style="position: relative; top: 0; right: 0;" src="<?=BASEURL?>public/img/delete_icon.png">
											</a>
										<?php endif; ?>										
										<span>Action</span>
									</div>
								</div>
							</div>
							<p class="price price_mobile">$<?=$_['price']?></p>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php
		$sub_total = round($sub_total, 2);
		$discount = 0;
		if(isset($discount_code[0])){
			if($discount_code[0]['type'] == 1){
				$discount = round($sub_total * $discount_code[0]['value']/100, 2);
			}
			else{
				$discount = $discount_code[0]['value'];
			}
		}

		// $shipping = 0;
		$tax = 0;
		$tax_percentage = 0;
		$total = 0;
		if ($total_quantity == 0) {
			$shipping = 0;
		}
		//calculate tax based on province dynamically
		foreach($tax_list as $key=>$_) {
			$list_province = strtolower(trim($_['province'], ""));
			$selected_province = strtolower(trim($province, ""));
			if ($selected_province == $list_province) {
				$tax_percentage = $_['tax'];
			}
		}
		// print_r($tax_percentage);
		// print_r($province);
		if($sim_custom_added=="yes") {
			$tax = round((($sub_total + $shipping + ($sim_price * $total_quantity) - $discount) * $tax_percentage), 2);
			$total = $sub_total + ($sim_price * $total_quantity) + $shipping - $discount + $tax;
		} else if($sub_total && $sim_custom_added != "yes"){
			$tax = round((($sub_total + $shipping + ($sim_price * $total_quantity) - $discount) * $tax_percentage), 2);
			$total = $sub_total + ($sim_price * $total_quantity) + $shipping - $discount + $tax;
		}
		
		if($tax<0) $tax = 0;
		if($total<0){
			$total = 0;
			$tax = 0;
		}
		if($sub_total<0) $sub_total = 0;
		// echo $total;
		?>
		<div class="col-lg-4">
			<div class="box_container">
			<div class="box summary">
				<p class="order">Order summary</p>
				<div class="componets">
					<ul>
						<li>Subtotal <span>$<?=sprintf("%.2f", $sub_total)?></span></li>
						<?php if($sim_price && $total): ?>
							<li>One-Time Sim Cost <span>$<?=sprintf("%.2f", $sim_price * $total_quantity)?></span></li>
						<?php endif; ?>
						<?php if($discount): ?>
							<li>Discount <span>$<?=sprintf("%.2f", $discount)?></span></li>
						<?php endif; ?>
						<?php if($shipping && $has_shipping): ?>
							<li>Shipping <span>$<?=sprintf("%.2f", $shipping)?></span></li>
						<?php elseif(!$has_shipping): ?>
							<li>Shipping <span>$<?=sprintf("%.2f", 0)?></span></li>
						<?php endif; ?>
						<li>Tax <span>$<?=sprintf("%.2f", $tax)?></span></li>
					</ul>
				</div>
				<div class="total">
					<ul>
						<li style="font-size: 19px;">Total <span>$<?=sprintf("%.2f", $total)?></span></li>
					</ul>
					<ul>
						<li class="admin_pages">
						  <div class="btn_shadow">
								<a href="<?=BASEURL?>cart/paymentmethod"><button class="btn long" type="submit">Proceed to Payment</button></a>
							</div>
							<?php if ($sim_custom_added != "yes"): ?>
								<br/>
								<div class="btn_shadow">
									<a href="<?=BASEURL?>activateplans"><button class="btn long" type="submit">Continue Shopping</button></a>
								</div>
							<?php endif; ?>
						</li>
					</ul>
				</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<?php if ($sim_custom_added != "yes"): ?>
	<?=load_view('common/footer')?>
<?php endif; ?>
<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
  <head>
    <title>Canadian Sim Cards | Billing</title>
    <?=load_view('common/files')?>
  </head>
  <style type="text/css">
    .textbox {
      padding: 5px 10px !important;
    }
    thead {
      font-weight: 800;
    }
    button {
      width: 20%;
    }
    .form_error {
      color: red;
      top: 5px;
      position: relative;
    }
    .admin_page input[type=radio], .admin_page input[type=checkbox]{display: inline;}
    .admin_page input[type=radio]:checked + label:before{content: "";}
  </style>
  <body>
    <?=load_view('common/empty_header')?>
    <div class="simdetail admin_page">
      <div class="container-fluid">
        <div class="container">
          <p class="all_plans">
            <a href="<?=BASEURL?>cart">Shopping Cart</a>
            <a href="<?=BASEURL?>activateplans">Continue Shopping</a>
          </p>
          <h1>SIM Card Details</h1>

          <div class="row">
              <div class="col-md-10 offset-md-1" style="padding: 20px;">
                
                <div class="col-md-12 mb-3 p-0">
                  <input type="radio" id="need_sim_card_no" name="need_sim_card" value="no">
                  <label for="css">I want to use my existing SIM card</label><br>
                </div>
                <div class="col-md-12 p-0">
                  <input type="radio" id="need_sim_card_yes" name="need_sim_card" value="yes">
                  <label for="html">I need new SIM card (selecting this option will add $<?=$sim_price?> to the shopping cart for new SIM card)</label>
                </div>
              </div>
          </div>

          <div class="row" >
            <div class="col-md-10 offset-md-1">
              <form method="post" autocomplete="off" role="form" id="addSimDetailForm" action="javascript:void(0)">
                <table id="example-1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Plan</th>
                      <th>Price</th>
                      <th>Quantity</th>
                      <th>SIM Card Number</th>
                      <!--<th>Phone Number</th>-->
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($simdetail as $key=>$value): ?>
                      <tr class="odd gradeX">
                        <td><?=$value['title']?></td>
                        <td>$<?=$value['price']?></td>
                        <td>1</td>
                        <td>
                          <input class="textbox" type="text" id="sim_number_<?=$key?>" name="sim_number_<?=$key?>" value="<?=$value['sim_number']?>" required>
                          <input type="hidden" id="store_<?=$key?>" name="store_<?=$key?>">
                          <input type="hidden" id="shipment_id_<?=$key?>" name="shipment_id_<?=$key?>">
                          <span class="form_error" id="sim_number_error_<?=$key?>"></span>
                        </td>
                        <!--<td>
                          <input class="textbox" type="text" id="phone_number_<?=$key?>" name="phone_number_<?=$key?>" value="<?=$value['phone_number']?>" required minlength="10" maxlength="10">
                          <span class="form_error" id="phone_number_error_<?=$key?>"></span>
                        </td>-->
                      </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
                <div class="underform text-center">
                  <div class="btn_shadow">
                    <button type="submit" class="btn long">Proceed</button>
                    <br/>
                    <br/>
                    <a href="<?=BASEURL?>activateplans" style="color: #fff; width: 20%;" class="btn long">Continue Shopping</a>
                  </div>
                </div>
              </form>
              <div class="underform text-center">
              <div class="btn_shadow mt-3" style="display: none;" id="hd_proceed">
                <a href="<?=BASEURL?>cart/add_sim_card/<?=$planid; ?>" style="color: #fff; width:20%; display: inline-block;" class="btn long">Proceed</a>
                <br/>
                <br/>
                <a href="<?=BASEURL?>activateplans" style="color: #fff; width:20%;" class="btn long">Continue Shopping</a>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?=load_view('common/footer')?>
    <?=load_view('common/js')?>
    <script type="text/javascript">
      $(document).ready(function(){

        $('input[type=radio][name=need_sim_card]').change(function() {
            if (this.value == 'yes') {
                $('#addSimDetailForm').hide();
                $('#hd_proceed').show();
            }
            else if (this.value == 'no') {
                $('#addSimDetailForm').show();
                $('#hd_proceed').hide();
            }
        });

        var simdetail = <?php echo json_encode($simdetail); ?>;
        
        function checkSimNumber (arr, value, index) {
          var areSimSame = false;
          for (var i=0; i<arr.length; i++) {
            if (i != index) {
              if (arr[i] == value) {
                areSimSame = true;
              }
            }
          }
          return areSimSame;
        }

        $('#addSimDetailForm').submit(function(event) {
          /*if($('input[type=radio][name=need_sim_card]:checked').val()=="yes"){
            alert('here');
            console.log('<?=BASEURL?>cart/add_sim_card');
            return window.location.href='<?=BASEURL?>cart/add_sim_card';
          }*/
          var counter = 0,
            hasError = false,
            simNumberArray = [], areSimSame = false;
          //  phoneNumberArray = [], arePhoneSame = false;
          // console.log(simdetail);
          
          simdetail.forEach(function(data, index) {
            var sim_number = $("#sim_number_" + index).val();
            simNumberArray.push(sim_number.replace(/\s+/g, '').toString());

            //var phone_number = $("#phone_number_" + index).val();
            //phoneNumberArray.push(phone_number.replace(/\s+/g, '').toString());
          });
          
          simNumberArray.forEach(function(data, index) {
            if (checkSimNumber(simNumberArray, data, index)) {
              areSimSame = true;
              counter++;
              $("#sim_number_error_"+index).text('Same SIM number cannot be used for more than one plans').show();
            } else {
              $("#sim_number_error_"+index).text('').hide();
            }
          });

          //phoneNumberArray.forEach(function(data, index) {
            //if (checkSimNumber(phoneNumberArray, data, index)) {
              //arePhoneSame = true;
              //counter++;
              //$("#phone_number_error_"+index).text('Same Phone number cannot be used for more than one plans').show();
            //} else {
              //$("#phone_number_error_"+index).text('').hide();
            //}
          //});

          if (!areSimSame) {
            simdetail.forEach(function(data, index) {
              hasError = false;
              $("#sim_number_error_" + index).text('').hide();
              var sim_number = $("#sim_number_" + index).val();
              sim_number = sim_number.replace(/\s+/g, '');
              if (sim_number.length < 16 || sim_number.length > 20) {
                counter++;
                hasError = true;
                $("#sim_number_error_"+index).text('SIM number should be 16 to 20 characters long').show();
              }  else {
                $("#sim_number_error_"+index).text('').hide();
              }
              var simByCarrier = getSimByCarrier(data['carrier']);
              console.log(simByCarrier);
              var isSimNotAvailable = true;
              if (!hasError) {
                // var truncated_sim_number = sim_number.substring(0, sim_number.length-1);
                // console.log(sim_number);
                simByCarrier.forEach(function(simdata){
                  simdata.shipment_detail.forEach(function(shipDetail){
                    if (isSimNotAvailable) {
                      if(BigInt(sim_number) == BigInt(shipDetail['sim_number'])) {
                        $("#store_"+index).val(simdata['store_id']);
                        $("#shipment_id_"+index).val(simdata['shipment_id']);
                        isSimNotAvailable = false;
                      }
                    }
                  });
                });
                /*if (isSimNotAvailable) {
                  counter++;
                  $("#sim_number_error_"+index).text('SIM Card Number is not valid, Please enter SIM Card Number purchased from the store').show();
                }*/
              }
            });
          }

          //alert(counter);
          if (counter === 0) {
            // console.log('simdetail');
            $('#addSimDetailForm').attr('action', '<?=BASEURL?>cart/saveSimDetails/<?=$planid; ?>');
          } else {
            event.preventDefault();
          }
        });

        function validateSIM(simnum){
					var check = simnum.toString().slice(-1);
					var simnumber =  simnum.toString().substring(0, simnum.length -1);
					var singleDigit = 0;
					var result = 0;
					var multiply = 0;
					var sum = 0;
					var splitsum =0;
					for (var i = 0; i < simnumber.length; i++) {
						singleDigit = parseInt(simnumber.charAt(i));

						if( i % 2 == 0){
							multiply  = singleDigit * 2;
							if( multiply > 9){
								splitsum = 0;
								multiply = multiply.toString();
								for (var i1 = 0; i1 < 2; i1++){
									splitsum += parseInt(multiply.charAt(i1));
								}
								sum += splitsum
							} else{
								sum +=  parseInt(multiply);
							}
						} else {
						sum += singleDigit;
						}
					}
					sum = sum * 9;
					var result1 = sum % 10;
					if(parseInt(result1) == parseInt(check)){
						return true;
					}
					return false;
				}

        function getSimByCarrier(carrierId) {
          var shipment = <?php echo json_encode($shipment); ?>;
          var shipment_detail = <?php echo json_encode($shipment_detail); ?>;
          // console.log(shipment);
          // console.log(shipment_detail);
          var simByCarrier = [], shipmentDetail = [];
          shipment.forEach(function(data) {
            var simData = {};
            if (data['carrier_id'] == carrierId) {
              simData = {
                'shipment_id': data['shipment_id'],
                'store_id': data['store_id'],
                'carrier_id': data['carrier_id'],
                'quantity': data['quantity'],
                'date_shipped': data['date_shipped'],
                'shipment_detail': []
              };
              shipment_detail.forEach(function(sdata) {
                if (sdata['shipment_id'] == data['shipment_id']) {
                  simData.shipment_detail.push(sdata);
                }
              });
              simByCarrier.push(simData);
            }
          });
          return simByCarrier;
        }
      });
    </script>
  </body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards | New Sim Card</title>
<?=load_view('common/files')?>
<!-- Global site tag (gtag.js) - Google Ads: 625313153 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-625313153"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'AW-625313153');
</script>

<!-- Event snippet for Submit lead form conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-625313153/dvyMCIbX6O0BEIGLlqoC'});
</script>
<style>
	.top_poster {
		margin-bottom: 20px !important;
	}
</style>
</head>
<body>
<?=load_view('common/header')?>

<div class="sim_activated order_confirmed">
<div class="main_page">
	<div class="how_works container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_4.png">
			</div>
		</div>
		</div>
	</div>
</div>
</div>
<div class="top_poster">
	<div class="container">
		<h4><?=$message;?></h4>
		<div class="btn_shadow">
			<?php if($status == 1): ?>
				<a href="<?=BASEURL?>cart/add_sim_card/20/<?=$orderid?>/<?=$itemid?>"><div class="btn">Ok</div></a>
			<?php elseif($status == 2): ?>
				<a href="<?=BASEURL?>"><div class="btn">Ok</div></a>
			<?php endif; ?>
		</div>
	</div>
</div>
</div>

<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
<style>
#parentVerticalTab .coverage .questions {
	padding-top: 0px;
	padding-bottom: 0px;
}
.resp-tab-content {
	display: none;
	padding: 0 22px 0 22px;
}
.footer {
	margin: 40px 0 0 0;
}
.main_page .qna {
 background: transparent;
}
</style>
</head>
<body>
<?=load_view('common/header')?>

<div class="main_page">
<div class="top_poster container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8">
				<div class="box_container">
					<div class="box">
						<h1>Contactless<br> Sim Card Delivery Straight To Your Doorstep</h1>
						<h3 style="font-weight: bold; color:red; font-size: 20px;">Plan Starting From $25 With Unlimited Calling, Texting & Data Throughout Canada</h3>
						<div class="row">
							<div class="col-md-auto col-6">
								<br>
								<div class="btn_shadow">
								  <div class="btn"><a href="#Allplans">View Plans</a></div>
								</div>
							</div>

							<div class="col-md-auto col-6">
								<div><img src="<?=BASEURL?>public/img/powered.jpg" alt=""></div>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="how_works container-fluid">
	<div class="container">
		<h4>Start Talking in 3 Easy Steps!</h4>
        <h5></h5>
		<!--<h5>Setup your phone with our network in 3 easy steps</h5>-->
		<div class="row">
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_1.png">
				
			</div>
			<p>
				Pick & schedule a plan as per your needs
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_2.png">
			</div>
			<p>
				Get your sim delivered to your home
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_3.png">
			</div>
			<p>
				Pop your sim in & enjoy the service
			</p>
		</div>
		</div>
	</div>
</div>
<div class="why_us camp-why container-fluid">
	<div class="container">
		<h4>Why choose us?</h4>
		<div class="row">
			<div class="col-lg-2 col-6">
				<div class="feature_box box_1">
					<p>Unlimited <br>talk & text</p>
				</div>	
			</div>
			<div class="col-lg-2 col-6">
				<div class="feature_box box_2">
					<p>Nationwide <br> Coverage</p>
				</div>	
			</div>
			<div class="col-lg-2 col-6">
				<div class="feature_box box_3">
					<p>High speed <br>data</p>
				</div>	
			</div>
			<div class="col-lg-2 col-6">
				<div class="feature_box box_4">
					<p>No <br>contracts</p>
				</div>	
			</div>
            
            <div class="col-lg-2 col-6">
				<div class="feature_box box_4">
					<p>No Credit<br>Checks</p>
				</div>	
			</div>
            
            <div class="col-lg-2 col-6">
				<div class="feature_box box_4">
					<p>Contactless <br>Delivery</p>
				</div>	
			</div>
		</div>
		
	</div>
</div>
<div class="reviews container-fluid">
	<div class="container">
		<h4>Hear from our customers</h4>
		<div class="owl-carousel">
			<div class="box_container">
				<div class="box">
					<p class="name">Jack F.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"Fantastic, I'm totally blown away by CSC."
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Stephen J.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"I get my SIM from here every time I visit Canada!!!"
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Ryan N.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"SIM arrived right at my doorstep!!! Great service!!! "
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Cilka Z.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"Very easy to use. Just what I was looking for."
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Desmund F.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"I would like to personally thank you for your outstanding product. Very easy to use."
					</p>
				</div>
			</div>


			
		</div>
	</div>
</div>

<!--<div class="started container-fluid">
	<div class="container">
		<h4>Let’s get started!</h4>
		<h5>Start saving on your monthly bills, order a sim today.</h5>
		<div class="btn_shadow">
			<div class="btn"><a href="<?=BASEURL?>allplans">View Plans</a></div>
		</div>
		<br>
		<div><img src="<?=BASEURL?>public/img/chatr.png" alt=""></div>
	</div>
</div>-->


</div>

<div class="all_plans" id="Allplans">
<div class="top_poster container-fluid">
	<div class="container">
		<h1>All plans</h1>
		<div class="row">

			<?php foreach ($plans as $_): ?>
			<div class="col-md-6">
				<div class="box_container">
					<div class="box">
						<h3><?=$_['title']?></h3>
						<div class="price">
							<p class="number">$<?=$_['price']?></p>
							<p class="month"><br><span>/month</span></p>
						</div>
						<?php if($_['save']): ?>
						<div class="price" >
							<p class="month" style="background-color: red;padding: 10px; color: #fff;">SAVE $<?=$_['save']?></p>
						</div>
						<?php endif;?>
						<div class="list_container">
						<ul>
							<?php if($_['plan_detail1']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail1']?></li>
							<?php endif;?>
							<?php if($_['plan_detail2']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail2']?></li>
							<?php endif;?>
							<?php if($_['plan_detail3']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail3']?></li>
							<?php endif;?>
							<?php if($_['plan_detail4']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail4']?></li>
							<?php endif;?>
							<?php if($_['plan_detail5']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail5']?></li>
							<?php endif;?>
							<?php if($_['plan_detail6']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail6']?></li>
							<?php endif;?>
							<?php if($_['plan_detail7']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail7']?></li>
							<?php endif;?>
							<?php if($_['plan_detail8']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail8']?></li>
							<?php endif;?>
							<?php if($_['plan_detail9']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail9']?></li>
							<?php endif;?>
							<?php if($_['plan_detail10']!=''): ?>
								<li><img src="<?=BASEURL?>public/img/tick.png"><?=$_['plan_detail10']?></li>
							<?php endif;?>	
						</ul>
						</div>
						<div class="row">
							<div class="col-6 text-right">
								<div class="btn_shadow">
									<div class="btn"><a href="<?=BASEURL?>cart/add/<?=$_['id']?>">Get Plan</a></div>
								</div>
							</div>
							<div class="col-6">
								<p class="view_details"><a href="<?=BASEURL?>plandetails/select/<?=$_['id']?>">View Details</a></p>
							</div>
						</div>
						<br>
						<div align="center"><img src="<?=BASEURL?>public/img/powered.jpg" alt="" width="100px"></div>
					</div>
				</div>
			</div>
			<?php endforeach;?>
		</div>
	</div>
</div>

<div class="main_page">
	<div class="qna container-fluid">
		<div class="container">
			<h4>Got a question?<br> <span>We’ve the answers</span></h4>
			
             <!--Vertical Tab-->
        <div id="parentVerticalTab">
            <ul class="resp-tabs-list hor_1">
                <li>General Questions</li>
                <li>Orders</li>
                <li>Data Questions</li>
                <li>Company Info</li>
               
            </ul>
            
            <div class="resp-tabs-container hor_1">
            
                <div class="first">
              <div id="accordion" class="custom-acco">
               
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Will my phone or tablet work with your service?
        </a>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        Make sure you have a cellular equipped tablet or an&nbsp;<strong>unlocked</strong>&nbsp;phone that will work with the chatr mobile network, it must be GSM/HSPA 850 MHz and 1900 MHz compatible. If you are unsure if your device supports these requirements please check you device compatibility. 
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          What network will I be using?
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
       We are an&nbsp;independent third party retailer and will provide you with a SIM card and activate it for you so you can use the Chatr mobile network in Canada.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingThr">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThr" aria-expanded="false" aria-controls="collapseThr">
          What is the coverage area?
        </a>
      </h5>
    </div>
    <div id="collapseThr" class="collapse" aria-labelledby="headingThr" data-parent="#accordion">
      <div class="card-body">
       For complete network information please visit&nbsp;the coverage section of our site.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingfour">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
          What happens if my phone is not unlocked?
        </a>
      </h5>
    </div>
    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
      <div class="card-body">
       If your phone is not unlocked, you will not be able to use our service. Please make sure you phone is unlocked before purchasing your plan. If you are unsure, please contact your carrier.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingfive">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
          Do I need to change any settings on my device?
        </a>
      </h5>
    </div>
    <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordion">
      <div class="card-body">
      In order to use some texting capabilities you may need to update your APN settings.<br><br><strong>If you have an iPhone/iPad:</strong><br><br>• for some IOS versions you can view the APN settings on your device in Settings &gt; Cellular &gt; Cellular Data Network and change it to chatrweb.apn<br>• for iOS 5 or later please click www.chatrmobile.com/setmyiphone from the internet browser of your iPhone/iPad to get the settings automatically pushed to your device.<br><br><strong>If you have another type of phone (Android, Symbian, etc.)</strong>&nbsp;<br><br><strong>APN Settings</strong><br>The data APN "Access Point Name" is chatrweb.apn<br>The MMS URL is http://mms.chatrwireless.com<br>The MMS IP address is 205.151.11.11<br>The MMS port is 8080
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingsix">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
         What is your refund policy?
        </a>
      </h5>
    </div>
    <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#accordion">
      <div class="card-body">
      All sales are final.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingseven">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
         What SIM card sizes do provide?
        </a>
      </h5>
    </div>
    <div id="collapseseven" class="collapse" aria-labelledby="headingseven" data-parent="#accordion">
      <div class="card-body">
      We provide a 3-1 triple SIM card which supports&nbsp;Mini, Micro and Nano SIM cards.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingeight">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
         Can I order just a SIM card without a plan?
        </a>
      </h5>
    </div>
    <div id="collapseeight" class="collapse" aria-labelledby="headingeight" data-parent="#accordion">
      <div class="card-body">
     No, we do not sell just the SIM card, a plan is required.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingnine">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsenine" aria-expanded="false" aria-controls="collapsenine">
        Can I purchase a SIM card locally?
        </a>
      </h5>
    </div>
    <div id="collapsenine" class="collapse" aria-labelledby="headingnine" data-parent="#accordion">
      <div class="card-body">
    No we cannot activate SIM cards that are not purchased directly from us.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingten">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseten" aria-expanded="false" aria-controls="collapseten">
        How do I check my Chatr account balance and how much I have available?
        </a>
      </h5>
    </div>
    <div id="collapseten" class="collapse" aria-labelledby="headingten" data-parent="#accordion">
      <div class="card-body">
    You’ve got a lot of different options for checking your chatr account:<br><br>Dial *225# then press send/call button from your chatr mobile phone for an on-screen notification.</p><p><br>OR call *611 from your chatr phone.<br>P.S All of these calls are free!
      </div>
    </div>
  </div>
  

               </div>
                
                </div>
                <div class="second">
                
                <div id="accordion2" class="custom-acco">
               
                <div class="card">
    <div class="card-header" id="headingele">
      <h5 class="mb-0">
        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseele" aria-expanded="true" aria-controls="collapseele">
          What happens when I order my plan?
        </a>
      </h5>
    </div>

    <div id="collapseele" class="collapse show" aria-labelledby="headingele" data-parent="#accordion2">
      <div class="card-body">
      
      <ol>
      <li>You will recieve an "Order Confirmation" email</li>
      <li>When your order is shipped you will recieve an&nbsp;"Order Shipped" email with your tracking number</li>
      <li>Once you recieve your SIM card you will see a link at the bottom of the "Order Shipped" email, click the link and enter the 20 digit SIM card number found on the back of the card</li>
      <li>Wait 1-2 business days and enjoy your service!!!</li>
     
      </ol>
        
      </div>
    </div>
  </div>

</div>
                
                </div>
                <div class="third">
                 <div id="accordion3" class="custom-acco">
               
                 <div class="card">
    <div class="card-header" id="headingtwe">
      <h5 class="mb-0">
        <a class="btn btn-link" data-toggle="collapse" data-target="#collapsetwe" aria-expanded="true" aria-controls="collapsetwe">
          How is data usage calculated?
        </a>
      </h5>
    </div>

    <div id="collapsetwe" class="collapse show" aria-labelledby="headingtwe" data-parent="#accordion3">
      <div class="card-body">
        Data usage is rounded to the next full KB.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingthurteen">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsethurteen" aria-expanded="false" aria-controls="collapsethurteen">
          What is the data overage rate above my data bucket?
        </a>
      </h5>
    </div>
    <div id="collapsethurteen" class="collapse" aria-labelledby="headingthurteen" data-parent="#accordion3">
      <div class="card-body">
       5¢/MB in Canada. Outside of Canada regular data roaming rates apply. Data usage is rounded to the next full KB.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingfourteen">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefourteen" aria-expanded="false" aria-controls="collapsefourteen">
         Can I use data without a data-add on with my Chatr Unlimited Talk plans?
        </a>
      </h5>
    </div>
    <div id="collapsefourteen" class="collapse" aria-labelledby="headingfourteen" data-parent="#accordion3">
      <div class="card-body">
       Unfortunately, no. chatr Unlimited Talk plans do not offer pay-per-use data.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingfifteen">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifteen" aria-expanded="false" aria-controls="collapsefifteen">
          Will I be notified when I am running out of data?
        </a>
      </h5>
    </div>
    <div id="collapsefifteen" class="collapse" aria-labelledby="headingfifteen" data-parent="#accordion3">
      <div class="card-body">
       Unfortunately, no. However, you can check your data balance anytime from your phone! Key in *225# then pressing the ‘send’ or phone key.
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingsixteen">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsesixteen" aria-expanded="false" aria-controls="collapsesixteen">
         Will I get a notification when I have used up all my data?
        </a>
      </h5>
    </div>
    <div id="collapsesixteen" class="collapse" aria-labelledby="headingsixteen" data-parent="#accordion3">
      <div class="card-body">
      Yes. Your data session will stop as soon as your data bucket is depleted. At this time, you will receive a text notification. If you have extra funds in your account, you will have the option to continue to use data at the pay-per-MB rate of 10¢/MB in Canada. If you do not want to use data at the pay-per-MB rate, your data will be blocked until the end of your billing cycle.
      </div>
    </div>
  </div>
  
  </div>

                </div>
                <div class="four">
                
                 <div id="accordion4" class="custom-acco">
                
                <div class="card">
    <div class="card-header" id="headingeighteen">
      <h5 class="mb-0">
        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseeighteen" aria-expanded="true" aria-controls="collapseeighteen">
          Do you monitor our usage?
        </a>
      </h5>
    </div>

    <div id="collapseeighteen" class="collapse show" aria-labelledby="headingeighteen" data-parent="#accordion4">
      <div class="card-body" style="display:block;">
       No. Never. Your privacy is important to us.
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingnineteen">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsenineteen" aria-expanded="false" aria-controls="collapsenineteen">
          What currency are your prices in?
        </a>
      </h5>
    </div>
    <div id="collapsenineteen" class="collapse" aria-labelledby="headingnineteen" data-parent="#accordion4">
      <div class="card-body">
       All prices are in Canadian dollars and are subject to the Ontario HST tax (13%).
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingtwentee">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsetwentee" aria-expanded="false" aria-controls="collapsetwentee">
         What is your business address?
        </a>
      </h5>
    </div>
    <div id="collapsetwentee" class="collapse" aria-labelledby="headingtwentee" data-parent="#accordion4">
      <div class="card-body">
       643 Chrislea rd, Unit 3<br>Woodbridge, ON<br>L4L 8A3
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" id="headingtwanteone">
      <h5 class="mb-0">
        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsetwanteone" aria-expanded="false" aria-controls="collapsetwanteone">
         Contact Us
        </a>
      </h5>
    </div>
    <div id="collapsetwanteone" class="collapse" aria-labelledby="headingtwanteone" data-parent="#accordion4">
      <div class="card-body">
       Email us at info@canadiansimcards.com and allow up to 48 hours for a repsonse.
      </div>
    </div>
  </div>
  
  </div>

                </div>
                
                  </div>
            
        </div>
			
		</div>
	</div>
</div>
</div>

<div class="clearfix"></div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

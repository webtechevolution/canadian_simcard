<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MCKDT8F"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<!--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-166021502-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-166021502-1');
</script>-->



<div class="popup_container sign_in_container admin_page">
	<div class="box_container">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="box sign_in">
			<h4>Sign in</h4>
			<p class="new_user">
				New user ? <a href="javascript:void(0)" class="register_btn">Create an account</a>
			</p>
			<form  action="<?=BASEURL?>user/login" id="login_user">
				<div class="input_container">
					<input type="email" name="email" placeholder="Enter email">
					<span>Email</span>
				</div>
				<div class="input_container">
					<input type="password" name="password" placeholder="Enter password">
					<span>Password</span>
				</div>
				<div class=btn_shadow>
					<button class="btn long" type="submit">SIGN IN</button>
					<!-- <div class="btn long">SIGN IN</div> -->
				</div>
			</form>
			<p class="forgot_pass">
				<a href="javascript:void(0)" class="forgot_password_btn">Forgot Password </a>
			</p>
			<img src="<?=BASEURL?>public/img/close_icon.png" class="close_icon">
		</div>
	</div>
</div>
<div class="popup_container create_account admin_page">
	<div class="box_container">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<div class="box sign_in">
			<h4>Create an account</h4>
			<p class="new_user">
				Existing user ? <a href="javascript:void(0)" class="sign_in_btn">Sign in</a>
			</p>
			<form action="<?=BASEURL?>user/adduser" id="add_user" name="userInfo" method="post" onsubmit="return createNewUser();">
				<div class="input_container">
				<input type="text" name="full_name" placeholder="Enter full name">
				<span>Full Name</span>
				</div>
				<div class="input_container">
					<input type="email" name="email" placeholder="Enter email">
					<span>Email</span>
				</div>
				<div class="input_container">
					<input type="password" name="password" placeholder="Enter password">
					<span>Password</span>
				</div>
				<div class="input_container">
					<input type="password" name="confirm_password" placeholder="Enter password">
					<span>Re - enter password</span>
				</div>
				<p class="terms">
				By clicking on Sign up, you agree to our <a href="<?=
				BASEURL?>terms">T&amp;C</a>
				</p>
				<div class="input_container">
					<?php if($logged_in): ?>
					<?php else: ?>
						<?php if(strstr(BASEURL, 'canadiansimcards.com')): ?>
						<div class="g-recaptcha" data-sitekey="6LeSwe8UAAAAAKqW54RKPa1_qc28lu2lGVdWP59c" style="transform:scale(0.90);-webkit-transform:scale(0.90); transform-origin:0 0;
    -webkit-transform-origin:0 0;"></div>
    					<?php endif;  ?>
					<?php endif; ?>
				</div>
				<div class=btn_shadow>
					<button class="btn long" type="submit">CREATE ACCOUNT</button>
					<!-- <div class="btn long">CREATE ACCOUNT</div> -->
				</div>
				<input type="hidden" name="baseURL" class="baseURL" id="baseURL" value="<?php echo BASEURL; ?>" >
			</form>
			<img src="<?=BASEURL?>public/img/close_icon.png" class="close_icon">
		</div>
	</div>
</div>
<div class="popup_container forgot_password_container admin_page">
	<div class="box_container">
		<button type="button" class="close" data-dismiss="modal">&times;</button> 
		<div class="box sign_in">
			<h4>Forgot Password</h4>
			<p class="new_user">
				
			</p>
			<form  action="<?=BASEURL?>user/forgotPassword" id="forgot_password_user">
				<div class="input_container">
					<input type="email" name="email" placeholder="Enter email">
					<span>Email</span>
				</div>				
				<div class=btn_shadow>
					<button class="btn long" type="submit">RESET</button>
					<!-- <div class="btn long">SIGN IN</div> -->
				</div>
			</form>
			<!-- <p class="forgot_pass">
				<a href="javascript:void(0)" class="forgot_password_btn">Forgot Password </a>
			</p> -->
			<img src="<?=BASEURL?>public/img/close_icon.png" class="close_icon">
		</div>
	</div>
</div>
<!-- <div class="popup_container new_user_checkout admin_page">
	<div class="box_container">
		<div class="box">
			<h4>Checkout</h4>
			<div class="continue">Continue as Guest <img src="<?=BASEURL?>public/img/forward.png"></div>
			<p class="or">Or</p>
			<h6>Create account</h6>
			<p class="new_user">
				Existing user ? <a href="#">Sign in</a>
			</p>
			<form>
			<div class="input_container">
				<input type="email" name="name" placeholder="Enter full name">
				<span>Full Name</span>
			</div>
			<div class="input_container">
				<input type="email" name="email" placeholder="Enter email">
				<span>Email</span>
			</div>
			<div class="input_container">
				<input type="password" name="password" placeholder="Enter password">
				<span>Password</span>
			</div>
			<div class="input_container">
				<input type="password" name="password" placeholder="Confirm Password">
				<span>Re - enter password</span>
			</div>
			<p class="terms">
				By clicking on Sign up, you agree to our <a href="#">T&amp;C</a>
			</p>
			<div class=btn_shadow>
				<div class="btn long" type="submit">Create Account</div>
			</div>
			</form>

			<img src="<?=BASEURL?>public/img/close_icon.png" class="close_icon">
		</div>
	</div>
</div> -->

<div class="header container-fluid">
<div class="top-bar">
<div class="container">
<p><i><img src="https://www.canadiansimcards.com/public/img/mail-icon.png"></i> <a href="mailto:info@candiansimcards.com">info@candiansimcards.com</a></p>
<p class="ph"><i><img src="https://www.canadiansimcards.com/public/img/phone-icon.png"></i> 647-338-8819</p>
</div>
</div>

	<div class="container">
	<div class="row">
		<div class="col-lg-2 col-4">
			<a href="<?=BASEURL?>"><img src="<?=BASEURL?>public/img/logo.png" class="logo img-fluid"></a>
		</div>
		<div class="col-lg-8 text-right d-none d-lg-block">
			<ul class="main_nav">
			 <!---<li><a href="<?=BASEURL?>allplans">All Plans</a></li>-->
			 <li><a href="<?=BASEURL?>activateplans">All Plans</a></li>
			 <li><a href="<?=BASEURL?>coverage">Coverage</a></li>
			 <li><a href="<?=BASEURL?>faq">FAQ</a></li>
			 <!-- <li><a href="<?=BASEURL?>blog">Blog</a></li> -->
			 <li><a href="<?=BASEURL?>cart">Cart <?=is_array($cart) && count($cart)>0?'('.count($cart).')':''?></a></li>
			 <?php if($logged_in): ?>
				<li><a href="<?=BASEURL?>myprofile">My Profile</a></li>
			 <?php endif; ?>
			</ul>
		</div>
		<div class="col-lg-2 text-right d-none d-lg-block">
			<!-- <div class="btn sign_in_btn">Sign in</div> -->
			<?php if($logged_in): ?>
				<a href="<?=BASEURL?>user/logout"><div class="btn">Sign out</div></a>
			<?php else: ?>
				<div class="btn sign_in_btn">Sign in</div>
			<?php endif; ?>
		</div>
		<div class="col-6 d-none d-block d-lg-none text-right">
			<img src="<?=BASEURL?>public/img/menu_icon.png" class="mob_menu_icon">
		</div>
	</div>
	</div>

	<div class="mob_nav">
	  <div class="content">
	  	<ul>
			<li class="active"><a href="#">Home</a></li>
			<li><a href="<?=BASEURL?>allplans">Plans</a></li>
			<li><a href="<?=BASEURL?>activateplans">Activate</a></li>
			<li><a href="<?=BASEURL?>coverage">Coverage</a></li>
			<li><a href="<?=BASEURL?>faq">FAQ</a></li>
			<!-- <li><a href="<?=BASEURL?>blog">Blog</a></li> -->
			<li><a href="<?=BASEURL?>cart">Cart <?=is_array($cart) && count($cart)>0?'('.count($cart).')':''?></a></li>
			<!-- <li class="sign_in_btn">Sign in</li> -->
			<?php if($logged_in): ?>
				<li><a href="<?=BASEURL?>myprofile">My Profile</a></li>
			 <?php endif; ?>
			<?php if($logged_in): ?>
				<li class="sign_in_btn"><a href="<?=BASEURL?>user/logout">Sign out</a></li>
			<?php else: ?>
				<li class="sign_in_btn">Sign in</li>
			<?php endif; ?>
		</ul>
		<img src="<?=BASEURL?>public/img/close_icon.png" class="close_icon">
	  </div>
	</div>
</div>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-166021502-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-166021502-1');
</script>

<div class="header container-fluid">
	<div class="container">
	<div class="row">
	<div class="col-lg-4 col-6">
			<a href="<?=BASEURL?>"><img src="<?=BASEURL?>public/img/logo.png" class="logo img-fluid"></a>
		</div>
		<div class="col-lg-6 text-right d-none d-lg-block">

		</div>
		<div class="col-lg-2 text-right d-none d-lg-block">

		</div>
		<div class="col-6 d-none d-block d-lg-none text-right">

		</div>
	</div>

	</div>

</div>

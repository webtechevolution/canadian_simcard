<script type="text/javascript" src="<?=BASEURL?>public/js/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js' async defer></script>
 <!-- <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script> -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVIdZi4C-RhiL5-gNH5FR3h1EylI9aZDw&libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8NhTK4-2KISSJpOsA3dxDLaNioYUEa9Y&libraries=places"></script>


																												
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="<?=BASEURL?>public/js/bootstrap.js"></script>

<script src="<?=BASEURL?>public/js/easyResponsiveTabs.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
       

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
</script>

<script type="text/javascript" src="<?=BASEURL?>public/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?=BASEURL?>public/js/pnotify.js"></script>
<script type="text/javascript">

$(document).ready(function(){
	PNotify.prototype.options.styling = "bootstrap3";

	$('#add_user').submit(function( event ) {
		event.preventDefault();

		var url = $(this).attr("action");
		var data = $(this).serializeArray();

		var posting = $.post(url, data);
		posting.done(function( result ) {
			// console.log(result);

			if(result != "Failed"){
				eval(result);
			}
			else{
				new PNotify({
					//text: result,
					text: "Email address already exist",
					type: 'error',
					icon: false,
					animate_speed: 'fast'
				})
			}
		});

	});

	$('#login_user').submit(function( event ) {
		event.preventDefault();

		var url = $(this).attr("action");
		var data = $(this).serializeArray();

		var posting = $.post(url, data);
		posting.done(function( result ) {
			// console.log(result);
			if(result != "Failed" && result != "Inactive"){
				eval(result);
			} else if (result === 'Inactive') {
				new PNotify({
					text: "Email verification pending, please verify the email and then Sign in.",
					type: 'error',
					icon: false,
					animate_speed: 'fast'
				})
			}
			else{
				new PNotify({
					text: "Incorrect Login Details",
					type: 'error',
					icon: false,
					animate_speed: 'fast'
				})
			}
		});
	});

	$('#forgot_password_user').submit(function( event ) {
		
		event.preventDefault();

		var url = $(this).attr("action");
		var data = $(this).serializeArray();

		var posting = $.post(url, data);
		posting.done(function( result ) {
		
			if(result != "Failed" 
				&& result != "Password updated successfully" 
				&& result != "Please enter correct email address"
				&& result != "Please enter email and password" 
				&& result != "Wrong email address"
				&& result != "Passwords do not match"){
				eval(result);
			}
			else{
				new PNotify({
					text: result,
					type: 'error',
					icon: false,
					animate_speed: 'fast'
				})
				if (result === "Password updated successfully") {
					$('.sign_in_container').fadeIn();
					$('.sign_in_container').click(function(){
						$(this).fadeOut();
					});
					$(".sign_in_container .sign_in").click(function(e) {
							e.stopPropagation();
					});
					$('.sign_in_container .sign_in .close_icon').click(function(){
						$('.sign_in_container').hide();
					});

					$('.popup_container.create_account').hide();
					$('.popup_container.forgot_password_container').hide();
				}
			}
		});
	});
	//sign in popup
	$('.sign_in_btn').click(function(){

		$('.sign_in_container').fadeIn();
		$('.sign_in_container').click(function(){
			$(this).fadeOut();
		});
		$(".sign_in_container .sign_in").click(function(e) {
				e.stopPropagation();
		});
		$('.sign_in_container .sign_in .close_icon').click(function(){
			$('.sign_in_container').hide();
		});

		$('.popup_container.create_account').hide();
	});

	$('.register_btn').click(function(){

		$('.popup_container.create_account').fadeIn();
		$('.popup_container.create_account').click(function(){
			$(this).fadeOut();
		});
		$(".popup_container.create_account .sign_in").click(function(e) {
				e.stopPropagation();
		});
		$('.popup_container.create_account .sign_in .close_icon').click(function(){
			$('.popup_container.create_account').hide();
		});

		$('.sign_in_container').hide();
	});

	//forgot password popup
	$('.forgot_password_btn').click(function(){
		
		$('.forgot_password_container').fadeIn();
		$('.forgot_password_container').click(function(){
			$(this).fadeOut();
		});
		$(".forgot_password_container .sign_in").click(function(e) {
				e.stopPropagation();
		});
		$('.forgot_password_container .sign_in .close_icon').click(function(){
			$('.forgot_password_container').hide();
		});

		$('.popup_container.create_account').hide();
		$('.sign_in_container').hide();
	});

	//remove order from cart
	// $('.your_cart .item p.remove').click(function(){
	//   $(this).parents('.item').remove();
	// });

	//initialize map
	//initMap();
	$(".owl-carousel").owlCarousel({
		loop:true,
		nav:true,
		navText : ["&larr;","&rarr;"],
		responsiveClass:true,
		responsive:{
			0:{
				items:1
			},
			768:{
				items:2
			},
			991:{
				items:3
			}
		}
	});
	//toggle menu
	$('.header .mob_menu_icon').click(function(){
		$('.header .mob_nav').slideDown();
		$('.header .mob_nav .close_icon').click(function(){
			$('.header .mob_nav').slideUp();
		});
	});

	//qna doropdown
	$('.qna .mobile .info_box .title').click(function(){
		console.log('clicked');
		$(this).find('.arrow').toggleClass('rotate');
		$(this).siblings('.content').slideToggle().toggleClass('open');
	});

	//coverage questions
	$('.coverage .questions .title').click(function(){
		$(this).siblings('.content').slideToggle();
		$(this).find('.arrow').toggleClass('rotate');
	});

	//shipping
	$('.shipping.admin_page input[name="shipping"]').click(function(){
		if ($(this).val() == 'saved') {
			$('.new_shipping').slideUp();
		}
		if ($(this).val() == 'new') {
			$('.new_shipping').slideDown();
			$('.btn_shadow').hide();
		} else {
			$('.new_shipping').slideUp();
			$('.btn_shadow').show();
		}
	});

	//billing
	$('.billing.admin_page input[name="shipping"]').click(function(){
		if ($(this).val() == 'saved') {
			$('.new_shipping').slideUp();
		}
		if ($(this).val() == 'new') {
			$('.new_shipping').slideDown();
			$('.btn_shadow').hide();
		} else {
			$('.new_shipping').slideUp();
			$('.btn_shadow').show();
		}
	});


	////////////map search
	//start search
	$('.map_page .search input').focusin(function(){
		$('.map_page .search .search_icon').fadeOut();
		$('.map_page .search').animate({
			 'padding-left' : 0,
		});
		$('.map_page .search .close_icon').fadeIn();
	});

	//cancel search
	$('.map_page .search .close_icon').click(function(){
		$('.map_page .search .close_icon').fadeOut();

		$('.map_page .search').animate({
			 'padding-left' : 35,
		});
		$('.map_page .search .search_icon').fadeIn();
		$('.map_page .search input').val('');
		$('.map_page .search_container .search_results').hide();
	});

	//map search suggestions
	$('.map_page .search input').keydown(function(){
		var input = $(this).val();
		var input = $.trim(input);
		if (input.length > 1) {
			//populate search results
			$('.map_page .search_container .search_results').show();
		}
		else{
			$('.map_page .search_container .search_results').hide();
		}
	});

	//choosen result
	$('.map_page .search_container .search_results li').click(function(){
		$('.map_page .search .close_icon').hide();
		$('.map_page .search p.change').show();
		$('.map_page .search_container .search_results').hide();
		$('.map_page .search_container .selected_area').show();
	});

	//change search
	$('.map_page .search p.change').click(function(){
		$('.map_page .search p.change').hide();
		$('.map_page .search .close_icon').show();
		$('.map_page .search_container .selected_area').fadeOut();
		$('.map_page .search input').val('');
		$('.map_page .search input').focus();
	});

	//change button style
	$('.btn').each(function(){
		var width = $(this).width();
		if (width > 200) {
			$(this).addClass('long');
		}
	});





	jQuery("#resetpassfrm").submit(function(  $){ 
			//event.preventDefault();
			jQuery(".form_error_ts").html('');
			jQuery(this).find(".frmerror").removeClass('frmerror');
			$pass = jQuery(this).find("#password").val();
			$confirm_pass= jQuery(this).find("#confirm_password").val();
			$err = 0;
			$errs=[];
			if($pass==""){
				$errs.push("Password is empty");
				jQuery( this ).find("#password").addClass('frmerror');
				$err++;
			} 
			if( $confirm_pass=="" ){
				$errs.push("Confirm Password is empty");
				jQuery( this ).find("#confirm_password").addClass('frmerror');
				$err++;
			} 
			if( ($pass!="" && $confirm_pass!="") && $pass!=$confirm_pass ){
				$errs.push("Confirm Password does not match with Password");
				jQuery( this ).find("#confirm_password").addClass('frmerror');
				$err++;
			}	

			if( forget_code==""){
				$errs.push("Invalid request can not be proceed");
				jQuery( this ).find("#password").addClass('frmerror');
				$err++;
			}

			if(  $err > 0 ){
				$options = '<ul>';
				jQuery($errs).each(function( index, element ){
					$options+= '<li>'+element+'</li>';
				});
				$options+= '</ul>';
				jQuery(".form_error_ts").html($options);
				return false;
			}

		})
});

var map;
function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 8
	});
}
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144537723-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144537723-1');
</script>

<script>
function checkOutOrder() {
	$URLs =  $(".baseURL").val();
	//alert($URLs);
	if($URLs=="https://canadiansimcards.com/"){
		var response = grecaptcha.getResponse();
		if(response.length == 0) {
			new PNotify({
				text: "Please verify Captcha.",
				type: 'error',
				icon: false,
				animate_speed: 'fast'
			})
			return false;
		}
	}
	return true;
}
</script>

<script>
function createNewUser() {

	var name = document.forms["userInfo"]["full_name"];
  var email = document.forms["userInfo"]["email"];
  var password = document.forms["userInfo"]["password"];
  var confirmPassword = document.forms["userInfo"]["confirm_password"];

	if( name.value == "" ) {
		new PNotify({
			text: 'Please provide your full name!',
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		name.focus() ;
		return false;
	}
	else if( email.value == "" ){
		new PNotify({
			text: 'Please provide your email!',
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		email.focus() ;
		return false;
	}
	else if( password.value == "" ){
		new PNotify({
			text: 'Please enter a password!',
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		password.focus() ;
		return false;
	}
	else if( confirmPassword.value == "" ){
		new PNotify({
			text: 'Please confirm your password!',
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		confirmPassword.focus() ;
		return false;
	}

	if( password.value != confirmPassword.value){
		new PNotify({
			text: 'Passwords do not match!',
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		confirmPassword.focus() ;
		return false;
	}

	var response = grecaptcha.getResponse();
	if(response.length == 0) {
		new PNotify({
			text: "Please verify Captcha.",
			type: 'error',
			icon: false,
			animate_speed: 'fast'
		})
		return false;
	}
	return true;
}
</script>

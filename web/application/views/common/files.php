<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MCKDT8F');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/owl.theme.default.min.css">
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/pnotify.css">
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=BASEURL?>public/css/easy-responsive-tabs.css " />
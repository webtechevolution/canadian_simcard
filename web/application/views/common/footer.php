<div class="footer container-fluid">
	<div class="container">
		<div class="row">
		<div class="col-md-4 col-12">
			<img src="<?=BASEURL?>public/img/logo.png" class="img-fluid logo">
			<p class="copy">
				Copyright &copy; 2018 Canadian Sim Cards. All Rights Reserved.
			</p>
			<!--<p class="copy">
				
			</p>-->
		</div>
		<div class="col-md-2 col-6">
			<ul class="footer-links">
			<li><a href="<?=BASEURL?>activateplans">All Plans</a></li>
			<!---<li><a href="<?=BASEURL?>activateplans">Activate</a></li>-->
			 <li><a href="<?=BASEURL?>coverage">Coverage</a></li>
			 <li><a href="<?=BASEURL?>faq">FAQ</a></li>
			 <!-- <li><a href="<?=BASEURL?>blog">Blog</a></li> -->
			 <?php if($logged_in): ?>
				<li><a href="<?=BASEURL?>myprofile">My Profile</a></li>
			 <?php endif; ?>	
			 <li>
					<?php if($logged_in): ?>
						<a href="<?=BASEURL?>user/logout" class="sign_in_btn">Sign out</a>
					<?php else: ?>
						<a href="javasript:void(0)" class="sign_in_btn">Sign in</a>
					<?php endif; ?>
				</li>
				<li class="hide_desk">
					<div class="social mob">
						<a target="_blank" href="https://www.facebook.com/CanadianSIMCard/">
							<img src="<?=BASEURL?>public/img/fb.png">	
						</a>
						<!--<a href="#">
							<img src="<//?=BASEURL?>public/img/twitter.png">
						</a>-->
						<a target="_blank" href="https://www.instagram.com/canadiansimcards/">
							<img src="<?=BASEURL?>public/img/instagram.png">
						</a>
					</div>
				</li>
			</ul>
		</div>
        
        <div class="col-md-3 col-6">
        <ul class="footer-links">
       <li> <a href="<?=BASEURL?>terms">Terms &amp; conditions</a> </li>
			<li>	<a href="<?=BASEURL?>privacy">Privacy Notice</a> </li>
			<li>	<a href="<?=BASEURL?>policies">Policies</a> </li>
			<li>	<a href="<?=BASEURL?>contactus">Contact Us</a></li>
        
        </ul>
        
        </div>
        
		<div class="col-md-3 col-12 text-right">
			<div class="social desk">
				<a target="_blank" href="https://www.facebook.com/CanadianSIMCard/">
					<img src="<?=BASEURL?>public/img/fb.png">	
				</a>
				<!--<a href="#">
					<img src="<//?=BASEURL?>public/img/twitter.png">
				</a>-->
				<a target="_blank" href="https://www.instagram.com/canadiansimcards/">
					<img src="<?=BASEURL?>public/img/instagram.png">
				</a>
			</div>
            
            <div class="footer-address">
	<p>3-643 Chrislea Road,<br />
Woodbridge,<br /> ON,
L4L 8A3 </p>

</div>
		</div>
		</div>
		
		
	</div>
</div>
<!doctype html>
<html lang="en">
<head>
<title>Invalid Request for Reset Password</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">		
		<h1>Invalid Request!</h1>	
		<?php if(isset($_GET['error']) && $_GET['error']!=""){ ?>	
		<h4><?php echo $_GET['error'] ; ?></h4>
		<?php } ?>
	</div>
</div>
</div>
<?=load_view('common/footer')?>
<?=load_view('common/js')?>
</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Password Changed successfully</title>
<?=load_view('common/files')?>
<!-- Global site tag (gtag.js) - Google Ads: 625313153 -->

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-625313153"></script>

<script>
window.dataLayer = window.dataLayer || [];

function gtag(){dataLayer.push(arguments);}

gtag('js', new Date());

 

gtag('config', 'AW-625313153');

</script>

<!-- Event snippet for Sign-up conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-625313153/mHpeCP-TjO4BEIGLlqoC'});
</script>
</head>
<body>
<?=load_view('common/header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">		
		<h1>Congratulations!</h1>			
		<h4>Password changed successfully, you can now login to website.</h4>		
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Reset Password</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">		
		<h1>Reset Password!</h1>
		<div class="form_error_ts">
		</div>
		<form class="l-canadiansim-form" action="<?=BASEURL?>resetmypass/submitforgetpass" method="post" id="resetpassfrm">
			<input type="hidden" name="forget_code" id="forget_code" value="<?php echo $_GET['vkey'] ?>">
				<div class="input_container">
					<label for="password">Password</label>
					<input type="password" id="password" name="password" placeholder="Enter password" class="">
				</div>
				<div class="input_container">
					<label for="confirm_password">Confirm Password</label>
					<input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm password" class="">
				</div>
				<div class="btn_shadow">
					<button class="btn long" type="submit">Reset</button>
					<!-- <div class="btn long">SIGN IN</div> -->
				</div>
	</form>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

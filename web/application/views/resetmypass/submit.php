<!doctype html>
<html lang="en">
<head>
<title>Processing...</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">		
		<h1>Processing...</h1>		
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

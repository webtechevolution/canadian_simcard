<!doctype html>
<html lang="en">
<head>
<title>Email Verification</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">
		<h1>Thank You for Signing Up!</h1>
		<h4>Please check your email to verify your account before logging in.</h4>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

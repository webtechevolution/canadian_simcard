<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>
<style type="text/css">
/*.box_container{
	 filter: 0 2px 6px 4px rgba(0, 0, 0, 0.23);
}
	.box {
		width: 200px;
		height: 200px;
		background: #000;
 		-webkit-clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
		clip-path: polygon(83% 0, 100% 20%, 100% 100%, 0 100%, 0 0);
}*/
</style>
<body>
<?=load_view('common/header')?>

<div class="terms">
<div class="container-fluid top_poster">
	<div class="container">
		<h1>Policies</h1>
	</div>
</div>
<div class="content container-fluid">
	<div class="container">
		<div class="section">
			<h2>Refund policy</h2>
			<p>Thanks for purchasing our products at Canadiansimcards.com operated by Canadian Sim Cards.</p>
			<p>We offer a full money-back guarantee for all purchases made on our website. If you are not satisfied with the product that you have purchased from us, you can get your money back no questions asked. You are eligible for a full reimbursement within 1 calendar days of your purchase.</p>
			<p>After the 1-day period, you will no longer be eligible and won't be able to receive a refund. We encourage our customers to try the product (or service) in the first day after their purchase to ensure it fits your needs.</p>
			<p>If you have any additional questions or would like to request a refund, feel free to contact us.</p>
		</div>

		<div class="section">
			<h2>Delivery Policy – Tangible Goods</h2>
			<p>All orders are subject to product availability. If an item in your order is unavailable, we will ship you the part of your order that is available. When that item becomes available, we will ship you the rest of your order. Shipping costs for your order are non-refundable and are based on the weight of the items you order and your location. The date of delivery for your order may vary due to carrier shipping practices, delivery location, method of delivery and the quantity of items ordered, and in addition your order may be delivered in separate shipments. Additional shipping charges may apply depending on your location and the size of your order. You are responsible for all taxes applicable to the delivery of your order, including sales tax, value added tax, custom duties and excise duties. If there is any damage to the items that you ordered on delivery, you must contact us within 5 days from receipt of your order.</p>
		</div>

		<div class="section">
			<h2>Delivery Policy – Downloadable Goods</h2>
			<p>Upon receipt of your order, you will either be prompted to begin your download immediately or you will receive an e-mail from us with instructions to complete your download. If you are prompted to begin your download immediately, your completion of the download will constitute delivery to you of the item(s) you purchased. If you receive an e-mail from us with instructions to complete your download, the receipt by you of the e-mail will constitute delivery by us to you of the item(s) you purchased. In case you are unable to download the item(s) you purchase or you do not receive an e-mail from us with instructions to complete your download, you must contact us within 5 days from the date of your order. If you do not contact us within 5 days from the date of your order, the item(s) you purchased will be considered received, downloaded and delivered to you.</p>
			
		</div>
		<div class="section">
			<h2>Delivery Policy – Services</h2>
			<p>Upon receipt of your order, the services will be performed to you in accordance with the terms applicable to the services that you purchased. The nature of the services you purchased and the date of your purchase may impact the timing of performance of the services. The services will be deemed to be successfully delivered to you upon performance of the services.</p>
			
		</div>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

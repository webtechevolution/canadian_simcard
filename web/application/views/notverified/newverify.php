<!doctype html>
<html lang="en">
<head>
<title>Email Verification</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/empty_header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">
		<h1>New Verification Email Sent!</h1>
		<h4>Please check your email to verify your account before logging in.</h4>
		<div align="center" class="btn_shadow">
			<a href="<?=BASEURL?>"><div class="btn">Go to Homepage</div></a>
		</div>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

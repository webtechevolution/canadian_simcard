<!doctype html>
<html lang="en">
<head>
<title>Account not Verified</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/header')?>

<div class="sim_activated activated_sim  admin_page">
<div class="top_poster">
	<div class="container">
		<h1>Account Not Activated!</h1>
		<h4>If you have lost the activation email, please re-enter your email address.</h4>
		<form action="<?=BASEURL?>notverified/verifyemail" method="post">
		<div align="center" class="input_container">
			<input type="email" name="email" placeholder="Enter email">
			<span>Email</span>
		</div>
		<div align="center" class="btn_shadow">
			<button tipe="submit" class="btn">Submit</button>
		</div>
		</form>
	</div>
</div>
</div>

<?=load_view('common/js')?>

</body>
</html>

<!doctype html>
<html lang="en">
<head>
<title>Email Verification Complete</title>
<?=load_view('common/files')?>
</head>
<body>
<?=load_view('common/empty_header')?>

<div class="email_verif email_verif_pad">
<div class="top_poster">
	<div class="container">
		<h1>Thank You for Verifying Your Account!</h1>
		<h4>You are able to log in to your account now.</h4>
		<div align="center" class="btn_shadow">
			<a href="<?=BASEURL?>"><div class="btn">Go to Homepage</div></a>
		</div>
	</div>
</div>
</div>

<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

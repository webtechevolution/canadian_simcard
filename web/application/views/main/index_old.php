<!doctype html>
<html lang="en">
<head>
<title>Canadian Sim Cards</title>
<?=load_view('common/files')?>
</head>

<body class="home_page">
<?=load_view('common/header')?>
<div class="main_page">
<div class="top_poster container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8">
				<div class="box_container">
					<div class="box">
						<h1>Talk freely across Canada</h1>
						<h3>Unlimited texting & calling throughout Canada & internationally</h3>
						<div class="btn_shadow">
						  <div class="btn"><a href="<?=BASEURL?>allplans">View Plans</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="how_works container-fluid">
	<div class="container">
		<h4>How it works</h4>
		<h5>Setup your phone with our network in 3 easy steps</h5>
		<div class="row">
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_1.png">
				
			</div>
			<p>
				Pick & schedule a plan per your needs
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_2.png">
			</div>
			<p>
				Get your sim delivered to your home
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_3.png">
			</div>
			<p>
				Pop your sim in & enjoy the service
			</p>
		</div>
		</div>
	</div>
</div>
<div class="why_us container-fluid">
	<div class="container">
		<h4>Why choose us?</h4>
		<div class="row">
			<div class="col-lg-3 col-6">
				<div class="feature_box box_1">
					<p>Unlimited <br>talk & text</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_2">
					<p>Nationwide <br> Coverage</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_3">
					<p>High speed <br>data</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_4">
					<p>No <br>contracts</p>
				</div>	
			</div>
		</div>
		
	</div>
</div>
<div class="reviews container-fluid">
	<div class="container">
		<h4>Hear from our customers</h4>
		<div class="owl-carousel">
			<div class="box_container">
				<div class="box">
					<p class="name">Kaaja Martin1 <img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						“Unlimited texting for these low prices… Its a steal !!!”
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Kaaja Martin2 <img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						“Unlimited texting for these low prices… Its a steal !!!”
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Kaaja Martin3 <img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						“Unlimited texting for these low prices… Its a steal !!!”
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Kaaja Martin4 <img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						“Unlimited texting for these low prices… Its a steal !!!”
					</p>
				</div>
			</div>
		</div>
	</div>
</div>



<div class="qna container-fluid">
	<div class="container">
		<h4>Got a question?<br> <span>We’ve the answers</span></h4>
		<div class="desktop">
			<div class="row">
				<?php foreach ($faq as $cat): ?>
				<div class="col-md-3 col-sm-6">
					<p class="title"><?=$cat['name']?></p>
					<ul>
						<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
						<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
						<?php endforeach;?>
						<li class="show_all"><a href="<?=BASEURL?>faq">Show all</a></li>
					</ul>
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<div class="mobile">
			<?php foreach ($faq as $cat): ?>
			<div class="info_box">
				<div class="title">
					<p><?=$cat['name']?></p><img src="<?=BASEURL?>public/img/arrow_up.png" class="arrow">
				</div>
				<div class="content">
					<ul>
				  		<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
						<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
			<?php endforeach;?>
		</div>
		
	</div>
</div>




<div class="qna container-fluid">
	<div class="container">
		<h4>Got a question?<br> <span>We’ve the answers</span></h4>
		<div class="desktop">
			<div class="row">
				<?php foreach ($faq as $cat): ?>
				<div class="col-md-3 col-sm-6">
					<p class="title"><?=$cat['name']?></p>
					<ul>
						<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
						<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
						<?php endforeach;?>
						<li class="show_all"><a href="<?=BASEURL?>faq">Show all</a></li>
					</ul>
				</div>
				<?php endforeach;?>
			</div>
		</div>
		<div class="mobile">
			<?php foreach ($faq as $cat): ?>
			<div class="info_box">
				<div class="title">
					<p><?=$cat['name']?></p><img src="<?=BASEURL?>public/img/arrow_up.png" class="arrow">
				</div>
				<div class="content">
					<ul>
				  		<?php foreach ($cat['sub_cat'] as $sub_cat): ?>
						<li><a href="<?=BASEURL?>faq/<?=$cat['url']?>/<?=$sub_cat['url']?>"><?=$sub_cat['name']?></a></li>
						<?php endforeach;?>
					</ul>
				</div>
			</div>
			<?php endforeach;?>
		</div>
		
	</div>
</div>
<div class="started container-fluid">
	<div class="container">
		<h4>Let’s get started!</h4>
		<h5>Start saving on your monthly bills, order a sim today.</h5>
		<div class="btn_shadow">
			<div class="btn"><a href="<?=BASEURL?>allplans">View Plans</a></div>
		</div>
	</div>
</div>
</div>


<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

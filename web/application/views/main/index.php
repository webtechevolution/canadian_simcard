<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="ATzN9CeGjo0uatdSuEICfXoSeyDoxL4OsxsYoUVuKSQ" />
<title>Canadian Sim Cards - Unlimited Calling, Texting & Data</title>
<meta name="description" 
content="Canadian Sim Cards with unlimited calling, texting & data. Great rates and worldwide coverage - Order today!">
<meta name="keywords" content="canadian sim cards, sim card, canada sim card, prepaid sim card canada, canada sim card travel, best sim card in canada, data sim card canada, data sim card, prepaid sim card" />
<?=load_view('common/files')?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZERR0SQNZ3"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'G-ZERR0SQNZ3');
</script>

</head>

<body class="home_page">
<?=load_view('common/header')?>
<div class="main_page">
<div class="top_poster container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-8">
				<div class="box_container">
					<div class="box">
						<h1>Sim Card Delivered Straight To Your Doorstep</h1>
						<h3 style="font-weight: bold; color:red; font-size: 20px;">Unlimited Calling, Texting & Data Throughout Canada</h3>
						<div class="row">
							<div class="col-md-auto">
								<br>
								<div class="btn_shadow">
								  <div class="btn"><a href="<?=BASEURL?>allplans">View Plans</a></div>
								</div>
							</div>

							<div class="col-md-auto">
								<div><img src="<?=BASEURL?>public/img/powered.jpg" alt=""></div>
							</div>
						</div>
						
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="how_works container-fluid">
	<div class="container">
		<h4>No Credit Checks!</h4>
		<h5>Setup your phone with our network in 3 easy steps</h5>
		<div class="row">
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_1.png">
				
			</div>
			<p>
				Pick & schedule a plan per your needs
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_2.png">
			</div>
			<p>
				Get your sim delivered to your home
			</p>
		</div>
		<div class="col-md-4">
			<div class="box">
				<img src="<?=BASEURL?>public/img/how_icon_3.png">
			</div>
			<p>
				Pop your sim in & enjoy the service
			</p>
		</div>
		</div>
	</div>
</div>
<div class="why_us container-fluid">
	<div class="container">
		<h4>Why choose us?</h4>
		<div class="row">
			<div class="col-lg-3 col-6">
				<div class="feature_box box_1">
					<p>Unlimited <br>talk & text</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_2">
					<p>Nationwide <br> Coverage</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_3">
					<p>High speed <br>data</p>
				</div>	
			</div>
			<div class="col-lg-3 col-6">
				<div class="feature_box box_4">
					<p>No <br>contracts</p>
				</div>	
			</div>
		</div>
		
	</div>
</div>
<div class="reviews container-fluid">
	<div class="container">
		<h4>Hear from our customers</h4>
		<div class="owl-carousel">
			<div class="box_container">
				<div class="box">
					<p class="name">Jack F.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"Fantastic, I'm totally blown away by CSC."
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Stephen J.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"I get my SIM from here every time I visit Canada!!!"
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Ryan N.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"SIM arrived right at my doorstep!!! Great service!!! "
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Cilka Z.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"Very easy to use. Just what I was looking for."
					</p>
				</div>
			</div>
			<div class="box_container">
				<div class="box">
					<p class="name">Desmund F.<img src="<?=BASEURL?>public/img/twitter.png">
					</p>
					<p class="text">
						"I would like to personally thank you for your outstanding product. Very easy to use."
					</p>
				</div>
			</div>


			
		</div>
	</div>
</div>

<div class="started container-fluid">
	<div class="container">
		<h4>Let’s get started!</h4>
		<h5>Start saving on your monthly bills, order a sim today.</h5>
		<div class="btn_shadow">
			<div class="btn"><a href="<?=BASEURL?>allplans">View Plans</a></div>
		</div>
		<br>
		<div><img src="<?=BASEURL?>public/img/chatr.png" alt=""></div>
	</div>
</div>
</div>


<?=load_view('common/footer')?>
<?=load_view('common/js')?>

</body>
</html>

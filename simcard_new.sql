-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2022 at 05:55 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simcard_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(500) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(500) NOT NULL,
  `province` varchar(500) NOT NULL,
  `zip` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `type`, `user_id`, `full_name`, `address`, `city`, `province`, `zip`, `country`, `contact_number`, `email`) VALUES
(1, 'shipping', 2, 'James Bond', '1606 - 5 Sanromanoway', 'Toronto', 'Ontario', 'M3N2Y4', 'Canada', '', ''),
(2, 'shipping', 1, 'Charan', 'Maker', 'Richmond hill', 'Ontario', 'L4b2j5', 'Canada', '', ''),
(3, 'shipping', 4, 'charan maker', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '', ''),
(4, 'shipping', 3, 'Ryan Nesharatnam', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '', ''),
(6, 'shipping', 6, 'Ryan Nesharatnam', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1C 0A1', 'Canada', '', ''),
(7, 'shipping', 7, 'Bobby Maker', '1 Glenayr Road', 'Richmond Hill', 'Ontario', 'L4B 2J5', 'Canada', '', ''),
(8, 'shipping', 5, 'Stephen Jeevaratnam', '265 south ocean dr', 'Oshawa', 'Ontario', 'L1L0K4', 'Canada', '', ''),
(9, 'shipping', 8, 'charan maker', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '', ''),
(10, 'shipping', 9, 'dsfas', 'sdfs', 'sdfsd', 'Ontario', 'sdfsd', 'Canada', '', ''),
(11, 'shipping', 11, 'Ruby Puri', '643 Chyrslea Road', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '', ''),
(12, 'shipping', 12, 'test', 'test', 'toronto', 'Ontario', 'm1v 1v7', 'Canada', '', ''),
(13, 'shipping', 15, 'Harendra singh', 'Nagla bhuriya', 'Mathura', 'Ontario', '281123', 'Canada', '', ''),
(15, 'shipping', 23, 'Jasdeep Singh ', '95 skyview shores court n.e Calgary Alberta T3N 0C9 Canada', 'Skyview', 'Alberta', 'T3N 0C9', 'Canada', '', ''),
(16, 'shipping', 26, 'muhammad saleem KHAN', 'Karachi Pakistan, Nazimabad block 2 house numbar A/199', 'karachi', 'Alberta', '74900', 'Canada', '', ''),
(17, 'shipping', 31, 'Prem singh', '618421335894', 'Hyd', 'Prince Edward Island', '500055', 'Canada', '', ''),
(18, 'shipping', 33, 'Mogff', 'Dnjfkf', 'Jdkfkf', 'Quebec', 'H8n2a5 ', 'Canada', '', ''),
(19, 'shipping', 44, 'Hari naik', 'Ummaipalli ', 'Kurnool', 'Ontario', '518123', 'Canada', '', ''),
(20, 'shipping', 39, 'Shakir ullah ', 'Egshakir3@gmail.com ', 'Pakistan ', 'New Brunswick', 'Shakir 1234', 'Canada', '', ''),
(21, 'shipping', 45, 'Harchan Kumar ', 'Vacay', 'Bacay', 'Ontario', 'Bharatgas', 'Canada', '', ''),
(22, 'shipping', 46, 'Sophie ann castellano', '4981 ave barclay apt 2', 'Montreal', 'Quebec', 'H3W1E3', 'Canada', '', ''),
(23, 'shipping', 48, 'Cary Robinson', '3 Point West Dr', 'Winnipeg ', 'Manitoba', 'R3t5n8', 'Canada', '', ''),
(24, 'shipping', 50, 'Arvind vishwakarma', 'madhopur dharagn', 'Azamgarh', 'Manitoba', '10th', 'Canada', '', ''),
(25, 'shipping', 53, 'Ravendra singh', 'Village tegwa post barambaba dist sidhi (m.p.)', 'Sidhi', 'Nova Scotia', '486661', 'Canada', '', ''),
(26, 'shipping', 53, 'Ravendra singh', 'Village tegwa post barambaba dist sidhi (m.p.)', 'Sidhi', 'Nova Scotia', '486661', 'Canada', '', ''),
(27, 'shipping', 54, 'Salim ', 'Bherunda ', 'Rajasthan ', 'Ontario', '341031', 'Canada', '', ''),
(28, 'shipping', 58, 'Dharmendar Kumar ', 'Reliance jio ', 'Bareilly ', 'Ontario', '243201', 'Canada', '', ''),
(29, 'shipping', 62, 'Pritam meena', 'Chanda ki dhani CHAWANDIYA', 'Jaipur', 'Newfoundland', '303301', 'Canada', '', ''),
(30, 'shipping', 77, 'Mathura Prasad Kori', 'Adhatal Jabalpur Madhya Pradesh India.', 'Jabalpur', 'Ontario', '482004', 'Canada', '', ''),
(31, 'shipping', 85, 'Gyanendra singh', 'Tajpur Post pusauli', 'Auraiya', 'Quebec', '206243', 'Canada', '', ''),
(32, 'shipping', 87, 'Aqeel mani', '999aqeelmani@gmail.com', 'Islamabad ', 'New Brunswick', '123', 'Canada', '', ''),
(33, 'shipping', 88, 'Danish Mansuri ', 'Sutar gali hatod ', 'Hatod', 'Ontario', '453111', 'Canada', '', ''),
(34, 'shipping', 94, 'Riyasat Hussain', 'Havitpur', 'Hasanpur', 'Ontario', '244241', 'Canada', '', ''),
(35, 'shipping', 101, 'Sandra Hermosilla', '1343  Borden Crescent ', 'Brockville ', 'Ontario', 'K6V5X7', 'Canada', '', ''),
(36, 'shipping', 103, 'Deepak attri', 'Dist Kangra vpo Varial ', 'nagrota Suriya', 'Ontario', 'Deepak ', 'Canada', '', ''),
(38, 'shipping', 112, 'Pramod ', 'Nagla Kazi ', 'Mathur', 'Ontario', '281003', 'Canada', '', ''),
(39, 'shipping', 113, 'Pramod kumar', 'Vill.Munshiganj  post.bibiyapur thana. kothi block. Siddhaur tahsil.haidargarh district.barabanki UP pincode.225119', 'Barabanki', 'Ontario', '225119', 'Canada', '', ''),
(40, 'shipping', 114, 'mukesh', 'city digri', 'digri', 'Ontario', '69330', 'Canada', '', ''),
(41, 'shipping', 124, 'Suhail khan', 'Ladpur', 'Khatoli', 'Ontario', '251201', 'Canada', '', ''),
(42, 'shipping', 133, 'Rakesh KUMAR Sah ', 'Uttar Pradesh', 'Baliya', 'New Brunswick', 'Barrier', 'Canada', '', ''),
(43, 'shipping', 136, 'Rahul', 'Zolapur', 'Zolapur', 'Ontario', '1080', 'Canada', '', ''),
(44, 'shipping', 136, 'Rahul ', 'Zolapur', 'Zolapur', 'Ontario', '1080', 'Canada', '', ''),
(45, 'shipping', 140, 'suraj singh bhabher', 'bavdi chhoti jhabua m.p. 457661', 'jhabua', 'Prince Edward Island', '457661', 'Canada', '', ''),
(46, 'shipping', 153, 'Adel benchikh', 'App 11 - 120 crois garden', 'Dorval', 'Quebec', 'H9S3G5 ', 'Canada', '', ''),
(48, 'shipping', 161, 'Cory Martin', '6 Jewell street ', 'Red Deer ', 'Alberta', 'T4p3w6 ', 'Canada', '', ''),
(49, 'shipping', 162, 'Kiran Kumar mahanta', 'Keonjher', 'Keonjher', 'Ontario', '758032', 'Canada', '', ''),
(50, 'shipping', 163, 'Rohit yadav', 'Village jeevpur post jeevpur thana zamania dist ghazipur ', 'Ghazipur ', 'Ontario', '232340', 'Canada', '', ''),
(51, 'shipping', 165, 'Dinesh Gautam', 'Amanabad', 'Basti', 'Ontario', '272170', 'Canada', '', ''),
(52, 'shipping', 176, 'Vivek bhoi', 'Kharora', 'Raipur', 'Ontario', '12345', 'Canada', '', ''),
(53, 'shipping', 176, 'Vivek bhoi', 'Kharora', 'Raipur', 'Ontario', '12345', 'Canada', '', ''),
(54, 'shipping', 189, 'Jadav sureshbhai narvatbhai ', 'At.po.sathrota', 'Halol', 'Ontario', '389350', 'Canada', '', ''),
(55, 'shipping', 190, 'magesh', 'megwalo ka bas kernadi', 'jerol', 'Ontario', '343040', 'Canada', '', ''),
(56, 'shipping', 191, 'Ravikant kumar ', 'Bhadwar ', 'Ara', 'Ontario', '802161', 'Canada', '', ''),
(57, 'shipping', 194, 'Ravi Kumar', 'Sikandara Rao', 'Hathars', 'Ontario', '204215', 'Canada', '', ''),
(58, 'shipping', 196, 'Pankaj Kumar', 'Madhupuri nagla semar mainpuri', 'Inaia', 'Alberta', '205267', 'Canada', '', ''),
(59, 'shipping', 202, 'Ramraksha chauhan', 'Post moohmabad sipah mau', 'Mau', 'Ontario', '275303', 'Canada', '', ''),
(60, 'shipping', 207, 'Ankosupu JAGADESH', 'Kongraklan Vivekananda school', 'Ibrahimpatan', 'Ontario', '501506', 'Canada', '', ''),
(61, 'shipping', 208, 'veerpalsingh', 'Veerpalsingh847694@gmail.com', 'Moradabad', 'Prince Edward Island', '847694', 'Canada', '', ''),
(62, 'shipping', 213, 'sarah younes', '5810 Ambler Drive, Unit 14 BEY 124164', 'Mississauga ', 'Ontario', 'L4W4J5', 'Canada', '', ''),
(63, 'shipping', 216, 'Wajib ', 'Hussain ', 'Multan ', 'Alberta', '1122', 'Canada', '', ''),
(64, 'shipping', 222, 'à¤°à¤¾à¤œ', 'à¤¬à¤‚à¤—à¤¾à¤²à¥€ à¤ªà¤¾à¤‚à¤¡à¥‡', 'à¤²à¤¾à¤­', 'Ontario', '27 45 02', 'Canada', '', ''),
(65, 'shipping', 223, 'Rahul Kumar', 'Jila bareilly tahsil badi post murder gram', '123456', 'Ontario', '123456', 'Canada', '', ''),
(66, 'shipping', 223, 'Rahul Kumar', 'Jila bareilly tahsil badi post Buddha', '123456', 'Ontario', '123456', 'Canada', '', ''),
(68, 'shipping', 233, 'Gulam rasool ', 'Babhani biskhor Siddhartha nagar up ', 'Uttar Pradesh ', 'Nova Scotia', 'Gulam rasool ', 'Canada', '', ''),
(69, 'shipping', 237, 'Sarvesh Kumar Shah ', 'Singroli ', 'Mp', 'Ontario', '590', 'Canada', '', ''),
(70, 'shipping', 239, 'Tajibur Rahaman', 'West Bengal', 'Murshidabad', 'Ontario', '742135', 'Canada', '', ''),
(71, 'shipping', 253, 'arslanmhr', 'mianapura', 'sialkot', 'Ontario', '1122', 'Canada', '', ''),
(72, 'shipping', 254, 'Sukhminder Miglani', '300 Killian Road', 'Maple', 'Ontario', 'L6A1A4', 'Canada', '', ''),
(73, 'shipping', 258, 'Numan saith', 'Javed street', 'Narang', 'Ontario', ' 786786', 'Canada', '', ''),
(74, 'shipping', 262, 'Nelson adams', '9816 102 st', 'Fort Saskatchewan ', 'Alberta', 'T8l2c1', 'Canada', '', ''),
(75, 'shipping', 263, 'AliakberKhan  ', 'Pirojpur ', 'Barisal ', 'Ontario', '8500', 'Canada', '', ''),
(76, 'shipping', 265, ' Vic handa', '123 ffff', 'Fff', 'Ontario', '4cecdc', 'Canada', '', ''),
(77, 'shipping', 267, 'Rathva Chetanbhai Gulsingbhai', 'Rathva gulsingbhai, badisimalfaliya, oed, vadobara, gujarat-391160', 'Chhotaudepur', 'British Columbia', '391160', 'Canada', '', ''),
(78, 'shipping', 267, 'Rathva Chetanbhai Gulsingbhai', 'Rathva gulsingbhai, badisimalfaliya, oed, vadobara, gujarat-391160', 'Chhotaudepur', 'British Columbia', '391160', 'Canada', '', ''),
(79, 'shipping', 267, 'Rathva Chetanbhai Gulsingbhai', 'Rathva gulsingbhai, badisimalfaliya, oed, vadobara, gujarat-391160', 'Chhotaudepur', 'British Columbia', '391160', 'Canada', '', ''),
(80, 'shipping', 267, 'Rathva Chetanbhai Gulsingbhai', 'Rathva gulsingbhai, badisimalfaliya, oed, vadobara, gujarat-391160', 'Chhotaudepur', 'British Columbia', '391160', 'Canada', '', ''),
(81, 'shipping', 267, 'Rathva Chetanbhai Gulsingbhai', 'Rathva gulsingbhai, badisimalfaliya, oed, vadobara, gujarat-391160', 'Chhotaudepur', 'British Columbia', '391160', 'Canada', '', ''),
(82, 'shipping', 268, '', '', '', 'British Columbia', '', 'Canada', '', ''),
(83, 'shipping', 272, 'cheranjeevi', 'subashnagar', 'warangal', 'Ontario', '56888', 'Canada', '', ''),
(84, 'shipping', 273, 'ATUL GAJBHIYE ', 'Madhya Pradesh ', 'Balaghat ', 'Newfoundland', '481224', 'Canada', '', ''),
(85, 'shipping', 276, 'RajeevKumar', 'RajeevKumar', 'Pipari', 'Manitoba', '7369', 'Canada', '', ''),
(88, 'shipping', 279, 'Perneet', 'ABC', 'ABC', 'Ontario', '12321', 'Canada', '', ''),
(89, 'shipping', 280, 'Bharat enwatee', 'Batra', 'Chhndwara', 'New Brunswick', '', 'Canada', '', ''),
(90, 'shipping', 284, 'Prince', 'Tane rod', 'Ganur', 'Ontario', '13247', 'Canada', '', ''),
(91, 'shipping', 288, 'Vishal popaniya ', 'Jamkalyanpur devbhoomi dwarka gujarat ', 'Jamkalyanpur ', 'Ontario', '361320', 'Canada', '', ''),
(92, 'shipping', 289, 'Vikky kumar shah', 'Koran', 'Vikky Raj ', 'Ontario', '124', 'à¤•à¤¨à¤¾à¤¡à¤¾', '', ''),
(93, 'shipping', 0, 'Vikky kumar shah', 'Koran', 'Vikky Raj ', 'Ontario', '124', 'à¤•à¤¨à¤¾à¤¡à¤¾', '', ''),
(94, 'shipping', 0, 'Vikky kumar shah', 'Koran', 'Vikky Raj ', 'Ontario', '124', 'à¤•à¤¨à¤¾à¤¡à¤¾', '', ''),
(95, 'shipping', 290, 'Nirmal das', 'Goalpara(assam)', 'Goalpara', 'Ontario', '783125', 'Canada', '', ''),
(96, 'shipping', 297, 'Alexis konkol', '1 king street west unit 2411', 'Toronto ', 'Ontario', 'M5h1a1', 'Canada', '', ''),
(97, 'shipping', 299, 'Raja', 'Cg champa', 'Champa', 'Ontario', '495671', 'Canada', '', ''),
(98, 'shipping', 299, 'Raja', 'Cg champa', 'Champa', 'Ontario', '495671', 'Canada', '', ''),
(99, 'shipping', 301, 'Duryodhan ojha', 'Nanput', 'Bhubaneswar', 'Ontario', '752054', 'Canada', '', ''),
(100, 'shipping', 307, 'Vishnutomar', 'Ranpur to eshah', 'Murena', 'British Columbia', '95223114', 'Canada', '', ''),
(102, 'shipping', 316, 'Petre Ontherun', '514 Ste-Catherine ouest', 'Montreal', 'Quebec', 'h3b1b7', 'Canada', '', ''),
(103, 'shipping', 318, 'Amit dhan', 'Ami t', '1234', 'Ontario', '1991', 'Canada', '', ''),
(104, 'shipping', 318, 'Amit dhan', 'Ami t', '1234', 'Ontario', '139', 'Canada', '', ''),
(105, 'shipping', 318, 'Amit dhan', 'Ami t', '1234', 'Ontario', '139', 'Canada', '', ''),
(106, 'shipping', 322, '85 Acorn place', 'Appartement  302', 'Mississauga', 'Ontario', 'L4Z3N2', 'Canada', '', ''),
(107, 'shipping', 325, 'Kshma kumari', 'Maiki garakh chapra saran', 'Bihar', 'Ontario', '841311', 'Canada', '', ''),
(108, 'shipping', 334, 'Mayuri Rajesh Marbate', 'Wathida juni vasti', 'Maharashtr', 'Ontario', 'Mayuruya', 'à¤•à¥…à¤¨à¤¡à¤¾', '', ''),
(109, 'shipping', 337, 'sonu kumar', 'mahugawsn p s pandu', 'jharkhand', 'Ontario', 'sonu', 'Canada', '', ''),
(111, 'shipping', 348, 'Venoth Nesharatnam', '843 Kilbirnie Dr', 'Ottawa', 'Ontario', 'K2J5T9', 'CA', '', ''),
(112, 'shipping', 350, 'Praval Kumar ', 'Gram nagla har post chitai ', 'Mainpuri ', '55558', '55558', 'CA', '', ''),
(113, 'shipping', 351, 'Angad Singhkohli', '48 Narang Colony', 'New Dehli', 'Dehli', '110058', 'IN', '', ''),
(114, 'shipping', 351, 'Angad Singhkohli', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1C0A1', 'CA', '', ''),
(115, 'shipping', 408, 'Venoth Nesharatnam', '110 Stagecoach Circle', 'Scarborough', 'Ontario', 'M1C0A1', 'CA', '', ''),
(117, 'shipping', 412, 'Charanpul Maker', '3-643 Chrislea Rd', 'Woodbridge', 'Ontario', 'L4L 8A3', 'CA', '', ''),
(118, 'shipping', 412, 'Venoth Nesharatnam', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1C 0A1', 'CA', '', ''),
(119, 'shipping', 415, 'Lane cardinal', '1002-5220-50A ave', 'Sylvan Lake', 'Alberta', 'T4S-1E5', 'CA', '', ''),
(121, 'shipping', 420, ' brigid kimani-oloyede', '413A 2903 unwin rd nw', 'calgary', 'Alberta', 'T2N 4M4', 'CA', '', ''),
(122, 'shipping', 423, 'Jubalee quick ', '3937 46 st', 'Reddder', 'Alberta', 'T4n1l9 ', 'CA', '', ''),
(123, 'shipping', 285, 'Dinesh Mehta', '7900 Hurontario St #308,  ', 'Brampton', ' ON ', 'L6Y 0P6', 'CA', '', ''),
(124, 'shipping', 433, 'Sukhminder Miglani', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '', ''),
(125, 'shipping', 434, 'Sukhminder Miglani', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '', ''),
(126, 'shipping', 8, 'charan maker', '1 glenayr rd', 'Richmond hill', 'Ontario', 'L4B2J5', 'CA', '', ''),
(127, 'shipping', 439, 'Charan', '643 chrislea rd', 'Woodbridge', 'Ontario', 'L4l8a3', 'CA', '', ''),
(128, 'shipping', 447, 'Sukhminder Miglani', '300 Killian road', 'Vaughan', 'ontario ', 'L6A 1A4', 'CA', '', ''),
(129, 'shipping', 445, 'Monesh', 'Near Akai school', 'Bangalore', 'karnataka', '560003', 'IN', '', ''),
(130, 'shipping', 449, 'Deisy Milena Rueda Romero', '348 elizabeth st', 'sudbury', 'ontario', 'p3b2p2', 'CA', '', ''),
(131, 'shipping', 451, 'A-78', 'Sec-13', 'noida', 'Uttar Pradesh', '201301', 'IN', '', ''),
(132, 'shipping', 431, 'test address1', 'Adddresws 2', 'New York', 'Punjab', '141002', 'CA', '', ''),
(133, 'shipping', 454, 'Shraddha', '1st Main, Near Dental Care Hospital', 'Bangalore', 'karnataka', '560003', 'IN', '', ''),
(135, 'shipping', 455, 'Jeetu', 'Sector 5', 'Noida', 'Uttar pradesh', '201301', 'CA', '', ''),
(136, 'shipping', 457, 'Shraddha', 'test street', 'Bangalore', 'Karnataka', '560004', 'IN', '', ''),
(147, 'shipping', 464, 'Sector 74', 'Cs6-1303, Supertech Capetown', 'Noida', 'Uttar Pradesh', '201301', 'IN', '', ''),
(148, 'shipping', 464, 'Test', 'Supertech Capetown Sector 74', 'Noida', 'Uttar Pradesh', '201301', 'IN', '', ''),
(149, 'shipping', 160, 'Pern', 'Ratan Colony Gorakhpur', 'Jabalpur', 'Madhya Pradesh', '482001', 'IN', '', ''),
(150, 'shipping', 463, 'Shraddha Mehta', 'test street', 'bangalore', 'karnataka', '778390', 'IN', '', ''),
(151, 'shipping', 465, 'test', 'test', 'Jabalpur', 'Madhya Pradesh', '201301', 'IN', '', ''),
(154, 'shipping', 456, 'Shraddha', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '', ''),
(155, 'shipping', 443, 'Shraddha', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '', ''),
(156, 'shipping', 443, 'Dinesh', 'Palace Street road', 'Delhi', 'Delhi', '490239', 'IN', '', ''),
(157, 'billing', 443, 'shraddha', 'test', 'Bengaluru', 'Karnataka', '637890', 'IN', '2347899032', 'shraddham@nanojot.com'),
(158, 'billing', 254, 'Sukhminder Miglani', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A1A4', 'CA', '8768768768768', 'smiglani@nanojot.com'),
(159, 'billing', 440, 'New address', 'new street road', 'Toronto', 'Ontario', '568789', 'CA', '3182740893740', 'st@gmail.com'),
(161, 'billing', 472, 'test ', 'address', 'Toronto', 'Ontario', '434678', 'CA', '3419873248', 'test@dayrep.com'),
(162, 'shipping', 472, 'Shraddha', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '', ''),
(163, 'billing', 464, 'Jaspreet Singh Lamba', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA', '9999403408', 'jslamba0811@gmail.com'),
(164, 'billing', 472, 'Dinesh', 'New police line road', 'Nashville', 'Tennessee', '489035', 'US', '4190362890', 'din@test.com'),
(165, 'billing', 456, 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '8902672389', 'shraddham@nanojot.com'),
(166, 'billing', 433, 'Monesh', 'Near postoffice', 'Pune', 'Maharashtra', '348890', 'IN', '2343242345', 'shraddhamaorya@gmail.com'),
(167, 'billing', 8, 'charanpaul Maker', '643', 'WOODBRIDGE', 'Ontario', 'L4L 8A3', 'CA', '6473388819', 'n@n.com'),
(168, 'billing', 474, 'HEEMANSHU BHALLA', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '1615014788', 'bhallaheemanshu@gmail.com'),
(169, 'billing', 456, 'Shraddha', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '9016733489', 'shraddham@nanojot.com'),
(170, 'shipping', 474, 'HEEMANSHU BHALLA', 'SHIVPURI CHOWNK NOORWALA ROAD NEAR LAKHANPAL DIARY', 'Nurwala (81)', 'Punjab', '141007', 'IN', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_article`
--

CREATE TABLE `blog_article` (
  `id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `category_ids` varchar(1000) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `short_description` varchar(1000) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `url` varchar(1000) NOT NULL,
  `active` int(1) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_article`
--

INSERT INTO `blog_article` (`id`, `title`, `category_ids`, `image`, `short_description`, `content`, `author`, `url`, `active`, `updated`) VALUES
(6, 'blog 1', '9, ', 'marioprofile.png', 'sadfas', '<p>sadfasfdasf</p>', '', 'blog-1', 1, '2018-09-20 01:29:43'),
(7, 'blog2', '10, ', 'yoshiprofile.png', 'asdfasdfa', '<p>aaaadfasfqwerqew rwcrq w erqwerqwerqwe&nbsp;</p>', '', 'blog2', 1, '2018-09-20 01:32:18'),
(8, 'blog3', '9, ', 'booprofile.png', 'asdfdas a adsf asd asdf a', '<p>&nbsp;safddasad sfs</p><p>ad f</p><p>asd</p><p>&nbsp;</p><p><img alt=\"\" src=\"http://localhost/files/source/drybonesprofile.png\" style=\"height:500px; width:500px\" /></p><p>fasd</p><p>f&nbsp;</p><p>asdf</p><p>&nbsp;asd</p><p>f ads</p><p>f&nbsp;</p><p>a</p>', '', 'blog3', 1, '2018-09-20 01:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `url` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `name`, `url`) VALUES
(9, 'cat1', 'cat1'),
(10, 'cat2', 'cat2');

-- --------------------------------------------------------

--
-- Table structure for table `carriers`
--

CREATE TABLE `carriers` (
  `id` int(11) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carriers`
--

INSERT INTO `carriers` (`id`, `carrier`, `logo`) VALUES
(1, 'Chatr', 'public/images/uploads/1614789977.png'),
(5, 'Helpr', 'public/images/uploads/1614790043.png'),
(6, 'Tester', 'public/images/uploads/1614790087.png'),
(7, 'Fido', ''),
(8, 'Freedom', ''),
(9, 'Koodo', ''),
(10, 'Virgin', ''),
(11, 'Lucky', ''),
(12, 'Public', '');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `user_id` int(11) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `activation_code` varchar(500) NOT NULL,
  `forget_code` varchar(500) NOT NULL,
  `inserted_timestamp` varchar(50) NOT NULL,
  `forget_code_time` datetime DEFAULT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`user_id`, `full_name`, `first_name`, `last_name`, `username`, `password`, `email`, `create_date`, `activation_code`, `forget_code`, `inserted_timestamp`, `forget_code_time`, `active`) VALUES
(1, 'Perneet123', '', '', 'charanpaul@hotmail.com', '1b8f64e4c435a203019625711d3ee4c14c37356c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4c34ec241d8208b2cbce3ab44da1fb2f', '28bea2d207c1030584e4d0e185b93453', '2018-11-08 17:43:11', NULL, 1),
(2, 'Perneet123', '', '', 'stephenjeevaratnam@gmail.com', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '06186bd4fc748287a8cdcad6327c38dd', 'df5fb7b669a31ae402abc06784304976', '2018-11-09 11:40:02', NULL, 1),
(3, 'Perneet123', '', '', 'ryan@infinitecorp.ca', '007b8b0d3eb0e7bbe5ab0056bb4ac2d24c65aaaa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '57db0be38dbded099473cc2881180644', '228cdb0f2511cc0e459357fd5ceaad90', '2018-11-13 20:38:22', NULL, 1),
(4, 'Perneet123', '', '', 'johndoe@hotmail.com', 'ec1b6d5fffefc2d8e46daf3e29f9bae2832ea411', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'dc84afe667326e6208f0f7b05be99905', '899e19a6412d5c108f1924ade71140b9', '2018-11-13 20:38:36', NULL, 1),
(5, 'Perneet123', '', '', 'stephen@redngen.com', '7e240de74fb1ed08fa08d38063f6a6a91462a815', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9028ed9c3c4646f1bf349c40ffc2217e', '8ffd884539cac76e81f81707212f049e', '2019-02-13 11:38:33', NULL, 1),
(6, 'Perneet123', '', '', 'ryan.nesharatnam@icloud.com', '007b8b0d3eb0e7bbe5ab0056bb4ac2d24c65aaaa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ff16107c66e64bacd6803a1e0c10e11f', '6feec22961b3dd5ddfa2142932b85bd5', '2019-02-13 15:02:32', NULL, 1),
(7, 'Perneet123', '', '', 'bobby.maker@cpvmgroup.com', '7ad9cfc33fe4b23004c06b4ea9d3375f89438835', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e2d8b65aaec549e9aa594f80ea287104', '35678a1606e8085b294db5f85e40f67e', '2019-02-13 17:28:02', NULL, 1),
(8, 'charan maker', '', '', 'charan.maker@cpvmgroup.com', '296e96fd6a35cf57e798ed40cd4d7bdd3f977a10', 'charan.maker@cpvmgroup.com', '2021-07-02 17:36:43', '4c22ee0ca2152565ac4af8a44c38689f', '1c5ba64484ad3195dfb56734c583dcf1', '2019-02-13 21:14:38', NULL, 1),
(9, 'Perneet123', '', '', 'vhanda99@gmail.com', '818f97e78c1584def11e0480792861d953af76a9', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'eeea51a2443abd9e02679b4bf0913140', 'd1324044706008e9ab5bb5101a58dc6a', '2019-02-14 12:31:38', NULL, 1),
(10, 'Perneet123', '', '', 'bmaker@cellularpoint.ca', '007b8b0d3eb0e7bbe5ab0056bb4ac2d24c65aaaa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c99129cb37c939dc5395ca640673f620', '4b79b0fc5fbe40136df194d9a9cbc182', '2019-02-14 15:52:47', NULL, 1),
(11, 'Perneet123', '', '', 'ruby.puri@cpvmgroup.com', '007b8b0d3eb0e7bbe5ab0056bb4ac2d24c65aaaa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'fe95396fed42321152e817b2f55337bf', 'fe8c07088a1a4b95bd4c04369665b139', '2019-03-14 15:58:48', NULL, 1),
(12, 'Perneet123', '', '', 'test@yahoo.com', '51abb9636078defbf888d8457a7c76f85c8f114c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4e3c04b0ff78096ff05fd5462c8033fe', 'e145ee64e29c98aae02a491badf7a4fc', '2019-07-22 14:34:02', NULL, 1),
(13, 'Perneet123', '', '', 'yoyogauravsingh18@gmail.com', '9048ead9080d9b27d6b2b6ed363cbf8cce795f7f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9a5a9298ff09203624d1b88db7296af4', '4154a5386206659532e5505b5cec3f76', '2019-07-30 11:17:31', NULL, 1),
(14, 'Perneet123', '', '', 'sahidnodoliya@gmail.com', 'b0c7e2e99b46068b5aff202d39ba6c82b4e98d33', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '06b696f59449b64b4bc8052022e4e194', 'e93673a4502fb448dd024a230f80da62', '2019-07-31 19:55:46', NULL, 1),
(15, 'Perneet123', '', '', 'harendrajaat161@gmail.com', '6a9422f40f3e1d4e6e573650f574d833ccae932f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'df28696c5e66a32a8123d23e29f0e982', '4a46c4c29524fdc0e9ed0797394b7f65', '2019-08-01 23:40:29', NULL, 1),
(16, 'Perneet123', '', '', 'a76576478@gmail.com', '4a4dfcfc713f596e4a5dddd3eae4966e9932f683', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '920beb8be2f7cce33525f9e21ad9b9c6', '8083a01ad7f5d83bd03fe82e9abfecdb', '2019-08-08 2:43:15', NULL, 1),
(17, 'Perneet123', '', '', 'rameshtfs@gmail.com', '685a49ed51d5a453e08d8d1c1d2a7f5cd531432f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '65f6769527237184cfbbc26f0b04f550', '5b5706a2346a0a06e36ec2f581f9e849', '2019-08-10 2:53:36', NULL, 1),
(18, 'Perneet123', '', '', 'awaismayo8778950@gmail.Com', '36b209a46252e356c481276e6220f2537303912a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b52f241a98c03f4cfe9a3e024085664d', 'ddc92510661c077b9a8484cf8fc43e45', '2019-08-10 7:13:52', NULL, 1),
(19, 'Perneet123', '', '', 'punjnaina@gmail.com', 'fc9e14ca8c1f37b3545cf661ea72e066f97b7a5f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'bcb63f08f1376aae189ea75e2e78bea4', '5a0d195f04dd4213584141ecffe2da7f', '2019-08-11 23:33:38', NULL, 1),
(20, 'Perneet123', '', '', 'shaikh03147925142@gmial.com', 'c13191213c07f418b144005d068e9cf2ab955dbf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9c509301d8e9b820745e6b2f5923a522', '9e96c8a7963e259250ead7c61732720b', '2019-08-13 2:52:28', NULL, 1),
(21, 'Perneet123', '', '', 'sayyedarsh0350@gmail.com', '9754adef355a5a632a7913cad67b89c98075e4cf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cf2ef6a767b34e9cc0e7d80833c54433', '6c875b40c189f794d04b88804495469c', '2019-08-13 23:47:51', NULL, 1),
(22, 'Perneet123', '', '', 'arvindjs4545@gmail.com', '67b8a08cbb4731c475aa084876d7e77db803144f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '687a1fd87b74698d589b7e987fba28a0', 'ce9b70257f685e8880fc410e96fc0d55', '2019-08-14 2:31:09', NULL, 1),
(23, 'Perneet123', '', '', 'bassijass@gmail.com', '8320429de35e4e5feb6ae1c069b086725b800d51', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '54fc9dde61e8e3a110dbda60a4140b08', '3c8f7315ff4f0eeb23c36fd767967806', '2019-08-14 19:34:01', NULL, 1),
(24, 'Perneet123', '', '', 'ankitpandeychamba123@gmail.com', 'a370836f2a9408e120c1ce71b35a8486779150d0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'de2cd94e408bc61d09668468932d281f', '650826089b4ac1314c61979fcab9ed3f', '2019-08-17 0:11:24', NULL, 1),
(25, 'Perneet123', '', '', 'sagarkumar7079048894@gmail.com', '208ef1aa4345cc67d6d659ef2f724bc1a772a17c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9b7fc7586ca0496f9c2b557bae45ccba', '886699a7fa4a658992ee3728c5448f00', '2019-08-17 1:17:44', NULL, 1),
(26, 'Perneet123', '', '', 'foodmanyo@gmail.com', '3f05e7baf6d5591a6aa935e6c4382d3de31ab21e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5c002feea54a2eba3bf7d5d883d54712', '77c52a3a27e6de2fa732891f30278aa0', '2019-08-17 6:34:22', NULL, 1),
(27, 'Perneet123', '', '', 'rajkaranfzd0000@gmail.com', '0414abe2fd4fe660e367189dc843fbb2782d5a1d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'de317fc554d755476fe9a7dba9cdfdea', 'a58f9f2bbf0a7d64f444f777a4aa9579', '2019-08-17 7:21:25', NULL, 1),
(28, 'Perneet123', '', '', 'rajrai@6635gmail.com', '09ff2deaf86192d09381a1533b35945511bcdbca', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '25e9d5ad6124b567869d6e7994b338db', '256e95c44ea15cc50a3bd8fda5848e36', '2019-08-17 8:43:34', NULL, 1),
(29, 'Perneet123', '', '', 'stamagoo@yahoo.com', '8149ef8e2d4aab6cea1dc863c11257a785ddc9db', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '075911d636a647f42e7a776790b824a7', '161a7c380c2a29a077f3f55960d7b8be', '2019-08-17 21:31:58', NULL, 1),
(30, 'Perneet123', '', '', 'lal936931@gmail.com', '6c5803798848e0f104178004681fa6cb498fba11', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5a9483a2c0bc4c72caa1a2830fcb1d53', 'f8abf0eee121b85e4413f252dfe1ae71', '2019-08-18 0:24:18', NULL, 1),
(31, 'Perneet123', '', '', 'aayanprem123@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5a978be56644bdd9177e981b1207627d', '17b17de49ea70164e45bca17db301bb0', '2019-08-19 4:02:10', NULL, 1),
(32, 'Perneet123', '', '', 'sabibhinder827@gmail.com', '5c406823bc93b1b967d8ca810de6de98615969a8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6f4d1fd77d2b23e0c2aa75671a712c45', 'ebc54eb85c6a62060fe6affc630cfe8a', '2019-08-19 6:13:33', NULL, 1),
(33, 'Perneet123', '', '', 'jeanrien82@gmail.com', '5474750f94131a1d685bebf25cf42d03bbfc3885', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '67dde46c1a4067b92bd01075872a2615', '54dddcf7dd2c928385026c68e96f44ae', '2019-08-20 12:51:00', NULL, 1),
(34, 'Perneet123', '', '', 'www.moeenkiani@gmail.com', 'b427bf45242d9ee3217aac623a46e58ec3a9b37b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '87500555f7691ea42743cc41b799d7de', '4369a6eb7d1dbdde0981c6b9e849f25c', '2019-08-21 6:38:54', NULL, 1),
(35, 'Perneet123', '', '', 'julie@gmail.com', 'f75066a2819dc9eeb69499531eb6936a51ee3f47', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd992a03c78e5bd8492403ff1f27b6639', '2bb86e107769ae25e2023e31596ea036', '2019-08-22 16:48:41', NULL, 1),
(36, 'Perneet123', '', '', 'husnuyurtcu@hotmail.com', '1bab3cb718229f0060eec4b6b11f3cbb7e63c973', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4d1798849d0c67f6e77d4d5bf97a951b', '81de3e3cd2cef7d73c4339147c2ed66f', '2019-08-22 19:24:50', NULL, 1),
(37, 'Perneet123', '', '', 'iftsaf135@email.com', '95794aafdc616a663ec81bee7675a639278f3aa3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '07bfa4fc1f2533bf91d044f58f9a7e07', '8b3032894161174104e279129fdc9ec4', '2019-08-23 5:03:49', NULL, 1),
(38, 'Perneet123', '', '', 'adesanya3@gmail.com', '7cf95a59d86159e98529381be3a5806ecbe104ce', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd61286acc9f0bdff185050d9b425ba0d', '764318913f21c9a492b6615402cd69a2', '2019-08-23 5:24:10', NULL, 1),
(39, 'Perneet123', '', '', 'egshakir3@gmail.com', '1028b7e62818de49447b2729765caa64ab492ea2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e9d66e6d23e380e5563555962e4de7b6', '0ab6aba22002ee42d1ffcb8dced39b3e', '2019-08-23 6:53:43', NULL, 1),
(40, 'Perneet123', '', '', '9051856@gmail.com', '31b2d365c975c82c4db67fe41b2eed30a30b7630', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8030bae4d68228dc882eec33cafbed8f', '4a18cee2a0b33fc04084ad04803ddf7c', '2019-08-23 10:25:01', NULL, 1),
(41, 'Perneet123', '', '', 'adnankhan58828@gmail.com', '23ad83a2206a56a8802ad9c933fd509df218b482', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0b03bf6a2be401ba5a26e3d21ed1a421', 'eff0e1589fadc8092e326d3ef13b0b38', '2019-08-23 12:55:08', NULL, 1),
(42, 'Perneet123', '', '', 'kerridhermary@gmail.com', 'e5d265ef99549cf5fb74ffd032643b04287d56a1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '46826f6911abc7b43ea20bf28b67145a', 'bcface40a05201adf098831a5ce100f7', '2019-08-23 19:57:11', NULL, 1),
(43, 'Perneet123', '', '', 'suwalalmeena@gmail.com', '3ccd1d7f02ca972df54b760e0f1947f760647723', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd482476e45a19f26ce35ae0ddf52149b', '61ceed9d1bc6218042a2870ee5fba6d4', '2019-08-24 4:31:15', NULL, 1),
(44, 'Perneet123', '', '', 'harinaik405@gamil.com', '68cc380739cfcae77014a0cdaaeb6cc362e58e17', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0a8a0585c72e54308f2257720b54197c', '843de06f2770fac6a5f10dc6968198f2', '2019-08-24 7:17:15', NULL, 1),
(45, 'Perneet123', '', '', 'harchankumar470@gmail.com', '88c5f9b59c26fc4d4a720fb408308bfbb8cfd3bc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4e3165b4908671df3af47c42153df380', 'c95c05410617774d519e2325e17218d2', '2019-08-24 13:27:30', NULL, 1),
(46, 'Perneet123', '', '', 'ledenw150@gmail.com', '540cd5cb44e6a1b6433004e322fcaa202c808dcc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4f478cb69e58cbadab996255aff54f6f', '6281da9cb811059b3fcec22d8a9e4c23', '2019-08-29 6:46:00', NULL, 1),
(47, 'Perneet123', '', '', 'buse13479@gmail.com', '6a02dbbc6b1c07f05dadace145ebf7fa991020d3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '45fbc4d267ca17013a77410579d783e6', '5579b323c0fd7c405ea8a025d8463451', '2019-08-30 16:46:54', NULL, 1),
(48, 'Perneet123', '', '', 'robinsoncary20@gmail.com', '2df6a05a5309d0f5bc98f9f1b599a05383fbadf3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c71085a9fa63792e04ffdb3e230b76b5', '4bc2c6304074d58a63b77e9b87089bc9', '2019-08-31 0:42:19', NULL, 1),
(49, 'Perneet123', '', '', 'Sumitdhull321@gmail.com', 'bfd997c8a81fc8ce64c860c2444f711e34db25b0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2c61f5ce716ce3ef6855a5eb6ecd4e84', '1d99eccd05c7902df51142b408a20807', '2019-09-01 10:20:42', NULL, 1),
(50, 'Perneet123', '', '', 'ak6723764@gmail.com', 'ed7df443a327a7d1ae60445bc883dc652573d3a1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e8bce1a56f8b80ea5ef4f9b956ca1466', 'd9bb318c2adcc7a64265d2086fd4f910', '2019-09-03 6:34:32', NULL, 1),
(51, 'Perneet123', '', '', 'vk5872709@gmail.com', '5c8f3d9f5d66e13364cab191d0ceb5af1be6e14d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5fa41b4264de7a3e656567663dfe7804', '4fc812cdb024ae5684ea3ce607dd3e9c', '2019-09-04 5:14:49', NULL, 1),
(52, 'Perneet123', '', '', 'khanrules@gmail.com', '812c3e3e0a4edbdf528dc28168cbf95116ec32aa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2de0a7c888ddcff1de6668183f882428', 'baf00972553d50acb9e8bfcfddefc316', '2019-09-04 13:14:10', NULL, 1),
(53, 'Perneet123', '', '', 'ravendrasinghtegwa706743@gmail.com', '633d6c7cb265233dbcf624c6cc80b2fdb8732608', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a45e0731e2af539675251e8cbe3ae8c2', '583124c964f3f04b7601ec64e366347f', '2019-09-05 2:11:17', NULL, 1),
(54, 'Perneet123', '', '', 'sp509786@gmail.com', '60c4cae5b7b85eb978865f8906344d2159045bb6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b9be32b395f978e81d6790bee26b3e1a', 'f324a30785df40dc5deb3d7537d6577b', '2019-09-05 6:04:56', NULL, 1),
(55, 'Perneet123', '', '', 'janeharrington3802@gmail.com', '12511957624efa50326b84d49953a58679f9625f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f29894734ceb94e41f8928a53a2bd674', '8e85c6ba93cc3b83c1245968713437d9', '2019-09-05 19:22:20', NULL, 1),
(56, 'Perneet123', '', '', 'gparmar3638@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '241eb974c2cf09ae37d917e7ef7784ba', '08fb767a2c3b9e900d7801994b6e755e', '2019-09-06 2:15:07', NULL, 1),
(57, 'Perneet123', '', '', 'shamsulhaq1104@gmail.com', 'f85033709d77368c2d3e328b13301ce4244776ec', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '304b40d07b13420411b3b9f83a6b5728', '1b01f8c7fedef103b324fcef3d07fe21', '2019-09-08 14:31:11', NULL, 1),
(58, 'Perneet123', '', '', 'edharmendarsaxena999@gmail.com', '57fe3b901083df51acbf8d94027569ff7b47fa2e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'abaa52f4ecd99199c7c948b77f9facf4', 'f0c64b1160c35ae2b54657e18ac20ddb', '2019-09-09 8:40:08', NULL, 1),
(59, 'Perneet123', '', '', 'Saratkarkaria@gimal.com', 'b1638abbac8c05bc2c698bfcac985e9f93754113', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd8a064846b617bc57385e2524315bfaa', 'e11021d61e532691a951bdbc6c1be480', '2019-09-10 13:10:46', NULL, 1),
(60, 'Perneet123', '', '', 'pashamajeed20@gmail.com', 'fd76a8566c7ea7435871f265bb78c5d7d5cc2e9b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b17c2ac431c2acef3653817c7125a7fc', '609a08dade85a80da7518d3398abb763', '2019-09-11 7:44:52', NULL, 1),
(61, 'Perneet123', '', '', 'bappanshukla1996@gmail.com', '5c66cf6a51e0ea7de1959d2523447005fe3e1e5f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f6c0a70c9a5053e3e5da2dc1793ee1ac', '8de475cb67387a49becf40b95e065cea', '2019-09-13 3:09:45', NULL, 1),
(62, 'Perneet123', '', '', 'meenspritam@gmail.com', '99e9382858a83f6d2eb82c27b74eeb68221cdc44', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5475a7ca3b515af6145cafdbdb6ca596', 'a6f2576d57bbc290d52b6bec93077ce2', '2019-09-13 6:02:54', NULL, 1),
(63, 'Perneet123', '', '', 'hasantamboli776@gmail.com', '6f3a25fb3e3ccc151039caf8cd484fa04dbe8df1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ebb6fdc2027db4d3b0bc26a6d23516a2', 'd2fa5279596d36c49313fe828c04977b', '2019-09-13 9:56:25', NULL, 1),
(64, 'Perneet123', '', '', 'mohy.looz123@gmail.com', '395dc262c555e7e498c80ede03927ab56ed24331', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'af33cf1471408653b2139c248ba57710', '66292d7ef10da77583b4da8e893e38f3', '2019-09-13 14:56:11', NULL, 1),
(65, 'Perneet123', '', '', 'simikuar4@gmail.com', 'f8fe0d25a4f051561394215662dd110e47e720a0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2f9acd289672814356383d9481ec0507', 'fb09fbaa71ac075377666647c9c3e915', '2019-09-15 0:09:23', NULL, 1),
(66, 'Perneet123', '', '', 'muhammadshehzad786785@gmail.com', '824ce7ad5044d39d4600c64e40a76c9a06615a11', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2805c3ad3969db4c980c65c29fa8918e', 'f01f9459bdb8cd313d220f1760a1daa5', '2019-09-17 4:40:21', NULL, 1),
(67, 'Perneet123', '', '', 'amt.salehuddin@gmail.com', '3e104f5d69be381f969909f0c02f20655efbf592', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '32bbd2a7e9f4ada981b95419bfdbee92', 'f8264fe24fc826c599e446519eadd5b8', '2019-09-17 4:55:17', NULL, 1),
(68, 'Perneet123', '', '', 'kirangsuryakarsuryakar1987@gmail.com', '936a014fc67e26abd1bc1405824b74ee958d016b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f7f7e2097dfbcc8e11e4c91896c0c14f', '55cce81cbd8fd34fe3b919bf53f7d178', '2019-09-18 10:26:55', NULL, 1),
(69, 'Perneet123', '', '', 'hs1430075@gmail.com', '59bad556f61a8240c3d08f5598b0dd1924568b5f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8e0afa0632ee86ef87b54f0e2f393a3e', '40146c97fcf6d46c1c15adbaca77d2aa', '2019-09-19 0:56:46', NULL, 1),
(70, 'Perneet123', '', '', 'khattak752@gamil.com', 'a27df795777309c33af7faf7eede8c00c04bbfaf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '81e7881ea32d7ed7b282f5571623c5d1', 'f20c46cdaee4322720668c41b2666747', '2019-09-19 16:22:26', NULL, 1),
(71, 'Perneet123', '', '', 'abhi15082003@gmail.com', '695394b17cd5ebd5e96945160418569e7933ebb4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '04a24d5cb53b100716326683e19a04e4', '2a214a4aa1d1a733365d3ff7c779af82', '2019-09-21 11:00:30', NULL, 1),
(72, 'Perneet123', '', '', 'mdkhansufiyan@gmail.com', 'ccf64116c4f82d25ce147b7c7ab9b4fcb63e56a8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '40107477a1579ecb8cc6db690fd0b85a', 'e9bba75ea9a541c7ddbb9392964f7c5d', '2019-09-22 1:50:13', NULL, 1),
(73, 'Perneet123', '', '', 'joans@hitachi-sunway-is.com', 'e66dc266c8087710086771a1fd03efb489ccc13e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9b635113716b42fb1b3e439ce2353ca4', '30fb3069756b7f3882fdc2d0c24efb09', '2019-09-22 5:13:47', NULL, 1),
(74, 'Perneet123', '', '', 'singhaditya300a@gmail.com', 'b40d5071cf779bef1c9172eb7d16cf9d4f4dc7ae', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6492d0f72329c4f88b9846a8bb87f616', '91051cfa263008fa66b03c40e87c5c47', '2019-09-22 6:25:40', NULL, 1),
(75, 'Perneet123', '', '', 'www.chakwal@gmail.com', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5b8e5fa6118a51f5ad2af6880f00f42d', '8d6fbdf1eeeb0387ccc32cbb853b709e', '2019-09-22 8:43:27', NULL, 1),
(76, 'Perneet123', '', '', 'mukulsaini831@gmail.com', '646d311a0eb632d2b7ffe2da2dcf2ffff01efd4a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd166a993d8d411a16209e69924ddb3d7', '472813776239e8857d3d3115cacdc846', '2019-09-22 12:15:53', NULL, 1),
(77, 'Perneet123', '', '', 'mathurakori47898@gmil.com', 'bb3acf149db4936fbaca693a61d56be89205d997', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '719982bc0be18d2e94e9d6a4081f0aea', '6eda11e3a5a0b1adf0751e370a24cc2c', '2019-09-25 1:19:33', NULL, 1),
(78, 'Perneet123', '', '', 'chunilal@gmail.com', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '29d1cc4a74471aa0f4670c46f3d2512d', '303478be4ab8d467b3a057a259b8d78f', '2019-09-25 1:40:12', NULL, 1),
(79, 'Perneet123', '', '', 'vishwakarmaa245@gmail.com', 'cec02ecea7102f1f11e4a9dd9da319629168a0d1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c0c61c66c9c0b20d4496a4ed7bd35094', 'a8cbd226f31d11cc2d86c7af13d4f728', '2019-09-25 12:24:00', NULL, 1),
(80, 'Perneet123', '', '', 'aj255075100@gmial.com', '77bce9fb18f977ea576bbcd143b2b521073f0cd6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '18d28df86308e9ec17011bd2d1d70916', '92bd0a0f69c0447c9f57cedc9e5514fc', '2019-09-25 12:33:14', NULL, 1),
(81, 'Perneet123', '', '', 'maryhenry8498@gmail.com', 'bebdeff424afd077d6ef1914d603d552b224a42c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '468996e429cbd1c7c793116e2c185cff', '8027a3224fbd8a86ff8082b81d357e3a', '2019-09-25 20:41:46', NULL, 1),
(82, 'Perneet123', '', '', 'phulware@gmail.com', '8968be39d2f44c4a6675eaf1ed13236e8ffbbf78', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '94f8ad266e6104efb6d02874409eafb1', '3065657f3b34f64c97035bb9296048e3', '2019-09-28 2:39:30', NULL, 1),
(83, 'Perneet123', '', '', 'ansarulhoque3333@gmail.com', '188000e1f0fb4075ae1c659697b96296f982cdc4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0da623b25833709816090c22fde05399', 'd184990355470b4aa88cec472a46b6a0', '2019-09-28 10:40:08', NULL, 1),
(84, 'Perneet123', '', '', 'salmanrizwan584@email.com', 'ce517421b67b41442b1a299e916290983a311e9e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5e79e527e387d33ff2c908ce70858f96', 'bd784869e1ae4094e0d2930a2d118218', '2019-09-29 9:13:22', NULL, 1),
(85, 'Perneet123', '', '', 'gauravsinghbdn3@gmail.com', '18e8effd28685ea0c606aca182336b226c334816', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3a14bbeeafc6db1bba14c6d996ea3195', 'd21e38d2ae16ebb4ca9ca8fbcd282e97', '2019-09-29 9:22:54', NULL, 1),
(86, 'Perneet123', '', '', 'jatinroutela46525@gmail.com', '7cbf7bef159a45813b2bb5ebdcc9d6677f5f36fa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c53b1c701f819371ec9b11c9aec04ce1', '8d1615de5109405738896231740c408b', '2019-09-29 10:27:50', NULL, 1),
(87, 'Perneet123', '', '', '999aqeelmani@gmail.com', 'eead7d9f90cb27dde8bd9b0ea725f767c9a21433', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '11a6c72de9e4f43f3b349c3c999c0500', 'a691938449b824457f2dee70e809121b', '2019-10-04 2:08:20', NULL, 1),
(88, 'Perneet123', '', '', 'danishmansuri380@email.com', '4b32b16dce83c1635c709b61dfa243e304594cac', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f2504b73db9ed545a0bc27be206d1d6e', '1eb8be14bcc2e465e124c2d8595ba54d', '2019-10-05 5:06:44', NULL, 1),
(89, 'Perneet123', '', '', 'ghatelakhan924@gmail.com', '3b6f0671975bf8bfc216b958f18a21d575170f24', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b6c5134026107ba5652596264fdee5e8', '130970891063fc2e59483f72ffd3ddd4', '2019-10-06 0:10:23', NULL, 1),
(90, 'Perneet123', '', '', 'smj062021@gmail.com', '3cd31e39f4419af1e70ed39e7295553f624bd4db', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e95d0769b786e9520ed8602cd07fbd05', 'e7c826acdea3be498a880dc27f1a8518', '2019-10-06 1:10:10', NULL, 1),
(91, 'Perneet123', '', '', 'Aakashbabu@gmail.com', '23c813964aa481f833abb46af7038d609b0fa89d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'de3500b16eff92daff57a368f5aa1b2a', '4b517a95c9cee7b03b2a1db92458ad7e', '2019-10-06 3:17:19', NULL, 1),
(92, 'Perneet123', '', '', 'Utpalpayeng1998@gmail.com', '37d1a007388dbd3bb4c8ab20a06c668d597613e6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '048379866ff7f7cb5f4676bb97c465b9', '3f390c34b0cc282c5daa13dc2a7e6e58', '2019-10-06 5:17:18', NULL, 1),
(93, 'Perneet123', '', '', 'ik4263475@gmail.com', '257c6069cee7afdf234fdc963d0d55209e76dcf7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '74a82698519a4b194c7a5a6f3e45fcef', '1c7be58be60236931953a583fc228638', '2019-10-06 6:21:55', NULL, 1),
(94, 'Perneet123', '', '', 'riyasathussain568@gmail.com', '0fe0f18f2ec40f4009187306990cf7caeae5f4af', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'af17f5eed63eaf7077ff17220ccaecb5', '2cdb8dce6dfcb842452e828491d2049f', '2019-10-07 0:21:21', NULL, 1),
(95, 'Perneet123', '', '', '8084muhammadirfan@gmail.com', '68019b2db49a8f4688a6e7366c833576f43323e1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c7954115923a9f7beb501680e148f0b7', 'b343d17c412e4a83c1fd6be7e01bb781', '2019-10-07 2:04:03', NULL, 1),
(96, 'Perneet123', '', '', 'janujanu9210@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9d15cba5d60bbd13fa96d14e3a5f31d0', 'f45e24ca0e7d69e88993c7a21103d488', '2019-10-07 13:59:21', NULL, 1),
(97, 'Perneet123', '', '', 'turbowalkmachine@gmail.com', 'e88a315f0c55a808f9976066afb2692688c7eff7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3df71f5c427f061985922dd5394b43a4', 'ee0299e23900cb61098a586635b13cc2', '2019-10-09 13:56:53', NULL, 1),
(98, 'Perneet123', '', '', 'Rahulkumarmaheta@2018gmail.co', 'f446b80af4a076a8abcc17500204e045ec762704', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0b340f394c474dd344ca5ef119d37127', '814d55d1578f976a6a0eb1cfc90add00', '2019-10-10 6:24:15', NULL, 1),
(99, 'Perneet123', '', '', 'hopeandreo59@gmail.com', '39f93c2d16995dc94dc42412967da3a86cbb41d4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '53f532e78b16739ecf64f6642370cff8', '7925d004114c945914f6a856d05985d7', '2019-10-10 11:14:10', NULL, 1),
(100, 'Perneet123', '', '', 'azam1.aa90@gmail.com', '55d3872571c5023d79ab9ced47435f615a4b64cc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '564911c905ee018f2ae9c36613e13fa3', 'c82965ea8389c36931228bf3341f2461', '2019-10-11 11:22:24', NULL, 1),
(101, 'Perneet123', '', '', 'josemariahermosilla@gmail.com', 'fcbcc5d254a236dff68679484ce759e54ef22c0a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0369cef0959278d0e15ddd4ed4535a9d', '7071c3e77c1d951c27e2933f1c00c864', '2019-10-11 17:12:12', NULL, 1),
(102, 'Perneet123', '', '', 'pooja9897644225@gmail.com', '24dd3b8481b4abbfbc11bbdaeae62132d14e3cc3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ffcb9646ffeb0803cf8376d004c85b6f', '68c47c50ff73b27bc70207e12c7b8088', '2019-10-13 3:22:29', NULL, 1),
(103, 'Perneet123', '', '', 'ddeepakattri@gmail.com', 'd2eb145872b47c0920b695830f9dc7ae5b3f191f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd69886476acd147cad34cf023e8e36ca', '62826907e4ff47fb3efd3eae3698af72', '2019-10-13 6:16:52', NULL, 1),
(104, 'Perneet123', '', '', 'monalisaparida59@gmail.com', '54ea1a3a0411d197a40c50ba16d18acda2df0eb7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f367d24190298b8df70c978dfc90bffa', 'bfe5de9dfd942eecca200985455e6645', '2019-10-13 7:06:51', NULL, 1),
(105, 'Perneet123', '', '', 'augustinehutchinson1804@gmail.com', '7949b1b80d161ab5ef4c1870433c198c0804157c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '58c5d701e1f779685f0f12e1730051e2', 'b84f9174d373e0cc7eb6aea3fe27d313', '2019-10-14 20:39:13', NULL, 1),
(106, 'Perneet123', '', '', 'nirajkr12354@gimel.com', '5f2b12b0aed06127fe76386767dff6b17b15274c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '583bd8633fa224f556dcae98a3b26239', '26a9dbd31e83f57e0f8454f59166a12a', '2019-10-15 2:29:26', NULL, 1),
(107, 'Perneet123', '', '', 'adilshahoque73@gmail.com', '7160a6cce342d6704b13f555ada89faf7b360db8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8791952c61a61902eaee6a488f838d59', '7511b34a0bc968139a8c820abbe97902', '2019-10-16 1:05:33', NULL, 1),
(108, 'Perneet123', '', '', 'umeshsubudhi665@gmail.com', '1396330a110f20f58e215d78cf8f74a74c2ac456', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f84c2f61b535ce511cdf4fe9094c02aa', 'fc955bf11eda08e35749e5a5e07d84c1', '2019-10-16 2:09:24', NULL, 1),
(109, 'Perneet123', '', '', 'biswallikan1@gmail.com', '1eb7a57f8f546f6cae7c1a56a7c6e090934d4902', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '247d6aa73465470bf9d0fc4413859567', 'e1feba445bcec52cb755362ad5fa844a', '2019-10-16 5:25:16', NULL, 1),
(110, 'Perneet123', '', '', 'volapradhan9593@gmail.com', 'fc9bdbb08504910af50439e126256e167e9c8753', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e63e312a2a7d35674d5c366e6abd203c', '6490effd77f1fc3fa065dd87a0723685', '2019-10-16 22:14:29', NULL, 1),
(111, 'Perneet123', '', '', 'sharmamohit99131@gmail.com', 'a9e3d477eb4168314ff76ec60c10762626f0595b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd7ad72550d38b0be54d1dfddf92c637d', '440a09a9be6b2d3b9ac53bc3345f8abd', '2019-10-17 4:43:08', NULL, 1),
(112, 'Perneet123', '', '', 'pramod45629@gmail.com', 'a0fc7d4c461f04ee2aa691f9c3d1735ac7931026', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9c2f689cef55b135944321732ad29063', 'f548b709f68ce51339a37111635fccd0', '2019-10-18 0:59:37', NULL, 1),
(113, 'Perneet123', '', '', 'pramodbbk8318@gmail.com', '899e8ef8a7217ae7c0e68e627215dc420de77518', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'eb30b3c9571a42350b41b020409401e7', '93e46ee57087f3176ab57c9bf6bea5b7', '2019-10-18 5:28:36', NULL, 1),
(114, 'Perneet123', '', '', 'mukeshkholi422@Email.com', 'b4bcde443991b8e0b9bb95e735be0fa9923e4443', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e41251f1a750f484306a1843a34e194b', '0a74ecf80623591e707c141f10080b24', '2019-10-18 9:09:29', NULL, 1),
(115, 'Perneet123', '', '', 'vishnuyadavvishnuyadav240@gmail.com', 'cee5e62988aee36e3e74e68fce57605f603dfa7e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '930fe188791c465ece6472fbc57186c1', 'e019597a9138e6164713f8701c431f02', '2019-10-18 9:23:03', NULL, 1),
(116, 'Perneet123', '', '', 'sharankenya234@gmail.com', '9d9ad08c2b4689069be64e5b3a4b9d9c9cb19486', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f6f040235011b9b4bb60cefd878f2827', '9a7f88c17c44496e7130da28c14428c6', '2019-10-19 2:48:53', NULL, 1),
(117, 'Perneet123', '', '', 'rajd89838@gmail.com', 'baa34824914bccdc232d8dcb533d184dccf11936', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cae082470bbdf61a06e69d3661b31436', '39e96d43a14ee393a9e8fa6b9d9432d4', '2019-10-21 5:14:19', NULL, 1),
(118, 'Perneet123', '', '', 'mk9815766@gmail.com', 'a36be8d72b176a2b494fc20b1f0cb39d476e4c9e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ae04d459bfa992a9cff7e39fb8170a07', 'a4f37e4c56ecf66e7b40abccb58831a2', '2019-10-22 2:04:43', NULL, 1),
(119, 'Perneet123', '', '', 'homchandhomchand2@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '92a063d7d866edcb47adbf0a9cb17b44', '10e29aabec1dab59ba93c35a0f548be9', '2019-10-22 2:10:50', NULL, 1),
(120, 'Perneet123', '', '', 'alokpaital4@gmail.com', '192e4f2d3eae3e4208e3425c2b9d508b7bba8429', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '26c691f71f20ee61ae22d2a42694ce54', 'f3a8d3fce0c323fd6206d980c5f203e9', '2019-10-22 4:11:19', NULL, 1),
(121, 'Perneet123', '', '', 'jatkuldeepjat123@gmail.com', '27bab79591722399ba597be6a47de2cf7fafeb58', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '35fd06d68fa57de8d2a3e09999f15287', '8072fcfb71bdc139c47b1fda9a7504df', '2019-10-24 0:06:50', NULL, 1),
(122, 'Perneet123', '', '', 'ALAMPIYARUDDIN2@GMAIL.COM', '90d11e78e7b77bd37fd1a6c32fab9bf58d29d82e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f8eedbf7de55339c1d64146c81120b7d', 'ceaec49f47ddbf04494311f5c9117aea', '2019-10-25 0:21:50', NULL, 1),
(123, 'Perneet123', '', '', 'rajkumarray462@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7fb70344643eff9bcd5d7d52bb3e5dcc', '219883a1082fa1d1fdc6ecb416c69591', '2019-10-27 2:13:02', NULL, 1),
(124, 'Perneet123', '', '', 'ssuhailkhan917@gmail.com', '8a85fedcff94583694e20d7656cf3e56881fe14d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a3f301c702a92934d1a7b5f2b94ef946', '996c8e33b96055a6976d15be94799090', '2019-10-27 3:17:47', NULL, 1),
(125, 'Perneet123', '', '', 'sshivaram839@gmail.com', 'b2abd9dc4aa406d931e652d7d2d34c28b8440216', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd92be24568e876119bd315589899534f', '4028e74d7d2e178f34c936b8fdb12603', '2019-10-28 10:30:46', NULL, 1),
(126, 'Perneet123', '', '', 'pal241407@gmail.com', '1bcdb05e79565f00ee21a746132eebf27bba9a4a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c8cfffd5564f20145cc6c056d6599588', '423736187bdb6376bc0559c40a6de30b', '2019-10-29 6:11:07', NULL, 1),
(127, 'Perneet123', '', '', 'chandanyadav2004cy@gmail.com', '6ed1b3634776355dd4dc19b31260c37e7493356f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f3354ffbe81cbdada1be14a7ca69673b', '54ea67a7c22e3af1e63b03b61b263cce', '2019-10-30 7:27:38', NULL, 1),
(128, 'Perneet123', '', '', 'subalk858@gmail.com', '6a4978520210436f833edc839d8da8a65a01c25f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c7e7a8b1a09034a1141439cbd04c5b47', '62991af3bb863b5eaabafc48f823723a', '2019-10-31 3:04:40', NULL, 1),
(129, 'Perneet123', '', '', '7420984228nivassarawade@gmail.com4228', '7c222fb2927d828af22f592134e8932480637c0d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '22d730cef9558673536baea472eb7f07', '483c5e9853c123110d8d152605125274', '2019-10-31 3:19:07', NULL, 1),
(130, 'Perneet123', '', '', 'alamkhankurdiyarawatsar9660@gmail.com', '43fac3a6c67cf71f906c775703f2f8ecedefbbe5', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c39a20abf29382df1439792ae34f6b08', 'c2737ff34316403ccf2059ccd800994e', '2019-10-31 4:21:36', NULL, 1),
(131, 'Perneet123', '', '', 'Nandkishor2323@gmail.com', 'd58c2bc7ea63abc8015639ab1d40cb2f4bb874e7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4cdd3eecabadc30ff0269f4b65f77316', 'e1e094da9d33d83a479438d87cca4f66', '2019-10-31 4:41:13', NULL, 1),
(132, 'Perneet123', '', '', 'asm18@gmail.com', '8e79a3ca91ee3f02534b72b22f88b8d3af367960', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '135eddc6b32425aca90d270f8a6b4c34', '679a15d710d3e7f2cee4bd25fc9c667d', '2019-11-03 1:22:47', NULL, 1),
(133, 'Perneet123', '', '', 'sahr88049@gmail.com', '0066d5f017ed5cef522d6bc299dec5f681c0e3fb', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7515e5afff590723dfff7131198001b3', '2c2bf70a3da749e172c2ffacea964e1c', '2019-11-04 0:18:09', NULL, 1),
(134, 'Perneet123', '', '', 'ankitankitkumar4099@gmail.com', 'a14f68d84b406db2665a358374a8ec5c62991656', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '64874d8a38747ef50ad0251eac83db94', '2a1805e8817a5cb245f162c737a30b8b', '2019-11-04 8:24:35', NULL, 1),
(135, 'Perneet123', '', '', 'cy2152350@gmail.com', '5b09eece4a5b11d5d0125c0f7eca424d593874ed', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '88272408812e289a3afc8273ebefb9a4', '1943a31aa64d696dc316111aa0a12803', '2019-11-06 0:03:44', NULL, 1),
(136, 'Perneet123', '', '', 'rahulpatel7486884222@gmail.com', 'b5f933dbc41bb8cd22d71372c42c0d4b82dcc5b2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '39815bc80fbf40f40b33cbaeacfc320b', '245bfdc7954250f7d506d493198409ff', '2019-11-06 12:50:15', NULL, 1),
(137, 'Perneet123', '', '', 'Samiullah1997@email.com', 'e62f4f868881dc1d425dcf8c9ce36bd88002c064', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b97e8fb439f9c0d58b49880c4902b74c', '9d91b961355bc6c6a36a0b1534199153', '2019-11-07 9:37:30', NULL, 1),
(138, 'Perneet123', '', '', 'madhuriaasanani199@gmail.com', '9eae79e74b244492d8be1d09de8aef882a80596d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'fa64a948b1ead6d242c29893926f9925', 'd85216ceaf102b1445fd73761326dcf0', '2019-11-08 2:20:47', NULL, 1),
(139, 'Perneet123', '', '', 'amitrajaji6@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '12b0ff0604f0d47371c4ed8f65568494', '0183257c093e3595646ab8262437ede7', '2019-11-08 7:21:56', NULL, 1),
(140, 'Perneet123', '', '', 'sb914729@gmail.com', '82264490b2f40fdc6a44ffd2a468b76cb3cd12fb', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a1c0f4ea8ecac014a240856e9fdd9901', 'c6c13542d9be6f3546f4a517a80a9407', '2019-11-08 11:35:18', NULL, 1),
(141, 'Perneet123', '', '', 'saroba1122ibrar@gmail.com', '4d082b4c5fa73953cc84398cbc597dd86d54c180', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9d8381fe489c63b1c869f7942994eeee', '33924c7af8728a2fe98ee90d8d635e44', '2019-11-10 7:29:59', NULL, 1),
(142, 'Perneet123', '', '', 'praveenkumar123@gamil.com', 'a7d579ba76398070eae654c30ff153a4c273272a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '103cb8940bc0fc3b4095769dc61a386b', '6af72254152b744e9324e64f14ebf492', '2019-11-12 1:43:22', NULL, 1),
(143, 'Perneet123', '', '', 'Adnanpasha123@gimal.com', 'a7d579ba76398070eae654c30ff153a4c273272a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3e1c78b3e6da3d2c7404c2d25bfce1d1', 'e7d64a14b90a790982375b981bdf4cb0', '2019-11-12 1:45:40', NULL, 1),
(144, 'Perneet123', '', '', 'pndittiwari123@gmail.com', '00e89a20606e498ad2c5a9aa340f0452a2ca3e05', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cd64510f5770f7e297e4f5acedce531c', 'ff51a9c532f9ca7d04a5c68990f47582', '2019-11-12 10:06:51', NULL, 1),
(145, 'Perneet123', '', '', 'mintugardia123@gmail.com', 'ef50835002a4d423712c22f922fe1258a82f5fe3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ee794975207c73f63544aea3ce7c7545', 'be18296d29884d30cc7ec2978023e84e', '2019-11-12 21:33:53', NULL, 1),
(146, 'Perneet123', '', '', 'kindakumarvajpay@gamil.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '78474e078c697d98cfe39af092c8d592', 'a7e668ad8f8d7aec9c75bdbd4ef0e9f8', '2019-11-13 9:14:52', NULL, 1),
(147, 'Perneet123', '', '', 'ggodwellgivet@gmail.com', '38009e32a38db007803eb9cef1f3c44a00380e3c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e0ceeb0294e8ee70756995befb5b05b5', 'fa7ac57c74977915d78a9e63eafcf1d0', '2019-11-14 4:41:15', NULL, 1),
(148, 'Perneet123', '', '', 'yogeshdalavi2019@gmali.com', 'a824b4850e7dd2bd0421abdbc68a89d8fb2df67d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e6c7505dede622c13bd21cd2e1e06947', '6060a398ed8a01e8ae646e2d7d9d075f', '2019-11-15 5:23:29', NULL, 1),
(149, 'Perneet123', '', '', 'premkumarbauri63@gmail.com', 'b4fc4d360c2ede3a3e5b1e8afaa09ac3b88580f0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd6d2d75e3e016c75f804dd91908bc9ca', '23e58612e3f95920c79ee5c9af252940', '2019-11-17 7:08:00', NULL, 1),
(150, 'Perneet123', '', '', 'pranjitb213@gmail.com', '4f36b94c5e818d3176afaa142ef22fed875a9824', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '84d8f3a98cde8fbb61d3922dc88399ff', '74394a6508f9e00c0eed2981edfff40f', '2019-11-17 9:33:58', NULL, 1),
(151, 'Perneet123', '', '', 'Vilaspatil@02.gmail.com', 'c028c0c77972d74c63adf322de6556ae11257f76', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd019390b51546424db9ccc15ec97ee89', 'e96d50aab50cfcc35aed378cca133b13', '2019-11-18 11:05:48', NULL, 1),
(152, 'Perneet123', '', '', 'Khansamim@gmail.com', '53a6b8ce11f37e5bddf6521cdb93c1f864e34280', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ca296fade1770cf63a6d02275ae5da0e', 'af88fc1804e50479740414ed1c5abd2b', '2019-11-18 20:52:35', NULL, 1),
(153, 'Perneet123', '', '', 'adel230992@gmail.com', '13a024eaaa8325e0cc72e88f1f0c8ec5c548efa8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3d27557bddc2c3d36eea150e5976ff69', '8d2cfe32bb8c7f42341f2beec73c5d9b', '2019-11-19 4:03:14', NULL, 1),
(154, 'Perneet123', '', '', 'ramashankar8002631859@gmail.com', '9b9217e3e6a23ab5efcbeee5083eb0114c253a0a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '397d8359d34dfdf5f510361b9951994c', '5fae4a60addbcb9522b9671d2a39b419', '2019-11-19 20:51:02', NULL, 1),
(155, 'Perneet123', '', '', 'anxumanmoran791@gmail.com', '2c4728db016d8e025b0987d38c2a05bd0d276bf5', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2f1052b74c7dcad72949a3857646407b', 'f4101b8001d492958977293176f3b301', '2019-11-19 21:03:39', NULL, 1),
(156, 'Perneet123', '', '', 'ihsanullahafridi099@gmail.com', 'cac8ac2df0bec429aaf67658550fd9e1d921bec5', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4513225c53768c2d2fdabdcb2eaca598', 'dd8312f25d5ea6cb31b89d422e02c45a', '2019-11-19 21:31:54', NULL, 1),
(157, 'Perneet123', '', '', 'suhelk013@gmail.com', '3a180a5659bb78a7891bd2f81e486d880ab092de', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '1b387560c83edc2dd0f2d39bbad9d41a', 'c63ac50b94723831db6edfb642c9257c', '2019-11-20 2:02:24', NULL, 1),
(158, 'Perneet123', '', '', 'sunainak922@gmail.com', '40744847360b0557af36f5ec09dfa3870b441d55', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '42f55867b12a5a7c0ec82d64005d9817', 'f9fd92f8ae3c81ca5c9db9c10b47ac52', '2019-11-20 4:55:17', NULL, 1),
(159, 'Perneet123', '', '', 'bhabeshmandal255@gmail.com', '5544d53c7c5f5171b3a5fccdcaa623a65110aabf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9fa3df715048b6a158402a5641bb0696', 'a52506d1fcaf36b1928eeddbbd70e54d', '2019-11-21 4:52:23', NULL, 1),
(160, 'Perneet', '', '', 'psingh1405@gmail.com', '6367c48dd193d56ea7b0baad25b19455e529f5ee', 'psingh1405@gmail.com', '2021-03-31 11:31:15', 'f67e1923c1b2209d68d379f1d23a642c', '', '2019-11-21 9:01:32', '2021-03-31 07:30:33', 1),
(161, 'Perneet123', '', '', 'cory_martin150@icloud.com', '19a64243d316423e63c5cf3589ce13bef713eb17', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6d2db416c8534c4ad1003c37a3edbf81', '8e3d9990d67b9a695995b6b01673f12d', '2019-11-22 19:07:07', NULL, 1),
(162, 'Perneet123', '', '', 'kirankumar59838@gmail.com', '50038b498f02985882015a245075087abe64ebb6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'bf4892dc04aa58602b58085ebf1b28eb', 'cb96086ae7e1c4dc2011e53537f60f6b', '2019-11-23 0:13:12', NULL, 1),
(163, 'Perneet123', '', '', 'ry9824888yadav@Gmail.com', '5691473103f72987ed22d6e1002ea0d67c070f7c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '48b0cd9b222b2c2339350060a20092f0', '7130eaba9837d16a5c133ceab1c17d4c', '2019-11-24 0:12:07', NULL, 1),
(164, 'Perneet123', '', '', 'pavan199864@gmail.com', 'e46987a5eba34cdb56dc06c58e7b2ead094f4ba0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6f7322d934e0a031f0d38ebc236f3940', 'e63a060b2a2df40e01dc1056a697b64b', '2019-11-24 2:28:39', NULL, 1),
(165, 'Perneet123', '', '', 'dc7972787796@gmail.com', '1e23343e02b003ed8744c03c2d1a421566801747', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f379d8bfbb08aba586a4af1334d56d6d', '39eda5a6b5ee72b581a8f20e3a170045', '2019-12-02 0:52:48', NULL, 1),
(166, 'Perneet123', '', '', 'dangikumarkundan@gmail.com', 'a76debc23867edb40f5a307f2615461b466954ec', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '177e25f16fd57a9d04813388561a2f5b', 'a0b726086f436ef0e6003eb32aadca82', '2019-12-02 1:56:59', NULL, 1),
(167, 'Perneet123', '', '', 'goutamkushwah@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd5822179d88fe6bb5b1db6f92896793a', '8fb229ecd190a14afa8f62f5d3068daa', '2019-12-02 11:58:12', NULL, 1),
(168, 'Perneet123', '', '', 'shahidmgs04@gmail.com', '7e338c2af61fec25aa5aaf4efe98f7c7275043e9', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '075fb14b8274c65799d98a218dca4965', '49a79ab6435d9fdd59a81ae2fca90a47', '2019-12-03 2:34:38', NULL, 1),
(169, 'Perneet123', '', '', 'challakere20@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f29e72eb5d83ec64bcd709ea7cd91d8e', '87fce28bda9f42698ea85da10851c622', '2019-12-03 8:41:31', NULL, 1),
(170, 'Perneet123', '', '', 'prasantadandia9@gmail.in', '16d930c7b78a4cc0c6c4f62869f90f121a4fe604', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '767bcaf98c428e3d80f681253a67f8af', '1745978259087aa501757417cd3ac938', '2019-12-05 22:25:59', NULL, 1),
(171, 'Perneet123', '', '', 'arunkumar991485@gmail.com', '032d463df8473097dbc140803b4fff65fa7c42b9', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c767a853eac0452e7ddb774db094cb9d', 'd49718df91870f56d7672792123e49e0', '2019-12-06 7:22:42', NULL, 1),
(172, 'Perneet123', '', '', 'sohangazi750@gmail.com', 'cac2612f820091484d8d4df7ceaf0b131953484c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '245f2a64757694c922776a0caae7f24b', 'f7f0ba86174986a7cc379250b9a857e4', '2019-12-06 10:37:52', NULL, 1),
(173, 'Perneet123', '', '', 'mrimteyajansarim@gmail.com', '428440db31c9453fc6d9ed50de3f6d8032249960', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '44d72855683a81fd84b23f86b91d0ce4', '7edb958bc32d2db04383c11fce6f2f38', '2019-12-07 10:09:24', NULL, 1),
(174, 'Perneet123', '', '', 'viswanathababuraob@yahoo.com', 'ac462656cc58f7dc39fc32d98662cd83f9626f86', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ac9f100ecb8a230ce8d4a1c9eb553f3f', '469816bdd625e1ffc90315d39e2e3f5b', '2019-12-07 15:26:32', NULL, 1),
(175, 'Perneet123', '', '', 'ksanjay21052002@gmail.com', '7e74979395516814f2bb4ea619977e1c0104e5d2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c6ba199fbdc3b7e2d14045aa267382a8', '540c56ba17f445db03f911fad022faa9', '2019-12-08 5:13:19', NULL, 1),
(176, 'Perneet123', '', '', 'vivekbhoiv@gmail.com', 'aa3a2235cbfca6444f2686c3a47782fac10fb14c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd8a3c9779abe756d5401bade914e0773', 'e17b477a868abe29cb46aabd20a3f19f', '2019-12-08 6:12:42', NULL, 1),
(177, 'Perneet123', '', '', 'bechankumar191@gmail.com', 'd3dbd6f32aaa1a109cbc5a9a38e2a4aab9198638', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c2a8d1ab3dbd42834c340a7bb2c950f3', '84b62b04e73b7d84d3440d30d44d5f9d', '2019-12-08 9:26:35', NULL, 1),
(178, 'Perneet123', '', '', 'rahamanajeejul@gmail.com', '8a14088b79ab31310013a0543ad4fb1156d8cd5b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f08eb088441da41de074dadb3ab8e1ce', '2844b2aa870c51da564d947eea191b0d', '2019-12-09 4:34:13', NULL, 1),
(179, 'Perneet123', '', '', 'aryan01tomer@gmail.com', '95676b0e6dbf2d9842c411008fb9e4fa7ef4709a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'aa1d938951036650bce7b60e844b93ac', '6aa18d9b58b9a482d2c62f6d95a56db5', '2019-12-09 13:37:47', NULL, 1),
(180, 'Perneet123', '', '', 'suryavanshisahil101@gmail.com', '12dd9ade926736faca4c5d335319069e1397a79d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd076cea4bb7c9ee6f28f5a73ae2901fe', '54360d6760c2cffcff405ce677a62605', '2019-12-10 8:16:39', NULL, 1),
(181, 'Perneet123', '', '', 'koul1.naveen@gmail.com', '1e0b539b16a01cd3dd23600946123e765d7e1c2a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c78b2ad08f136f36349050e1d913c825', '0cc4d47954c78071edb226af9ea7606b', '2019-12-12 3:09:47', NULL, 1),
(182, 'Perneet123', '', '', 'noovophone@hotmail.com', 'fdc83553f78059239fe0c39bf0d126c4a3b9cfe4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '970976289d0c70aa891bcfc2c1d5ab55', '3cc21a3f4865a57fcce3f0220b1c1e18', '2019-12-12 13:45:57', NULL, 1),
(183, 'Perneet123', '', '', 'awan73623@gmail.com', 'c1f06864ed41ec006c807a9a277cc68e537a9012', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '029beee15c2f9d34538f8c2427bfb931', 'f1c177b0e367d1c3cd0bfe16b4bab7b3', '2019-12-13 4:56:47', NULL, 1),
(184, 'Perneet123', '', '', 'ranjananeware036@gmail.com', '1087a0aad51f1647c6db84b12ddb7a6b8c94135e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8246422ec1ec0d5912ec06859f1cdd51', 'b19820b56a5e3cd32c593c6ec2c959c3', '2019-12-13 11:25:55', NULL, 1),
(185, 'Perneet123', '', '', 'ramanagoudrug@gmail.com', '8b7902b17d7266f3960491fa3474cb8627c8dbec', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '768035b03c9717cf3ca923536d6101bb', 'a96506fb7babc57f5b11602626266807', '2019-12-14 4:01:26', NULL, 1),
(186, 'Perneet123', '', '', 'nehapaswan359@gmail.com', 'dcb3d302411361b646202aa640abacece4239226', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3708056f245a480c09a8ec4c00407d5e', 'ab7a7a3389a7db63643cc05c87b99a6d', '2019-12-14 6:40:36', NULL, 1),
(187, 'Perneet123', '', '', 'pavan8052677531@gmail.com', 'd62860fe49b5eadd4a0a5c6647e3f41f378d8c72', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8d519b5f48c14f111946921eb466ad34', '2429df1cffa5fab05a65ee99cc69db13', '2019-12-15 8:48:03', NULL, 1),
(188, 'Perneet123', '', '', 'ajeetsharma@gemail.com', 'cae77722deee6ca505df4f3d80fbf37113d85835', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '02ee862c6aa10c4885db4e316018eab0', 'da7c96d2670e9077f0cea818194eb9fb', '2019-12-16 8:34:40', NULL, 1),
(189, 'Perneet123', '', '', 'jadavsuresh2988@gmail.com', '8cb2237d0679ca88db6464eac60da96345513964', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e0fe54755853a9bc974b5d28b4a88715', 'cea6baefdc6a8c4faa6627f0596ed9ad', '2019-12-16 10:32:45', NULL, 1),
(190, 'Perneet123', '', '', 'Mageshmakwana480@gmail.com', '91a5d4063bd33442f80956a47fe9128932a42350', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f8c41a8bd16e38822f4323688ea1a790', '5cf0d4da7dd4083a6b459ee4f2479cf1', '2019-12-16 10:46:40', NULL, 1),
(191, 'Perneet123', '', '', 'rkbhadwar1234@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7f4f4b69244efdcfbca99bfc14c8c764', '70cf4ab44171565f8e9fc52e1b3d68d3', '2019-12-17 4:38:09', NULL, 1),
(192, 'Perneet123', '', '', 'dasniteshkumar944@gmail.com', '7b41dd81f883401bf7a4ff613be53dd159e96365', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cc1a8e455f7b5cf27c9d30feff7e4fe7', '8f5edff58567eadbbe3f1caec206aeba', '2019-12-17 5:03:53', NULL, 1),
(193, 'Perneet123', '', '', 'pankajkalasua212@gmail.com', '9d75980186d59b4bdb1ff4a423b0f2c1a3a10ea0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '030a54ac42d92b4cad92f3b3534bc8f1', 'cb9e5aecd5c0b3facc5f6d66653d892a', '2019-12-18 2:11:30', NULL, 1),
(194, 'Perneet123', '', '', 'ravikumar115200@gmail.com', 'bf93b1f54d7b3136cf85f32e285f7f7abb43d464', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3c111f0bc85ac838911ce26a75159cc5', '68d3dcf45445ca65780671321a832ce5', '2019-12-19 2:24:52', NULL, 1),
(195, 'Perneet123', '', '', 'patelakash8951@gmail.com', 'bda91d666b94e8d753c5267650eccfd3e48ac0d1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '26f18221d20392b745d45fa24f5eb337', '8b1f98368b6e09ab9059bcb0225528b9', '2019-12-21 1:46:29', NULL, 1),
(196, 'Perneet123', '', '', 'pankaj9690309878@gmail.com', 'c59d47316dc7ddb4920479f490619d54d1e9a648', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4b1ccf5d82d7e23b8ce8816b25bc7752', '85018925999582efee49234c7feb5e03', '2019-12-21 2:06:13', NULL, 1),
(197, 'Perneet123', '', '', 'rajsondhi885@gmail.com', '29c43eb0bfbc2c0292b17d10f99b29e3dd235c84', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '570ee156c899a6e532495b21f113cdce', 'e66d15e0779b2c2f231cf195c334193d', '2019-12-21 2:19:14', NULL, 1),
(198, 'Perneet123', '', '', 'ramn04320@gmail.com', 'bd4a9960de2f007ef9c9d16e770ec574efc7c7c3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9465c061327a87c8376f15b94d4692cf', 'b5589396c522a92bfcfa0cd589e25cbf', '2019-12-26 0:19:06', NULL, 1),
(199, 'Perneet123', '', '', 'dheerajkumarsingh12@gmail.com', 'cd3713980a10793cefa4496828d84387b8bd9b1e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'aee064e21751041aee0095aadf7da4a1', '3de07385c943212c77a0eeb45ebf7d1a', '2019-12-27 0:13:22', NULL, 1),
(200, 'Perneet123', '', '', 'ravindrayadav10577@gmail.com', 'fbff8b48067a367c936556834e9de88cf78375d0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6e6989ae602f81d50397a53849b71a3d', '9fba11b0e898399a1056716635530f11', '2019-12-27 0:19:31', NULL, 1),
(201, 'Perneet123', '', '', 'mtiullah93@gmail.com', 'a8ce74a7eba649a0154075d26bc68d1e13b911bf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '18cfe60caa5f46628914cd9a90b59bf5', '8bf446157e61537a453d960c02f2ffce', '2019-12-27 3:30:02', NULL, 1),
(202, 'Perneet123', '', '', 'ramrakshchauhan@95gmail.com', 'cc425d4ac5304c9f56ecb27fe52ba7aa8537df93', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ab7d3014fc21cb26c25848b6f0efebda', '07edd6c95eb24a81bc0f0271a875daa6', '2019-12-28 9:12:35', NULL, 1);
INSERT INTO `client` (`user_id`, `full_name`, `first_name`, `last_name`, `username`, `password`, `email`, `create_date`, `activation_code`, `forget_code`, `inserted_timestamp`, `forget_code_time`, `active`) VALUES
(203, 'Perneet123', '', '', 'ajitgour18@gmail.com', '9b4143656f78a45cc88257c6d68bdc716ade4181', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a23007def4f4dfebef269898f9d59c56', 'aa30a116759149d26abc5bd8e2af8178', '2019-12-29 3:55:45', NULL, 1),
(204, 'Perneet123', '', '', 'ammi113514@gmail.com', '6193cdb4b23b7c441fec28689eef6858c48ecb05', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'baa6951628eb0e815b10ce6b3032cd6a', 'c96cb5281a194b323ab615e6dd68c628', '2019-12-30 0:17:35', NULL, 1),
(205, 'Perneet123', '', '', 'mohdu3774@gmail.com', 'd7a3c3727d6a192bb071ace92ce9f427d9343a70', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '59992e83a9a6deb45446090e7a165dfe', '4345e1f32bec69115d5658d649140d76', '2019-12-30 3:03:03', NULL, 1),
(206, 'Perneet123', '', '', 'dilbarhussain382079@gmail.com', '2a46a4c7d5e47d59155d3827c3517fe04d05e618', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '44ee6734f78f588bc692a14043a7c333', '16562a63cdb180690c38153cd0c1370f', '2019-12-31 1:07:02', NULL, 1),
(207, 'Perneet123', '', '', 'jagadesh7895@gmail.com', '0d4933218bd8774b4ade8e5dd2ea3c804c69bf31', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ab60b19c4dadbd6b5643c17c7df6d862', '9a7fcaf5b6fbdd9f5f59bad6ddf46d50', '2019-12-31 3:32:34', NULL, 1),
(208, 'Perneet123', '', '', 'veerpalsingh847694@gmail.com', '6164115e46dc2e54d63d3c18ff90c9709f448628', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cd4cf2e1dee3f46e62f776691d879ed3', 'e15fb16f28eeb640553609c930da848b', '2020-01-01 2:53:30', NULL, 1),
(209, 'Perneet123', '', '', 'kamleshkumar981@gmail.com', '1309ce0ec738f79dc8d07144dd23e583be91c2e8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '745b4f59e6dd05f22969b16731f2ab39', '437eabca46237ec40d1d5f21b614dcc4', '2020-01-01 12:05:48', NULL, 1),
(210, 'Perneet123', '', '', 'shadabtar14@email.com', 'a573c8ab56d7622bb8544c2b8bc551dcb6ef9f5f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '80522a158bf84d2ad872b99ff2d440cc', '170fc6d2b4da096e47b1cf29ec422b6b', '2020-01-01 17:23:39', NULL, 1),
(211, 'Perneet123', '', '', 'sk2246704@gmail.com', '7e3d2bdbde73ae480100fad6ef12bada7cffde09', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f4394d4f6aaf7e4520574f41e423d31f', '08c807975877f250410f1d6d15391ace', '2020-01-02 2:57:59', NULL, 1),
(212, 'Perneet123', '', '', 'chandansahu4547@gmail.com', '278b9c8cd47054e0293613239a90b0585841d9ca', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'bcf8f60b1dc336e42b9c8315f63a21b6', '090d4aa5cbd804522cff525c76ac9234', '2020-01-02 6:35:52', NULL, 1),
(213, 'Perneet123', '', '', 'sarah_younes@live.com', '9585cafc15b5cc207842633e36004ea0192c4f3e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd5476443e9a9dfcd4d4c1db7170fbff9', '32b9b62404bb641584ae989ef2787669', '2020-01-03 8:00:04', NULL, 1),
(214, 'Perneet123', '', '', 'shivarajhugger@gmail.com', '349a13409fa752c40d529b7c073689dec017a019', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '43d371f10bd60b88cb4a72ba2fa40e91', '8cc45d829e15cf708f6fceaadf5dfe28', '2020-01-03 8:01:47', NULL, 1),
(215, 'Perneet123', '', '', 'vishaljadhav43549@gmail.com', 'be12ee01f4421099a588e3445b59100df20e59ba', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd2637d979f37fdb8a397ced41a9837a6', '623896401673da4f64afcc6f5338975d', '2020-01-03 12:06:12', NULL, 1),
(216, 'Perneet123', '', '', 'wajib122@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c7ff69e5e9ee4a5311af8a7fe67cad51', '6ccf857a47274e9c7e29c881d16a3d53', '2020-01-04 11:25:41', NULL, 1),
(217, 'Perneet123', '', '', 'ahmadmajeed059@gmail.com', '199df2fca69173acb5e98a5782eadd53fdb167ec', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0d898fbad2795c85daa2c80f0e253652', '570824e2faa342e3d16ea2796f43247f', '2020-01-04 14:32:23', NULL, 1),
(218, 'Perneet123', '', '', 'shivamsharma23739@email.com', '54351fe0b4b314a6bd03e037f53a00d7e53a6bc4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7fcf930004f84d9345dc556d9595159a', '3b090b79754e14dcb0f70cb2e87c3827', '2020-01-04 22:00:34', NULL, 1),
(219, 'Perneet123', '', '', 'bhagathammulu143@gamil.com', '17f5933d02a7d0e9714ae3b1ff57606f713f3046', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8f471c624a6336a862187c82b7b55b9d', 'bc81014c988e92f9f09f0f3b56e07244', '2020-01-05 2:14:23', NULL, 1),
(220, 'Perneet123', '', '', 'mary_lynn_1@hotmail.com', 'c4c10accaa0f9d5d1a95c7b44da07a1feef4d3e1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8406211c35059561765e235ff1dbe1a1', 'adc292299a8ef9434aadb4d038ada4a7', '2020-01-05 18:44:15', NULL, 1),
(221, 'Perneet123', '', '', 'Vaibhavhakke13@email.com', '28f7fde4c0ae8badc391b5c71819ff59f8444724', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ddd14de1bc6ca0665652ada1ca4e1e4c', '8173031c4d1dea2aa942390ee674f14c', '2020-01-06 2:27:48', NULL, 1),
(222, 'Perneet123', '', '', 'rajpandey@gamil.com', '6ff3bc7a33a0a6dfc3bc4691136d76c0be3359ad', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd09f7e51afca6f72d7e325adc3bf0989', 'c5b9ec91e1a242d619fe8ece87865989', '2020-01-08 7:57:00', NULL, 1),
(223, 'Perneet123', '', '', 'rahulkumaruganpur@gmail.com', 'c0cb365699864c59e3e5755cb91c296138b6f3b6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8fabf368a693349eed3405a03ef2896f', 'e27280828cde0877ce4101cccf1ce517', '2020-01-08 9:10:39', NULL, 1),
(224, 'Perneet123', '', '', 'pathankesharkhan@gmail.com', 'a733544470e720295473f2172abee0520ac13879', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '570c4c8165c79a2557f994dd627a7200', '7c8fa4eec299ea70b483117512ab3e91', '2020-01-08 10:13:22', NULL, 1),
(225, 'Perneet123', '', '', 'godinsly1@gmail.com', '15dcc0e5d01c0f4ca6804ee6175dedeb671be76b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '49e83b7d48edbf83be1bae5b017c5821', 'ecfa3cf7350857628b617b476ac707ef', '2020-01-08 21:58:09', NULL, 1),
(226, 'Perneet123', '', '', 'Kajaldevi5191@gmail.com', '713061c18b3403958b57ddcf2018099c93a860ca', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c5c28b8bc99a7a396cddf79714d96398', '6eed505c04bee9ae29b5ce1dee458243', '2020-01-08 22:27:12', NULL, 1),
(227, 'Perneet123', '', '', 'mm5444298@gmail.com', '5f0fcad96e75b16faa7fffca4d56872891166c2e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '523adb918ebec1fc85565ccb0dc29149', 'eb3e61a4d3c053afed02d47051c473de', '2020-01-09 1:32:38', NULL, 1),
(228, 'Perneet123', '', '', 'ajayd0085@gmail.com', '50fd6b9882cfc893953f92003326cb40db9813e8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ea66bf56f0f01690c62ea7cbc4d72915', '9dd790d3d53e7e39023227510d2c1e91', '2020-01-09 5:41:32', NULL, 1),
(229, 'Perneet123', '', '', 'mrdev331@gmail.com', '1376422430999292360a70508dc2f86ad8c31861', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '35496624bc4f22f5789c88da6d143bc6', '3198237560d8f5979edd3b3c0563f04a', '2020-01-09 9:40:03', NULL, 1),
(230, 'Perneet123', '', '', 'danielkakwata7@gmail.com', 'b2619843896d2d41270b40ea6c388f4f273a4a95', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5b51c48aaa8f8907ee562ac381ab82d9', '3bdcd9e33228140343e74972d5432dbb', '2020-01-10 14:39:37', NULL, 1),
(231, 'Perneet123', '', '', 'hamedullah.hab@gmail.com', '051dc7770adb12fbcdb0d43835d1512914994144', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '06c15ba2e1de0a493ebef50b615df60e', 'd6ad0b08c447e05525253850efd0f411', '2020-01-10 15:58:06', NULL, 1),
(232, 'Perneet123', '', '', 'qnaseem@gmial.com', '4abefa04ddb2f35e83af8f3d3000fbac54ae4c29', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd038a8ed90f11547e18dcd8d7d03c2d6', '50c8389065370e89ee500cc79ebb469a', '2020-01-12 16:05:53', NULL, 1),
(233, 'Perneet123', '', '', 'gulamrasool6386846616@gmail.com', '9fb4efd352edeac555c11a491c62c5bfde78e0cc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '11e8231b019826e5a1bd1db8590deff4', 'a017633a0a44c3cc32eb8f660d5eea16', '2020-01-13 4:57:42', NULL, 1),
(234, 'Perneet123', '', '', 'basharottanti00@gmail.com', '1f03a0a8c9498844274f4d789a310b415060c1d0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a82c95a42eab4b662b3f59496ca24740', '04640a598d8ce9220e362d6a132e03bb', '2020-01-13 5:20:13', NULL, 1),
(235, 'Perneet123', '', '', 'usamaadullah@gmai.com', 'fdf6e0cb2793cb068b2808a66127b0d86dfb0fba', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'bbc9d2b26a2a1600b8bb3738c5f85bac', 'a45709fa3ac376e4f155fb5742cc5f67', '2020-01-13 7:53:08', NULL, 1),
(236, 'Perneet123', '', '', 'gandhamramesh@gmail.com', '2b10a1acd483a7931edd950a9e237df543caaeb3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd156ea21456fe3b12073eb1d28292078', 'bf0ed0fa6b727d476c9c23a93428a232', '2020-01-14 5:47:41', NULL, 1),
(237, 'Perneet123', '', '', 'sarveshkumarShah590@gmail.com', 'e70d5a2dc320dfa6c774565e4f554f5764b764fa', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '96e15b8841f2bc2ab27c7efb6e834860', 'e66967344ea052920eadd971632a4ab4', '2020-01-16 0:11:08', NULL, 1),
(238, 'Perneet123', '', '', 'saeedqureshi030@gmail.com', 'c4e22d00f1ce6a5cb3b079c6517a464d219815eb', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4077330ec444292e1e6f3e86c3e8c48b', 'b4e681e7289cb72cbfef3911a1de79ce', '2020-01-16 3:17:24', NULL, 1),
(239, 'Perneet123', '', '', 'tajiburrahaman81128@gmail.com', 'df37fc04746bd8f4a10d3f366d0661336b6816bb', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6d009cfc93b1d3e7a590227bf416762e', '0446e384368508a2678fff66189ddf21', '2020-01-16 7:02:28', NULL, 1),
(240, 'Perneet123', '', '', 'ar5471480@gmail.com', 'f1f17c9d5a30b6c711a055eeba587c2abe80ab21', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'dc8c0835f3bbc08bd4c381d45797c398', '85027f3d77015463e04d1b7ce1e10135', '2020-01-19 4:36:41', NULL, 1),
(241, 'Perneet123', '', '', 'mi893655@gmail.com', 'd6d3ccaa384a65532061e26836842adc48d43df4', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9b94a9ebd7bae6923ff460413edcbf59', '9cd5f0842ecf97609a02c6547d798537', '2020-01-21 2:26:42', NULL, 1),
(242, 'Perneet123', '', '', 'qamarrehman301@gmail.com', '1ed56936876c3773cf3d09540bcda15f9b35f587', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f771e3cfa8dd6b5da855ee8541fd10bf', '42f949bcd3f5918c3328c2820334af9c', '2020-01-23 2:12:58', NULL, 1),
(243, 'Perneet123', '', '', 'sajjadkhan@Gmail.com', 'ef9b408a0fb14c6719d0378937a72e2375aabaea', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '76ac88833a6a8d255495b6b5a13a923a', 'f00ce5454667ddccd9282d3493ce4ed7', '2020-01-23 3:44:16', NULL, 1),
(244, 'Perneet123', '', '', 'SnniKumar@gail.com', 'db25f2fc14cd2d2b1e7af307241f548fb03c312a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b95ee6446c05853ebc4a90baf0ea308a', '225e148ff7967f0d2dcea20cb8d602fe', '2020-01-27 6:31:49', NULL, 1),
(245, 'Perneet123', '', '', 'farahnisar978@gmail.com', '1f27d177b0e226ab59abc59494b104549d46295c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a4d0464c8baaffa23cdefa53b92ee930', 'e4a95035621a240e06308888fb567421', '2020-01-29 3:28:17', NULL, 1),
(246, 'Perneet123', '', '', 'jasbanta1994rout@gmail.com', '691696d672e2d354d609954cd8eaa146c9acdea0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ae813a4faf81206d61f7d138fc35f1f8', 'e5bad7c7388376a44e30191495662643', '2020-01-29 3:31:40', NULL, 1),
(247, 'Perneet123', '', '', 'tajimrajput148@gmail.com', '45f0e4610c208e96450851d22545065083bbde3c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ac6e1027a76a36c71fd353874a5d5bef', '89a78729b2d3586161e964bd9051b606', '2020-01-29 6:56:14', NULL, 1),
(248, 'Perneet123', '', '', 'tanveerhussainkorai@gmail.com', '5dbe0a40560cda546c02858e09942242c1ed2f5f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd0299c4b652d34654e19ce2dd6238df1', '9a450c59b7587efedaf2bb6019dc618d', '2020-01-29 11:29:34', NULL, 1),
(249, 'Perneet123', '', '', 'zaighamsherazi@gmail.com', 'f46d2e0b41bdd2458d78ce818bf218089dcc095c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a60e0016fc70bab132d8e27240800489', 'ddc96a4967a6f5b1ef0f275f9af9be3d', '2020-01-30 11:28:20', NULL, 1),
(250, 'Perneet123', '', '', 'MKG808@gmail.com', '09a9ed2c9b4c439667f00e5b07f7283971654f6c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '98e7399099f1de565aa80d589753bfb8', 'd1f6a0bc23ec9febfcc6bd4bd2d4419f', '2020-01-31 9:54:55', NULL, 1),
(251, 'Perneet123', '', '', 'archanapatel0937@gmail.com', '4cc88856ab3f33aab1f072fce44c2262ba1e8bc1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '80fc10d457a45257b3927fb1948e971a', 'cdfeea5ed99d2b072af3298639c6a31a', '2020-01-31 12:48:32', NULL, 1),
(252, 'Perneet123', '', '', 'vishnukmnraj1111@gmail.com', 'ba1cd82c492b919244c79588cf66b3553413bd99', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '80acab92ce493ee09e58220ec68c54a5', '1ae8d561d46abbf12960b45c0f65c5c6', '2020-02-01 8:10:02', NULL, 1),
(253, 'Perneet123', '', '', 'arslanmhr@example.com', 'cb04544c43ee0d984fee32289f5d20f15561afe8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd6b5a6d3eee02ff4a9b4b908ea6b6a30', '492ee0837ea167fa4d23404e2396e669', '2020-02-02 6:36:45', NULL, 1),
(254, 'Perneet123', '', '', 'smiglani@nanojot.com', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'upin2612@yahoo.com', '2021-04-04 11:41:05', '6aeefe6beb868c71aaea0b19cb4bc720', '', '2020-02-03 9:00:27', '2021-04-04 07:39:59', 1),
(255, 'Perneet123', '', '', 'farhanoman028@gmail.com', 'f41bc38537a5e0677a0e12e12fb861a1ac6344bd', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '1d5ef29f29cfec6a253abc289022cdaa', '4716d36b45077d06a249ff8246f3dc50', '2020-02-04 0:05:27', NULL, 1),
(256, 'Perneet123', '', '', 'abhishekma316@gmail.com', 'a954eafcd7774bb5b70f0511e113a60b6a0f95b6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'bc6f622aa23f3bc4252b3554da0a7993', '2936993be08f6e81e752425aba3faeba', '2020-02-05 0:54:05', NULL, 1),
(257, 'Perneet123', '', '', 'ssk56165@gmail.com', '811f54d5a2639e49360b5f907d9c03d4254d0508', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5029d52dde0e3a611100591e1e7af8f2', '907a00df3faa0371a8a79a1c54f8b9d8', '2020-02-05 1:34:48', NULL, 1),
(258, 'Perneet123', '', '', 'ndon277@gmail.com', '37001739cdb344c957ed10739fba523c4f43a16e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd88792e625d38b2565d6175954d8aa69', '52a595adfc1bd43d3a0bffe084cab4eb', '2020-02-05 20:46:11', NULL, 1),
(259, 'Perneet123', '', '', 'inderdevK58@gimal.com', '4cf7668b32909d6b20526c86bf4820a5c4dc56f3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'cc5d2476ff8bcf5c9081af228c805b66', '9475c754ea95b63a011990f03a09d192', '2020-02-07 7:12:39', NULL, 1),
(260, 'Perneet123', '', '', 'lisafb@live.it', '80e26952017e04f2e68c21d68a44180304709b3e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3c418d5d2799aefcb0ff8f18435e60f5', 'f5e177400694fa423ea7c4ba900ebaf7', '2020-02-10 7:31:47', NULL, 1),
(261, 'Perneet123', '', '', 'alejocano86@icloud.com', 'ffdd9b66bcd85a00f3a208a05465e97a117e2626', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3501efbdb468b7fed0c5d624678c6030', '17138800d0e6bf7a32ac8f9655c1f626', '2020-02-11 13:38:05', NULL, 1),
(262, 'Perneet123', '', '', '97na9797@gmail.com', '9faa87c5cbffcf871c3713bc2a74fcaf319f9bf2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a7e7330ce4344d72fe738697f4e9ab61', '56a1fad3e703771a5d3f3e8c9ed28ca6', '2020-02-12 7:59:47', NULL, 1),
(263, 'Perneet123', '', '', 'Kmaliakbar8@gmail.com', '667030e3d85dffb90650831b7eeac2e49e316d87', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0688ad5598500e603ce07c32dfc85a34', 'ed12349085dc9717c6b2f9b816840c22', '2020-02-12 9:08:28', NULL, 1),
(264, 'Perneet123', '', '', 'bhartinakul97@gmail.com', 'cb069c147779a8ff90059f4d09aa06c996d85e87', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2445f6cb9c729dd0ff95143648772cf0', '06d56e9514158a52fe3bfb231408d7ae', '2020-02-14 1:54:27', NULL, 1),
(265, 'Perneet123', '', '', 'Vic.handa@cpvmgroup.con', 'a780541aa60e7f045326105c5ecd3125b530dc23', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5a7f16bead476ea4ee36dc6d58ec06e8', 'ec9c866770555b0fcbbe552db5104809', '2020-02-14 18:34:36', NULL, 1),
(266, 'Perneet123', '', '', '38867@gmail.com', '9b0e9dca17a86cc3b5b80b0957164bb2d38fd6a0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8cd6c49e20eca4b5bdaf87fc2d10e2ed', 'bc8d39106d58a84d67abec517870672b', '2020-02-15 0:26:38', NULL, 1),
(267, 'Perneet123', '', '', 'chetanrathva278@gmail.com', 'b9fc3256bbbf58273894b2b0877a08fe1e9f97a3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '398024dc007ac7a500a28cebf9cdeb8c', 'cb6e09ca0bfe561ce06ee9df93360b1c', '2020-02-15 2:07:29', NULL, 1),
(268, 'Perneet123', '', '', 'lingaraj@gmail.com', '8dda5a4e36a2fe000e5fe0a527c3187c3b220b17', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '919e794cc6f36409aca87aba8ba4c79c', 'c57987294f122f18466dfc535e681fa6', '2020-02-15 7:32:59', NULL, 1),
(269, 'Perneet123', '', '', 'rahulsainirahulsaini16@gmail.com', '974460cab525cd8afb2cd8f0c7fe787a5688be8a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5b6c878926abca4b81267745a7ab9b71', '2241ba266da34e475bf97b3f1fb2d80c', '2020-02-19 4:08:52', NULL, 1),
(270, 'Perneet123', '', '', 'Patilakash2728@gmail.com', 'ea51afcf44a65bb831c38b6a00e30aa9998457f8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5a825fdd4052c416252ba44a618ed1e4', '34e96eb18bc32afc239f5409b11fc4c8', '2020-02-19 5:07:42', NULL, 1),
(271, 'Perneet123', '', '', 'jagdishdamor432@gmail.com', '23bf58a871779b7af335eaa1d9b3fce91979bc5e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '981022f3b1d1619c5ff06e399b1867cc', '506c433964580f05b3b4232828999a85', '2020-02-21 9:10:46', NULL, 1),
(272, 'Perneet123', '', '', 'gajulacheranjeevi@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b84f11ac01d6fd3f3c9f94adad6e489a', '8e5b29d716b60b40dd58655d9047d6a6', '2020-02-22 7:16:40', NULL, 1),
(273, 'Perneet123', '', '', 'atuljesus125@gmail.com', '4f8d6745689400a2ecd254774ca6f1feda657b1e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '274f7ce4b2b6bd99e6f607ebdbda0ccf', '5092b6d43ad43a86b3d340370c1dae55', '2020-02-24 4:30:36', NULL, 1),
(274, 'Perneet123', '', '', 'patrickdonahue357@gmail.com', '22a7aafd85a204efbd3dabfef82e514ca6ebb880', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4bbc091341647fb31fcf12c069d8b9bb', '04194b3dc0176f23dc63ca0716b16aeb', '2020-02-24 10:28:06', NULL, 1),
(275, 'Perneet123', '', '', 'wulffmike.mw@gmail.com', '548e01cbdb51c1ce31736744ab6c92277d2d000b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c9d2f6014b9ef3de9f944ef0d7f60f05', '0ce2193bc5dc90536a4a43a9fa26451a', '2020-02-25 14:42:40', NULL, 1),
(276, 'Perneet123', '', '', 'rajeevkumar754125@gmail.com', '8ff43d7ca2cf43a742557f16567acee7c051e24e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd3946505a139e784a3fe805ca36acdca', 'a6e9aaa13ed61abb7b2a0ac461ce0269', '2020-02-26 1:04:08', NULL, 1),
(277, 'Perneet123', '', '', 'alexandra_58@hotmail.fr', '377ff1570024315149d82c1686f47599aa7f9720', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '09a60abff0852f5a3d6e21302aa7cdd7', 'e8d7cd3e5d599a15d47a171351e350ad', '2020-02-26 2:10:43', NULL, 1),
(278, 'Perneet123', '', '', 'kalashrahangdale66@gmail.com', 'b7ede76f9260b685c2058ab11ab8ff58890851d1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8198515ec3e0966618c0bf10ad90853e', 'bba76c93bfb9e491fc3b9ea4a51dfb5d', '2020-02-27 2:34:21', NULL, 1),
(280, 'Perneet123', '', '', 'bharatenwatee297@gmail.com', 'e596b959a0db08aa0a0c3208e19aea18e2947543', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f26f07c908e3f2f5e11e05a6d0253c9a', '70138aba8133511571b93cb4580465b5', '2020-02-28 9:01:46', NULL, 1),
(281, 'Perneet123', '', '', 'aakhil@gmail.com', '45f0e4610c208e96450851d22545065083bbde3c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '802032dedac17f7df187d4cd41f85f7e', 'f084df2d9f5e7a7b568decd03151487f', '2020-02-28 9:14:42', NULL, 1),
(282, 'Perneet123', '', '', 'waseemakahamd533@gmail.com', '37b66065ad1a3dbc3d2e21c47f5a4ab7224f0e2e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '50aeb40c2decbec38d92e1cf90cb6dda', '93dae745ef15cd622360202afa489d71', '2020-03-05 5:32:46', NULL, 1),
(283, 'Perneet123', '', '', 'emmanuelnarho1@gmail.com', 'adb7c946132b47e7a4079deac0a33354780fa083', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f75bc9e7301fa3161a4742cee98cde34', 'ba99b1fbef79a3097c7f3e93770ebff9', '2020-03-08 8:18:24', NULL, 1),
(284, 'Perneet123', '', '', 'pk61181@gamil.com', '1e66440c74f01f9b0d69ec49c1f011e2d784cc38', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '49be0d1ea480d05639044782a98ada0e', 'def6d668f1a76aa1bb9ac2f52449c45f', '2020-03-10 2:12:11', NULL, 1),
(285, 'Perneet123', '', '', 'dineshm@nanojot.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e3bb89d2f74e5a7f5c28267118d67dcb', 'f8a57b65a1780247780358675c22d7c5', '2020-03-11 5:30:03', '2020-11-04 16:15:13', 1),
(286, 'Perneet123', '', '', 'shraddham@nanojot.com', '2a3aeb2050fea4a940db91424648803c61250971', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c01ef5337e25853552e6ed2dac1a7ba4', 'd78609c7efc77759a9dae68da822dcde', '2020-03-11 8:56:53', '2020-11-06 11:10:36', 1),
(287, 'Perneet123', '', '', 'Karimulislam2004@gmail.com', '138f84e83a588ab111d8c86531e052709e0d6aa9', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f999cfe6e3bff6a769f2a54e7e752cd1', '834512ee2b1ce662c5ef387b4908d686', '2020-03-13 5:19:29', NULL, 1),
(288, 'Perneet123', '', '', 'vvvvaahirgm@il.com', '1e942fc406ba5efa0c0406d1fe0902c3ed56fe65', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'efcaff019d2bfd378fa75f27dbc78211', '9015b85cf74f30eadd67e654d41c497c', '2020-03-16 11:44:43', NULL, 1),
(289, 'Perneet123', '', '', 'vikkyraj9285@gmail.com', '01b307acba4f54f55aafc33bb06bbbf6ca803e9a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b55632e789398f7faeae542c62a1cfb0', '1136351ff12b5bf7925ab407764b0898', '2020-03-20 8:01:49', NULL, 1),
(290, 'Perneet123', '', '', 'mamanilove144@gmail.com', 'fc23ad2628f543a4ec54294951b403cf01c983e0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e9a086fb74a645fa20be6305d823da8a', '081edfc423a6ea6ead3c119fe2a33cf7', '2020-03-20 13:47:40', NULL, 1),
(291, 'Perneet123', '', '', 'Hazarikabijay123@gmail.com', '86c000adf270874105d801f90b6af9d2472231f2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0fb3d12daa51885baf96350b6c39e4ea', 'a7ea8deb2d9d0dadaff8f9844d43a86a', '2020-03-20 14:14:18', NULL, 1),
(292, 'Perneet123', '', '', 'randhwadurgesh015@gmail.com', 'ff9911f170222cb9999968a5196bba3b91d334b8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '921e3442e6be12684afc8e96cb9c44c3', '38144f554ddad9963ea8dd15683c4acc', '2020-03-20 19:38:31', NULL, 1),
(293, 'Perneet123', '', '', 'Ravikanojiya.rravi.764865@gmail.com', '77c533353900935e47a28dc47d5949474cb96512', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c116f1ffac234b754d09e4bef44d62a5', '46937721167360a7bd77821b7cc695b8', '2020-03-21 6:24:26', NULL, 1),
(294, 'Perneet123', '', '', 'ahmed2692@gmail.com', 'bb75b20055362c480651fb43c0734a867b37f417', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c99d22fc68582f0d0e8669c0208156d2', '1c85a78a427c67d7dc6f07d2774ebb82', '2020-03-24 16:22:05', NULL, 1),
(295, 'Perneet123', '', '', 'miglesias@ciccp.es', 'f6356d09f7abfd95eb31350e35c1c3360942219f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'dd167e38034b42a6a1bcb1d38ae1389f', 'd7e0827053c86271bd1066105b2b311f', '2020-03-25 15:01:19', NULL, 1),
(296, 'Perneet123', '', '', 'Suranjitbalo@gmail.com', '82c5798c2dad7e2f70b676542dc74960d9add186', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ce1766b8118d7d1bd9a9fe8353a76d58', 'ea8867e53e651fec9953fb9555bd6dfb', '2020-03-28 0:15:40', NULL, 1),
(297, 'Perneet123', '', '', 'matt_whiteland@hotmail.com', 'dd23a7d4d511355608c397b60de58eb8e62bbb5b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c9d087b0eb5a7c26b1fb9326ca990340', '993598f5232e7657cc95882d23c7e781', '2020-03-28 7:53:56', NULL, 1),
(298, 'Perneet123', '', '', 'benjuminmarak@gmail.com', 'cf5d208cbc05cfd3e18dff25dc482b4a19868e4a', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b851c0a383c03647ec08975541553384', '0dd6349636f90dd068509728f7f6ee04', '2020-03-29 0:23:00', NULL, 1),
(299, 'Perneet123', '', '', 'appyraja18@gmail.com', '8186e8398096a981244ab2b68a241dbe960267b7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ded417ae8ab2671f70b5fc39af320884', '1011991b8b1eae48dc891616a851e4e5', '2020-03-30 0:05:55', NULL, 1),
(300, 'Perneet123', '', '', 'shujaatali9682574095@gmail.com', 'fe15d2162b36bc5aab1b8e80b49dd060cedcb8bb', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '1d7c43638e6626ba16d737f5220ba113', '135e47fc0917e8a405bd9add2d57c834', '2020-03-31 0:30:49', NULL, 1),
(301, 'Perneet123', '', '', 'bulu.ojha88@gmail.com', '371cb8b711fd2fc81c1276afd4eaaec916fb5319', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '57c92f30bd45d04cc2d7154343512b56', 'cdf0033a67723eb9d93fb49099d2b4cb', '2020-04-01 0:13:17', NULL, 1),
(302, 'Perneet123', '', '', 'dudve1234@gmail.com', '4b03a548eed9faac2375233e580639d1971afbf3', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'fe7c69174a0888b405f1180dfd95e187', 'f492b63ccf20e09c33b646c7edacbdf1', '2020-04-01 0:15:04', NULL, 1),
(303, 'Perneet123', '', '', 'kazinargis56@gmail.com', '00a39788c47ae056d053a8118bfcda75e8056310', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'daf6a7b6d616f887ec520379f2c12fd0', '63d8a591724263ef6e894d782327c87c', '2020-04-01 0:23:23', NULL, 1),
(304, 'Perneet123', '', '', 'taiyabansari73469@gmail.com', 'd9e2cbfde1e71f5d5b0762f8b062f3605d2dc6f5', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '39ac3bab903ba9360a49045ba1cd7de9', '5f4882bef457b379e26168c35bcf0837', '2020-04-02 0:25:58', NULL, 1),
(305, 'Perneet123', '', '', 'prathvirajdsatpute6687@gmail.com', 'd29f27d4355a275df36dea279eeea4d98714e428', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '316e10807a26172de0cc2a92fa9017ac', '6f5add65db45d69097ec28d1a35e135d', '2020-04-02 0:31:48', NULL, 1),
(306, 'Perneet123', '', '', 'moirangthemitobi@gmail.com', '3027ac62ea6ea129b588fb13cd137d05d895f7ca', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '74cc00acc41e39431ea94ab647b94b22', '71521291c8667f5139dcbef5f942f40e', '2020-04-04 0:16:41', NULL, 1),
(307, 'Perneet123', '', '', 'vishnutomar8305142733@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '84674c2393f46068b18058a15d147f82', '1b851056a0e93e4fe6d5dc97d4dc8c67', '2020-04-04 0:24:19', NULL, 1),
(308, 'Perneet123', '', '', 'kk2022408@gmail.com', '8e3ee9d5d3c305f9525b9cb6d284c5236c15c503', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '42b1e0cfb1804f3b8de07e9f8fae3a39', 'b7781a2eab67b1a898a0fd4bbe74ad1e', '2020-04-05 0:24:02', NULL, 1),
(309, 'Perneet123', '', '', 'SWAMY8088196559@G-mail.COM', '74417c747b4cfeebfc27a7bff6a6ee7a5ce5cef6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '143edad2fadafbf2f71c1a33bdb9b812', 'f25b2e0cf34993b00d8189fbcbd69582', '2020-04-07 22:40:15', NULL, 1),
(310, 'Perneet123', '', '', 'saddamsk49923@gmail.com', 'eee6b3cc7b8cd11af6240c217a8edada4b1e778e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e4efc58808ac758e15099c72796f3a25', 'ed1a40a64026599d2d7c27f3c2bc7303', '2020-04-08 4:55:21', NULL, 1),
(311, 'Perneet123', '', '', 'sawairamjaat08@gmail.com', '6e8dee13804bd534b14376abe9abc480e2bc6943', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5285be6bf8901da021a936b593b982f3', '2b1d85104e18a6183cd70bcb13df1571', '2020-04-08 9:02:45', NULL, 1),
(312, 'Perneet123', '', '', 'mohanta75am@gmail.com', 'c2681de246cd201845d657df6cc64fd8369f1175', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2a4b587346a8c6206431b617eacf8d6b', 'ae1182c2bb36ae3d6298a1a85cbbb806', '2020-04-08 9:27:40', NULL, 1),
(313, 'Perneet123', '', '', 'mishrapiyush067@gmail.com', '93bd7322e8d4ab32105dd7536e04b45c61a2a44b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'fb15af0d456f1883f4cb7a59bbe301a1', '5b17a7b53151af18c04a31192d8c7918', '2020-04-09 0:39:45', NULL, 1),
(314, 'Perneet123', '', '', 'Kumarilata82450@gmail.com', 'a83dddb4d9b7529d5167246a520e8720bd515087', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b01c30b416ac7332c5482e3ed3e2e93c', '44077a0fa21504de30ed92f31f73f8f4', '2020-04-09 6:15:48', NULL, 1),
(315, 'Perneet123', '', '', 'sangtea@gmai.com', '1e1b5e8a510fa263102c972d753790fdc57adf80', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '86cf1d2a0a785c86d32dbc296fa1e66c', '43d148119f3ad6ed590f8f75a6f3fa82', '2020-04-09 6:33:25', NULL, 1),
(316, 'Perneet123', '', '', 'peter.ontherun@pm.me', 'c3893b165a62cd9bf78fc6dbb3042b204b317173', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '1f491ccf950e2bae0afc6b92a02135eb', 'fd105bda697c1f40b6b8441bb2042813', '2020-04-09 20:09:31', NULL, 1),
(317, 'Perneet123', '', '', 'vijaykumar9719097190@gmail.com', '54ff41979b0cfaab3181743a1a30d53e37615280', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd92cf1ac7f49602c51630380f7d43da4', 'd46e9c2496bfa90acb2153ed72eca970', '2020-04-10 3:38:50', NULL, 1),
(318, 'Perneet123', '', '', 'amitdhanamitdhan45@gmail.com', 'de0295858aea8100550db4e682fe0ee8749909fc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3be19b57138e8018d6379358c7700ce7', 'e7783e631b0d5161aa91864a352f72bb', '2020-04-11 0:17:40', NULL, 1),
(319, 'Perneet123', '', '', 'sajayk94338@gmail.com', 'f5b95568715e4ee48fec7170fe1195b826469ca2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9c1dfad0e114c47ab9ceec49b22fd2bd', '3c8d010ef39d4fa85fb9aaff26bb0d88', '2020-04-11 1:30:01', NULL, 1),
(320, 'Perneet123', '', '', 'Noraiznazir7@gmail.com', '0921066e3e5a450c1a6155179c8ddc46261da20e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0715d61389e8903aac0aa5d7837c106c', '155eb51d3aae901476af1dad6456c3d6', '2020-04-11 1:36:55', NULL, 1),
(321, 'Perneet123', '', '', 'mgreadman@gmail.com', '6c4e9f6e3cb7e38a3d9be01c6d619ce243d3de3d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ba4eaf84dd7bd53da570df639f2d3a39', '6424e7efe70d6b5056230d5d95aeaf63', '2020-04-11 20:28:37', NULL, 1),
(322, 'Perneet123', '', '', 'mbamba@hotmail.ca', '605f927cb99e21f518c1627434765f171b05aa8e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2d45822d926e2fd9baf4ca4eeace5409', '8d46a75479d4063dedb86b90155d0139', '2020-04-11 23:44:18', NULL, 1),
(323, 'Perneet123', '', '', 'mkhawar306@gmal.com', '24f0948eac1be8c7ed573186038ce971bfb41f6c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8b4941151e46cdb9d9b77ffbd2f859ac', 'b19ca0243ea6a66cfffbf28e2515706c', '2020-04-12 0:32:10', NULL, 1),
(324, 'Perneet123', '', '', 'kumarajay46159@gmail.com', 'ebd22deb7504663b2d665aa15b179eca5848218d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a58df0ca7198b83100f42d43d8f5e048', '0dbfbab8bdb26fcd0e7b62addc34f773', '2020-04-13 0:19:13', NULL, 1),
(325, 'Perneet123', '', '', '9661354865/kumarikshma@gmail.com', '987f896c5d06a151c7f00be36ff8c5a6508b0c64', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4f48f08390a2ef9ad3af6f4f1ab1b5e6', '91318b87407cb9347a8b83b82025b305', '2020-04-13 0:22:46', NULL, 1),
(326, 'Perneet123', '', '', 'karthiktony45@gmail.com', '5ee565c18988be7b7cb0168bdbe8954da4e3dd35', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '45984891889c995571bdf7eb55db0e00', 'd4568c7a226ba719b95bf60dd085aa67', '2020-04-13 0:30:29', NULL, 1),
(327, 'Perneet123', '', '', 'mdshahbazalammdshahbazalam366@gmail.com', '7e338c2af61fec25aa5aaf4efe98f7c7275043e9', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0dce62724c64d63be8e605eb248c65e3', 'bd114879cd623bc4f09623319a07f0d3', '2020-04-13 1:21:09', NULL, 1),
(328, 'Perneet123', '', '', 'ziaratkhan642@gmail.com', 'b2b5d6f3f86fd3fb37cc2011a7ffb9581294b771', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7f89a62d9d5c671c23e9c9117e5cb75c', '65f0a67cc7f27a9ba16cdb4a8efd2f2f', '2020-04-13 3:14:17', NULL, 1),
(329, 'Perneet123', '', '', 'senthil_raja16@rediffmail.com', 'ffcce2083a4d213c196ae6ba566aea41ccac9a15', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9c3b9e497992e278f4ec3765f011d93c', 'd7ac55e02cfc1613167b9943ffe21ad3', '2020-04-15 0:23:26', NULL, 1),
(330, 'Perneet123', '', '', 'akashkewat8999@gmail.com', 'e22261d8a2db7a0950c5a5a0044b53788b8ddd30', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '69663602c8b0bf444eb50a9aaa4d28ea', 'fc1b7a48bad5a7058bd7f70b69627c14', '2020-04-16 0:25:24', NULL, 1),
(331, 'Perneet123', '', '', 'brijmohansharma0143@gmail.com', 'a0b98e96f090aa6de4743a17f0447a1cbdfd9539', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ed030eae1203e90a2cce003fda4df66d', '88079625d5a295f759eaac8dc93ae825', '2020-04-17 2:19:18', NULL, 1),
(332, 'Perneet123', '', '', 'Ajaykumar112@gmail.com', '4df97fbbe37af7a9b448daecd69dc310b8b85f09', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '66ffbf8128b5320a09de0250f5447008', '6e7aa432aa1e9b8193b264d7306d4686', '2020-04-18 0:24:10', NULL, 1),
(333, 'Perneet123', '', '', 'sameersoreng812@gmail.com', 'cec1ef6b488c98925eb8030bdb191d8ab1d68f64', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '06a6048198958c08972782fa041d2936', 'a9f45bdfc7fd026891df84e2c6aa57b5', '2020-04-18 3:20:39', NULL, 1),
(334, 'Perneet123', '', '', 'mayurimarbate44@gmail.com', '1f129e17ab1eeebe07fb23d3f6a8436171343f70', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '96b356ec8ac246830617a811858c73d0', 'bb7e8e9696aebcb1a6cc25de7aaa8fa5', '2020-04-20 0:28:03', NULL, 1),
(335, 'Perneet123', '', '', 'Biplapdas39@gmail.com', '26b55fe07da93d0345bc3e25fb08cc1121951283', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2dbf3a51b39550e3b6937ec667faab46', '5d871677b5d04d159c3dea4da9d1a579', '2020-04-20 14:01:01', NULL, 1),
(336, 'Perneet123', '', '', 'rajnikumaribsr306@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4b0149cea5869468206e24cab1505145', 'd271909ee5d3f3dd695ee9dc2751eb7f', '2020-04-22 0:23:12', NULL, 1),
(337, 'Perneet123', '', '', 'sk8814402@gimel.come', 'b1be1c1ea75470f07acac86f882597eecfa1ac33', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4f5f7edf198b5832d52c16c983ad42d9', 'a69f9f6a3afdde08b05b89816cf4c525', '2020-04-23 0:29:20', NULL, 1),
(338, 'Perneet123', '', '', 'rahulkumar8401210686@gmail.com', 'd9e26daed70b4f948b543c5a2f6c2a8a241cd752', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0c2ca2456660f7240214ee6f3e2e1f39', '54a4ef5222c8b03bf7e21d0b68993620', '2020-04-23 0:32:42', NULL, 1),
(339, 'Perneet123', '', '', 'shitalgavade47870@gmail.com', '02161a520bc6523c15313214bcaed631cf63ac3b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a856545c0c991da6b839ffbba751adf6', '62b411634c5f5deb2b7cc7c1be7ddf44', '2020-04-24 0:08:26', NULL, 1),
(340, 'Perneet123', '', '', 'oraonsandeep457@gmail.com', 'b73ad447558816bc52a6b682574c84f9e01a77da', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '22ebf9b3f41712c4308b8b136258df67', 'ac3e75e94a9c7e2da253047ddd9d1104', '2020-04-24 0:19:36', NULL, 1),
(341, 'Perneet123', '', '', 'Ranjitdutta9296@gmail.com', '5a5951fc6348cb201b5d69532324b1b754567cfc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'af133f055d23dedd5e4ca64d089b4ccf', 'b4a7fa5c123c265c2e4e93f26b1e8600', '2020-04-24 4:34:30', NULL, 1),
(342, 'Perneet123', '', '', 'skmosaraf6295@gmail.com', '022633f3959be89f92e8c8582b728973ed67b695', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8236f1b5c4e0f19f0c9581a3fd1904a7', '96512017570b0062df5e44ce54311cb5', '2020-04-25 0:25:13', NULL, 1),
(343, 'Perneet123', '', '', 'souravsingh78155@gmail.com', 'ccba1ad140d33d1ab0ec1ba15390a68b9f43e4e2', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5f3029ed7fef78735ce72822478b51f2', 'c070f51def979648b96c8df8cfeaf30f', '2020-04-25 0:25:37', NULL, 1),
(344, 'Perneet123', '', '', 'Rohot45@gmail.com', 'd90ee967d83fd82f93213854fa032db80c6d4e34', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e76f83d6a690051534cb9faf9bd52555', 'b000e6447bb5906939c357a9402d4ec7', '2020-04-25 0:29:04', NULL, 1),
(345, 'Perneet123', '', '', 'dk9304058@gmail.com', '5f8ef170d08a16d9cc73cca457a957074209e86f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '52976cc7e7205a33ca2bd3c206e69ebc', 'c869e34f8e5a30fe51dfab58e7ff6254', '2020-04-25 1:11:58', NULL, 1),
(346, 'Perneet123', '', '', 'Pankajkumar77037@gmail.com', 'fb71cf4d0f074f1bed53c06954369c8ca0fdde94', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b29b9920f2420f00d0657e0872e3bc15', '7d762ca1369c06fa1a9330d0be8a0b4e', '2020-04-25 10:17:25', NULL, 1),
(348, 'Perneet123', '', '', 'venoth@gmail.com', '6d52a9948fe5db8fc8a6e5bfd47ec2cd45deb84c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '2ad1233d406751c06762b13d5a80e535', '2aac02d41bb4079b706d279f5a0d2e28', '2020-04-26 19:37:24', NULL, 1),
(349, 'Perneet123', '', '', 'aadie1056@gmail.com', '40f6e5f375bd62406b291a4071f63f659b1f9a91', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd331c8c3fc611a9155aa68fe1b6cdc3e', 'c6cfbef38abf7cc63ab302d5514f6ed3', '2020-04-27 2:12:21', NULL, 1),
(350, 'Perneet123', '', '', 'pravalkumar421@gmail.com', '336743834146eebc2a2bd91b70520f66e93af706', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e71d953b5d0af167b83a413b099ab7c1', '4d857616a87e03bf9d6176d697b9207c', '2020-04-28 2:22:57', NULL, 1),
(351, 'Perneet123', '', '', 'venoth@censera.com', '1e120fff23aedaf23ab14fd14e8f2d031593c222', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'edd69777e151dd9e3a7537775d6e91c4', '45bbabe65dad66b4d10476bac855b0e1', '2020-04-28 15:04:38', NULL, 1),
(354, 'Perneet123', '', '', 'guddukalwa097@gmail.com', '2a00cea06a5da188e6d00947e8f1a06809dbfd09', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '59efb3ae5bada51e447be60e0f03e391', '9e7b241c83eab8b7aff8e8c1d1d0020d', '2020-04-29 0:17:41', NULL, 1),
(364, 'Perneet123', '', '', 'dineshmarandimarandi165@gmail.com', '6a92586b06cacab484a6aa6e2a3632c63ea5de44', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'f1eddfbec68595f18cde8acb6c59128e', '5638fcfeee720e9bfcd519c86a36bb9e', '2020-05-02 1:17:33', NULL, 1),
(401, 'Perneet123', '', '', 'mda996123@gmail.com', '0e30bda66e16c02fdc50c5f9de5449bf8e0893e6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e3ec8a198ce87cf247bc538e18e3e784', 'dfc58dd1caa08e50187840acc7aa0444', '2020-05-04 0:17:31', NULL, 0),
(409, 'Perneet123', '', '', 'bhushankumarnpg0@gmail.com', '998e7ea5b933b1adeb49998116b507321858f370', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5fef5449058cbf8b1694b092e7c2dcd2', '88d6f38ec8cd38b5f537ca4e134ba008', '2020-05-06 0:21:04', NULL, 0),
(412, 'Perneet123', '', '', 'venothtim@gmail.com', '99c884b90f6d2c6086075661a84f11798d0bddf6', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '098d8399a1bda35da4ffae2046b5bd89', 'da04a5e1adfd13c60793452877709afa', '2020-05-08 18:44:44', NULL, 1),
(413, 'Perneet123', '', '', 'fk5750@yahoo.com', 'b4b7814734076dfc41c0466402defb7bdfdcf38f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5c62af0ea1a7fca1f43e975802e192eb', 'f61ea3c4aaeca0c939508b8defe16c4c', '2020-05-10 0:27:43', NULL, 1),
(414, 'Perneet123', '', '', 'akashmetha123@gmail.com', '86b181ef7067e2c5e2f9a0ffdc3bd71fd845906c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '402a653c845d6e2fd64edf2fb3812359', '10f3f44c0bd75bb5b534793c1d3bb2ee', '2020-05-10 1:15:58', NULL, 0),
(415, 'Perneet123', '', '', 'lwcard@hotmail.com', 'dcf941de2cdcb675c5eb8668035113d67395b377', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '01ec2cf39099ac909b78b264e87ca57b', '245163a02df068fb88cbee6460ab3226', '2020-05-15 1:23:33', NULL, 1),
(416, 'Perneet123', '', '', 'bhumikabhalla6@gmail.com', '757554ab324146830f97250a79192d1166ea9626', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '963a19012b34a326e4d9cff1b4d0eada', 'ba6d3c4fd3e4c0b6378945a089af5d90', '2020-05-15 1:28:12', NULL, 0),
(417, 'Perneet123', '', '', 'diljitroy781038@gmial.com', 'be27dd750d226420be01f480d5cc6e9cd0d1c32c', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '8668d2c50ca64f4e4294524d93038e21', 'a6f9f352bdd90f4daa1756b4a3d9dea7', '2020-05-18 10:09:58', NULL, 0),
(418, 'Perneet123', '', '', 'govindkumarrawani1992@gmail.com', '4a2d6f691943ae60ce608ad1b225ceedd24e169e', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e87c3095df78207e34666d188d5cf900', 'dfc6a4cf03d3ee2612cb00c72b01d3c7', '2020-05-21 4:55:55', NULL, 1),
(419, 'Perneet123', '', '', 'lijalk28@gmail.com', '5cf7ed62112796a28d4590791838d3fe85126b1d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '10ccef6bf0af6676cb9d705ef8275560', 'c624bfcd35f221eed2c630822695d10d', '2020-05-23 7:36:04', NULL, 1),
(420, 'Perneet123', '', '', 'brigidkimani@gmail.com', 'c33d1d3d482bc346a4cf9c139f3ecc4ed8783bec', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '131443f33422bc6d034c20cedf0f95bd', 'a5d4fced50a4c2baf98771be1aa0a705', '2020-05-24 17:53:48', NULL, 1),
(421, 'Perneet123', '', '', 'nutlick55@gmail.com', '6ef34894a8d5e3ebaf4882c83347b390d4f6caf7', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7a9613c93d1544fd8750d3e3b7c83aff', '0e042706977a66da4a6741d58de4192b', '2020-05-30 1:57:40', NULL, 1),
(422, 'Perneet123', '', '', 'sk9727654@gmail.com', '4c0d32432f250cf7057becea5a8ef70973d81812', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c7e34cc89e137a2202f0c128db1612eb', 'dd1723972ff0ffb14a84f610e9e33a75', '2020-06-02 1:34:33', NULL, 0),
(423, 'Perneet123', '', '', 'luckypepsi2@gmail.com', '0eabdc0fb6273c9e2d826386aa0e548267ce82a0', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'ddd81735e54775475dc250e50aa06f24', '687d975130f6c02ba253494cb2fc5c29', '2020-06-05 13:11:15', NULL, 1),
(424, 'Perneet123', '', '', 'KTC1984@GMAIL.COM', '6a2617f8c1f637c36776bdd056b8966a117ddba1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '545d3c18e434bf75f917e76e145ef8c4', '198d4898da7a0e25856ef825ef41b5e4', '2020-06-06 0:23:13', NULL, 0),
(425, 'Perneet123', '', '', 'sabairfan74@gmail.com', '5ebd54f056510042cc7ffb168ebcfdf823dc4326', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '639a29ff5b325e0f202899e6ef24fd88', '78af1d724f862e5677d38b56594da167', '2020-06-06 0:50:59', NULL, 1),
(426, 'Perneet123', '', '', 'sr0493351@gmail.com', '1794811718ad29207c5aa1c6be4f68ca5cc22656', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '9ee9e20c8338f5073ad3bf4fdce1471e', 'd982a05af5f4ade7d7b46b11ab44ce1b', '2020-06-07 0:10:51', NULL, 0),
(427, 'Perneet123', '', '', 'anilgangwar3731@gmail.com', '813f56f9b6a328974d08c942d9e6aa77cc612a97', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'efe4e02033f58beefbf04561ff69f9c2', 'a35098f151fe48c29f0b6386011b101e', '2020-06-08 2:06:52', NULL, 0),
(428, 'Perneet123', '', '', 'shiwdeep960@gmail.com', '0abe5391c25471ce165b4f615755b7b4cd31fd56', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'dc932f2955659787231e6f9f98846024', '5ec4ebd1b73f06409caa38cfc8e3382d', '2020-06-08 2:20:58', NULL, 1),
(429, 'Perneet123', '', '', 'test@gamil.com', 'a29c57c6894dee6e8251510d58c07078ee3f49bf', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c16f8e5f16ab822e3c106f32c748c29f', '8585ca7c30787d651db41cd65fa9cd14', '2020-06-10 3:06:53', NULL, 0),
(430, 'Perneet123', NULL, NULL, 'talwinder.singh991@gmail.com', '66caa44de0804796717ebbc0dc4d0335c605b899', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'c76d52253a7f0f4e6bc43bf06fcd0e4d', '8ebd9694815b169ac6ee3c6d8eac2cc8', '2020-09-04 6:34:58', '2020-12-10 13:54:08', 1),
(431, 'Perneet123', NULL, NULL, 'manpreetforeach@gmail.com', '66caa44de0804796717ebbc0dc4d0335c605b899', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e8b393f87a097fb4dd77398eb2cbd5fc', 'beccc58cada6bfa87e242e6e34b38698', '2020-09-04 16:38:39', NULL, 1),
(432, 'Perneet123', NULL, NULL, 'dineshm@nanojot.in', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a083e5b999a2bc18813dc1b9296a2d10', '', '2020-09-04 16:55:47', '2020-11-12 13:38:36', 1),
(433, 'Perneet123', NULL, NULL, 'shraddhamaorya@gmail.com', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'edd61f5c1b3a73cb9e1d11c4efa83a40', '365277907e8716fb470be9915fc89ae2', '2020-09-14 8:07:00', '2020-11-06 11:24:01', 1),
(434, 'Perneet123', NULL, NULL, 'smiglani@nanojot.in', '8be3c943b1609fffbfc51aad666d0a04adf83c9d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '34658bf861f003032654129f6c2560f4', 'cec44c23f07c8c44119f91f938ab4e22', '2020-09-15 11:03:35', NULL, 1),
(435, 'Perneet123', NULL, NULL, 'jason.willoughby@tdsb.on.ca', '4476039ff606206f38e7315c3f8814d54f192d37', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e6c4187e6a82917ca61daf4e4ba2852a', '9015577be666a81a11e0e0773c6bebee', '2020-09-28 8:28:07', NULL, 1),
(436, 'Perneet123', NULL, NULL, 'atiberio@outlook.com', 'd671e52c33e9ccabf4d3b6e519fd4cd68a17e64f', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b0bb70c8ef0f9b3ef4b28f68f1952457', 'ea349811ebfd2944d70b1c9f4f19ed89', '2020-09-28 14:32:31', NULL, 1),
(438, 'Perneet123', NULL, NULL, 'dineshm@nanojot.net', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '4e7f32597bdcd8a90d278338c0253dcd', '7b9f14129c335d3e7de74ae6bfb34f96', '2020-11-04 16:14:49', NULL, 1),
(439, 'Perneet123', NULL, NULL, 'royalaccessoriess@gmail.com', '296e96fd6a35cf57e798ed40cd4d7bdd3f977a10', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '0fdc667fc1846a664a091ddadf6b23b0', 'c34c9371eed7ed8a6d64bb4f01a8f1e9', '2020-11-05 2:33:46', NULL, 1),
(440, 'Perneet123', NULL, NULL, 'Rita@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '3f3b3b4ec4bb5d4228b95a56f85a9798', '5257107f87d7ade1c5d499e4c2c84fc0', '2020-11-05 10:19:07', '2020-11-05 10:35:56', 1),
(442, 'Perneet123', NULL, NULL, 'ashvinderkaur85@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '689543ee632a07f7b7a1ebb8e3691433', '', '2020-11-06 2:47:54', '2020-11-06 13:59:46', 1),
(443, 'Perneet123', NULL, NULL, 'John@nanojot.ca', '20eabe5d64b0e216796e834f52d61fd0b70332fc', 'upin2612@yahoo.com', '2021-03-31 12:39:53', '6f847b8a523ca60bdfb85b1acf9aca7e', '', '2020-11-06 11:07:23', '2021-03-31 08:38:05', 1),
(444, 'Perneet123', NULL, NULL, 'Bob@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '26afe41f2a9371b04f35c67a06de66c4', '3eeb74bef00f9f5437c7a44ca8c99868', '2020-11-06 11:48:31', NULL, 1),
(445, 'Perneet123', NULL, NULL, 'moneshfoodography@gmail.com', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '415bb8f65682152e4dcdf093cd9e8504', '', '2020-11-06 11:51:47', '2020-12-16 07:30:08', 1),
(446, 'Perneet123', NULL, NULL, 'rajaraniindia600@gmail.com', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a1e1306a99faad5b017981a575c5a996', '628644a944e912a03cb673e00df4f765', '2020-11-06 13:59:53', NULL, 1),
(447, 'Perneet123', NULL, NULL, 'tamrakarlalit82@gmail.com', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'fedc3e5ed314debb1afb863e6e68efa6', '6c62fc19b98633a4b1599ec9a01d0897', '2020-11-06 14:05:45', NULL, 1),
(448, 'Perneet123', NULL, NULL, 'webdevel87@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'b48f29729905c5bc8009611c4ebd70b2', '31167920d8ec8eac289658edb4febc68', '2020-12-10 13:49:49', NULL, 1),
(449, 'Perneet123', NULL, NULL, 'deisyr_10@hotmail.com', '55e029dd669d9a5a101ee437f076d936c1cdd465', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '7124c8c1cb3fe5722140f284b21788f8', '48c07e944053c7f96f9d8c5a5ff993e4', '2020-12-18 15:07:08', NULL, 1),
(450, 'Perneet123', NULL, NULL, 'jschauhan64@gmail.com', '8871d97f4e97b0fafcbbe291f2f54966211dcde5', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '562162974d3554d263b77fc16eb1e532', 'e409366e14823b801a66e4e9b33a6b76', '2020-12-18 22:45:52', NULL, 1),
(451, 'Perneet123', NULL, NULL, 'test@test.com', 'cb972614470b90976efe3ec8b73dc78ba9b7b6a1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '5ffd6af8e46612218f3a096b66cc93a7', 'c6913ebdc2487b0aaf3fb86b2551b18e', '2020-12-19 10:14:45', NULL, 1),
(452, 'Perneet123', NULL, NULL, 'kanikamathur33@gmail.com', 'f12437f8c42382f479a267a019c386ed6ae4b902', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'a821d0cf3f14f21eb13336d4dbbc2b9c', 'cee5ece65a3f52cf7a4fc70e64efb918', '2020-12-20 0:59:28', NULL, 1),
(453, 'Perneet123', NULL, NULL, 'Jitendrasdigital2019@gmail.com', '19b58543c85b97c5498edfd89c11c3aa8cb5fe51', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '32747fce08a0aab2127a538761917b08', '5e46fc75aaa1a8d1a21ebe545875832a', '2021-01-01 5:58:04', NULL, 1),
(454, 'Perneet123', NULL, NULL, 'maddy@nanojot.ca', '0aba636c0edcaecbd896dbcf9f017a8cab3c4894', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '6764c26c6174cc762175b7b7f742a0ca', '0e69e14291121ae1b21478ada956d96f', '2021-01-04 10:12:10', NULL, 1),
(455, 'Perneet123', NULL, NULL, 'jitendr.global@gmail.com', '29f030ebbb7ae595dacb904103d82abe79b69262', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'e7543ebaadc278ada3cd35a98fb2a59e', 'c206d41ccee4f90fac0953b98ae3b18d', '2021-01-04 10:57:55', NULL, 1),
(456, 'Perneet123', NULL, NULL, 'Sandy@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Sandy@nanojot.ca', '2021-12-19 14:19:44', '3b1ccf40dda7cb9a36b21ff60f773d61', '82a168c4b5fd06bc6edb95971c43ce3e', '2021-01-04 11:32:47', NULL, 1),
(457, 'Perneet123', NULL, NULL, 'Jenny@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'eb37789d3b2121695716296b004b0ad2', 'd41f78948c14b3f4b5141ec6e2cf187c', '2021-01-06 10:41:36', NULL, 1),
(463, 'Perneet123', NULL, NULL, 'Tony@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'upin2612@yahoo.com', '2021-03-02 14:21:58', 'd719ffc8fc0cbe837ad03bdce2a3d419', '53eaa1652778a87a8f0e928ddd756522', '2021-01-07 11:35:12', NULL, 1),
(464, 'Perneet123', NULL, NULL, 'jslamba0811@gmail.com', 'a29c57c6894dee6e8251510d58c07078ee3f49bf', 'upin2612@yahoo.com', '2022-01-19 12:48:11', '2696de158c0db7cfcb3cae9f23872178', '', '2021-01-08 11:21:18', '2022-01-19 07:47:40', 1),
(465, 'Perneet Singh', NULL, NULL, 'upin2612@yahoo.com', '23d42f5f3f66498b2c8ff4c20b8c5ac826e47146', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '34faae7229270e764b87171cfdc4b348', '7a63b1e76cea65471f54d25cbc9cbcd5', '2021-01-09 2:54:10', NULL, 1),
(466, 'Perneet123', NULL, NULL, 'abc@gmail.com', '60e6e1f3813b12f15c84660da0ec69c6c50d0bf1', 'upin2612@yahoo.com', '2021-03-02 14:21:58', '50b62ac07b99a316840063936c04d2d0', '342c66f56a4bfc2006c8ec200d4b6246', '2021-01-09 3:35:57', NULL, 0),
(467, 'Julie Thomas', NULL, NULL, 'Julie@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Julie@nanojot.ca', '2021-03-02 14:21:58', '5eb8a20774e17f693bdf5a1ab91f9525', '5d5d8393735556938808e90fcb170964', '2021-01-09 9:28:51', NULL, 1),
(468, 'Rashmeek', NULL, NULL, 'rsayal64@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'rsayal64@gmail.com', '2021-04-02 06:31:40', 'd2c5ea447e95fdfc752bd5513a0e267a', '2e44b8b4713442af1b962fb071c5c0c4', '2021-04-02 2:31:40', NULL, 0);
INSERT INTO `client` (`user_id`, `full_name`, `first_name`, `last_name`, `username`, `password`, `email`, `create_date`, `activation_code`, `forget_code`, `inserted_timestamp`, `forget_code_time`, `active`) VALUES
(469, 'Gurpreet', NULL, NULL, 'gurpreetsinghsangari13@gmail.com', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'gurpreetsinghsangari13@gmail.com', '2021-04-02 06:42:41', '4a1ea2519f7aa5b7c191220698095503', 'a37a3a6eee4031b4bb5628575b42d25d', '2021-04-02 2:41:17', NULL, 1),
(470, 'Sukhminder Miglani', NULL, NULL, 'miglaniss@gmail.com', '9733f2b671f802134cbd91fa32daedb1dc648ea8', 'miglaniss@gmail.com', '2021-04-04 11:49:33', '699ba801b9ea600632f29b97ccc38afd', '5e336c4a99266a01332755657e63fa1e', '2021-04-04 7:49:33', NULL, 0),
(471, 'Perneet singh', NULL, NULL, 'perneet_lamba@yahoo.com', '23d42f5f3f66498b2c8ff4c20b8c5ac826e47146', 'perneet_lamba@yahoo.com', '2021-04-05 12:47:27', 'ff1a37a947aa68152291a87c1eda09e1', 'a85ff64c8737d0d2dbf9162558741721', '2021-04-05 8:47:08', NULL, 1),
(472, 'Mo', NULL, NULL, 'mo@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'mo@nanojot.ca', '2021-04-05 13:09:20', '483bbd71839e4b70c71daf187ba99f06', '4258b19b174c51735c821de90ffc941f', '2021-04-05 9:08:18', NULL, 1),
(473, 'Cortez', NULL, NULL, 'cortez@nanojot.ca', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'cortez@nanojot.ca', '2021-04-20 12:53:13', 'a1f563765d187d80aff21e4b5d73a213', 'c128a54dc942cf096ef6561cb10b70b6', '2021-04-20 8:52:59', NULL, 1),
(474, 'HEEMANSHU BHALLA', NULL, NULL, 'bhallaheemanshu@gmail.com', 'fd0cd9cbe6f778d38a9a2413e814fdaae4fa5072', 'bhallaheemanshu@gmail.com', '2021-06-08 13:48:41', 'c360922a32ae3880c2da2dbedd0ea61c', '3ef7afceeb0b25be50f9bd2d69ecc30a', '2021-06-08 9:48:27', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `discount_code`
--

CREATE TABLE `discount_code` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `type` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `discount_code`
--

INSERT INTO `discount_code` (`id`, `name`, `code`, `value`, `type`, `updated`) VALUES
(0, 'Discount', 'TYSD78', '20', 2, '2021-04-09 13:39:12'),
(1, 'TDSB discount', 'TDSB40', '40', 1, '2021-03-02 14:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `faq_article`
--

CREATE TABLE `faq_article` (
  `id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `category_ids` varchar(1000) NOT NULL,
  `short_description` varchar(1000) NOT NULL,
  `answer` text NOT NULL,
  `url` varchar(1000) NOT NULL,
  `active` int(1) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_article`
--

INSERT INTO `faq_article` (`id`, `question`, `category_ids`, `short_description`, `answer`, `url`, `active`, `updated`) VALUES
(12, 'Will my phone or tablet work with your service?', '21, ', '', '<p>Make sure you have a cellular equipped tablet or an&nbsp;<strong>unlocked</strong>&nbsp;phone that will work with the chatr mobile network, it must be GSM/HSPA 850 MHz and 1900 MHz compatible. If you are unsure if your device supports these requirements please check you device compatibility. 2</p>', 'will-my-phone-or-tablet-work-with-your-service?', 1, '2019-03-20 00:46:11'),
(13, 'What network will I be using?', '21, ', '', '<p>We are an&nbsp;independent third party retailer and will provide you with a SIM card and activate it for you so you can use the Chatr mobile network in Canada.</p>', 'what-network-will-i-be-using?', 1, '2019-04-02 00:05:55'),
(14, 'What is the coverage area?', '21, ', '', '<p>For complete network information please visit&nbsp;the coverage section of our site</p>', 'what-is-the-coverage-area?', 1, '2019-04-02 00:06:50'),
(15, 'What happens if my phone is not unlocked?', '21, ', '', '<p>If your phone is not unlocked, you will not be able to use our service. Please make sure you phone is unlocked before purchasing your plan. If you are unsure, please contact your carrier.</p>', 'what-happens-if-my-phone-is-not-unlocked?', 1, '2019-04-02 00:08:55'),
(16, 'Do I need to change any settings on my device?', '21, ', '', '<p>In order to use some texting capabilities you may need to update your APN settings.<br /><br /><strong>If you have an iPhone/iPad:</strong><br /><br />&bull; for some IOS versions you can view the APN settings on your device in Settings &gt; Cellular &gt; Cellular Data Network and change it to chatrweb.apn<br />&bull; for iOS 5 or later please click www.chatrmobile.com/setmyiphone from the internet browser of your iPhone/iPad to get the settings automatically pushed to your device.<br /><br /><strong>If you have another type of phone (Android, Symbian, etc.)</strong>&nbsp;<br /><br /><strong>APN Settings</strong><br />The data APN &quot;Access Point Name&quot; is chatrweb.apn<br />The MMS URL is http://mms.chatrwireless.com<br />The MMS IP address is 205.151.11.11<br />The MMS port is 8080</p>', 'do-i-need-to-change-any-settings-on-my-device?', 1, '2019-04-02 00:10:20'),
(17, 'What is your refund policy?', '21, ', '', '<p>All sales are final.</p>', 'what-is-your-refund-policy?', 1, '2019-04-02 00:11:10'),
(18, 'What SIM card sizes do provide?', '21, ', '', '<p>We provide a 3-1 triple SIM card which supports&nbsp;Mini, Micro and Nano SIM cards.</p>', 'what-sim-card-sizes-do-provide?', 1, '2019-04-02 00:12:24'),
(19, 'Can I order just a SIM card without a plan?', '21, ', '', '<p>No, we do not sell just the SIM card, a plan is required.</p>', 'can-i-order-just-a-sim-card-without-a-plan?', 1, '2019-04-02 00:12:55'),
(20, 'What happens when I order my plan?', '22, ', '', '<p>1) You will recieve an &quot;Order Confirmation&quot; email</p><p>2) When your order is shipped you will recieve an&nbsp;&quot;Order Shipped&quot; email with your tracking number</p><p>3) Once you recieve your SIM card you will see a link at the bottom of the &quot;Order Shipped&quot; email, click the link and enter the 20 digit SIM card number found on the back of the card</p><p>4) Wait 1-2 business days and enjoy your service!!!</p>', 'what-happens-when-i-order-my-plan?', 1, '2019-04-02 00:28:30'),
(21, 'Can I purchase a SIM card locally?', '21, ', '', '<p>No we cannot activate SIM cards that are not purchased directly from us.</p>', 'can-i-purchase-a-sim-card-locally?', 1, '2019-04-02 00:32:51'),
(22, 'How do I check my Chatr account balance and how much I have available?', '21, ', '', '<p>You&rsquo;ve got a lot of different options for checking your chatr account:<br /><br />Dial *225# then press send/call button from your chatr mobile phone for an on-screen notification.</p><p><br />OR call *611 from your chatr phone.<br />P.S All of these calls are free!</p>', 'how-do-i-check-my-chatr-account-balance-and-how-much-i-have-available?', 1, '2019-04-02 00:34:56'),
(23, 'How is data usage calculated?', '23, ', '', '<p>Data usage is rounded to the next full KB.</p>', 'how-is-data-usage-calculated?', 1, '2019-04-02 00:37:06'),
(24, 'What is the data overage rate above my data bucket?', '23, ', '', '<p>5&cent;/MB in Canada. Outside of Canada regular data roaming rates apply. Data usage is rounded to the next full KB.</p>', 'what-is-the-data-overage-rate-above-my-data-bucket?', 1, '2019-04-02 00:37:50'),
(25, 'Can I use data without a data-add on with my Chatr Unlimited Talk plans?', '23, ', '', '<p>Unfortunately, no. chatr Unlimited Talk plans do not offer pay-per-use data.</p>', 'can-i-use-data-without-a-data-add-on-with-my-chatr-unlimited-talk-plans?', 1, '2019-04-02 00:38:25'),
(26, 'Will I be notified when I am running out of data?', '23, ', '', '<p>Unfortunately, no. However, you can check your data balance anytime from your phone! Key in *225# then pressing the &lsquo;send&rsquo; or phone key.</p>', 'will-i-be-notified-when-i-am-running-out-of-data?', 1, '2019-04-02 00:38:51'),
(27, 'Will I get a notification when I have used up all my data?', '23, ', '', '<p>Yes. Your data session will stop as soon as your data bucket is depleted. At this time, you will receive a text notification. If you have extra funds in your account, you will have the option to continue to use data at the pay-per-MB rate of 10&cent;/MB in Canada. If you do not want to use data at the pay-per-MB rate, your data will be blocked until the end of your billing cycle.</p>', 'will-i-get-a-notification-when-i-have-used-up-all-my-data?', 1, '2019-04-02 00:39:38'),
(28, 'Do you monitor our usage?', '24, ', '', '<p>No. Never. Your privacy is important to us.</p>', 'do-you-monitor-our-usage?', 1, '2019-04-02 00:41:27'),
(29, 'What currency are your prices in?', '24, ', '', '<p>All prices are in Canadian dollars and are subject to the Ontario HST tax (13%).</p>', 'what-currency-are-your-prices-in?', 1, '2019-04-02 00:41:56'),
(30, 'What is your business address?', '24, ', '', '<p>643 Chrislea rd, Unit 3<br />Woodbridge, ON<br />L4L 8A3</p>', 'what-is-your-business-address?', 1, '2019-04-02 00:42:25'),
(31, 'Contact Us', '24, ', '', '<p>Email us at info@canadiansimcards.com and allow up to 48 hours for a repsonse.</p>', 'contact-us', 1, '2020-07-14 21:15:17');

-- --------------------------------------------------------

--
-- Table structure for table `faq_category`
--

CREATE TABLE `faq_category` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `url` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_category`
--

INSERT INTO `faq_category` (`id`, `name`, `url`) VALUES
(11, 'Questions', 'questions');

-- --------------------------------------------------------

--
-- Table structure for table `faq_sub_category`
--

CREATE TABLE `faq_sub_category` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `url` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_sub_category`
--

INSERT INTO `faq_sub_category` (`id`, `cat_id`, `name`, `url`) VALUES
(21, 11, 'General Questions', 'general-questions'),
(22, 11, 'Orders', 'orders'),
(23, 11, 'Data Questions', 'data-questions'),
(24, 11, 'Company Info', 'company-info');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_items`
--

CREATE TABLE `ordered_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `price` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `with_sim` int(1) NOT NULL,
  `carrier` varchar(100) NOT NULL,
  `store` varchar(255) NOT NULL,
  `sim_number` varchar(255) NOT NULL,
  `sim_number_client` varchar(255) NOT NULL,
  `check_number` varchar(255) NOT NULL,
  `plan_detail1` varchar(1000) NOT NULL,
  `plan_detail2` varchar(1000) NOT NULL,
  `plan_detail3` varchar(1000) NOT NULL,
  `plan_detail4` varchar(1000) NOT NULL,
  `plan_detail5` varchar(1000) NOT NULL,
  `plan_detail6` varchar(1000) NOT NULL,
  `plan_detail7` varchar(1000) NOT NULL,
  `plan_detail8` varchar(1000) NOT NULL,
  `plan_detail9` varchar(1000) NOT NULL,
  `plan_detail10` varchar(1000) NOT NULL,
  `suggest_mail_sent` int(11) NOT NULL,
  `offer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordered_items`
--

INSERT INTO `ordered_items` (`id`, `order_id`, `title`, `price`, `quantity`, `with_sim`, `carrier`, `store`, `sim_number`, `sim_number_client`, `check_number`, `plan_detail1`, `plan_detail2`, `plan_detail3`, `plan_detail4`, `plan_detail5`, `plan_detail6`, `plan_detail7`, `plan_detail8`, `plan_detail9`, `plan_detail10`, `suggest_mail_sent`, `offer`) VALUES
(1, 1, 'test2224444f', '666.00', 1, 0, '', '', '', '', '', 'adsafasd', 'asdfasdf', 'asdfasdf', 'asdf', 'adsfasdf', '', '', '', '', '', 0, 0),
(2, 2, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(3, 2, 'Talk & Text + 4 GB Data', '60.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '4 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(4, 3, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(5, 3, 'Talk & Text + 4 GB Data', '60.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '4 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(6, 4, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(7, 5, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(8, 6, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(9, 7, 'Talk & Text + 4 GB Data', '60.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '4 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(10, 8, 'Talk & Text + 1 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(11, 9, 'test2224444f', '666.00', 1, 0, '', '', '', '', '', 'adsafasd', 'asdfasdf', 'asdfasdf', 'asdf', 'adsfasdf', '', '', '', '', '', 0, 0),
(12, 10, 'test2224444f', '1.00', 1, 0, '', '', '', '', '', 'adsafasd', 'asdfasdf', 'asdfasdf', 'asdf', 'adsfasdf', '', '', '', '', '', 0, 0),
(13, 11, 'Canada-Wide Talk & International Text + 4 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '4 GB data at 3G speed', 'Includes SIM Card', '', '', '', '', '', '', 0, 0),
(14, 13, 'Test', '1.00', 1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(15, 14, 'tes', '-9.00', 1, 0, '', '', '', '', '', 'sdsdasda', '', '', '', '', '', '', '', '', '', 0, 0),
(16, 15, 'tes', '-9.00', 1, 0, '', '', '', '', '', 'sdsdasda', '', '', '', '', '', '', '', '', '', 0, 0),
(17, 16, 'tes', '-9.00', 1, 0, '', '', '', '', '', 'sdsdasda', '', '', '', '', '', '', '', '', '', 0, 0),
(18, 17, 'Test', '-10.00', 1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(19, 18, 'Test', '1.00', 1, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0),
(20, 19, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(21, 20, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(22, 21, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 2, 0, '', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(23, 22, 'Test plan', '1.00', 1, 0, '', '', '', '', '', 'Test plan', '', '', '', '', '', '', '', '', '', 0, 0),
(24, 23, 'TestPlan', '1.00', 1, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(25, 24, 'TestPlan', '1.00', 1, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(26, 25, 'TestPlan', '1.00', 1, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(27, 26, 'TestPlan', '1.00', 1, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(28, 27, 'TestPlan', '1.00', 2, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(29, 28, 'TestPlan', '1.00', 1, 0, '', '', '', '', '', 'Testing details', '', '', '', '', '', '', '', '', '', 0, 0),
(30, 29, 'Canada-Wide Talk & International Text + 4 GB Data', '40.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '4 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(31, 30, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(32, 31, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(33, 32, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(34, 33, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(35, 34, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(36, 35, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(37, 36, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(38, 37, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(39, 38, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(40, 39, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, 'Chatr', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(41, 39, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 0, 'Helpr', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(42, 40, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '1', '1000000000000095', '1000000000000095', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(43, 40, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '1', '1000000000000099', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(44, 41, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(45, 42, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(46, 42, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(47, 43, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(48, 43, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(49, 44, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '3', '1000000000000000101', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(50, 45, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '1', '1000000000000012', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(51, 46, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(52, 46, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '2', '1000000000000300', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(53, 47, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(54, 48, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(55, 49, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(56, 50, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(57, 50, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(58, 51, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '3', '1000000000000000013', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(59, 52, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '3', '100000000000070007', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(60, 52, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '1', '1000000000000016', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(61, 53, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(62, 54, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(63, 55, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(64, 56, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(65, 56, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(66, 57, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(67, 57, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(68, 58, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(69, 59, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(70, 59, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(71, 60, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(72, 60, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(73, 61, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(74, 61, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '57676576765665746', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(75, 61, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(76, 62, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(77, 62, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(78, 62, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(79, 63, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(80, 63, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(81, 64, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(82, 64, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(83, 64, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(84, 64, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(85, 65, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(86, 66, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 1, '5', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(87, 66, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(88, 67, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '1000000000000000012', '1000000000000000012', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(89, 68, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(90, 68, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(91, 69, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(92, 69, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 1, '5', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(93, 70, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '100000000000000800', '100000000000000800', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(94, 70, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '1000000000000000012', '1000000000000000012', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(95, 71, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(96, 72, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(97, 73, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(98, 73, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '1', '1000000000000001', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(99, 74, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(100, 74, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '1', '1000000000000001', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(101, 75, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(102, 75, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(103, 76, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(104, 77, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(105, 77, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '4', '1000000000000000200', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(106, 78, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 1, '5', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(107, 79, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(108, 80, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(109, 81, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(110, 82, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(111, 83, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(112, 84, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '3', '1000000000000000012', '1000000000000000012', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(113, 84, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '100000000000000800', '100000000000000800', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(114, 85, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '100000000000000800', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(115, 85, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '3', '1000000000000000012', '', '', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(116, 86, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '5', '8930272052384002213', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(117, 87, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '5', '8930272052384002214', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(118, 88, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '5', '8930272052384002217', '', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(119, 89, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '3', '893027205238400222', '893027205238400222', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(120, 90, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '1', '8930272052384002214', '8930272052384002214', '3', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(121, 91, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '6', '8930272052384002240', '8930272052384002240', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(122, 92, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '6', '8930272052384002244', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(123, 92, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '6', '8930272052384002244', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(124, 93, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '6', '8930272052384002247', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(125, 94, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '6', '8930272052384002247', '8930272052384002247', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(126, 95, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '6', '8930272052384002245', '8930272052384002245', '7', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(127, 95, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '6', '8930272052384002245', '', '7', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(128, 96, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '6', '8930272052384002245', '', '7', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(129, 96, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '6', '8930272052384002247', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(130, 97, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '5', '8930272052384002219', '8930272052384002219', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(131, 97, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '5', '8930272052384002218', '8930272052384002218', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(132, 98, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '7', '893027205238400222', '893027205238400222', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(133, 99, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '6', '8930272052384002250', '', '7', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(134, 100, 'Canada-Wide Talk & International Text + 6GB Data', '40.00', 1, 0, '5', '3', '8930272052384002237', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(135, 101, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '6', '8930272052384002240', '', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(136, 102, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '7', '8930272052384002263', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(137, 103, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '7', '8930272052384002270', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(138, 104, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '7', '893027205238400228', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(139, 105, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '8', '8930272052384002286', '', '1', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(140, 106, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '7', '893027205238400228', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(141, 107, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '8', '8930272052384002284', '8930272052384002284', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(142, 108, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '8', '8930272052384002284', '', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(143, 109, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '7', '893027205238400228', '893027205238400228', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(144, 110, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '3', '893027205238400228', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(145, 111, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '8', '8930272052384002282', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(146, 112, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '8', '8930272052384002282', '8930272052384002282', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(147, 113, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '8', '8930272052384002282', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(148, 114, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '8', '8930272052384002283', '', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(149, 115, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '1', '8930272052384002213', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(150, 116, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '9', '8930272052384002287', '8930272052384002287', '9', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(151, 117, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '9', '8930272052384002285', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(152, 118, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '9', '893027205238400228', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(153, 118, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '9', '8930272052384002283', '', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(154, 119, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '9', '8930272052384002278', '8930272052384002278', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(155, 119, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '9', '8930272052384002284', '', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(156, 120, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '9', '893027205238400228', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(157, 121, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '10', '8930272052384002242', '8930272052384002242', '4', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(158, 121, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '10', '8930272052384002216', '', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(159, 122, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '10', '8930272052384002236', '8930272052384002236', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(160, 122, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '10', '8930272052384002229', '8930272052384002229', '1', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(161, 123, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '10', '8930272052384002407', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(162, 124, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '11', '8930272052384002420', '', '6', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(163, 125, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '10', '8930272052384002406', '', '5', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(164, 125, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '10', '8930272052384002405', '', '7', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(165, 126, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(166, 126, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 1, '5', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(167, 127, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '11', '8930272052384002419', '', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(168, 128, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '11', '8930272052384002413', '', '1', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(169, 129, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '11', '8930272052384002418', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(170, 130, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '11', '8930272052384002417', '8930272052384002417', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(171, 131, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '11', '8930272052384002422', '8930272052384002422', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(172, 131, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '11', '8930272052384002423', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(173, 132, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '11', '8930272052384002415', '8930272052384002415', '6', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(174, 132, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '11', '8930272052384002414', '8930272052384002414', '9', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(175, 132, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '11', '8930272052384002421', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(176, 132, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '11', '8930272052384002416', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(177, 133, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '11', '8930272052384002424', '8930272052384002424', '8', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(178, 133, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 1, '1', '11', '8930272052384002413', '', '1', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(179, 134, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '11', '8930272052384002426', '8930272052384002426', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(180, 134, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '11', '8930272052384002425', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(181, 135, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '12', '8930272052384002433', '8930272052384002433', '9', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(182, 135, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '12', '8930272052384002432', '', '1', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(183, 136, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '', 'h8h8h8h8h8h8h8h', '', '8', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(184, 137, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '', '8930272052384002441', '', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(185, 138, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '', '8930272052384002247', '', '3', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(186, 139, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 0, '1', '', '893027205238400243', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(187, 140, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '14', '8930272052384002247', '8930272052384002247', '3', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(188, 141, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 1, '1', '12', '8930272052384002441', '', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(189, 142, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '12', '8930272052384002434', '', '7', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(190, 143, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(191, 144, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 1, '1', '10', '8930272052384002404', '', '0', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(192, 145, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 1, '1', '11', '8930272052384002427', '', '1', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(193, 146, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '12', '8930272052384002435', '', '4', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(194, 147, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '12', '8930272052384002436', '', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(195, 148, 'Canada-Wide Talk & International Text + 6GB Data', '40.00', 1, 0, '5', '', '89302720523840024313', '', '	', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(196, 149, 'Canada-Wide Talk & International Text + 6GB Data', '40.00', 1, 0, '5', '', '89302720523840024313', '', '	', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(197, 151, 'Canada-Wide Talk & International Text + 6GB Data', '40.00', 1, 0, '5', '', '8930272052384002422', '', '2', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(198, 152, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 1, '1', '11', '8930272052384002428', '', '9', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(199, 153, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 1, '1', '', '8930272052384002418', '', '7', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(200, 154, 'Canada-Wide Talk & International Text + 6GB Data', '40.00', 1, 0, '5', '', '893027205238400242', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(201, 155, 'Canada-Wide Talk & International Text + 1 GB Data', '30.00', 1, 0, '1', '', '2384789235748957948', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(202, 156, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '15', '8930272052384002832', '', '2', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(203, 157, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(204, 159, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '', '8930272052384002832', '', '0', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(205, 160, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '15', '8930272052384002841', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(206, 161, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '', '4564653735687876', '', '8', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(207, 162, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 0, '1', '', '2413543534534564', '', '6', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(208, 163, 'Unlimited Canada-Wide Talk & International Text', '20.00', 1, 1, '1', '', '2413543534534565', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 1, 1),
(209, 164, 'Canada-Wide Talk & International Text + 4.5 GB Data', '35.00', 1, 0, '1', '', '782978297829782', '', '9', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0),
(210, 165, 'Canada/US Talk & International Text + 8 GB Data', '50.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(211, 165, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 0, '1', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 1, 0),
(212, 166, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '38902377243242389', '', '2', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(213, 167, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '2782323456781234903', '', '8', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(214, 167, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 0, '5', '', '', '', '', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(215, 168, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 1, '1', '', '3932434324348971267', '', '3', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(216, 168, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(217, 169, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '677667766776677', '', '6', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(218, 170, 'Unlimited Canada-Wide Talk & International Text', '25.00', 1, 1, '1', '', '6782990282323474382', '', '4', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', 0, 0),
(219, 171, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(220, 172, 'Canada-Wide Talk & International Text + 6GB Data', '45.00', 1, 1, '5', '', '688299199919243', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(221, 172, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0),
(222, 173, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 0, '1', '', '5772388188189999', '', '3', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(223, 174, 'Canada-Wide Talk & International Text + 4.5 GB Data', '40.00', 1, 1, '1', '', '688299199919243', '', '5', 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', 0, 0);
INSERT INTO `ordered_items` (`id`, `order_id`, `title`, `price`, `quantity`, `with_sim`, `carrier`, `store`, `sim_number`, `sim_number_client`, `check_number`, `plan_detail1`, `plan_detail2`, `plan_detail3`, `plan_detail4`, `plan_detail5`, `plan_detail6`, `plan_detail7`, `plan_detail8`, `plan_detail9`, `plan_detail10`, `suggest_mail_sent`, `offer`) VALUES
(224, 175, 'Canada/US Talk & International Text + 8 GB Data', '55.00', 1, 1, '1', '', '', '', '', 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', 0, 0),
(225, 176, 'Canada-Wide Talk & International Text + 1 GB Data', '35.00', 1, 0, '1', '', '5555666677778888999', '', '9', 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(500) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `city` varchar(500) NOT NULL,
  `province` varchar(500) NOT NULL,
  `zip` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL,
  `total_price` varchar(500) NOT NULL,
  `discount` varchar(100) NOT NULL,
  `shipping` varchar(50) NOT NULL,
  `discount_code` varchar(50) NOT NULL,
  `sub_total` varchar(500) NOT NULL,
  `tax` varchar(500) NOT NULL,
  `total_items` int(11) NOT NULL,
  `authorizing_merchant_id` varchar(100) NOT NULL,
  `order_number` varchar(100) NOT NULL,
  `bambora_id` varchar(100) NOT NULL,
  `created` varchar(100) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  `amount` varchar(50) NOT NULL,
  `shipped` int(11) NOT NULL,
  `carrier` varchar(100) NOT NULL,
  `tracking_number` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `acct_number` varchar(100) NOT NULL,
  `phn_number` varchar(20) NOT NULL,
  `billing_full_name` varchar(500) NOT NULL,
  `billing_address` varchar(500) NOT NULL,
  `billing_city` varchar(500) NOT NULL,
  `billing_province` varchar(500) NOT NULL,
  `billing_zip` varchar(500) NOT NULL,
  `billing_country` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `full_name`, `email`, `address`, `city`, `province`, `zip`, `country`, `total_price`, `discount`, `shipping`, `discount_code`, `sub_total`, `tax`, `total_items`, `authorizing_merchant_id`, `order_number`, `bambora_id`, `created`, `updated`, `amount`, `shipped`, `carrier`, `tracking_number`, `info`, `acct_number`, `phn_number`, `billing_full_name`, `billing_address`, `billing_city`, `billing_province`, `billing_zip`, `billing_country`) VALUES
(1, 1, 'Charan', 'charanpaul@hotmail.com', 'Maker', 'Richmond hill', 'Ontario', 'L4b2j5', 'Canada', '752.58', '', '', '', '666.00', '86.58', 1, '300206040', '1541890163', '', '2018-11-10T14:49:23', '2018-11-10 22:49:23', '752.58', 2, '', '', '{\"id\":\"10000077\",\"authorizing_merchant_id\":300206040,\"approved\":\"1\",\"message_id\":\"1\",\"message\":\"Approved\",\"auth_code\":\"TEST\",\"created\":\"2018-11-10T14:49:23\",\"order_number\":\"1541890163\",\"type\":\"P\",\"payment_method\":\"CC\",\"risk_score\":0,\"amount\":752.58,\"custom\":{\"ref1\":\"\",\"ref2\":\"\",\"ref3\":\"\",\"ref4\":\"\",\"ref5\":\"\"},\"card\":{\"card_type\":\"VI\",\"last_four\":\"1234\",\"address_match\":0,\"postal_result\":0,\"avs_result\":\"0\",\"cvd_result\":\"1\",\"avs\":{\"id\":\"U\",\"message\":\"Address information is unavailable.\",\"processed\":false}},\"links\":[{\"rel\":\"void\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000077\\/void\",\"method\":\"POST\"},{\"rel\":\"return\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000077\\/returns\",\"method\":\"POST\"}]}', '', '', '', '', '', '', '', ''),
(2, 4, 'charan maker', 'johndoe@hotmail.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '124.30', '', '', '', '110.00', '14.30', 2, '300206040', '1542159912', '10000078', '2018-11-13T17:45:12', '2018-11-14 01:45:13', '124.3', 0, '', '', '{\"id\":\"10000078\",\"authorizing_merchant_id\":300206040,\"approved\":\"1\",\"message_id\":\"1\",\"message\":\"Approved\",\"auth_code\":\"TEST\",\"created\":\"2018-11-13T17:45:12\",\"order_number\":\"1542159912\",\"type\":\"P\",\"payment_method\":\"CC\",\"risk_score\":0,\"amount\":124.3,\"custom\":{\"ref1\":\"\",\"ref2\":\"\",\"ref3\":\"\",\"ref4\":\"\",\"ref5\":\"\"},\"card\":{\"card_type\":\"VI\",\"last_four\":\"1234\",\"address_match\":0,\"postal_result\":0,\"avs_result\":\"0\",\"cvd_result\":\"1\",\"avs\":{\"id\":\"U\",\"message\":\"Address information is unavailable.\",\"processed\":false}},\"links\":[{\"rel\":\"void\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000078\\/void\",\"method\":\"POST\"},{\"rel\":\"return\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000078\\/returns\",\"method\":\"POST\"}]}', '', '', '', '', '', '', '', ''),
(3, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '124.30', '', '', '', '110.00', '14.30', 2, '300206040', '1542159933', '10000079', '2018-11-13T17:45:33', '2018-11-14 01:45:34', '124.3', 0, '', '', '{\"id\":\"10000079\",\"authorizing_merchant_id\":300206040,\"approved\":\"1\",\"message_id\":\"1\",\"message\":\"Approved\",\"auth_code\":\"TEST\",\"created\":\"2018-11-13T17:45:33\",\"order_number\":\"1542159933\",\"type\":\"P\",\"payment_method\":\"CC\",\"risk_score\":0,\"amount\":124.3,\"custom\":{\"ref1\":\"\",\"ref2\":\"\",\"ref3\":\"\",\"ref4\":\"\",\"ref5\":\"\"},\"card\":{\"card_type\":\"VI\",\"last_four\":\"1234\",\"address_match\":0,\"postal_result\":0,\"avs_result\":\"0\",\"cvd_result\":\"1\",\"avs\":{\"id\":\"U\",\"message\":\"Address information is unavailable.\",\"processed\":false}},\"links\":[{\"rel\":\"void\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000079\\/void\",\"method\":\"POST\"},{\"rel\":\"return\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000079\\/returns\",\"method\":\"POST\"}]}', '', '', '', '', '', '', '', ''),
(4, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '56.50', '', '', '', '50.00', '6.50', 1, '300206040', '1542161322', '10000080', '2018-11-13T18:08:42', '2018-11-14 02:08:43', '56.5', 0, '', '', '{\"id\":\"10000080\",\"authorizing_merchant_id\":300206040,\"approved\":\"1\",\"message_id\":\"1\",\"message\":\"Approved\",\"auth_code\":\"TEST\",\"created\":\"2018-11-13T18:08:42\",\"order_number\":\"1542161322\",\"type\":\"P\",\"payment_method\":\"CC\",\"risk_score\":0,\"amount\":56.5,\"custom\":{\"ref1\":\"\",\"ref2\":\"\",\"ref3\":\"\",\"ref4\":\"\",\"ref5\":\"\"},\"card\":{\"card_type\":\"VI\",\"last_four\":\"1234\",\"address_match\":0,\"postal_result\":0,\"avs_result\":\"0\",\"cvd_result\":\"1\",\"avs\":{\"id\":\"U\",\"message\":\"Address information is unavailable.\",\"processed\":false}},\"links\":[{\"rel\":\"void\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000080\\/void\",\"method\":\"POST\"},{\"rel\":\"return\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000080\\/returns\",\"method\":\"POST\"}]}', '', '', '', '', '', '', '', ''),
(5, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '56.50', '', '', '', '50.00', '6.50', 1, '300206040', '1542162809', '10000081', '2018-11-13T18:33:29', '2018-11-14 02:33:35', '56.5', 0, '', '', '{\"id\":\"10000081\",\"authorizing_merchant_id\":300206040,\"approved\":\"1\",\"message_id\":\"1\",\"message\":\"Approved\",\"auth_code\":\"TEST\",\"created\":\"2018-11-13T18:33:29\",\"order_number\":\"1542162809\",\"type\":\"P\",\"payment_method\":\"CC\",\"risk_score\":0,\"amount\":56.5,\"custom\":{\"ref1\":\"\",\"ref2\":\"\",\"ref3\":\"\",\"ref4\":\"\",\"ref5\":\"\"},\"card\":{\"card_type\":\"VI\",\"last_four\":\"1234\",\"address_match\":0,\"postal_result\":0,\"avs_result\":\"0\",\"cvd_result\":\"1\",\"avs\":{\"id\":\"U\",\"message\":\"Address information is unavailable.\",\"processed\":false}},\"links\":[{\"rel\":\"void\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000081\\/void\",\"method\":\"POST\"},{\"rel\":\"return\",\"href\":\"https:\\/\\/www.beanstream.com\\/api\\/v1\\/payments\\/10000081\\/returns\",\"method\":\"POST\"}]}', '', '', '', '', '', '', '', ''),
(6, 5, 'Stephen', 'stephen@redngen.com', 'Jeevaratnam, 511 -5 SanRomanoway', 'Toronto', 'Ontario', 'M3N2Y4', 'Canada', '56.50', '', '', '', '50.00', '6.50', 1, 'ch_1E3YKPDq0SRHSIFy2VnPF3I2', '1550105029', '', '1550105029', '2019-02-14 00:43:50', '5650', 2, '', '', '{\"id\":\"ch_1E3YKPDq0SRHSIFy2VnPF3I2\",\"object\":\"charge\",\"amount\":5650,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1E3YKPDq0SRHSIFy8rkplumI\",\"captured\":true,\"created\":1550105029,\"currency\":\"cad\",\"customer\":null,\"description\":null,\"destination\":null,\"dispute\":null,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":27,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"receipt_email\":\"tamilromeo@gmail.com\",\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1Dxi3SDq0SRHSIFy\\/ch_1E3YKPDq0SRHSIFy2VnPF3I2\\/rcpt_EWbXuYzpcOsi1CQRUsfh0QiDTtFw6QZ\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1E3YKPDq0SRHSIFy2VnPF3I2\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1E3YKPDq0SRHSIFygN4yxcXK\",\"object\":\"card\",\"address_city\":\"Toronto\",\"address_country\":\"Canada\",\"address_line1\":\"Jeevaratnam, 511 -5 SanRomanoway\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"Ontario\",\"address_zip\":\"M3N2Y4\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":7,\"exp_year\":2020,\"fingerprint\":\"aNqA2Xh1HQGCuDVF\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":\"James B.\",\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}', '', '', '', '', '', '', '', ''),
(7, 5, 'Stephen Jeevaratnam', 'stephen@redngen.com', '265 south ocean dr', 'Oshawa', 'Ontario', 'L1L0K4', 'Canada', '67.80', '', '', '', '60.00', '7.80', 1, 'ch_1E3YRxDq0SRHSIFy8SyVDohN', '1550105497', '', '1550105497', '2019-02-14 00:51:38', '6780', 2, '', '', '{\"id\":\"ch_1E3YRxDq0SRHSIFy8SyVDohN\",\"object\":\"charge\",\"amount\":6780,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1E3YRxDq0SRHSIFyiWcXEjQU\",\"captured\":true,\"created\":1550105497,\"currency\":\"cad\",\"customer\":null,\"description\":null,\"destination\":null,\"dispute\":null,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":39,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"receipt_email\":\"tamilromeo@gmail.com\",\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1Dxi3SDq0SRHSIFy\\/ch_1E3YRxDq0SRHSIFy8SyVDohN\\/rcpt_EWbfEWhOrVBUkDW1mtvaiI66usKh1wG\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1E3YRxDq0SRHSIFy8SyVDohN\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1E3YRxDq0SRHSIFybA1rSvof\",\"object\":\"card\",\"address_city\":\"Oshawa\",\"address_country\":\"Canada\",\"address_line1\":\"265 south ocean dr\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"Ontario\",\"address_zip\":\"L1L0K4\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":7,\"exp_year\":2020,\"fingerprint\":\"aNqA2Xh1HQGCuDVF\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":\"James B.\",\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}', '', '', '', '', '', '', '', ''),
(8, 5, 'Stephen Jeevaratnam', 'stephen@redngen.com', '265 south ocean dr', 'Oshawa', 'Ontario', 'L1L0K4', 'Canada', '56.50', '', '', '', '50.00', '6.50', 1, 'ch_1E3YW8Dq0SRHSIFyp21jucM4', '1550105755', '987978798', '1550105756', '2019-02-14 00:55:56', '5650', 1, 'testy', '123456789876', '{\"id\":\"ch_1E3YW8Dq0SRHSIFyp21jucM4\",\"object\":\"charge\",\"amount\":5650,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1E3YW8Dq0SRHSIFy27vSnCMs\",\"captured\":true,\"created\":1550105756,\"currency\":\"cad\",\"customer\":null,\"description\":null,\"destination\":null,\"dispute\":null,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":34,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"receipt_email\":\"tamilromeo@gmail.com\",\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1Dxi3SDq0SRHSIFy\\/ch_1E3YW8Dq0SRHSIFyp21jucM4\\/rcpt_EWbj3uB6LZFGHy5KHOINOACQRPlvYlQ\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1E3YW8Dq0SRHSIFyp21jucM4\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1E3YW7Dq0SRHSIFyvbD1uZQC\",\"object\":\"card\",\"address_city\":\"Oshawa\",\"address_country\":\"Canada\",\"address_line1\":\"265 south ocean dr\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"Ontario\",\"address_zip\":\"L1L0K4\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":7,\"exp_year\":2020,\"fingerprint\":\"aNqA2Xh1HQGCuDVF\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":\"James B.\",\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}', '', '', '', '', '', '', '', ''),
(9, 5, 'Stephen Jeevaratnam', 'stephen@redngen.com', '265 south ocean dr', 'Oshawa', 'Ontario', 'L1L0K4', 'Canada', '752.58', '', '', '', '666.00', '86.58', 1, 'ch_1E3ZcBCO1ar3MX0EtM7eXR6q', '1550109975', '333', '1550109975', '2019-02-14 02:06:15', '75258', 3, 'eeeeee', 'aaaaaaaa', '{\"id\":\"ch_1E3ZcBCO1ar3MX0EtM7eXR6q\",\"object\":\"charge\",\"amount\":75258,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1E3ZcBCO1ar3MX0ES09tn9FC\",\"captured\":true,\"created\":1550109975,\"currency\":\"cad\",\"customer\":null,\"description\":null,\"destination\":null,\"dispute\":null,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":false,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"risk_score\":24,\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"receipt_email\":\"tamilromeo@gmail.com\",\"receipt_number\":null,\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1DfQgHCO1ar3MX0E\\/ch_1E3ZcBCO1ar3MX0EtM7eXR6q\\/rcpt_EWcrrqjDPa8DtabCuZRsFZzl9eiLwh6\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1E3ZcBCO1ar3MX0EtM7eXR6q\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1E3ZcACO1ar3MX0E87Txw652\",\"object\":\"card\",\"address_city\":\"Oshawa\",\"address_country\":\"Canada\",\"address_line1\":\"265 south ocean dr\",\"address_line1_check\":\"pass\",\"address_line2\":null,\"address_state\":\"Ontario\",\"address_zip\":\"L1L0K4\",\"address_zip_check\":\"pass\",\"brand\":\"Visa\",\"country\":\"US\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":7,\"exp_year\":2020,\"fingerprint\":\"aU8M8C65zK95Gn8v\",\"funding\":\"credit\",\"last4\":\"4242\",\"metadata\":[],\"name\":\"James B.\",\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}', '', '', '', '', '', '', '', ''),
(10, 8, 'charan maker', 'charan.maker@cpvmgroup.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '1.13', '', '', '', '1.00', '0.13', 1, 'ch_1E3ZnlCO1ar3MX0EqlM8TmeK', '1550110692', '+56+52+', '1550110693', '2019-02-14 02:18:14', '113', 1, '', '5146485987465465', '{\"id\":\"ch_1E3ZnlCO1ar3MX0EqlM8TmeK\",\"object\":\"charge\",\"amount\":113,\"amount_refunded\":0,\"application\":null,\"application_fee\":null,\"application_fee_amount\":null,\"balance_transaction\":\"txn_1E3ZnmCO1ar3MX0Et1pwsCwq\",\"captured\":true,\"created\":1550110693,\"currency\":\"cad\",\"customer\":null,\"description\":null,\"destination\":null,\"dispute\":null,\"failure_code\":null,\"failure_message\":null,\"fraud_details\":[],\"invoice\":null,\"livemode\":true,\"metadata\":[],\"on_behalf_of\":null,\"order\":null,\"outcome\":{\"network_status\":\"approved_by_network\",\"reason\":null,\"risk_level\":\"normal\",\"seller_message\":\"Payment complete.\",\"type\":\"authorized\"},\"paid\":true,\"payment_intent\":null,\"receipt_email\":\"tamilromeo@gmail.com\",\"receipt_number\":\"1513-8874\",\"receipt_url\":\"https:\\/\\/pay.stripe.com\\/receipts\\/acct_1DfQgHCO1ar3MX0E\\/ch_1E3ZnlCO1ar3MX0EqlM8TmeK\\/rcpt_EWd3f1lHIyyrU9jtLt8iB0RoDo8Ywoa\",\"refunded\":false,\"refunds\":{\"object\":\"list\",\"data\":[],\"has_more\":false,\"total_count\":0,\"url\":\"\\/v1\\/charges\\/ch_1E3ZnlCO1ar3MX0EqlM8TmeK\\/refunds\"},\"review\":null,\"shipping\":null,\"source\":{\"id\":\"card_1E3ZnkCO1ar3MX0Eeg0Q7ZbX\",\"object\":\"card\",\"address_city\":\"WOODBRIDGE\",\"address_country\":\"Canada\",\"address_line1\":\"643 CHRISLEA RD, unit 3\",\"address_line1_check\":\"fail\",\"address_line2\":null,\"address_state\":\"Ontario\",\"address_zip\":\"L4L8A3\",\"address_zip_check\":\"pass\",\"brand\":\"American Express\",\"country\":\"CA\",\"customer\":null,\"cvc_check\":\"pass\",\"dynamic_last4\":null,\"exp_month\":10,\"exp_year\":2022,\"fingerprint\":\"9ggmv96hdyCqhAiM\",\"funding\":\"credit\",\"last4\":\"2031\",\"metadata\":[],\"name\":\"charanpaul maker\",\"tokenization_method\":null},\"source_transfer\":null,\"statement_descriptor\":null,\"status\":\"succeeded\",\"transfer_data\":null,\"transfer_group\":null}', '', '', '', '', '', '', '', ''),
(11, 2, 'James Bond', 'stephenjeevaratnam@gmail.com', '1606 - 5 Sanromanoway', 'Toronto', 'Ontario', 'M3N2Y4', 'Canada', '56.50', '', '', '', '50.00', '6.50', 1, 'null', '1552715156', '', 'null', '2019-03-16 05:45:57', 'null', 2, '', '', '', '', '', '', '', '', '', '', ''),
(12, 2, 'James Bond', 'stephenjeevaratnam@gmail.com', '1606 - 5 Sanromanoway', 'Toronto', 'Ontario', 'M3N2Y4', 'Canada', '0.00', '', '', '', '0.00', '0.00', 0, 'null', '1552715225', '', 'null', '2019-03-16 05:47:06', 'null', 2, '', '', '', '', '', '', '', '', '', '', ''),
(13, 8, 'charan maker', 'charan.maker@cpvmgroup.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '1.13', '', '', '', '1.00', '0.13', 1, '664764290010020010', '1552859697', '12345678912345', '2019-03-17', '2019-03-17 21:54:59', '1.13', 2, 'Canada Post', '123456788990', '', '', '', '', '', '', '', '', ''),
(14, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '4.83', '0', '15', '', '0.00', '0.00', 1, '664764290010030010', '1563819156', '123456789', '2019-07-22', '2019-07-22 18:12:38', '4.83', 3, 'FedEx', '156565455556', '', '', '', '', '', '', '', '', ''),
(15, 8, 'charan maker', 'charan.maker@cpvmgroup.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '4.83', '0', '15', '', '0.00', '0.00', 1, '664764290010030020', '1563819613', '1234567891', '2019-07-22', '2019-07-22 18:20:14', '4.83', 3, 'Fedex', '155666556665', '', '', '', '', '', '', '', '', ''),
(16, 8, 'charan maker', 'charan.maker@cpvmgroup.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '4.83', '0', '15', '', '0.00', '0.00', 1, '664764290010030030', '1563821220', '123', '2019-07-22', '2019-07-22 18:47:01', '4.83', 2, '', '', '', '', '', '', '', '', '', '', ''),
(17, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '3.70', '0', '15', '', '0.00', '0.00', 1, '664764290010040010', '1564069568', '', '2019-07-25', '2019-07-25 15:46:10', '3.70', 2, '', '', '', '', '', '', '', '', '', '', ''),
(18, 3, 'Ryan Nesharatnam', 'ryan@infinitecorp.ca', '110 Stagecoach Circle', 'Toronto', 'Ontario', 'M1c0a1', 'Canada', '16.13', '0', '15', '', '1.00', '0.13', 1, '664764290010040020', '1564076002', '777666555', '2019-07-25', '2019-07-25 17:33:24', '16.13', 3, 'fedex', '123456788990', '', '', '', '', '', '', '', '', ''),
(19, 153, 'Adel benchikh', 'adel230992@gmail.com', 'App 11 - 120 crois garden', 'Dorval', 'Quebec', 'H9S3G5 ', 'Canada', '72.63', '0', '15', '', '50.00', '6.63', 1, '664764290010080010', '1574154331', '', '2019-11-19', '2019-11-19 09:05:32', '72.63', 2, 'Fedex', '777086773504', '', '', '', '', '', '', '', '', ''),
(20, 213, 'sarah younes', 'sarah_younes@live.com', '5810 Ambler Drive, Unit 14 BEY 124164', 'Mississauga ', 'Ontario', 'L4W4J5', 'Canada', '72.63', '0', '15', '', '50.00', '6.63', 1, '664764290010090010', '1578056829', '89302720524848676641', '2020-01-03', '2020-01-03 13:07:11', '72.63', 1, '', '', '', '', '', '', '', '', '', '', ''),
(21, 297, 'Alexis konkol', 'matt_whiteland@hotmail.com', '1 king street west unit 2411', 'Toronto ', 'Ontario', 'M5h1a1', 'Canada', '130.26', '0', '15', '', '100.00', '13.26', 2, '664764290010160020', '1585396625', '83902720533841288809 / 89302720533841288791', '2020-03-28', '2020-03-28 11:57:06', '130.26', 1, 'Fedex', '770304654120', '', '', '', '', '', '', '', '', ''),
(22, 351, 'Angad Singhkohli', 'venoth@censera.com', '48 Narang Colony', 'New Dehli', 'Dehli', '110058', 'IN', '17.26', '0', '15', '', '1.00', '0.26', 1, '664764290010170010', '1588100982', '5343839472343242', '2020-04-28', '2020-04-28 19:09:44', '17.26', 3, 'Chatr', '343424234234234234', '', '3425523452345234523', '+1 (613) 796-8404', '', '', '', '', '', ''),
(23, 412, 'Charanpul Maker', 'venothtim@gmail.com', '3-643 Chrislea Rd', 'Woodbridge', 'Ontario', 'L4L 8A3', 'CA', '17.26', '0', '15', '', '1.00', '0.26', 1, '664764290010180020', '1588980007', '', '2020-05-08', '2020-05-08 23:20:08', '17.26', 2, '', '', '', '', '', '', '', '', '', '', ''),
(28, 412, 'Charanpul Maker', 'venothtim@gmail.com', '3-643 Chrislea Rd', 'Woodbridge', 'Ontario', 'L4L 8A3', 'CA', '29.38', '0', '15', '', '1.00', '3.38', 1, '664764290010180030', '1588985744', '5756756345634634456', '2020-05-08', '2020-05-09 00:55:46', '29.38', 3, 'Chatr', '453453453523452345', '', '5646345634', '+1 (613) 796-8404', '', '', '', '', '', ''),
(29, 420, ' brigid kimani-oloyede', 'brigidkimani@gmail.com', '413A 2903 unwin rd nw', 'calgary', 'Alberta', 'T2N 4M4', 'CA', '68.25', '0', '15', '', '40.00', '3.25', 1, '664764290010190010', '1590358112', '89302720533844810575', '2020-05-24', '2020-05-24 22:08:33', '68.25', 1, 'Fedex', '770635627731', '', '', '', '', '', '', '', '', ''),
(30, 423, 'Jubalee quick ', 'luckypepsi2@gmail.com', '3937 46 st', 'Reddder', 'Alberta', 'T4n1l9 ', 'CA', '78.75', '0', '15', '', '50.00', '3.75', 1, '664764290010200010', '1591377299', '89302720533844810567', '2020-06-05', '2020-06-05 17:15:01', '78.75', 1, 'Fedex', '770661278202', '', '', '', '', '', '', '', '', ''),
(31, 433, 'Sukhminder Miglani', 'shraddhamaorya@gmail.com', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '50.85', '0', '10', '', '25.00', '5.85', 1, '664764290010220010', '1600086104', '', '2020-09-14', '2020-09-14 12:21:44', '50.85', 0, '', '', '', '', '', '', '', '', '', '', ''),
(32, 434, 'Sukhminder Miglani', 'smiglani@nanojot.in', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '50.85', '0', '10', '', '25.00', '5.85', 1, '664764290010230010', '1600182482', '', '2020-09-15', '2020-09-15 15:08:06', '50.85', 0, '', '', '', '', '', '', '', '', '', '', ''),
(33, 8, 'charan maker', 'charan.maker@cpvmgroup.com', '643 CHRISLEA RD, unit 3', 'WOODBRIDGE', 'Ontario', 'L4L8A3', 'Canada', '84.75', '0', '10', '', '55.00', '9.75', 1, '664764290010240010', '1604341017', '', '2020-11-02', '2020-11-02 18:17:01', '84.75', 0, '', '', '', '', '', '', '', '', '', '', ''),
(34, 447, 'Sukhminder Miglani', 'tamrakarlalit82@gmail.com', '300 Killian road', 'Vaughan', 'ontario ', 'L6A 1A4', 'CA', '45.00', '0', '10', '', '25.00', '0.00', 1, '664764290010250010', '1604690465', '', '2020-11-06', '2020-11-06 19:21:06', '45.00', 0, '', '', '', '', '', '', '', '', '', '', ''),
(35, 445, 'Monesh', 'moneshfoodography@gmail.com', 'Near Akai school', 'Bangalore', 'karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '664764290010280010', '1608122445', '12345678901234567890', '2020-12-16', '2020-12-16 12:40:45', '45.00', 3, 'Chatr', '423142314231', '', '', '', '', '', '', '', '', ''),
(36, 445, 'Monesh', 'moneshfoodography@gmail.com', 'Near Akai school', 'Bangalore', 'karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '664764290010290010', '1608209320', '', '2020-12-17', '2020-12-17 12:48:41', '45.00', 0, '', '', '', '', '', '', '', '', '', '', ''),
(37, 445, 'Monesh', 'moneshfoodography@gmail.com', 'Near Akai school', 'Bangalore', 'karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '664764290010310060', '1608573299', '', '2020-12-21', '2020-12-21 17:54:59', '45.00', 0, '', '', '', '', '', '', '', '', '', '', ''),
(38, 454, 'Shraddha', 'maddy@nanojot.ca', '1st Main, Near Dental Care Hospital', 'Bangalore', 'karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '664764290010320010', '1609773730', '', '2021-01-04', '2021-01-04 15:22:10', '45.00', 0, '', '', '', '', '', '', '', '', '', '', ''),
(39, 464, 'Sector 74', 'jslamba0811@gmail.com', 'Cs6-1303, Supertech Capetown', 'Noida', 'Uttar Pradesh', '201301', 'IN', '130.00', '0', '10', '', '100.00', '0.00', 2, '123445555', '1614857011', '', '2021-03-04', '2021-03-04 11:23:31', '130', 0, 'Chatr Helpr ', '', 'null', '', '', '', '', '', '', '', ''),
(40, 464, 'Test', 'jslamba0811@gmail.com', 'Supertech Capetown Sector 74', 'Noida', 'Uttar Pradesh', '201301', 'IN', '135.00', '0', '10', '', '105.00', '0.00', 2, '1234455667788', '1616921401', '', '2021-03-28', '2021-03-28 08:50:01', '135.00', 3, '', '12123232323', 'null', '5646345634', '+1 (613) 796-8404', '', '', '', '', '', ''),
(41, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '1234455667788', '1617195285', '', '2021-03-28', '2021-03-31 12:54:45', '45.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(42, 443, 'Dinesh', 'John@nanojot.ca', 'Palace Street road', 'Delhi', 'Delhi', '490239', 'IN', '90.00', '0', '10', '', '60.00', '0.00', 2, '1234455667788', '1617195786', '', '2021-03-28', '2021-03-31 13:03:06', '90.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(43, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '95.00', '0', '10', '', '65.00', '0.00', 2, '1234455667788', '1617196682', '', '2021-03-28', '2021-03-31 13:18:02', '95.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(44, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 1, '1234455667788', '1617196850', '', '2021-03-28', '2021-03-31 13:20:50', '75.00', 1, '', '689454', 'null', '', '', '', '', '', '', '', ''),
(45, 443, 'shraddha', 'John@nanojot.ca', 'test', 'Bengaluru', 'Karnataka', '637890', 'IN', '70.00', '0', '10', '', '50.00', '0.00', 1, '1234455667788', '1617204768', '', '2021-03-28', '2021-03-31 15:32:48', '70.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(46, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '105.00', '0', '10', '', '75.00', '0.00', 2, '1234455667788', '1617282480', '', '2021-03-28', '2021-04-01 13:08:00', '105.00', 1, '', '345345', 'null', '', '', '', '', '', '', '', ''),
(47, 443, 'shraddha', 'John@nanojot.ca', 'test', 'Bengaluru', 'Karnataka', '637890', 'IN', '70.00', '0', '10', '', '50.00', '0.00', 1, '1234455667788', '1617284693', '', '2021-03-28', '2021-04-01 13:44:53', '70.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(48, 443, 'shraddha', 'John@nanojot.ca', 'test', 'Bengaluru', 'Karnataka', '637890', 'IN', '70.00', '0', '10', '', '50.00', '0.00', 1, '1234455667788', '1617288436', '', '2021-03-28', '2021-04-01 14:47:16', '70.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(49, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '1234455667788', '1617288957', '', '2021-03-28', '2021-04-01 14:55:57', '45.00', 3, '', '34346', 'null', '', '', '', '', '', '', '', ''),
(50, 443, 'Shraddha', 'John@nanojot.ca', 'Near Akai Public School', 'Bengaluru', 'Karnataka', '560003', 'IN', '105.00', '0', '10', '', '75.00', '0.00', 2, '1234455667788', '1617289253', '', '2021-03-28', '2021-04-01 15:00:53', '105.00', 3, '', '234355', 'null', '', '', '', '', '', '', '', ''),
(51, 254, 'Sukhminder Miglani', 'smiglani@nanojot.com', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A1A4', 'CA', '79.10', '0', '10', '', '50.00', '9.10', 1, '1234455667788', '1617540210', '', '2021-03-28', '2021-04-04 12:43:30', '79.10', 3, '', '', 'null', '4654654654', '6478188131', '', '', '', '', '', ''),
(52, 254, 'Sukhminder Miglani', 'smiglani@nanojot.com', '300 Killian Road', 'Maple', 'Ontario', 'L6A1A4', 'Canada', '152.55', '0', '10', '', '105.00', '17.55', 2, '1234455667788', '1617541659', '', '2021-03-28', '2021-04-04 13:07:39', '152.55', 1, '', '786576576', 'null', '', '', '', '', '', '', '', ''),
(53, 472, 'Test address', 'mo@nanojot.ca', 'test street address', 'Toronto', 'Ontario', '578890', 'CA', '45.20', '0', '10', '', '20.00', '5.20', 1, '1234455667788', '1617628998', '', '2021-03-28', '2021-04-05 13:23:18', '45.20', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(54, 472, 'test ', 'mo@nanojot.ca', 'address', 'Toronto', 'Ontario', '434678', 'CA', '22.60', '0', '0', '', '20.00', '2.60', 1, '9999403408', '1617710991', '', '2021-04-05', '2021-04-06 12:09:51', '22.60', 3, '', '', 'null', '892389', '4169229977', '', '', '', '', '', ''),
(55, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '45.00', '0', '10', '', '45.00', '0.00', 1, '9999403408', '1617711270', '', '2021-04-05', '2021-04-06 12:14:30', '45.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(56, 472, 'test ', 'mo@nanojot.ca', 'address', 'Toronto', 'Ontario', '434678', 'CA', '33.90', '20', '0', 'TDSB40', '50.00', '3.90', 2, '9999403408', '1617712953', '', '2021-04-05', '2021-04-06 12:42:33', '33.90', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(57, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '66.00', '24', '10', 'TDSB40', '90.00', '0.00', 2, '9999403408', '1617713836', '', '2021-04-05', '2021-04-06 12:57:16', '66.00', 3, '', '454675756', 'null', '6457676', '7892349387', '', '', '', '', '', ''),
(58, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '45.00', '0', '10', '', '45.00', '0.00', 1, '9999403408', '1617715314', '', '2021-04-05', '2021-04-06 13:21:54', '45.00', 1, '', '46565', 'null', '', '', '', '', '', '', '', ''),
(59, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '65.00', '0', '10', '', '65.00', '0.00', 2, '9999403408', '1617716042', '', '2021-04-05', '2021-04-06 13:34:02', '65.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(60, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '85.00', '0', '10', '', '85.00', '0.00', 2, '9999403408', '1617719679', '', '2021-04-05', '2021-04-06 14:34:39', '85.00', 3, '', '2323234', 'null', '32434', '78933346784', '', '', '', '', '', ''),
(61, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '110.00', '0', '10', '', '110.00', '0.00', 3, '9999403408', '1617721164', '', '2021-04-05', '2021-04-06 14:59:24', '110.00', 3, '', '5676675', 'null', '7665767', '34589574897594768957', '', '', '', '', '', ''),
(62, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '85.00', '0', '10', '', '85.00', '0.00', 3, '9999403408', '1617722639', '', '2021-04-05', '2021-04-06 15:23:59', '85.00', 3, '', '34324', 'null', '', '', '', '', '', '', '', ''),
(63, 472, 'test ', 'mo@nanojot.ca', 'address', 'Toronto', 'Ontario', '434678', 'CA', '45.20', '0', '0', '', '40.00', '5.20', 2, '9999403408', '1617723281', '', '2021-04-05', '2021-04-06 15:34:41', '45.20', 3, '', '', 'null', '7827834367', '8902345678', '', '', '', '', '', ''),
(64, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '130.00', '0', '10', '', '130.00', '0.00', 4, '9999403408', '1617723594', '', '2021-04-05', '2021-04-06 15:39:54', '130.00', 3, '', '334344', 'null', '52465657', '3413413345', '', '', '', '', '', ''),
(65, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1617971103', '', '2021-04-08', '2021-04-09 12:25:03', '45.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(66, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '100.00', '0', '10', '', '70.00', '0.00', 2, '9999403408', '1617971601', '', '2021-04-08', '2021-04-09 12:33:21', '100.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(67, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '41.00', '14', '10', 'TDSB40', '35.00', '0.00', 1, '9999403408', '1617972186', '', '2021-04-08', '2021-04-09 12:43:06', '41.00', 2, '', '3454545', 'null', '67568', '76578587588', '', '', '', '', '', ''),
(68, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '53.00', '22', '10', 'TDSB40', '55.00', '0.00', 2, '9999403408', '1617972717', '', '2021-04-08', '2021-04-09 12:51:57', '53.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(69, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '110.00', '0', '10', '', '80.00', '0.00', 2, '9999403408', '1617973860', '', '2021-04-08', '2021-04-09 13:11:00', '110.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(70, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '80.00', '0', '10', '', '50.00', '0.00', 2, '9999403408', '1617974549', '', '2021-04-08', '2021-04-09 13:22:29', '80.00', 2, '', '426546546456', 'null', '', '', '', '', '', '', '', ''),
(71, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '35.00', '20', '10', 'TYSD78', '35.00', '0.00', 1, '9999403408', '1617975881', '', '2021-04-08', '2021-04-09 13:44:41', '35.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(72, 472, 'test ', 'mo@nanojot.ca', 'address', 'Toronto', 'Ontario', '434678', 'CA', '22.60', '0', '0', '', '20.00', '2.60', 1, '9999403408', '1617976462', '', '2021-04-08', '2021-04-09 13:54:22', '22.60', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(73, 464, 'Test', 'jslamba0811@gmail.com', 'Supertech Capetown Sector 74', 'Noida', 'Uttar Pradesh', '201301', 'IN', '65.00', '0', '10', '', '45.00', '0.00', 2, '9999403408', '1617979030', '', '2021-04-08', '2021-04-09 14:37:10', '65.00', 0, '', '', 'null', '', '', 'Jaspreet Singh Lamba', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA'),
(74, 464, 'Test', 'jslamba0811@gmail.com', 'Supertech Capetown Sector 74', 'Noida', 'Uttar Pradesh', '201301', 'IN', '65.00', '0', '10', '', '45.00', '0.00', 2, '9999403408', '1617979283', '', '2021-04-08', '2021-04-09 14:41:23', '65.00', 0, '', '', 'null', '', '', 'Jaspreet Singh Lamba', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA'),
(75, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '90.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1617981329', '', '2021-04-08', '2021-04-09 15:15:29', '90.00', 1, '', '343453', 'null', '', '', 'test ', 'address', 'Toronto', 'Ontario', '434678', 'CA'),
(76, 472, 'test ', 'mo@nanojot.ca', 'address', 'Toronto', 'Ontario', '434678', 'CA', '22.60', '0', '0', '', '20.00', '2.60', 1, '9999403408', '1617981853', '', '2021-04-08', '2021-04-09 15:24:13', '22.60', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(77, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '85.00', '0', '10', '', '65.00', '0.00', 2, '9999403408', '1617982167', '', '2021-04-08', '2021-04-09 15:29:27', '85.00', 0, '', '', 'null', '', '', 'test ', 'address', 'Toronto', 'Ontario', '434678', 'CA'),
(78, 472, 'Shraddha', 'mo@nanojot.ca', 'New bel road', 'Hyderabad', 'Telangana', '390283', 'IN', '65.00', '0', '10', '', '45.00', '0.00', 1, '9999403408', '1617982526', '', '2021-04-08', '2021-04-09 15:35:26', '65.00', 0, '', '', 'null', '', '', 'Dinesh', 'New police line road', 'Nashville', 'Tennessee', '489035', 'US'),
(79, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618238119', '', '2021-04-08', '2021-04-12 14:35:19', '45.00', 0, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(80, 433, 'Sukhminder Miglani', 'shraddhamaorya@gmail.com', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '62.15', '0', '10', '', '35.00', '7.15', 1, '9999403408', '1618238629', '', '2021-04-08', '2021-04-12 14:43:49', '62.15', 0, '', '', 'null', '', '', 'Monesh', 'Near postoffice', 'Pune', 'Maharashtra', '348890', 'IN'),
(81, 464, 'Test', 'jslamba0811@gmail.com', 'Supertech Capetown Sector 74', 'Noida', 'Uttar Pradesh', '201301', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618241762', '', '2021-04-08', '2021-04-12 15:36:02', '55.00', 0, '', '', 'null', '', '', 'Jaspreet Singh Lamba', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA'),
(82, 433, 'Sukhminder Miglani', 'shraddhamaorya@gmail.com', '300 Killian Road', 'Vaughan', 'Ontario', 'L6A 1A4', 'CA', '50.85', '0', '10', '', '25.00', '5.85', 1, '9999403408', '1618242470', '', '2021-04-08', '2021-04-12 15:47:50', '50.85', 0, '', '', 'null', '', '', 'Monesh', 'Near postoffice', 'Pune', 'Maharashtra', '348890', 'IN'),
(83, 433, 'Monesh', 'shraddhamaorya@gmail.com', 'Near postoffice', 'Pune', 'Maharashtra', '348890', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1618242609', '', '2021-04-08', '2021-04-12 15:50:09', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(84, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '90.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1618402822', '', '2021-04-08', '2021-04-14 12:20:22', '90.00', 2, '', '3524354', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(85, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '55.00', '0', '0', '', '55.00', '0.00', 2, '9999403408', '1618403384', '', '2021-04-08', '2021-04-14 12:29:44', '55.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(86, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618668269', '', '2021-04-08', '2021-04-17 14:04:29', '45.00', 1, '', '32443', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(87, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1618672500', '', '2021-04-08', '2021-04-17 15:15:00', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(88, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1618673203', '', '2021-04-08', '2021-04-17 15:26:43', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(89, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618674392', '', '2021-04-08', '2021-04-17 15:46:32', '55.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(90, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '60.00', '0', '10', '', '40.00', '0.00', 1, '9999403408', '1618675044', '', '2021-04-08', '2021-04-17 15:57:24', '60.00', 2, '', '334334', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(91, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618675848', '', '2021-04-08', '2021-04-17 16:10:48', '55.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(92, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '65.00', '0', '10', '', '45.00', '0.00', 2, '9999403408', '1618681332', '', '2021-04-08', '2021-04-17 17:42:12', '65.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(93, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618682257', '', '2021-04-08', '2021-04-17 17:57:37', '45.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(94, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618682370', '', '2021-04-08', '2021-04-17 17:59:30', '55.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(95, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1618683037', '', '2021-04-08', '2021-04-17 18:10:37', '75.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(96, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 2, '9999403408', '1618683363', '', '2021-04-08', '2021-04-17 18:16:03', '50.00', 3, '', '', 'null', '', '', '', '', '', '', '', ''),
(97, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '90.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1618684440', '', '2021-04-08', '2021-04-17 18:34:00', '90.00', 2, '', '', 'null', '78934', '8902347840', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(98, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618835040', '', '2021-04-08', '2021-04-19 12:24:00', '55.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(99, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1618835732', '', '2021-04-08', '2021-04-19 12:35:32', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(100, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '40.00', '0', '0', '', '40.00', '0.00', 1, '9999403408', '1618836085', '', '2021-04-08', '2021-04-19 12:41:25', '40.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(101, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1618836313', '', '2021-04-08', '2021-04-19 12:45:13', '30.00', 3, '', '', 'null', '34344', '7894532167', '', '', '', '', '', ''),
(102, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '60.00', '0', '10', '', '40.00', '0.00', 1, '9999403408', '1618840369', '', '2021-04-08', '2021-04-19 13:52:49', '60.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(103, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618842718', '', '2021-04-08', '2021-04-19 14:31:58', '55.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(104, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618842879', '', '2021-04-08', '2021-04-19 14:34:39', '55.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(105, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1618843121', '', '2021-04-08', '2021-04-19 14:38:41', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(106, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618843231', '', '2021-04-08', '2021-04-19 14:40:31', '55.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(107, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618930605', '', '2021-04-08', '2021-04-20 14:56:45', '45.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(108, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1618930962', '', '2021-04-08', '2021-04-20 15:02:42', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(109, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618931088', '', '2021-04-08', '2021-04-20 15:04:48', '45.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(110, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1618931389', '', '2021-04-08', '2021-04-20 15:09:49', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(111, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1618933504', '', '2021-04-08', '2021-04-20 15:45:04', '55.00', 1, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(112, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1618933982', '', '2021-04-08', '2021-04-20 15:53:02', '45.00', 3, '', '', 'null', '48900', '4562902167', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(113, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1618934218', '', '2021-04-08', '2021-04-20 15:56:58', '20.00', 3, '', '', 'null', '37889', '3267892134', '', '', '', '', '', ''),
(114, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1618934576', '', '2021-04-08', '2021-04-20 16:02:56', '30.00', 3, '', '', 'null', '', '', '', '', '', '', '', ''),
(115, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1620216974', '', '2021-04-08', '2021-05-05 12:16:14', '45.00', 3, '', '3432434', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(116, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '60.00', '0', '10', '', '40.00', '0.00', 1, '9999403408', '1620218865', '', '2021-04-08', '2021-05-05 12:47:45', '60.00', 2, '', '56345758', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(117, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1620220010', '', '2021-04-08', '2021-05-05 13:06:50', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(118, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1620236872', '', '2021-04-08', '2021-05-05 17:47:52', '75.00', 1, '', '2343434', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(119, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1620237713', '', '2021-04-08', '2021-05-05 18:01:53', '75.00', 3, '', '', 'null', '3413434', '8902323456', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(120, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1620238886', '', '2021-04-08', '2021-05-05 18:21:26', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(121, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '80.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1620653720', '', '2021-04-08', '2021-05-10 13:35:20', '80.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(122, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '90.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1620656140', '', '2021-04-08', '2021-05-10 14:15:40', '90.00', 2, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN');
INSERT INTO `orders` (`id`, `user_id`, `full_name`, `email`, `address`, `city`, `province`, `zip`, `country`, `total_price`, `discount`, `shipping`, `discount_code`, `sub_total`, `tax`, `total_items`, `authorizing_merchant_id`, `order_number`, `bambora_id`, `created`, `updated`, `amount`, `shipped`, `carrier`, `tracking_number`, `info`, `acct_number`, `phn_number`, `billing_full_name`, `billing_address`, `billing_city`, `billing_province`, `billing_zip`, `billing_country`) VALUES
(123, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1620656460', '', '2021-04-08', '2021-05-10 14:21:00', '20.00', 3, '', '', 'null', '42354', '53456576576', '', '', '', '', '', ''),
(124, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1620659596', '', '2021-04-08', '2021-05-10 15:13:16', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(125, 464, 'Jaspreet Singh Lamba', 'jslamba0811@gmail.com', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA', '89.25', '0', '0', '', '85.00', '4.25', 2, '9999403408', '1620717317', '', '2021-04-08', '2021-05-11 07:15:17', '89.25', 3, '', '', 'null', '44332211', '', '', '', '', '', '', ''),
(126, 464, 'Sector 74', 'jslamba0811@gmail.com', 'Cs6-1303, Supertech Capetown', 'Noida', 'Uttar Pradesh', '201301', 'IN', '130.00', '0', '10', '', '100.00', '0.00', 2, '9999403408', '1620717832', '', '2021-04-08', '2021-05-11 07:23:52', '130.00', 0, '', '', 'null', '', '', 'Jaspreet Singh Lamba', 'Cs6-1303, Supertech Capetown', 'Noida', 'Alberta', '201301', 'CA'),
(127, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1620744350', '', '2021-04-08', '2021-05-11 14:45:50', '20.00', 3, '', '', 'null', '12121212', '', '', '', '', '', '', ''),
(128, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1620744494', '', '2021-04-08', '2021-05-11 14:48:14', '55.00', 1, '', '8889999', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(129, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1620822967', '', '2021-04-08', '2021-05-12 12:36:07', '30.00', 3, '', '', 'null', '38789789', '', '', '', '', '', '', ''),
(130, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '10', '', '35.00', '0.00', 1, '9999403408', '1620823579', '', '2021-04-08', '2021-05-12 12:46:19', '55.00', 3, '', '7676089156', 'null', '778979', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(131, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1620824562', '', '2021-04-08', '2021-05-12 13:02:42', '75.00', 3, '', '756876879', 'null', '890402', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(132, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '155.00', '0', '10', '', '125.00', '0.00', 4, '9999403408', '1620825524', '', '2021-04-08', '2021-05-12 13:18:44', '155.00', 3, '', '778769', 'null', '674349', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(133, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1620909432', '', '2021-04-08', '2021-05-13 12:37:12', '75.00', 3, '', '5656576', 'null', '645768768', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(134, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 2, '9999403408', '1620919968', '', '2021-04-08', '2021-05-13 15:32:48', '75.00', 3, '', '78899', 'null', '89900', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(135, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '80.00', '0', '10', '', '60.00', '0.00', 2, '9999403408', '1620921668', '', '2021-04-08', '2021-05-13 16:01:08', '80.00', 3, '', '767678', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(136, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623237276', '', '2021-04-08', '2021-06-09 11:14:36', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(137, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623248882', '', '2021-04-08', '2021-06-09 14:28:02', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(138, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623249746', '', '2021-04-08', '2021-06-09 14:42:26', '50.00', 3, '', '', 'null', '88989', '', '', '', '', '', '', ''),
(139, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1623250089', '', '2021-04-08', '2021-06-09 14:48:09', '20.00', 3, '', '', 'null', '43252435', '', '', '', '', '', '', ''),
(140, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 1, '9999403408', '1623252021', '', '2021-04-08', '2021-06-09 15:20:21', '75.00', 3, '', '88993', 'null', '5656', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(141, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623675153', '', '2021-04-08', '2021-06-14 12:52:33', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(142, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623676028', '', '2021-04-08', '2021-06-14 13:07:08', '50.00', 3, '', '', 'null', '2', '', '', '', '', '', '', ''),
(143, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 1, '9999403408', '1623676714', '', '2021-04-08', '2021-06-14 13:18:34', '75.00', 0, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(144, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623682567', '', '2021-04-08', '2021-06-14 14:56:07', '30.00', 1, '', '4523654645', 'null', '', '', '', '', '', '', '', ''),
(145, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623685245', '', '2021-04-08', '2021-06-14 15:40:45', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(146, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623691116', '', '2021-04-08', '2021-06-14 17:18:36', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(147, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623693261', '', '2021-04-08', '2021-06-14 17:54:21', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(148, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '40.00', '0', '0', '', '40.00', '0.00', 1, '9999403408', '1623847900', '', '2021-04-08', '2021-06-16 12:51:40', '40.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(149, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '40.00', '0', '0', '', '40.00', '0.00', 1, '9999403408', '1623848586', '', '2021-04-08', '2021-06-16 13:03:06', '40.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(150, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '0.00', '0', '0', '', '0.00', '0.00', 0, '9999403408', '1623849476', '', '2021-04-08', '2021-06-16 13:17:56', '0.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(151, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '40.00', '0', '0', '', '40.00', '0.00', 1, '9999403408', '1623849965', '', '2021-04-08', '2021-06-16 13:26:05', '40.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(152, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623851031', '', '2021-04-08', '2021-06-16 13:43:51', '50.00', 1, '', '', 'null', '', '', '', '', '', '', '', ''),
(153, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623856385', '', '2021-04-08', '2021-06-16 15:13:05', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(154, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '40.00', '0', '0', '', '40.00', '0.00', 1, '9999403408', '1623929617', '', '2021-04-08', '2021-06-17 11:33:37', '40.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(155, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '30.00', '0', '0', '', '30.00', '0.00', 1, '9999403408', '1623933989', '', '2021-04-08', '2021-06-17 12:46:29', '30.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(156, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623934437', '', '2021-04-08', '2021-06-17 12:53:57', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(157, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1623935839', '', '2021-04-08', '2021-06-17 13:17:19', '45.00', 0, '', '', 'null', '', '', 'Shraddha', 'New School road', 'Berlin', 'Berlin', '790189', 'DE'),
(158, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '0.00', '0', '0', '', '0.00', '0.00', 0, '9999403408', '1623937811', '', '2021-04-08', '2021-06-17 13:50:11', '0.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(159, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623938132', '', '2021-04-08', '2021-06-17 13:55:32', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(160, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '35.00', '0', '0', '', '35.00', '0.00', 1, '9999403408', '1623942039', '', '2021-04-08', '2021-06-17 15:00:39', '35.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(161, 456, 'Shraddha', 'sandy@nanojot.ca', 'New School road', 'Berlin', 'Berlin', '790189', 'DE', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623942141', '', '2021-04-08', '2021-06-17 15:02:21', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(162, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '50.00', '0', '0', '', '50.00', '0.00', 1, '9999403408', '1623944897', '', '2021-04-08', '2021-06-17 15:48:17', '50.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(163, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '20.00', '0', '0', '', '20.00', '0.00', 1, '9999403408', '1624003983', '', '2021-04-08', '2021-06-18 08:13:03', '20.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(164, 474, 'HEEMANSHU BHALLA', 'bhallaheemanshu@gmail.com', 'gg', 'Nurwala (81)', 'Punjab', '141007', 'IN', '35.00', '0', '0', '', '35.00', '0.00', 1, '9999403408', '1631811951', '', '2021-04-08', '2021-09-16 17:05:51', '35.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(165, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '95.00', '0', '10', '', '75.00', '0.00', 2, '9999403408', '1631900866', '', '2021-04-08', '2021-09-17 17:47:46', '95.00', 0, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(166, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '55.00', '0', '0', '', '55.00', '0.00', 1, '9999403408', '1631902043', '', '2021-04-08', '2021-09-17 18:07:23', '55.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(167, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '100.00', '0', '0', '', '100.00', '0.00', 2, '9999403408', '1631902442', '', '2021-04-08', '2021-09-17 18:14:02', '100.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(168, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '110.00', '0', '10', '', '90.00', '0.00', 2, '9999403408', '1632151453', '', '2021-04-08', '2021-09-20 15:24:13', '110.00', 1, '', '4444', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(169, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '55.00', '0', '0', '', '55.00', '0.00', 1, '9999403408', '1632318605', '', '2021-04-08', '2021-09-22 13:50:05', '55.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(170, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '45.00', '0', '10', '', '25.00', '0.00', 1, '9999403408', '1632424255', '', '2021-04-08', '2021-09-23 19:10:55', '45.00', 1, '', '3243245', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(171, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '55.00', '0', '0', '', '55.00', '0.00', 1, '9999403408', '1632426623', '', '2021-04-08', '2021-09-23 19:50:23', '55.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(172, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '120.00', '0', '10', '', '100.00', '0.00', 2, '9999403408', '1635166211', '', '2021-04-08', '2021-10-25 12:50:11', '120.00', 1, '', '67890', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(173, 456, 'Shraddha 2', 'sandy@nanojot.ca', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '55.00', '0', '0', '', '55.00', '0.00', 1, '9999403408', '1635167236', '', '2021-04-08', '2021-10-25 13:07:16', '55.00', 0, '', '', 'null', '', '', '', '', '', '', '', ''),
(174, 456, 'Shraddha', 'sandy@nanojot.ca', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '60.00', '0', '10', '', '40.00', '0.00', 1, '9999403408', '1635168414', '', '2021-04-08', '2021-10-25 13:26:54', '60.00', 1, '', '23434', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(175, 456, 'Shraddha', 'ankitchoudhary87@gmail.com', 'test street', 'Bengaluru', 'Karnataka', '560030', 'IN', '75.00', '0', '10', '', '55.00', '0.00', 1, '9999403408', '1639923412', '', '2021-04-08', '2021-12-19 14:16:52', '75.00', 0, '', '', 'null', '', '', 'Shraddha 2', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN'),
(176, 456, 'Shraddha 2', 'ankitchoudhary87@gmail.com', 'Hemunagar', 'Bilaspur', 'Chhattisgarh', '495004', 'IN', '35.00', '0', '0', '', '35.00', '0.00', 1, '9999403408', '1639923489', '', '2021-04-08', '2021-12-19 14:18:09', '35.00', 0, '', '', 'null', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `price` int(11) NOT NULL,
  `with_sim` int(1) NOT NULL,
  `plan_detail1` varchar(1000) NOT NULL,
  `plan_detail2` varchar(1000) NOT NULL,
  `plan_detail3` varchar(1000) NOT NULL,
  `plan_detail4` varchar(1000) NOT NULL,
  `plan_detail5` varchar(1000) NOT NULL,
  `plan_detail6` varchar(1000) NOT NULL,
  `plan_detail7` varchar(1000) NOT NULL,
  `plan_detail8` varchar(1000) NOT NULL,
  `plan_detail9` varchar(1000) NOT NULL,
  `plan_detail10` varchar(1000) NOT NULL,
  `carrier` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `shipping` tinyint(4) NOT NULL DEFAULT 1,
  `save` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `title`, `price`, `with_sim`, `plan_detail1`, `plan_detail2`, `plan_detail3`, `plan_detail4`, `plan_detail5`, `plan_detail6`, `plan_detail7`, `plan_detail8`, `plan_detail9`, `plan_detail10`, `carrier`, `type`, `sort_order`, `shipping`, `save`) VALUES
(0, 'New Sim', 0, 1, '', '', '', '', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(9, 'Canada/US Talk & International Text + 8 GB Data', 55, 1, 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(10, 'Canada-Wide Talk & International Text + 6GB Data', 45, 1, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', '5', 1, 0, 1, 0),
(11, 'Canada-Wide Talk & International Text + 1 GB Data', 35, 1, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(13, 'Unlimited Canada-Wide Talk & International Text', 25, 1, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(14, 'Canada-Wide Talk & International Text + 4.5 GB Data', 40, 1, 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(15, 'Canada/US Talk & International Text + 8 GB Data', 50, 1, 'Unlimited Canada/US Talk', 'Unlimited Text to Canada, US & International', '8 GB data at 3G speed', '', '', '', '', '', '', '', '1', 1, 0, 1, 0),
(16, 'Canada-Wide Talk & International Text + 6GB Data', 40, 0, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '6 GB data at 3G speed', '', '', '', '', '', '', '', '5', 1, 0, 0, 0),
(17, 'Canada-Wide Talk & International Text + 1 GB Data', 30, 0, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '1 GB data at 3G speed', '', '', '', '', '', '', '', '1', 1, 0, 0, 0),
(18, 'Unlimited Canada-Wide Talk & International Text', 20, 0, 'Unlimited Canada-wide Talk', 'Unlimited International Text', '', '', '', '', '', '', '', '', '1', 1, 0, 0, 0),
(19, 'Canada-Wide Talk & International Text + 4.5 GB Data', 35, 0, 'Unlimited Canada-wide Talk', 'Unlimited Text to Canada, US & International', '4.5 GB at 3G speed', 'International talk Saver', '', '', '', '', '', '', '1', 1, 0, 0, 0),
(20, 'New Sim', 0, 2, '', '', '', '', '', '', '', '', '', '', '1', 1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `sim_cost` decimal(10,0) NOT NULL,
  `new_sim_content` varchar(5000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `sim_cost`, `new_sim_content`) VALUES
(1, '30', 'Dear {{clientname}},<br/><br/>Provided SIM Number is not working. New SIM card will cost ${{cost}} and a new number will be activated for the new SIM card. Please click the button below to accept or reject the offer.<br/><a href=\'http://phpprojects.nanojot.com/CanadianSimCard-Prod-Copy/cart/add/{{planid}}\'><u>Accept Offer</ul></a>   <a href=\'http://phpprojects.nanojot.com/CanadianSimCard-Prod-Copy/cart/sim_card_status?status=0&orderid={{orderid}}\'><u>Reject Offer</ul></a><br/><br/>\r\nThanks<br/>\r\nCustomer Service<br/>\r\ncanadiansimcards.com<br/>');

-- --------------------------------------------------------

--
-- Table structure for table `shipments`
--

CREATE TABLE `shipments` (
  `shipment_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `date_shipped` varchar(100) NOT NULL,
  `date_received` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shipments`
--

INSERT INTO `shipments` (`shipment_id`, `store_id`, `carrier_id`, `quantity`, `date_shipped`, `date_received`) VALUES
(1, 1, 1, 101, '03/28/2021', ''),
(15, 9, 1, 9, '05/05/2021', ''),
(16, 10, 1, 11, '05/06/2021', ''),
(17, 10, 1, 4, '05/06/2021', ''),
(18, 11, 1, 19, '05/10/2021', ''),
(19, 7, 1, 3, '05/12/2021', ''),
(20, 1, 12, 0, '05/12/2021', ''),
(21, 6, 1, 0, '05/12/2021', ''),
(22, 12, 1, 10, '05/13/2021', ''),
(23, 5, 7, 0, '05/13/2021', ''),
(24, 14, 1, 4, '06/09/2021', ''),
(25, 15, 1, 4, '06/17/2021', '');

-- --------------------------------------------------------

--
-- Table structure for table `shipment_detail`
--

CREATE TABLE `shipment_detail` (
  `id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `sim_number` varchar(30) NOT NULL,
  `sold` tinyint(1) NOT NULL,
  `number_activated_on` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shipment_detail`
--

INSERT INTO `shipment_detail` (`id`, `shipment_id`, `sim_number`, `sold`, `number_activated_on`) VALUES
(1, 1, '89302720523840022135', 0, ''),
(2, 15, '89302720523840022838', 1, '3789922789'),
(3, 15, '8930272052384002284', 1, '56657787687878'),
(4, 15, '89302720523840022846', 1, '2134343902'),
(5, 15, '89302720523840022853', 1, '77892443242343241324'),
(6, 15, '89302720523840022879', 1, '4169325674'),
(7, 15, '89302720523840022788', 1, '8924320912'),
(8, 16, '89302720523840024024', 0, ''),
(9, 16, '89302720523840024032', 1, ''),
(10, 16, '89302720523840024040', 1, ''),
(11, 16, '89302720523840024057', 1, '9999403409'),
(12, 16, '89302720523840024065', 1, '9999403408'),
(13, 16, '89302720523840024073', 1, '7823203498'),
(14, 16, '89302720523840024081', 0, ''),
(15, 16, '89302720523840024099', 0, ''),
(16, 16, '89302720523840024107', 0, ''),
(17, 16, '89302720523840024115', 0, ''),
(18, 16, '89302720523840024123', 0, ''),
(19, 15, '89302720523840022267', 0, ''),
(20, 15, '89302720523840022275', 0, ''),
(21, 15, '89302720523840022291', 1, '6238579845'),
(22, 17, '89302720523840022291', 1, '6238579845'),
(23, 17, '89302720523840022366', 1, '7238579845'),
(24, 17, '89302720523840022424', 1, '4169229977'),
(25, 17, '89302720523840022168', 1, '4169229977'),
(26, 18, '89302720523840024131', 1, '6476789788'),
(27, 18, '89302720523840024149', 1, '2348090891'),
(28, 18, '89302720523840024156', 1, '2348090890'),
(29, 18, '89302720523840024164', 1, '2348090893'),
(30, 18, '89302720523840024172', 1, '7820182378'),
(31, 18, '89302720523840024180', 1, '4169325675'),
(32, 18, '89302720523840024198', 1, '121212'),
(33, 18, '89302720523840024206', 1, '5676897609'),
(34, 18, '89302720523840024214', 1, '2348090892'),
(35, 18, '89302720523840024222', 1, '6790232488'),
(36, 18, '89302720523840024230', 1, '6790232489'),
(37, 18, '89302720523840024248', 1, '6476789789'),
(38, 18, '89302720523840024255', 1, '7890248091'),
(39, 18, '89302720523840024263', 1, '7890248092'),
(40, 18, '89302720523840024271', 1, ''),
(41, 18, '89302720523840024289', 0, ''),
(42, 18, '89302720523840024297', 0, ''),
(43, 18, '89302720523840024305', 0, ''),
(44, 19, '89302720523840024222', 0, ''),
(48, 22, '89302720523840024321', 1, '5556760890'),
(49, 22, '89302720523840024412', 1, ''),
(50, 22, '89302720523840024339', 1, '4556760890'),
(51, 22, '89302720523840024347', 1, '2'),
(52, 22, '89302720523840024354', 1, ''),
(53, 22, '89302720523840024362', 1, ''),
(54, 22, '89302720523840024370', 0, ''),
(55, 22, '89302720523840024388', 0, ''),
(56, 22, '89302720523840024396', 0, ''),
(57, 22, '89302720523840024404', 0, ''),
(58, 24, '89302720523840022457', 0, ''),
(59, 24, '89302720523840022465', 0, ''),
(60, 24, '89302720523840022473', 1, '2689389021'),
(61, 24, '8930272052384002435', 0, '3457856780'),
(62, 25, '89302720523840028124', 0, ''),
(63, 25, '89302720523840028223', 0, ''),
(64, 25, '89302720523840028322', 1, ''),
(65, 25, '89302720523840028413', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`id`, `name`, `value`) VALUES
(1, 'shipping', '10'),
(2, 'sim', '10');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `shipment_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `carrier_id` int(11) NOT NULL,
  `quantity_purchased` bigint(20) NOT NULL,
  `quantity_sold` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `shipment_id`, `store_id`, `carrier_id`, `quantity_purchased`, `quantity_sold`) VALUES
(15, 15, 9, 1, 9, 7),
(16, 16, 10, 1, 11, 6),
(17, 17, 10, 1, 4, 4),
(18, 18, 11, 1, 19, 17),
(19, 19, 7, 1, 3, 0),
(20, 20, 1, 12, 0, 0),
(21, 21, 6, 1, 0, 0),
(22, 22, 12, 1, 10, 6),
(23, 23, 5, 7, 0, 0),
(24, 24, 14, 1, 4, 1),
(25, 25, 15, 1, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(50) NOT NULL,
  `postal_code` varchar(7) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(30) NOT NULL,
  `contact_number` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `address`, `city`, `province`, `postal_code`, `contact_name`, `email`, `contact_number`) VALUES
(1, 'Supertech Capetown', 'Sector 74', 'Noida', 'Uttar Pradesh', '201301', '', 'jslamba0811@gmail.com', '19999403408'),
(3, 'Online Store', '643 Chrislea rd, Unit 3', 'Woodbridge', 'Ontario', 'L4L8A3', '', 'shraddham@nanojot.com', '4169229977'),
(4, 'Welcome SIM Store', 'Sunrise Road ', 'Round Rock', 'Texas', '78665', '', 'welcome@store.com', '41692299784'),
(5, 'Online Store 1', 'Militärstrasse ', 'Zürich', 'Zürich', '8004', '', 'online@st.com', '7903382321'),
(6, 'New Online store', 'Noordersingel Borgerhout', 'Antwerpen', 'Vlaanderen', '2140', '', 'new@nanojot.com', '8902345678'),
(7, 'New SIM store', 'Test Street ', 'test', 'Otago', '9400', 'Shraddha', 'simstore@gmail.com', '3545567821'),
(8, 'SIM selling store', 'Kamla Market Ajmeri Gate', 'New Delhi', 'Delhi', '110002', '', 'selling@test.com', '7676346789'),
(9, '5th May New Store', 'South Avalon Boulevard South Los Angeles', 'Los Angeles', 'California', '90011', 'Shraddha', 'shraddham@nanojot.com', '4169229977'),
(10, '6th May new store', 'Lock Street University Heights', 'Newark', 'New Jersey', '07103', 'Shraddha', 'shraddham@nanojot.com', '5645567789'),
(11, '10th May 2021 New Store', 'Lake View Terrace ', 'Afton', 'Oklahoma', '74331', 'Shraddha', 'shraddham@nanojot.com', '90343488932'),
(12, '13th May Store', 'Ru Xue Lu Yangxin Xian', 'Huangshi Shi', 'Hubei Sheng', '267383', 'Shraddha', 'shraddham@nanojot.com', '28945435345'),
(13, '13th May A Store', 'Don Shula Dr ', 'Miami Gardens', 'Florida', '33056', 'shraddha', 'shraddham@nanojot.com', '78098097809'),
(14, '9th June 2021 Store', 'South Market Street Downtown', 'Boston', 'Massachusetts', '02109', 'Mike', 'sm@nanojot.ca', '4892892084'),
(15, '17th June Store', 'Shaganappi Trail Northwest Northwest Calgary', 'Calgary', 'Alberta', 'T3A 0E2', 'sh', 'sh@gmail.com', '5656890126');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `username`, `password`, `email`, `create_date`, `active`) VALUES
(1, '', '', 'james', '1d2bba5d0b80938327ac901264bcf7d4fe492fe9', 'james@bond.ca', '2021-03-02 14:21:59', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_article`
--
ALTER TABLE `blog_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carriers`
--
ALTER TABLE `carriers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `discount_code`
--
ALTER TABLE `discount_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_article`
--
ALTER TABLE `faq_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_category`
--
ALTER TABLE `faq_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_sub_category`
--
ALTER TABLE `faq_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_items`
--
ALTER TABLE `ordered_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`shipment_id`);

--
-- Indexes for table `shipment_detail`
--
ALTER TABLE `shipment_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `carriers`
--
ALTER TABLE `carriers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=475;

--
-- AUTO_INCREMENT for table `ordered_items`
--
ALTER TABLE `ordered_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=226;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipments`
--
ALTER TABLE `shipments`
  MODIFY `shipment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `shipment_detail`
--
ALTER TABLE `shipment_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
